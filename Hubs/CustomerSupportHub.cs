﻿using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.SignalR;
using Microsoft.EntityFrameworkCore;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using SignalRMasterServer.Data;
using SignalRMasterServer.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using Microsoft.AspNetCore.SignalR.Hubs;
using Microsoft.AspNetCore.Http.Features;
using SignalRMasterServer.Controllers;
using Mindscape.Raygun4Net;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Caching.Memory;
using SignalRMasterServer.Services;

namespace SignalRMasterServer.Hubs
{
    public class CustomerSupportHub : Hub
    {

        private IMemoryCache _memoryCache;
        private AppDbContext _context;
        //private AppDbContext _context1;
        //private AppDbContext _context2;
        //private AppDbContext _context3;
        private readonly SignInManager<ApplicationUser> _signInManager;
        // private IConfiguration _configuration;
        //private IHubIncomingInvokerContext _hubContext;
        private IHubIncomingInvokerContext i;
        private readonly UserManager<ApplicationUser> _userManager;
        //private ElasticClient _elasticClient;
        //private ElasticsearchContext _elasticContext;
        //private const string ConnectionString = "http://localhost:9200/";
        //private readonly IElasticsearchMappingResolver _elasticSearchMappingResolver = new ElasticsearchMappingResolver();
        public CustomerSupportHub(AppDbContext _context, IMemoryCache _memoryCache, SignInManager<ApplicationUser> _signInManager, UserManager<ApplicationUser> userManager)//, IConfiguration _configuration)//, IHubIncomingInvokerContext _hubContext)
        {

            //         var nodes = new Uri[]
            // {
            // new Uri("http://localhost:9200"
            // //new Uri("http://myserver2:9200"),
            // //new Uri("http://myserver3:9200")
            // )};

            //var pool = new StaticConnectionPool(nodes);
            //var settings = new ConnectionSettings(pool);


            //_elasticClient = new ElasticClient(settings);
            //_elasticContext=  new ElasticsearchContext(ConnectionString, _elasticSearchMappingResolver);

            _userManager = userManager;
            this._memoryCache = _memoryCache;

            //this._hubContext = _hubContext;
            this._signInManager = _signInManager;
            // this._configuration = _configuration;
            //this._context3 = _context3;
            this._context = _context;
            //this._context1 = _context1;
            //this._context2 = _context2;
        }
        public static List<CustomerSupportUser> loggedInUsers = new List<CustomerSupportUser>();
        static List<GroupEventMessage> GroupMessages = new List<GroupEventMessage>();
        static List<WebsitesSignalR> Website = new List<WebsitesSignalR>();
        static List<ChatAdminGroups> AdminGroup = new List<ChatAdminGroups>();
        // private void LogPageChangeMessage(CustomerSupportUser user,string PageTitle)
        // {
        //    if(user.CurrentPage != PageTitle)
        //    {
        //        Clients.Group(user.groupName, new string[] { user.connectionId }).onClientPageChange(user);
        //        user.CurrentPage = PageTitle;
        //        SitePage p = new SitePage
        //        {
        //            PageTitle = PageTitle,
        //            DateTime = DateTime.Now,
        //            Url = ""// SomeHow Get Url Here
        //        };                
        //    }
        // }
        private CoolParameter BreakDownCommand(string message)
        {
            CoolParameter c = new CoolParameter();
            if (message.StartsWith("[["))
            {
                if (message.EndsWith("]]"))
                {
                    c.Command = message.Remove(message.IndexOf("=")).Substring(2).ToLower();
                    c.Param = message.Substring(message.IndexOf("=") + 1).Substring(0, message.Substring(message.IndexOf("=") + 1).Count() - 2);
                }
            }
            return c;
        }
        public void DashBoardReport(int websiteId)
        {
            ReportViewModel report = ReportService.Report(websiteId);               //websiteId);
            var DbReport = new List<ReportDb>();                                  
            Clients.Caller.onDashBoardReport(report);
        }
        public void AnalyticsReport(int websiteId)
        {
            //string[] Ips = new[] {"223.255.230.11"};
            string[] Ips = loggedInUsers.Where(x => x.webSite == ElasticSearchGlobal.GetWebsiteNameById(websiteId)).Select(x => x.ip).ToArray();            
            var AnalyticsWidget = Services.AnalyticsReport.GetAnalyticsWidget(websiteId,Ips,loggedInUsers.Where(x=>x.webSite == ElasticSearchGlobal.GetWebsiteNameById(websiteId)).Count());

            Clients.Caller.onAnalyticsReport(AnalyticsWidget);
        }


        public void ServerMessage(ServerMessageClass serverMessageClass)
        {

        }
        public void GetClientInformation(string GroupName)
        {
            Clients.Group(GroupName).onGetClientInformation();
        }
        public void OfflineCustomerInfo(OfflineCustomerInfo offlineCustomerinfo)
        {
            if (CurrentUser() == null) OnConnected();
            if (offlineCustomerinfo.email == null) return;
            if (offlineCustomerinfo.fullName == null) return;
            if (offlineCustomerinfo.message == null) return;

            CustomerInfo c = new CustomerInfo();
            c.email = offlineCustomerinfo.email;
            c.fullName = offlineCustomerinfo.fullName;
            c.phoneNumber = offlineCustomerinfo.phoneNumber;
            c.groupName = CurrentUser().groupName;
            CustomerInfoChanged(c);
            Message(offlineCustomerinfo.message);
            Clients.Caller.onOfflineCustomerInfo(true);
        }
        public void Message(string Message, string groupName = null)
        {
            ChangingAdvert();
            //Console.WriteLine("Message ayo : " + Message + " " + "For Group " + groupName);
            if (string.IsNullOrEmpty(Message)) return;
            bool pushMarco = false;
            string ConnecitonId = Context.ConnectionId;
            //var messageToUser = ;
            var currentUser = loggedInUsers.SingleOrDefault(x => x.ConnectionId.Contains(ConnecitonId));
            if (currentUser == null)
            {
                OnConnected();
                currentUser = loggedInUsers.SingleOrDefault(x => x.ConnectionId.Contains(ConnecitonId));
            }

            if (currentUser.IsAdmin)
            {
                //                
                string userChattingfromWebsite = loggedInUsers.SingleOrDefault(y => y.groupName == groupName).webSite;
                var marco = ElasticSearchGlobal.GetChatMarcoByWebsiteTerm(userChattingfromWebsite, Message.Replace("\n", ""));// _context.ChatMarco.Where(x => x.Website.Name == loggedInUsers.SingleOrDefault(y => y.groupName == groupName).webSite).SingleOrDefault(x => x.Name == Message.Replace("\n", ""));
                if (marco != null)
                {
                    if (marco.MarcoType == MarcoType.Push)
                    {
                        //Todo send to only single client
                        pushMarco = true;
                        Clients.Group(groupName).onNewPushMarco(marco.TextorURL);
                        Message = "Redirected to : <a href=\"" + marco.TextorURL + "\">" + marco.TextorURL + "</a>";
                    }
                    else
                    {
                        Message = marco.TextorURL;

                    }
                }
                var m = BreakDownCommand(Message);
                if (m.Command != null)
                    switch (m.Command.ToString())
                    {
                        case "push":
                            Clients.Group(groupName).onNewPushMarco(m.Param);
                            Message = "Redirected to : <a href=\"" + m.Param + "\">" + m.Param + "</a>";
                            break;
                        default:
                            Console.WriteLine("Command not found : " + m.Command + "With Parameters : " + m.Param);
                            break;
                    }


            }
            if (groupName == null)
            {
                groupName = currentUser.groupName;
            }

            //Awaiting Support Logic
            //if (currentUser.AwaitingSupport == true) {
            if (loggedInUsers.Where(x => x.groupName == groupName).Where(x => x.IsAdmin == false).Where(x => x.AwaitingSupport == true).SingleOrDefault() != null)
            {
                if (!currentUser.IsAdmin)
                {
                    var adminUser = loggedInUsers.Where(x => x.Sites != null).Where(x => x.Sites.Contains(currentUser.webSite));
                    if (!adminUser.Any())
                    {
                        //var websiteAdminUser = _context.UserRoles.Where(x => x.Website.Name == currentUser.webSite);
                    }
                    else
                    {
                        Clients.Group(currentUser.webSite).onCallingForSupport(currentUser.groupName);
                    }
                }
                else
                {
                    //send to other admin that Admin initiated the chat with the client!
                    string userGroupName = groupName;//loggedInUsers.Where(x => x.groupName == groupName).SingleOrDefault().groupName;
                    Groups.Add(Context.ConnectionId, userGroupName);
                    SupportRequestAccepted(userGroupName, currentUser.fullName, loggedInUsers.Where(x => x.groupName == groupName).SingleOrDefault().webSite);
                    // currentUser.AwaitingSupport = false;
                }
                // 1st time the client is sending message 
                // now ask all admin who is going to accept the conneciton
                //if message is not sent by admin then proceed
            }
            currentUser.LastActive = DateTime.Now;
            //currentUser.LastMessage = Message;
            try
            {
                loggedInUsers.SingleOrDefault(x => x.groupName == groupName).LastMessage = Message;
            }
            catch { }
            MessageViewModel mvm = new MessageViewModel();
            mvm.Message = Message;


            MessageUserViewModelUser muvm = new MessageUserViewModelUser();
            if (currentUser.IsAdmin)
            {
                try
                {
                    muvm.avatar = currentUser.AdminWebsiteDescription.Where(x => x.websiteName == loggedInUsers.Where(y => y.groupName == groupName).SingleOrDefault().webSite).SingleOrDefault().avatar;
                }
                catch
                {
                    muvm.avatar = currentUser.avatar;
                }
                try
                {
                    muvm.fullName = currentUser.AdminWebsiteDescription.Where(x => x.websiteName == loggedInUsers.Where(y => y.groupName == groupName).SingleOrDefault().webSite).SingleOrDefault().fullName;
                }
                catch
                {
                    muvm.fullName = currentUser.fullName;
                }
                muvm.IsAdmin = true;
                //muvmemail = muvmavatar = currentUser.AdminWebsiteDescription.Where(x=>x.websiteName == loggedInUsers.Where(x=>x.groupName == groupName).SingleOrDefault().webSite).SingleOrDefault().
            }
            else
            {
                muvm.avatar = currentUser.avatar;
                muvm.fullName = currentUser.fullName;
                muvm.IsAdmin = false;
                //muvmemail = currentUser.email;
            }



            mvm.User = muvm;//currentUser;
            mvm.groupName = groupName;
            mvm.DateTime = DateTime.Now;
            Clients.Group(groupName).onNewMessage(mvm);
            //Clients.All.onNewMessage(mvm);
            EventMessage e = new EventMessage();
            e.DateTime = DateTime.Now;
            e.Message = Message;
            e.User = new CustomerSupportUserViewModel(currentUser);
            if (pushMarco)
            {
                e.Type = enumTicketMesssages.PushMarco;
            }
            e.Type = enumTicketMesssages.TextMessage;

            GroupMessages.SingleOrDefault(x => x.groupName == groupName).EventMessage.Add(e);
            ElasticChatEventMessage cem = new ElasticChatEventMessage();
            cem.Message = Message;
            cem.DateTime = DateTime.Now;
            //cem.WebsiteId = GroupMessages.SingleOrDefault(x => x.groupName == groupName).WebsiteId;
            if (GroupMessages.SingleOrDefault(x => x.groupName == groupName).DataBaseId == null)
            {
                //Create a new field to initiate the conversation
                ChatMessage cm = new ChatMessage
                {
                    GroupId = groupName,
                    //FullName = currentUser.fullName, // Need to fix this later                    
                    WebsiteId = GroupMessages.SingleOrDefault(x => x.groupName == groupName).WebsiteId
                };
                ElasticChatMessage cm1 = new ElasticChatMessage
                {
                    GroupId = groupName,
                    //FullName = currentUser.fullName, // Need to fix this later                    
                    WebsiteId = GroupMessages.SingleOrDefault(x => x.groupName == groupName).WebsiteId
                };
                // if (_context.ChatMessage.SingleOrDefault(x => x.GroupId == groupName) == null)
                if (ElasticSearchGlobal.GetChatMessage(groupName) == null)
                {
                    cm.Id = ElasticSearchGlobal.AddChatMessage(cm1);
                    cem.ChatMessageId = cm.Id;
                    // Add User Ip Too !!
                    // var IpInfoDb = loggedInUsers.Where(x => x.groupName == groupName).SingleOrDefault().IpInfoDb; // Acuta;                    
                    // Models.IpInfoDb ipinfoDb = new Models.IpInfoDb(); // to be saved in db;
                    // ipinfoDb.Cityname = IpInfoDb.cityname;
                    // ipinfoDb.Countrycode = IpInfoDb.countrycode;
                    // ipinfoDb.Countryname = IpInfoDb.countryname;
                    // ipinfoDb.Ipaddress = IpInfoDb.ipaddress;
                    // ipinfoDb.Latitude = IpInfoDb.latitude;
                    // ipinfoDb.Longitude = IpInfoDb.longitude;
                    // ipinfoDb.Regionname = IpInfoDb.regionname;
                    // ipinfoDb.Timezone = IpInfoDb.timezone;
                    // ipinfoDb.Zipcode = IpInfoDb.zipcode;
                    // ipinfoDb.ChatMessage = cm;
                    // try
                    // {
                    //     // _context.IpInfoDb.Add(ipinfoDb);
                    //     // _context.SaveChanges();
                    // }
                    // catch (Exception ex)
                    // {
                    //     Global.Live_Crash_Report(ex);
                    // }
                    // cem.ChatMessageId = cm.Id;

                }
                else
                {
                    cem.ChatMessageId = ElasticSearchGlobal.GetChatMessage(groupName).Id;//_context.ChatMessage.SingleOrDefault(x => x.GroupId == groupName).Id;                    
                }
                GroupMessages.SingleOrDefault(x => x.groupName == groupName).DataBaseId = cm.Id;
            }
            else
            {
                cem.ChatMessageId = GroupMessages.SingleOrDefault(x => x.groupName == groupName).DataBaseId;
            }
            if (ElasticSearchGlobal.GetSignalRUser(cem.ChatMessageId) == null)
            {
                ElasticSignalRUser u = new ElasticSignalRUser();
                u.ChatMessageId = cem.ChatMessageId;
                u.Email = CurrentUser().avatar; //loggedInUsers.Where(x => x.groupName == groupName).SingleOrDefault().email;
                u.FullName = CurrentUser().avatar; //loggedInUsers.Where(x => x.groupName == groupName).SingleOrDefault().fullName;
                u.Avatar = CurrentUser().avatar; //loggedInUsers.Where(x => x.groupName == groupName).SingleOrDefault().avatar;
                u.IpAddress = CurrentUser().ip;
                //u.SitePages.Add(new SitePage
                //{
                //    DateTime = 
                //}
                //);
                u.GroupId = groupName;
                u.WebsiteGuid = ElasticSearchGlobal.GetWebsitebyName(loggedInUsers.Where(x => x.groupName == groupName).SingleOrDefault().webSite).Guid.ToString();
                try
                {
                    // _context3.SignalRUser.Add(u);
                    // _context3.SaveChanges();
                    string SignalrUserId = ElasticSearchGlobal.AddSignalRUser(u);
                }
                catch (Exception ex)
                {
                    Global.Live_Crash_Report(ex);
                }
            }
            if (currentUser.IsAdmin)
            {
                cem.AdminId = currentUser.DbAdminId;
            }
            else
            {
            }
            try
            {
                //Immediately Needa workaround here cem is transfered to elastic chat
                ElasticSearchGlobal.AddChatEventMessage(cem);
                // _context.ChatEventMessage.Add(cem);
                // _context.SaveChanges();
            }
            catch (Exception ex)
            {
                Global.Live_Crash_Report(ex);
                Console.WriteLine("Error Occured");
            }
        }

        // public class ChatEventMessageElasticSearch{
        //     public Guid 
        // }


        public void SupportRequestAccepted(string groupName, string adminName, string websiteName) // When a Admin Accepts support request from a ConnectionId
        {
            //var report = ElasticSearchGlobal.GetDashBoardReportByWebsiteForDate(websiteName,DateTime.Now);
            var userToSupport = loggedInUsers.SingleOrDefault(x => x.groupName == groupName);
            var report = ElasticSearchGlobal.GetDashBoardReportByWebsiteForDate(ElasticSearchGlobal.GetWebsitebyName(userToSupport.webSite).Id, DateTime.Now);
            if (report != null)
            {
                report.Answered_Call_Count += 1;
                ElasticSearchGlobal.SaveReport(report);
            }
            var adminUser = loggedInUsers.Where(x => x.ConnectionId.Contains(Context.ConnectionId)).Select(x => new { x.fullName, x.avatar, x.description }).SingleOrDefault();
            var adminUsertoSend = loggedInUsers.Where(x => x.ConnectionId.Contains(Context.ConnectionId)).SingleOrDefault().AdminWebsiteDescription.Where(x => x.websiteName == websiteName).Select(x => new { x.fullName, x.avatar, x.description }).SingleOrDefault();
            if (userToSupport != null)
            {
                userToSupport.AwaitingSupport = false;
                // userToSupport.AwaitingSupport = false;
                var adminUser1 = loggedInUsers.Where(x => x.ConnectionId.Contains(Context.ConnectionId)).SingleOrDefault();

                adminUser1.AcceptedSupportRequest.Add(groupName);
                var supportUser = loggedInUsers.SingleOrDefault(x => x.ConnectionId.Contains(Context.ConnectionId));
                //send to the customer someone joined the support request
                Clients.Clients(userToSupport.ConnectionId).onSupportRequestAccepted((adminUsertoSend != null) ? adminUsertoSend : adminUser);
                // Send message to other Admin that the SupportRequest Is Accepted
                //Clients.OthersInGroup
                Clients.OthersInGroup(userToSupport.webSite).onSupportRequestAccepted(new SupportRequestAcceptedViewModel
                {
                    adminName = adminName,
                    groupName = groupName
                });
                if (GroupMessages.SingleOrDefault(x => x.groupName == userToSupport.groupName) != null)
                {
                    var GroupMessage = GroupMessages.SingleOrDefault(x => x.groupName == userToSupport.groupName).EventMessage.ToList();
                    if (GroupMessage.Any())
                        Clients.Caller.PreviousMessages(GroupMessage);
                }
            }
        }

        public void UserTyping(string GroupName = null)
        {
            string ConnecitonId = Context.ConnectionId;
            var currentUser = loggedInUsers.SingleOrDefault(x => x.ConnectionId.Contains(ConnecitonId));
            if (currentUser == null)
            {
                OnConnected();
                return;
                // Feels no need to go below line. TO DO : check from every aspects
                // System.Threading.Thread.Sleep(2000);
                // currentUser = loggedInUsers.SingleOrDefault(x => x.ConnectionId.Contains(ConnecitonId));
            }
            UserTypingViewModel u = new UserTypingViewModel
            {
                f = currentUser.fullName,
                g = (GroupName != null) ? GroupName : currentUser.groupName,
                a = currentUser.avatar
            };
            Clients.OthersInGroup((GroupName != null) ? GroupName : currentUser.groupName).onUserTyping(u);
        }
        public void MessageSeen(string GroupName)
        {
            string ConnecitonId = Context.ConnectionId;
            var currentUser = loggedInUsers.SingleOrDefault(x => x.ConnectionId.Contains(ConnecitonId));
            //Clients.Clients(currentUser.ConnectionId.ToList().Remove(currentUser).onMessageSeen(GroupName);
        }

        public void UserStoppedTyping(string GroupName = null)
        {
            string ConnecitonId = Context.ConnectionId;
            var currentUser = loggedInUsers.SingleOrDefault(x => x.ConnectionId.Contains(ConnecitonId));
            if (currentUser == null) return;
            UserTypingViewModel u = new UserTypingViewModel
            {
                f = currentUser.fullName,
                g = (GroupName != null) ? GroupName : currentUser.groupName
            };
            Clients.OthersInGroup((GroupName != null) ? GroupName : currentUser.groupName).onUserStoppedTyping(u);
        }
        public void JoinedSupportChat(string GroupName) // Admin Joined Some Support Chat Instance !
        {
            string ConnecitonId = Context.ConnectionId;
            var currentUser = loggedInUsers.SingleOrDefault(x => x.ConnectionId.Contains(ConnecitonId));
            if (currentUser.IsAdmin)
                Clients.Group(GroupName).onJoinedSupportChat(currentUser);
        }

        public async void ShowToastMessage(string Message)
        {
            //Need to show toast message
            //onShowToastMessage()
        }
        public void LoadMoreClients(int page_number=0)
        {
            int count = 10;
            List<SignalRUser> clients = new List<SignalRUser>();
            List<LoadMoreClientViewModel> l = new List<LoadMoreClientViewModel>();
            if (CurrentUser() == null)
            {
                System.Threading.Thread.Sleep(1000);
                LoadMoreClients(0);
            }
            foreach (var items in CurrentUser().Sites)
            {
                try
                {
                    clients = ElasticSearchGlobal.GetSignalRUserByWebsite(items,10);//_context.ChatMessage.Where(x => x.Website.Name == items).Skip(page_number * count).Take(count).ToList();
                    foreach (var client in clients.DistinctBy(x => x.GroupId).ToList())
                    {
                        if (loggedInUsers.Where(x=>x.clientScript == true).Where(x => x.groupName.Contains(client.GroupId)).SingleOrDefault() == null)
                        {
                            LoadMoreClientViewModel group = new LoadMoreClientViewModel
                            {
                                fullName = client.FullName,//ElasticSearchGlobal.GetSignalRUser(client.Id).FullName,
                                groupName = client.GroupId,
                                avatar = client.Avatar,//(null,client.grou).Avatar // Need ot workaround this line                    
                                webSite = ElasticSearchGlobal.GetWebsitebyGuid(Guid.Parse(client.WebsiteGuid)).Name
                            };
                            l.Add(group);
                        }
                    }
                }
                catch (Exception ex)
                {
                    Global.Live_Crash_Report(ex);
                    Console.WriteLine("LoadMoreClients");
                }
            }
            Clients.Caller.onLoadMoreClients(l);
        }
        public async void CustomerInfoChanged(CustomerInfo customerInfo)
        {
            // Received customer Info now  tell to every admin the client changed the info
            //Validate from all angles!
            //Save if already there
            if (string.IsNullOrEmpty(customerInfo.fullName) || string.IsNullOrEmpty(customerInfo.email))
            {
                return;
            }
            SignalRUser s = new SignalRUser
            {
                Email = customerInfo.email,
                FullName = customerInfo.fullName,
                PhoneNumber = customerInfo.phoneNumber,
                InfoProvided = true
            };
            var meUser = CurrentUser();//loggedInUsers.Where(x => x.clientScript == true).Where(x => x.groupName == CurrentUser().groupName).SingleOrDefault();

            if (meUser == null)
            {
                System.Threading.Thread.Sleep(500);
                await OnConnected();
                meUser = CurrentUser();
            }
            meUser.fullName = customerInfo.fullName;
            meUser.email = customerInfo.email;
            meUser.phoneNumber = customerInfo.phoneNumber;
            //meUser.InfoProvided = true;
            
            if (ElasticSearchGlobal.GetChatMessage(meUser.groupName) == null)// _context.ChatMessage.SingleOrDefault(x => x.GroupId == meUser.groupName) == null)
            {
                //No User IN Db
                ChatMessage c = new ChatMessage();
                c.GroupId = meUser.groupName;
                try
                {
                    c.WebsiteId = GetWebsite(Context.Request.Host.Value).Id;
                    //c. = CurrentUser().ip;
                    //_context.Website.SingleOrDefault(x => x.Name == Context.Request.Host.Value).Id;
                    // _context.ChatMessage.Add(c);
                    // _context.SaveChanges();
                    string id = ElasticSearchGlobal.UpdateChatMessage(c);
                    s.ChatMessageId = id;
                }
                catch (Exception ex)
                {
                    Global.Live_Crash_Report(ex);
                }
            }
            else
            {
                //s.ChatMessageId = _context.ChatMessage.SingleOrDefault(x => x.GroupId == meUser.groupName).Id;
                s.ChatMessageId = ElasticSearchGlobal.GetChatMessage(meUser.groupName).Id;
            }
            try
            {
                // Check if there is alread an signalr user in db;
                SignalRUser u = new SignalRUser();
                u = ElasticSearchGlobal.GetSignalRUser(null, CurrentUser().groupName,ElasticSearchGlobal.GetWebsitebyName(CurrentUser().webSite).Guid.ToString());
                if (u != null)
                {
                    u.Avatar = meUser.avatar;
                    u.FullName = meUser.fullName;
                    u.InfoProvided = true;
                    u.GroupId = meUser.groupName;
                    u.WebsiteGuid = ElasticSearchGlobal.GetWebsitebyName(meUser.webSite).Guid.ToString();
                    //_context.SignalRUser.Add(u);// _context.SignalRUser.Update(u);
                }
                else
                {
                    s.GroupId = meUser.groupName;
                    s.IpAddress = meUser.ip;
                    ElasticSearchGlobal.AddSignalRUser(s);
                    //_context.SignalRUser.Add(s);
                }
                try
                {
                    //_context.SaveChanges();
                }
                catch (Exception ex)
                {
                    Global.Live_Crash_Report(ex);
                }
                try
                {
                    Clients.Caller.onCustomerInfoChanged(u.Id);
                }
                catch
                {
                    Clients.Caller.onCustomerInfoChanged();
                }
            }
            catch (Exception ex)
            {
                Global.Live_Crash_Report(ex);
            }
            //Context.Request.Host.Host;
            string ConnecitonId = Context.ConnectionId;
            var currentCustomerGroup = meUser.webSite;
            Clients.Group(currentCustomerGroup, CurrentUser().ConnectionId.ToArray()).onCustomerInfoChanged(customerInfo);
            Clients.Clients(CurrentUser().ConnectionId.ToArray()).onCustomerInfoChanged("true");
            //Save to database ##ToDo
        }

        public void GeoInformation(string GroupName)
        {
            var userInfo = loggedInUsers.SingleOrDefault(x => x.groupName == GroupName);
            GeoinfoAndPages g = new GeoinfoAndPages();
            g.GroupName = GroupName;
            g.SitePage = new List<SitePage>();
            g.CurrentPage = userInfo.CurrentPage;
            g.CurrentUrl= userInfo.CurrentUrl;
            if (userInfo != null)
            {
                if (userInfo.IpInfoDb == null)
                {
                     g.IpInfoDb =ElasticSearchGlobal.GetIpInfoDb(userInfo.ip);                    
                }
                else
                {
                    g.IpInfoDb = userInfo.IpInfoDb;
                }
                g.SitePage = userInfo.SitePages;
            }
            else
            {

            }
            Clients.Caller.onGeoInformation(g);
        }
        public void Authorization(string siteName, string ApiKey, string FullName, string Email, string PhoneNumber) // Done By Clients only !
        {
            siteName = Context.Request.Host.Host;
            var connectionId = Context.ConnectionId;
            CustomerSupportUser u = loggedInUsers.SingleOrDefault(x => x.ConnectionId.Contains(connectionId));
            u.fullName = FullName;
            u.phoneNumber = PhoneNumber;
            u.email = Email;
        }
        public async void Block(string groupName)
        {
            //Todo Blocking User Logic
            var userToBlock = loggedInUsers.Where(x => x.groupName == groupName).SingleOrDefault();
            if (userToBlock != null)
            {
                var usertoBlock = ElasticSearchGlobal.GetSignalRUser(null,groupName, ElasticSearchGlobal.GetWebsitebyName(userToBlock.webSite).Guid.ToString());//ElasticSearchGlobal.GetChatMessage(groupName).Id, null);//_context.SignalRUser.Where(x => x.ChatMessage.GroupId == groupName).FirstOrDefault();
                if (usertoBlock != null)
                {
                    usertoBlock.Blocked = true;
                    //_context.SignalRUser.Update(usertoBlock);
                    //_context.SaveChanges();
                    ElasticSearchGlobal.UpdateSignalRUser(usertoBlock);
                }
                userToBlock.blocked = true;
                Clients.Clients(userToBlock.ConnectionId.ToList()).onBlocked();
                Clients.Group(userToBlock.groupName).onBlocked(userToBlock.groupName);
            }
        }
        public async void UnBlock(string groupName)
        {
            //Todo UnBlocking Logic
            var userToBlock = loggedInUsers.Where(x => x.groupName == groupName).SingleOrDefault();
            if (userToBlock != null)
            {
                var usertoUnBlock = ElasticSearchGlobal.GetSignalRUser(null, groupName, ElasticSearchGlobal.GetWebsitebyName(userToBlock.webSite).Guid.ToString());//ElasticSearchGlobal.GetChatMessage(groupName).Id, null);//_context.SignalRUser.Where(x => x.ChatMessage.GroupId == groupName).FirstOrDefault();
                if (usertoUnBlock != null)
                {
                    usertoUnBlock.Blocked = false;
                    ElasticSearchGlobal.UpdateSignalRUser(usertoUnBlock);

                    //_context.SignalRUser.Update(usertoBlock);

                    //_context.SaveChanges();
                }
                userToBlock.blocked = false;
                Clients.Clients(userToBlock.ConnectionId.ToList()).onUnBlocked();
                Clients.Group(userToBlock.groupName).onUnBlocked(userToBlock.groupName);
            }
        }
        public void SendPreviousMessage(string GroupName, int page = 0)
        {
            int count = 10;
            //List<EventMessage> eventMessage = new List<EventMessage>();
            //try
            //{
            //    eventMessage =GroupMessages.SingleOrDefault(x => x.groupName == GroupName).EventMessage;
            //}
            //catch
            //{
            //    //return;
            //}
            //if (eventMessage != null && eventMessage.Count >0)
            //{
            //    var GroupMessage = GroupMessages.SingleOrDefault(x => x.groupName == GroupName).EventMessage.ToList();
            //    if (GroupMessage.Count > 0)
            //    {
            //        if (page == 0)
            //        {
            //            //var message = GroupMessage.OrderByDescending(x => x.DateTime).Skip(page * count).Take(count).OrderBy(x => x.DateTime).ToList();
            //            var message = GroupMessage.OrderByDescending(x => x.DateTime).Skip(page * count).Take(count).ToList();
            //            Clients.Caller.onAuthorization(message);
            //            //send to admin
            //            System.Threading.Thread.Sleep(2000);
            //            var messagetoSend = GroupMessages.Select(x => new { x.groupName, x.EventMessage }).SingleOrDefault(x => x.groupName == GroupName);
            //            // foreach(var items in messagetoSend.EventMessage){
            //            //     items.User.
            //            // }
            //            Clients.Caller.onSendPreviousMessage(messagetoSend);
            //            //SendLogToSuperUser(JsonConvert.SerializeObject(messagetoSend));
            //        }
            //        else
            //        {
            //            var messageToSend = GroupMessage.OrderByDescending(x => x.DateTime).Skip(page * count).Take(count);
            //            if (messageToSend.Any())
            //            {
            //                Clients.Caller.onAuthorization(messageToSend);
            //                Clients.Caller.onSendPreviousMessage(messageToSend);
            //                //SendLogToSuperUser(JsonConvert.SerializeObject(messageToSend));
            //            }
            //            else
            //            {
            //                Clients.Caller.onSendPreviousMessage(null);
            //                Clients.Caller.onAuthorization(null);
            //            }
            //        }
            //    }
            //}
            //else
            //{
                List<EventMessage> dbEventMessage = new List<EventMessage>();
                GroupEventMessage innerGroupMessages = new GroupEventMessage();
                innerGroupMessages.EventMessage = new List<EventMessage>();
                innerGroupMessages.groupName = GroupName;

            if(CurrentUser()!= null)
            if (string.IsNullOrEmpty(GroupName))GroupName = CurrentUser().groupName;
                var dbMessages = ElasticSearchGlobal.GetChatEventMessage(GroupName,page);//_context.ChatEventMessage.Where(x => x.ChatMessage.GroupId == GroupName).Include(x => x.ChatMessage).Include(x => x.Admin).ToList();
                if (dbMessages != null)
                    foreach (var items in dbMessages)
                    {
                        EventMessage dbe = new EventMessage();
                        dbe.DateTime = items.DateTime;
                        dbe.Message = items.Message;
                        dbe.Type = enumTicketMesssages.TextMessage;                        
                        if (items.AdminId != null)
                        {
                            
                            var userwebsitedescription = ElasticSearchGlobal.GetUserWebsiteDescriptionbyUserIdWebsiteId(ElasticSearchGlobal.GetChatMessage(GroupName).WebsiteId, ElasticSearchGlobal.GetApplicationUserbyId(items.AdminId.Value).Id);
                            CustomerSupportUser c = new CustomerSupportUser();
                            var adminuser = ElasticSearchGlobal.GetApplicationUserbyId(items.AdminId.Value);//GetUser(items.Admin.Email);
                            c.fullName = (userwebsitedescription ==null)?adminuser.FullName : userwebsitedescription.FullName;
                            c.avatar = (userwebsitedescription == null) ? adminuser.Image : userwebsitedescription.Avatar;  //;
                            c.IsAdmin = true;
                            dbe.User = new CustomerSupportUserViewModel(c);
                            innerGroupMessages.EventMessage.Add(dbe);
                        }
                        else
                        {                            
                            var customerUser = new CustomerSupportUser();                            
                            var dbSignalRUser = ElasticSearchGlobal.GetSignalRUser(ElasticSearchGlobal.GetChatMessage(GroupName).Id); //_context.SignalRUser.Where(x => x.ChatMessage.GroupId == GroupName).FirstOrDefault();
                            if (dbSignalRUser != null)
                            {
                                customerUser.avatar = (dbSignalRUser.Avatar == null) ? Global.NoImage() : dbSignalRUser.Avatar;
                                customerUser.fullName = dbSignalRUser.FullName;
                            }
                            else
                            {
                                customerUser.avatar = Global.NoImage();
                            }
                            dbe.User = new CustomerSupportUserViewModel(customerUser);
                            innerGroupMessages.EventMessage.Add(dbe);                            
                        }
                    }
                        if (CurrentUser().clientScript)
                            Clients.Caller.onAuthorization(innerGroupMessages.EventMessage);
                        if (!CurrentUser().clientScript)
                            Clients.Caller.onSendPreviousMessage(innerGroupMessages);                
        }        
        public async void ChangingAdvert()
        {
            try
            {
                var advertScript = ElasticSearchGlobal.GetAdverts().OrderBy(r => Guid.NewGuid()).FirstOrDefault();
                if (advertScript != null) ChangeAdvert(advertScript.Script);
            }
            catch (Exception ex)
            {
                Global.Live_Crash_Report(ex);
                Console.WriteLine("Advert Change Error");
            }
        }
        private int GetUserId()
        {
            string userName = loggedInUsers.Where(x => x.IsAdmin == true).Where(x => x.ConnectionId.Contains(Context.ConnectionId)).SingleOrDefault().userName;
            return GetUser(userName).Id;
            //return _context.Users.Where(x => x.UserName == userName).SingleOrDefault().Id;
        }
        private ApplicationUser GetUser(string Email = null)
        {
            if (Email == null) Email = Context.User.Identity.Name;
            ApplicationUser _user1 = ElasticSearchGlobal.GetApplicationUserbyEmail(Email); //new ApplicationUser();            
            return _user1;
        }
        private Website GetWebsite(string Name = null)
        {
            return ElasticSearchGlobal.GetWebsitebyName(Name);
        }
        public async void UpdateMe(string whatToUpdate)
        {
            var connectionId = Context.ConnectionId;
            Enum.TryParse(whatToUpdate, out SignalrUpdate i);
            // System.Threading.Thread.Sleep(10000);
            switch (i)
            {
                case SignalrUpdate.MeControllerEditUser:
                    try
                    {
                        var userFromDb = GetUser();
                        //var kk = userFromDb.;
                        loggedInUsers.Where(x => x.ConnectionId.Contains(connectionId)).SingleOrDefault().avatar = userFromDb.Avatar;
                        loggedInUsers.Where(x => x.ConnectionId.Contains(connectionId)).SingleOrDefault().fullName = userFromDb.FullName;
                        loggedInUsers.Where(x => x.ConnectionId.Contains(connectionId)).SingleOrDefault().description = userFromDb.Description;
                        var usertoModify = loggedInUsers.Where(x => x.ConnectionId.Contains(connectionId)).SingleOrDefault();
                        //usertoModify.avatar = userFromDb.Avatar;
                        //usertoModify.fullName = userFromDb.FullName;
                        //usertoModify.description = userFromDb.Description;
                        //belowLine is for special admins ( those who are admin but runs as Client )
                        try
                        {
                            loggedInUsers.Where(x => x.clientScript == true)
                                .Where(x => x.authenticate == true)
                                .Where(x => x.email == userFromDb.Email)
                            .SingleOrDefault().fullName = userFromDb.FullName;
                            loggedInUsers.Where(x => x.clientScript == true)
                                .Where(x => x.authenticate == true)
                                .Where(x => x.email == userFromDb.Email)
                            .SingleOrDefault().avatar = userFromDb.Avatar;
                            // var siguserToUpdate = //_context.SignalRUser.Where(x => x.ChatMessage.GroupId == loggedInUsers.Where(y => y.authenticate == true).Where(y => y.email ==
                            //     loggedInUsers.Where(z => z.ConnectionId.Contains(connectionId)).SingleOrDefault().email
                            // ).SingleOrDefault().groupName).SingleOrDefault();
                            // var siguserToUpdate = ElasticSearchGlobal.GetSignalRUser()
                            // siguserToUpdate.Avatar = userFromDb.Avatar;
                            // siguserToUpdate.FullName = userFromDb.FullName;
                            CustomerInfo c = new CustomerInfo();
                            c.email = userFromDb.Email;
                            c.avatar = userFromDb.Avatar;
                            c.fullName = userFromDb.FullName;
                            c.GroupName = loggedInUsers.Where(x => x.clientScript == true).Where(x => x.email ==
                            loggedInUsers.Where(y => y.ConnectionId.Contains(connectionId)).SingleOrDefault().email).SingleOrDefault().groupName;
                            c.groupName = c.GroupName;
                            CustomerInfoChanged(c);
                        }
                        catch (Exception ex)
                        { // The user is admin but rununing cientt so no worries !}
                        }


                        var groupsTosend = usertoModify.Groups.Where(x => x.Contains("-Admin")).ToList();
                        //groupsTosend.Add("admin.livecustomer.chat-Admin");
                        Clients.Groups(groupsTosend, usertoModify.ConnectionId.ToArray()).onNewUserAuthorization(usertoModify);
                    }
                    catch (Exception ex)
                    {
                        Global.Live_Crash_Report(ex);
                    }
                    break;
                case SignalrUpdate.WebsiteControllerPostWebsitesite:
                case SignalrUpdate.RolesControllersetUserToRole:
                    var AdminWebsites = UserWebsites(GetUserId());
                    List<string> AdminMemorySites = CurrentUser().Sites.ToList();
                    List<string> AdminDbSites = new List<string>();
                    try
                    {
                        foreach (var websites in AdminWebsites)
                        {
                            AdminDbSites.Add(websites.Name);
                            try
                            {
                                await Groups.Add(Context.ConnectionId, websites.Name);
                                CurrentUser().Groups.Add(websites.Name);
                            }
                            catch
                            {

                            }
                            //Add connection if there is any loggedin users
                            try
                            {
                                foreach (var clientGroups in loggedInUsers.Where(x => x.webSite == websites.Name))
                                {
                                    await Groups.Add(Context.ConnectionId, clientGroups.groupName);
                                    CurrentUser().Groups.Add(clientGroups.groupName);
                                }
                            }
                            catch
                            {

                            }
                        }

                        //# todo Remove all signalr clients that were in admin list n more

                        // Assign to website in signalr
                    }
                    catch (Exception ex)
                    {
                        Global.Live_Crash_Report(ex);
                    }
                    break;
                default:
                    break;
            }
        }
        private CustomerSupportUser CurrentUser()
        {
            var currentUserToSend = loggedInUsers.Where(x => x.ConnectionId.Contains(Context.ConnectionId)).SingleOrDefault();
            Console.WriteLine(currentUserToSend + " Admin.livecustomer.chat CurrentUser called");
            return currentUserToSend;
        }

        public async void SendMessageToAllClients(string message, string website)
        {
            //$$To do check if admin
            var websiteName = ElasticSearchGlobal.GetWebsitebyName(website).Name;// ()_context.Website.Where(x => x.Id == Int32.Parse(website)).SingleOrDefault().Name;
            var messageToUser = loggedInUsers.Where(x => x.webSite != null).Where(x => x.webSite.ToLower() == websiteName.ToLower());
            foreach (var items in messageToUser)
            {
                Clients.Clients(items.ConnectionId).onAdminSpecialMessage(message);
            }
            //##todo Mass Message Part
        }
        private async void ChangeAdvert(string AdvertText)
        {
            try
            {
                var user = loggedInUsers.Where(x => x.contextName.Contains(Context.ConnectionId)).SingleOrDefault();
                if (user.IsAdmin && !user.AnyPaidSite)
                    Clients.Caller.onAdvertChanged(AdvertText);
            }
            catch (Exception ex)
            { Global.Live_Crash_Report(ex); }
        }
        public async void SendCoreReport(string whattoSend)
        {
            // Clients.Caller.onSendCoreReport(loggedInUsers.ToList());            
            switch (whattoSend.ToLower())
            {
                case "users":
                    Clients.Caller.onSendCoreReport(loggedInUsers.ToList());
                    break;
                case "report":
                    DashBoardReport(Int32.Parse((whattoSend.ToLower().Replace("report",""))));
                    break;
                default:
                    break;
            }
        }
        private async void SendLogToSuperUser(object k)
        {
            var superUser = loggedInUsers.Where(x => x.email == Global.SuperUser()).Where(y => y.IsAdmin == true).FirstOrDefault();
            if (superUser != null)
                await Clients.Clients(superUser.ConnectionId.ToList()).onSendCoreReport(JsonConvert.DeserializeObject(k.ToString()));
        }
        private async void SendClientListToAdmin(List<string> adminSites)
        {
            //System.Threading.Thread.Sleep(1500);
            var loggedinUserToSendToAdmin = loggedInUsers.
                        OrderByDescending(x => x.online).OrderByDescending(x => x.webSite).ToList();
            List<CustomerSupportUserViewModel> cvm = new List<CustomerSupportUserViewModel>();
            // List<string> adminSites = new List<string>();
            // try{
            //     adminSites = loggedInUsers.Where(x => x.ConnectionId.Contains(Context.ConnectionId)).SingleOrDefault().Sites;
            // }
            // catch
            // {
            //     return;
            // }
            foreach (var items in adminSites.Distinct())
            {
                var usersToSend = loggedinUserToSendToAdmin.Where(x => x.webSite == items).ToList();
                foreach (var users in usersToSend)
                {
                    cvm.Add(new CustomerSupportUserViewModel(users));
                }
            }
            System.Threading.Thread.Sleep(500);
            await Clients.Caller.onJoiningAllClients(cvm.Distinct());
            //SendLogToSuperUser(JsonConvert.SerializeObject(cvm));
        }
        private async void SendAdminListToAdmin(List<string> sites, string excludeUserName)
        {
            List<CustomerSupportUserViewModel> cvm = new List<CustomerSupportUserViewModel>();
            List<CustomerSupportUserViewModel> cvmforCaller = new List<CustomerSupportUserViewModel>();
            foreach (var items in sites)
            {
                var adminToSend = loggedInUsers.Where(x => x.IsAdmin == true).Where(x => x.clientScript == false).Where(x => x.Sites.Contains(items)).Where(x => x.userName != excludeUserName);
                foreach (var users in adminToSend.Distinct())
                {
                    Clients.Group(users.id.ToString()).onNewUserAuthorization(new CustomerSupportUserViewModel(CurrentUser()));
                    //Send New Loggedin Admininfo to old users;
                    cvm.Add(new CustomerSupportUserViewModel(users));
                }
                //SendNewAdminToPreviousAdmin(items);
            }
            System.Threading.Thread.Sleep(500);
            await Clients.Caller.onJoiningAllClients(cvm.Distinct());
            SendLogToSuperUser(JsonConvert.SerializeObject(cvm.Distinct()));
        }
        //private async void SendNewAdminToPreviousAdmin(string websiteName)
        //{
        //    List<string> connectionToExclude = new List<string>();
        //    var otherAdminUsers = loggedInUsers.Where(x => x.IsAdmin).Where(x => x.Sites.Contains(websiteName)).Where(x => x.userName != CurrentUser().userName).Where(x => x.online == true);
        //    foreach (var admin in otherAdminUsers)
        //    {
        //        if (!connectionToExclude.Contains(admin.ConnectionId.FirstOrDefault()))
        //        {
        //            //GroupEventMessage g = new GroupEventMessage();
        //            //u.groupName = "Admin" + Guid.NewGuid().ToString();
        //            //g.groupName = u.groupName;
        //            //g.EventMessage = new List<EventMessage>();
        //            //g.WebsiteId = _context.Website.SingleOrDefault(x => x.Name == u.webSite).Id;
        //            //Need to workaround this
        //            //GroupMessages.Add(g);
        //            //Clients.Clients(admin.ConnectionId).onNewUserAuthorization(u);
        //            //admin.groupName = u.groupName;
        //            //Clients.Caller.onNewUserAuthorization(admin);
        //            connectionToExclude.AddRange(admin.ConnectionId);
        //            foreach (var clientConneciton in admin.ConnectionId)
        //            {
        //                await Groups.Add(clientConneciton, u.groupName);
        //            }
        //            await Groups.Add(Context.ConnectionId, u.groupName);
        //        }
        //        ChatAdminGroups c = new ChatAdminGroups();
        //        c.User = new List<CustomerSupportUser>();
        //        c.UserName = new List<string>();
        //        c.GroupName = u.groupName;
        //        c.User.Add(u);
        //        c.User.Add(admin);
        //        c.UserName.Add(u.userName);
        //        c.UserName.Add(admin.userName);
        //        AdminGroup.Add(c);
        //    }
        //}
        public async void AdminAuthorization(string UserName, string Password)
        {
            if (UserName == "1" && Password == "1") return;
            {

                bool LoginSuccedded = false; // For Mobile Devices
                List<Website> Websites = new List<Website>();
                //bool demoMode = true;
                string ConnectionId = Context.ConnectionId;
                List<GroupEventMessage> GEM = new List<GroupEventMessage>();

                if (Context.User.Identity.IsAuthenticated)
                {
                    UserName = Context.User.Identity.Name;
                }
                else
                {
                    try
                    {
                        try
                        {
                            var result = await _signInManager.PasswordSignInAsync(UserName, Password, false, lockoutOnFailure: false);
                            if (result.Succeeded)
                            {
                                LoginSuccedded = true;
                                Clients.Caller.onLoginAttempt(new GenericResult
                                {
                                    Succeded = true,
                                });
                                System.Threading.Thread.Sleep(1000);
                            }
                            else
                            {
                                if (!Global.DevelopmentMode())
                                {
                                    Clients.Caller.onLoginAttempt(new GenericResult
                                    {
                                        Succeded = false,
                                        Message = "Invalid Username / Password"
                                    });
                                    return;
                                }
                                UserName = Global.DevelopmentUserName();
                                Clients.Caller.onLoginAttempt(new GenericResult
                                {
                                    Succeded = true,
                                    Message = "Logged In"
                                });
                            }
                        }
                        catch (Exception ex)
                        {
                            if (!Global.DevelopmentMode())
                            {
                                Clients.Caller.onLoginAttempt(new GenericResult
                                {
                                    Succeded = false,
                                    Message = "Invalid Username / Password"
                                });
                                return;
                            }
                            UserName = Global.DevelopmentUserName();
                            Clients.Caller.onLoginAttempt(new GenericResult
                            {
                                Succeded = true,
                                Message = "Logged In"
                            });
                        }
                    }
                    catch (Exception ex)
                    {
                        Global.Live_Crash_Report(ex);
                        Console.WriteLine("Check this for more hints : AdminAutorization");
                        return;
                    }
                }
                //Should fall in below line if logged in or in Development Mode                

                ChangingAdvert();


                var dbAdmin = (loggedInUsers.Count() > 0) ? loggedInUsers.SingleOrDefault(x => x.userName == UserName && x.IsAdmin == true) : null;
                if (dbAdmin != null) // Memory Ma Admin Cha Process and return                 
                {
                    if (dbAdmin.online == false)
                    {
                        var admintoUpdate = GetUser(dbAdmin.email);
                        admintoUpdate.Online = true;
                        ElasticSearchGlobal.AddUpdateApplicationUser(admintoUpdate);
                    }
                    dbAdmin.online = true;
                    if(!dbAdmin.ConnectionId.Contains(Context.ConnectionId))


                        if(CurrentUser() == null) dbAdmin.ConnectionId.Add(Context.ConnectionId);

                    foreach (var groups in dbAdmin.Groups)
                    {
                        try
                        {
                            await Groups.Add(ConnectionId, groups);
                        }
                        catch (Exception ex)
                        {
                            Global.Live_Crash_Report(ex);
                        }
                    }

                    SendClientListToAdmin(dbAdmin.Sites);
                    SendAdminListToAdmin(dbAdmin.Sites, dbAdmin.userName);
                    return;
                }
                else
                { // Admin Not In Memory :: Load From Db
                    ApplicationUser User = GetUser(UserName);
                    User.Online = true;
                    var adminUser = User;
                    var UserWebsitesa = UserWebsites(User.Id);//_context.UserRoles.Include(x => x.Website).Where(x => x.UserId == UserId).Where(x => x.Website.Delflag == false).Select(x => x.Website).Distinct();//.Where(x => x.User.UserName == User.Identity.Name).Select(x => x.Website).ToList();
                    int UserId;
                    try
                    {
                        try
                        {
                            await Groups.Add(ConnectionId, User.SignalrGuid.ToString());
                        }
                        catch (Exception ex)
                        {
                            Global.Live_Crash_Report(ex);
                        }
                        UserId = User.Id;
                    }
                    catch (Exception ex)
                    {
                        Global.Live_Crash_Report(ex);
                        Console.WriteLine("Needa Fix This : AdminAuthorization");
                        return;
                    }
                    CustomerSupportUser u = new CustomerSupportUser();
                    u.ConnectionId = new List<string>();
                    u.AcceptedSupportRequest = new List<string>();
                    u.Groups = new List<string>();
                    u.Sites = new List<string>();
                    u.AdminWebsiteDescription = new List<AdminUserWebsiteDescriptionViewModel>();
                    u.avatar = (!string.IsNullOrEmpty(adminUser.Image)) ? adminUser.Image : Global.NoImage();
                    u.fullName = adminUser.FullName;
                    u.userName = UserName;
                    u.Sites.AddRange(UserWebsitesa.Select(x => x.Name).ToList());
                    u.online = true;
                    u.IsAdmin = true;
                    u.email = adminUser.Email;
                    u.description = adminUser.Description;
                    u.DbAdminId = adminUser.Id;
                    u.id = adminUser.SignalrGuid;
                    u.groupName = "AdminGroup" + u.fullName + Guid.NewGuid().ToString();
                    if(!u.ConnectionId.Contains(Context.ConnectionId))
                    u.ConnectionId.Add(ConnectionId);
                    u.Groups.Add(User.SignalrGuid.ToString());
                    List<UserWebsiteDescription> adminWebsiteDescription = new List<UserWebsiteDescription>();
                    adminWebsiteDescription = ElasticSearchGlobal.GetUserWebsiteDescriptionsbyUserId(adminUser.Id);//_context.UserWebsiteDescription.Where(x => x.UserId == UserId).Include(x => x.Website).ToList();
                    foreach (var items in adminWebsiteDescription)
                    {
                        AdminUserWebsiteDescriptionViewModel d = new AdminUserWebsiteDescriptionViewModel();
                        d.avatar = (!string.IsNullOrEmpty(items.Avatar)) ? items.Avatar : u.avatar;
                        d.description = items.Description;
                        d.firstName = items.FirstName;
                        d.lastName = items.LastName;
                        d.websiteName = ElasticSearchGlobal.GetWebsiteNameById(items.WebsiteId);//items.Website.Name;
                        u.AdminWebsiteDescription.Add(d);
                    }
                    loggedInUsers.Add(u);
                    List<string> connectionToExclude = new List<string>();
                    try
                    {
                        foreach (var sites in UserWebsitesa)//user.Sites)
                        {
                            try
                            {
                                await Groups.Add(ConnectionId, sites.Name);
                                u.Groups.Add(sites.Name);
                            }
                            catch { }
                            try
                            {
                                await Groups.Add(ConnectionId, sites.Name + "-Admin");
                                u.Groups.Add(sites.Name + "-Admin");
                            }
                            catch { }
                            //try
                            //{
                            //    await Groups.Add(ConnectionId, sites.Alias);
                            //    u.Groups.Add(sites.Alias);
                            //}
                            //catch { }



                            //await Groups.Add(ConnectionId, sites.Alias);
                            //************************************************************************ */ Not Needed for now as send only list of people;
                            // foreach (var groupName in loggedInUsers.Where(x => x.webSite != null).Where(x => x.webSite == sites.Name).Select(x => x.groupName))
                            // {
                            //     await Groups.Add(ConnectionId, groupName);
                            //     u.Groups.Add(groupName);
                            //     var userMessage = GroupMessages.Where(x => x.groupName == groupName);
                            //     GEM.AddRange(userMessage);
                            // }

                            //Testing Removed
                            var otherAdminUsers = loggedInUsers.Where(x => x.IsAdmin).Where(x => x.Sites.Contains(sites.Name)).Where(x => x.userName != UserName).Where(x => x.online == true);
                            //foreach (var admin in otherAdminUsers)
                            //{
                            //    if (!connectionToExclude.Contains(admin.ConnectionId.FirstOrDefault()))
                            //    {
                            //        GroupEventMessage g = new GroupEventMessage();
                            //        u.groupName = "Admin" + Guid.NewGuid().ToString();
                            //        g.groupName = u.groupName;
                            //        g.EventMessage = new List<EventMessage>();
                            //        //g.WebsiteId = _context.Website.SingleOrDefault(x => x.Name == u.webSite).Id;
                            //        //Need to workaround this
                            //        GroupMessages.Add(g);
                            //        Clients.Clients(admin.ConnectionId).onNewUserAuthorization(u);
                            //        admin.groupName = u.groupName;
                            //        Clients.Caller.onNewUserAuthorization(admin);
                            //        connectionToExclude.AddRange(admin.ConnectionId);
                            //        foreach (var clientConneciton in admin.ConnectionId)
                            //        {
                            //            await Groups.Add(clientConneciton, u.groupName);
                            //        }
                            //        await Groups.Add(ConnectionId, u.groupName);
                            //    }
                            //    ChatAdminGroups c = new ChatAdminGroups();
                            //    c.User = new List<CustomerSupportUser>();
                            //    c.UserName = new List<string>();
                            //    c.GroupName = u.groupName;
                            //    c.User.Add(u);
                            //    c.User.Add(admin);
                            //    c.UserName.Add(u.userName);
                            //    c.UserName.Add(admin.userName);
                            //    AdminGroup.Add(c);
                            //}
                            if (otherAdminUsers.Count() == 0)
                            {
                                //if no admin was online send a message to all connected clients about the onlinestatus
                                var userDescriptiontoSendtoLoggedinUser = loggedInUsers.Where(x => x.Sites != null).Where(x => x.Sites.Contains(sites.Name)).Select(x => x.AdminWebsiteDescription).FirstOrDefault();
                                var aa = userDescriptiontoSendtoLoggedinUser.Where(x => x.websiteName == sites.Name).SingleOrDefault();
                                if (aa != null)
                                    foreach (var items in loggedInUsers.Where(x => x.webSite == sites.Name).Select(x => x.ConnectionId))
                                    {
                                        Clients.Clients(items).onAdminStatus(new CustomerSupportAdminViewModel(aa));
                                    }
                                else
                                {
                                    var admintoSend = loggedInUsers.Where(x => x.IsAdmin == true).Where(x => x.Sites.Contains(sites.Name)).FirstOrDefault();
                                    foreach (var items in loggedInUsers.Where(x => x.webSite == sites.Name).Select(x => x.ConnectionId))
                                    {
                                        Clients.Clients(items).onAdminStatus(new CustomerSupportAdminViewModel(admintoSend));
                                    }
                                }
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        Global.Live_Crash_Report(ex);
                        Console.WriteLine("Serious Error Needa Check" + "On AdminAuthorization : " + ex.Message);
                    }
                    ElasticSearchGlobal.AddUpdateApplicationUser(User);
                    SendClientListToAdmin(u.Sites);
                    SendAdminListToAdmin(u.Sites, u.userName);
                }
            }
        }

        private List<Website> UserWebsites(int UserId)
        {
            List<Website> userWebite = new List<Website>();
            if (!_memoryCache.TryGetValue(UserId.ToString() + "Website", out userWebite))
            {
                List<Website> _userWebsite1 = new List<Website>();
                //_userWebsite1 = _context.UserRoles.Include(x => x.Website).Where(x => x.UserId == UserId).Where(x => x.Website.Delflag == false).Select(x => x.Website).Distinct().ToList();                
                _userWebsite1 = ElasticSearchGlobal.GetWebsiteByUserId(UserId);
                _memoryCache.Set(UserId.ToString() + "Website", _userWebsite1, new MemoryCacheEntryOptions()
                                .SetAbsoluteExpiration(TimeSpan.FromMinutes(Global.CacheDuration())));
                userWebite = _userWebsite1;
            }
            return userWebite;
        }
        public override Task OnConnected()
        {
            string ConnectionId = Context.ConnectionId;
            var ip = Context.Request.HttpContext.Connection.RemoteIpAddress;
            if (Context.Request.Query["isAdmin"].ToString() == "true")
            {
                AdminAuthorization("1", "1");
                return null;
            }
            string groupName = "Support" + ConnectionId;
            CustomerSupportUser u = new CustomerSupportUser();
            u.CurrentPage = Context.Request.Query["PageTitle"].ToString();
            u.CurrentUrl = Context.Request.Query["PageURL"].ToString();
            u.CurrentHost = Context.Request.Query["Host"].ToString();
            u.CurrentHost = u.CurrentHost.Replace("www.", "").Replace("https://", "").Replace("http://", "");
            string siteName = u.CurrentHost;
            u.online = true;
            var report = ElasticSearchGlobal.GetDashBoardReportByWebsiteForDate(ElasticSearchGlobal.GetWebsitebyName(u.CurrentHost).Id,DateTime.Now);
            try
            {
                // Need to workaround this part
                u.webSite = u.CurrentHost;// Context.Request.Host.Host;
            }
            catch (Exception ex)
            {
                Global.Live_Crash_Report(ex);
                //From Mobile device
            }
            if (!string.IsNullOrEmpty(Context.Request.Query["Cookie"].ToString()))
            {
                groupName = (Context.User.Identity.IsAuthenticated) ? GetUser(Context.User.Identity.Name).SignalrGuid.ToString() : Context.Request.Query["Cookie"].ToString();
                if (report != null)
                {
                    report.User_Count = report.User_Count + 1;
                    ElasticSearchGlobal.SaveReport(report);
                }
                else
                {
                    ReportDb db = new ReportDb();
                    db.Unique_User_Count = 1;
                    db.WebsiteId = GetWebsite(u.CurrentHost).Id;
                    _context.Reports.Add(db);
                    _context.SaveChanges();
                    ElasticSearchGlobal.SaveReport(db);
                }
            }
            else
            {
                if (report != null)
                {
                    report.Unique_User_Count = report.Unique_User_Count + 1;
                    ElasticSearchGlobal.SaveReport(report);
                }
                else
                {
                    ReportDb db = new ReportDb();
                    db.Unique_User_Count = 1;
                    db.WebsiteId = GetWebsite(u.CurrentHost).Id;
                    _context.Reports.Add(db);
                    _context.SaveChanges();
                    ElasticSearchGlobal.SaveReport(db);
                }
                var ipa = Context.Request.HttpContext.Connection.RemoteIpAddress;
                groupName = (Context.User.Identity.IsAuthenticated) ? GetUser().SignalrGuid.ToString() : groupName;
                //GeoService(ip.ToString(), ConnectionId);
            }
            Groups.Add(ConnectionId, groupName);
            //Assign Admin to the User Group
            foreach (var connectionIds in loggedInUsers.Where(x => x.IsAdmin == true).Where(x => x.Sites.Contains(u.CurrentHost)))
            {
                foreach (var cId in connectionIds.ConnectionId)
                {
                    Groups.Add(cId, groupName);
                    //Todo Check Again and again
                    loggedInUsers.Distinct().SingleOrDefault(x => x.ConnectionId.Contains(cId)).Groups.Add(groupName);
                }
            }
            try
            {                
                loggedInUsers.SingleOrDefault(x => x.groupName == groupName).online = true;
                loggedInUsers.SingleOrDefault(x => x.groupName == groupName).CurrentPage = Context.Request.Query["PageTitle"].ToString();
                loggedInUsers.SingleOrDefault(x => x.groupName == groupName).CurrentUrl = Context.Request.Query["PageURL"].ToString();
                if(!loggedInUsers.SingleOrDefault(x => x.groupName == groupName).ConnectionId.Contains(Context.ConnectionId))
                loggedInUsers.SingleOrDefault(x => x.groupName == groupName).ConnectionId.Add(ConnectionId);
                SitePage s = new SitePage();
                s.DateTime = DateTime.Now;
                s.PageTitle = Context.Request.Query["PageTitle"].ToString();
                s.Url = Context.Request.Query["PageURL"].ToString();                
                loggedInUsers.SingleOrDefault(x => x.groupName == groupName).SitePages.Add(s);
                if (CurrentUser().blocked == true)
                {
                    Clients.Clients(CurrentUser().ConnectionId.ToList()).onBlocked();
                }
                var signalRUser = ElasticSearchGlobal.GetSignalRUser(null, groupName,ElasticSearchGlobal.GetWebsitebyName(u.webSite).Guid.ToString());//_context3.SignalRUser.Where(x => x.ChatMessage.GroupId == groupName).AsNoTracking().FirstOrDefault();
                if (signalRUser != null)
                {
                    loggedInUsers.SingleOrDefault(x => x.groupName == groupName).fullName = signalRUser.FullName;
                    loggedInUsers.SingleOrDefault(x => x.groupName == groupName).phoneNumber = signalRUser.PhoneNumber;
                    loggedInUsers.SingleOrDefault(x => x.groupName == groupName).avatar = (signalRUser.Avatar == null) ? Global.NoImage() : signalRUser.Avatar;                    
                }
                //u.online = true;
            }
            catch
            {

                //var ip = Context.Request.HttpContext.Connection.RemoteIpAddress;
                u.ConnectionId = new List<string>();
                u.groupName = groupName;
                u.AwaitingSupport = true;
                u.clientScript = true;
                u.online = true;
                u.id = Guid.NewGuid();
                u.SitePages = new List<SitePage>();
                SitePage s = new SitePage();
                s.DateTime = DateTime.Now;
                s.PageTitle = Context.Request.Query["PageTitle"].ToString();
                s.Url = Context.Request.Query["PageURL"].ToString();
                u.SitePages.Add(s);
                if (Context.User.Identity.IsAuthenticated)
                {
                    string username = Context.User.Identity.Name;
                    var user = GetUser();//_context.Users.Where(x => x.Email == username).SingleOrDefault();//_context.Users.Where(x => x.Email == username).SingleOrDefault();
                    u.fullName = user.FullName;
                    u.avatar = (string.IsNullOrEmpty(user.Avatar)) ? Global.NoImage() : user.Image;
                    u.authenticate = true;
                    u.email = username;
                    Clients.Caller.onCustomerInfoChanged(true);
                }
                else
                {
                    u.avatar = Global.NoImage(); //TODO shoud mention in admin panel
                    u.fullName = ip.ToString();// "(" + siteName + ")"; //+ //" : " + Guid.NewGuid().ToString();
                }
                //var reportdate = GetReportDate();
                if(!u.ConnectionId.Contains(Context.ConnectionId)){
                    u.ConnectionId.Add(ConnectionId);
                    loggedInUsers.Add(u);                
                }
                //var signalRUser = _context3.SignalRUser.Where(x => x.ChatMessage.GroupId == groupName).SingleOrDefault();
                // below code is for timebeing only
                var signalRUser = ElasticSearchGlobal.GetSignalRUser(null, groupName,ElasticSearchGlobal.GetWebsitebyName(u.webSite).Guid.ToString());//_context3.SignalRUser.Where(x => x.ChatMessage.GroupId == groupName).AsNoTracking().FirstOrDefault();
                if (signalRUser != null)
                {
                    u.fullName = signalRUser.FullName;
                    u.phoneNumber = signalRUser.PhoneNumber;
                    u.avatar = (signalRUser.Avatar == null) ? Global.NoImage() : signalRUser.Avatar;
                }                
                Clients.Caller.onSupportGroupIssued(groupName);                
            }

            if (GroupMessages.SingleOrDefault(x => x.groupName == groupName) == null)           
            {
                try
                {

                    GroupEventMessage gem = new GroupEventMessage();
                    gem.groupName = groupName;
                    gem.WebsiteId = GetWebsite(u.webSite).Id;
                    gem.EventMessage = new List<EventMessage>();
                    GroupMessages.Add(gem);                    
                    List<EventMessage> dbEventMessage = new List<EventMessage>();
                    GroupMessages.SingleOrDefault(x => x.groupName == groupName).EventMessage = dbEventMessage;
                }
                catch (Exception ex)
                {
                    Global.Live_Crash_Report(ex);
                }
            }
            try
            {
                //SendPreviousMessage(groupName);
            }
            catch (Exception ex)
            {
                Global.Live_Crash_Report(ex);
                Console.WriteLine("SendPrevious Message for groupname :" + groupName + " Failed.");
            }
            if (loggedInUsers.Count() > 0)
                try
                {
                    if (loggedInUsers.SingleOrDefault(x => x.ConnectionId.Contains(Context.ConnectionId)).ConnectionId.Count == 1)                    
                        Clients.Group(siteName).onNewUserAuthorization(new CustomerSupportUserViewModel(CurrentUser()));                    
                }
                catch (Exception ex)
                {
                    Global.Live_Crash_Report(ex);
                }
            GeoService(ip.ToString(), ConnectionId);
            return base.OnConnected();
        }
        public override Task OnDisconnected(bool stopCalled)
        {
            string ConnectionId = Context.ConnectionId;
            var user = CurrentUser();//loggedInUsers.SingleOrDefault(x => x.ConnectionId.Contains(ConnectionId));
            if (user != null)
            {
                if (!user.IsAdmin && user.ConnectionId.Count == 1)
                {
                    user.DisconnectedTime = DateTime.Now;
                    user.online = false;
                    testsendingbeforeoffline(ConnectionId);
                    return base.OnDisconnected(false);
                }
                if (user.IsAdmin && user.ConnectionId.Count == 1)
                {
                    RemoveAdmin30SecondsBeforeOffline(ConnectionId);
                    return base.OnDisconnected(false);
                }
                try
                {
                    loggedInUsers.SingleOrDefault(x => x.groupName == user.groupName).ConnectionId.Remove(ConnectionId);
                }
                catch (Exception ex)
                {
                    Global.Live_Crash_Report(ex);
                }
            }
            return base.OnDisconnected(false);
        }
        public async void SwitchedToFreePlan(int WebsiteId)
        {
            //Todo check if admin
            var currentWebsite = ElasticSearchGlobal.GetWebsitebyWebsiteId(WebsiteId);// //_context.Website.Where(x => x.Id == WebsiteId).SingleOrDefault();
            currentWebsite.WebstieType = WebsiteType.Free;
            ElasticSearchGlobal.AddUpdateWebsite(currentWebsite);
            //_context.Website.Update(currentWebsite);
            //_context.SaveChanges();
            Clients.Caller.onSwitchedToFreePlan(WebsiteId);
        }
        public async void CheckAndSendAdminToClient(List<string> WebSiteName)
        {
            foreach (string websiteName in WebSiteName)
            {
                var otherAdminUsers = loggedInUsers.Where(x => x.IsAdmin).Where(x => x.Sites.Contains(websiteName)).Where(x => x.online == true);
                if (otherAdminUsers != null)
                {
                    var userDescriptiontoSendtoLoggedinUser = loggedInUsers.Where(x => x.Sites != null).Where(x => x.Sites.Contains(websiteName)).Select(x => x.AdminWebsiteDescription).FirstOrDefault();
                    var aa = userDescriptiontoSendtoLoggedinUser.Where(x => x.websiteName == websiteName).SingleOrDefault();
                    if (aa != null)
                        foreach (var items in loggedInUsers.Where(x => x.webSite == websiteName).Select(x => x.ConnectionId))
                        {
                            Clients.Clients(items).onAdminStatus(new CustomerSupportAdminViewModel(aa));
                        }
                    else
                    {
                        var admintoSend = loggedInUsers.Where(x => x.IsAdmin == true).Where(x => x.Sites.Contains(websiteName)).FirstOrDefault();
                        foreach (var items in loggedInUsers.Where(x => x.webSite == websiteName).Select(x => x.ConnectionId))
                        {
                            Clients.Clients(items).onAdminStatus(new CustomerSupportAdminViewModel(admintoSend));
                        }
                    }
                }
            }
            //var otherAdminUsers = loggedInUsers.Where(x => x.IsAdmin).Where(x => x.Si tes.Contains(webSiteName)).Where(x => x.online == true).SingleOrDefault();
        }
        public async void RemoveAdmin30SecondsBeforeOffline(string connectionId)
        {
            //Send to peeps that the user lost connectionk

            await Task.Delay(30000);
            var whoUser = loggedInUsers.SingleOrDefault(x => x.ConnectionId.Contains(connectionId));
            if (whoUser.ConnectionId.Count == 1)
            {
                whoUser.online = false;
                var User = GetUser(whoUser.email);//_userManager.FindByEmailAsync(whoUser.email).Result;//_context.Users.SingleOrDefault(x => x.Email == whoUser.email);
                User.Online = false;
                Clients.Groups(whoUser.Groups.ToList()).onAdminStatus(new CustomerSupportAdminViewModel(whoUser));
                ElasticSearchGlobal.AddUpdateApplicationUser(User);
                //_context.Users.Update(User);
                try
                {
                    //    _context.SaveChanges();
                }
                catch (Exception ex)
                {
                    Global.Live_Crash_Report(ex);
                }

                CheckAndSendAdminToClient(whoUser.Sites.ToList());
                loggedInUsers.Remove(whoUser);
            }
            else
            {
                whoUser.ConnectionId.Remove(connectionId);
            }
        }
        public async void testsendingbeforeoffline(string connectionId)
        {
            //Send to peeps that the user lost connection
            await Task.Delay(10000);
            var decisiontoRemove = loggedInUsers.SingleOrDefault(x => x.ConnectionId.Contains(connectionId));
            if (decisiontoRemove.ConnectionId.Count == 1)
            {
                Clients.Others.onUserOffline(decisiontoRemove.groupName);
                var CurrentSignalrUser = ElasticSearchGlobal.GetSignalRUser(decisiontoRemove.groupName);
                if ( CurrentSignalrUser!= null)
                {
                    CurrentSignalrUser.IpAddress = decisiontoRemove.ip;
                    CurrentSignalrUser.SitePages.AddRange(decisiontoRemove.SitePages);
                    ElasticSearchGlobal.UpdateSignalRUser(CurrentSignalrUser);
                };
                loggedInUsers.Remove(decisiontoRemove);
            }
            else
            {
                loggedInUsers.SingleOrDefault(x => x.ConnectionId.Contains(connectionId)).ConnectionId.Remove(connectionId);
            }
        }
        private static HttpClient Client = new HttpClient();
        private async void GeoService(string ip, string connectionId)
        {
            loggedInUsers.Where(x => x.ConnectionId.Contains(connectionId)).FirstOrDefault().ip = "202.166.194.252";
            if (ElasticSearchGlobal.GetIpInfoDb(ip) == null)
                try
                {
                    Client.DefaultRequestHeaders.Accept.Clear();
                    Client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));                    
                    HttpResponseMessage response = await Client.GetAsync("http://api.ipinfodb.com/v3/ip-city/?format=json&key=d9f65559398a43e1d2ca972f9ddf8edca19d34cc54487908b3b51e588c50e465&ip=" + ip);
                    var result = await response.Content.ReadAsStringAsync();
                    IpInfoDb geoInformation = JsonConvert.DeserializeObject<IpInfoDb>(result);
                    geoInformation.countrycode = geoInformation.countrycode.ToLower();
                    if(ElasticSearchGlobal.GetIpInfoDb(ip) == null){
                        ElasticSearchGlobal.AddIpInfoDb(geoInformation);
                    }                
                    response.RequestMessage.Dispose();
                    response.Dispose();                    
                }
                catch (Exception ex)
                {
                    Global.Live_Crash_Report(ex);
                }
        }
    }    
    public class CustomerSupportUser
    {
        //public string connectionId { get; set; }
        public Guid id { get; set; }
        public string ip {get;set;}
        public string description { get; set; }
        public string fullName { get; set; }
        public bool clientScript { get; set; }
        public string CurrentHost { get; set; }
        public string status { get; set; }
        public string email { get; set; }
        public List<string> ConnectionId { get; set; }
        public List<string> AcceptedSupportRequest { get; set; }
        public string memberType { get; set; }
        public List<string> Sites { get; set; }
        public string avatar { get; set; }
        public string contextName { get; set; }
        public string deviceId { get; set; }
        public string CurrentUrl { get; set; }
        public string userName { get; set; }
        public string phoneNumber { get; set; }
        public string groupName { get; set; }
        public bool blocked { get; set; }
        public bool online { get; set; }
        public bool Busy { get; set; }
        public int DbAdminId { get; set; }
        public int TotalTabs { get; set; }
        public List<AdminUserWebsiteDescriptionViewModel> AdminWebsiteDescription { get; set; }
        public string webSite { get; set; }
        public bool authenticate { get; set; }
        public List<string> Groups { get; set; }
        public bool AwaitingSupport { get; set; }
        public string CurrentPage { get; set; }
        public string LastMessage { get; set; }
        public List<SitePage> SitePages { get; set; }
        public DateTime LastActive { get; set; }
        public bool IsAdmin { get; set; }
        public GeoInformation GeoInformation { get; set; }
        public IpInfoDb IpInfoDb { get; set; }
        public DateTime DisconnectedTime { get; set; }
        public bool AnyPaidSite { get; set; }

    }
    public class CustomerSupportUserViewModel
    {
        public CustomerSupportUserViewModel(CustomerSupportUser c)
        {
            if (c != null)
            {
                fullName = c.fullName;
                avatar = c.avatar;
                IsAdmin = c.IsAdmin;
                groupName = c.groupName;
                blocked = c.blocked;
                online = c.online;
                webSite = c.webSite;
            }
        }
        public CustomerSupportUserViewModel(AdminUserWebsiteDescriptionViewModel c)
        {
            fullName = c.fullName;
            avatar = c.avatar;
            IsAdmin = true;
        }
        public string fullName { get; set; }
        public string avatar { get; set; }
        public bool IsAdmin { get; set; }
        public string groupName { get; set; }
        public bool blocked { get; set; }
        public bool online { get; set; }
        public string webSite { get; set; }
    }
    public class AdminUserWebsiteDescriptionViewModel
    {
        public string websiteName { get; set; }
        public string firstName { get; set; }
        public string lastName { get; set; }
        public string description { get; set; }
        public string fullName
        {
            get { return firstName + " " + lastName; }
        }
        public string avatar { get; set; }
    }
    public class CustomerSupportAdminViewModel
    {
        public CustomerSupportAdminViewModel(CustomerSupportUser c)
        {
            fullName = c.fullName;
            avatar = c.avatar;
            description = c.description;
            online = c.online;
        }
        public CustomerSupportAdminViewModel(AdminUserWebsiteDescriptionViewModel c)
        {
            fullName = c.fullName;
            avatar = c.avatar;
            description = c.description;
            online = true;
        }
        public string fullName { get; set; }
        public string description { get; set; }
        public string avatar { get; set; }
        public bool online { get; set; }
    }
    public class WebsitesSignalR
    {
        public string Name { get; set; }
        public List<DepartmentSignalR> Departments { get; set; }
    }
    public class MarcoSignalR
    {
        //public int Id { get; set; }
        public string Name { get; set; }
        public MarcoType MarcoType { get; set; }
        public string TextorURL { get; set; }
        public string KeyBoardShortcut { get; set; }
    }
    public class DepartmentSignalR
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public List<MarcoSignalR> Macro { get; set; }
    }
    public class GeoInformation
    {
        public GeoInformation(string json)
        {

        }
        public string ip { get; set; }
        public string country_code { get; set; }
        public string country_name { get; set; }
        public string region_code { get; set; }
        public string region_name { get; set; }
        public string city { get; set; }
        public string zip_code { get; set; }
        public string time_zone { get; set; }
        public string latitude { get; set; }
        public string longitude { get; set; }
        public string metro_code { get; set; }
    }
    // public class IpInfoDb
    // {
    //     public IpInfoDb(string json)
    //     {

    //     }
    //     public IpInfoDb() { }
    //     public string ipaddress { get; set; }
    //     public string countrycode { get; set; }
    //     public string countryname { get; set; }
    //     public string cityname { get; set; }
    //     public string regionname { get; set; }
    //     public string zipcode { get; set; }
    //     public string timezone { get; set; }
    //     public string latitude { get; set; }
    //     public string longitude { get; set; }
    // }
    public class ChatAdminGroups
    {
        public string GroupName { get; set; }
        public List<CustomerSupportUser> User { get; set; }
        public List<string> UserName { get; set; }
    }
    public class GroupEventMessage
    {
        public int Id { get; set; }
        public string groupName { get; set; }
        public int WebsiteId { get; set; }
        public string DataBaseId { get; set; }
        public List<EventMessage> EventMessage { get; set; }
    }
    public class EventMessage
    {
        public DateTime DateTime { get; set; }
        public enumTicketMesssages Type { get; set; }
        public CustomerSupportUserViewModel User { get; set; }
        public string UserId { get; set; }
        public string Message { get; set; }
    }
    public class Department
    {
        public string Name { get; set; }
    }
    public enum enumTicketMesssages
    {
        [Description(" Started Typing")]
        Typing = 1,
        [Description(" StoppedTyping Typing")]
        StoppedTyping = 2,

        [Description(" has joined the Support")]
        JoinedSupport = 3,

        [Description(" has left the Support")]
        LeftSupport = 3,

        [Description("Text Message")]
        TextMessage = 3,

        [Description("Image Message")]
        ImageMessage = 3,
        [Description("Push Marco")]
        PushMarco = 3,

    }
    public class SignalRMessage
    {
        public int ticketId { get; set; }
        public string message { get; set; }
    }
    public class SupportRequestAcceptedViewModel
    {
        public string groupName { get; set; }
        public string adminName { get; set; }
    }
    public class CoolParameter
    {
        public string Command { get; set; }
        public string Param { get; set; }
    }
    public class MessageViewModel
    {
        public string groupName { get; set; }
        public string Message { get; set; }
        public DateTime DateTime { get; set; }
        public MessageUserViewModelUser User { get; set; }
        //public CustomerSupportUser User { get; set; }
    }
    public class MessageUserViewModelUser
    {
        public string fullName { get; set; }
        public string email { get; set; }
        public string avatar { get; set; }
        public bool IsAdmin { get; set; }
    }
    public class CustomerInfo
    {
        public string GroupName { get; set; }
        public string fullName { get; set; }
        public string groupName { get; set; }
        public string email { get; set; }
        public string phoneNumber { get; set; }
        public string avatar { get; set; }
    }
    public class GeoinfoAndPages
    {
        public string GroupName { get; set; }
        public IpInfoDb IpInfoDb { get; set; }
        public string CurrentPage { get; set; }
        public string CurrentUrl { get; set; }
        public List<SitePage> SitePage { get; set; }

    }
    public class OfflineCustomerInfo
    {
        public string fullName { get; set; }
        public string email { get; set; }
        public string phoneNumber { get; set; }
        public string message { get; set; }
    }
    public class UserTypingViewModel
    {
        [JsonProperty("g")]
        public string g { get; set; }
        [JsonProperty("f")]
        public string f { get; set; }
        [JsonProperty("a")]
        public string a { get; set; }
    }
    //private class Group
    //public class GenericResult
    //{
    //    public bool Success { get; set; }
    //    public string Message { get; set; }
    //}
    public class ServerMessageClass
    {
        public string Segment { get; set; }
        public string Param { get; set; }
        public Guid Guid { get; set; }
    }
}
