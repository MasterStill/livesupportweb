﻿using MailKit.Net.Smtp;
using MailKit.Security;
using MimeKit;
using RestSharp;
using RestSharp.Authenticators;
using System;
using System.Collections.Generic;
using System.IO;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;

namespace SignalRMasterServer.Services
{
    public class AuthMessageSender : IEmailSender, ISmsSender
    {
        public async Task SendEmailAsync(string email, string subject, string message,string FromAddress, string FromAdressTitle)
        {
            message = message.Replace("http://admin.livecustomer.chat","https://admin.livecustomer.chat");                        
            string ToAddress = email;
            string ToAdressTitle = subject;
            string Subject = subject;
            string BodyContent =message;                    
            string SmtpServer = "smtp.mailgun.org";
            int SmtpPortNumber = 587;
                var mimeMessage = new MimeMessage();
                mimeMessage.From.Add(new MailboxAddress(FromAdressTitle, FromAddress));
                mimeMessage.To.Add(new MailboxAddress(ToAdressTitle, ToAddress));
                mimeMessage.Subject = Subject;                                
                mimeMessage.Body = new TextPart("html")
                {
                    Text = BodyContent
                };
                using (var client = new SmtpClient())
                {

                    client.Connect(SmtpServer, SmtpPortNumber, SecureSocketOptions.StartTlsWhenAvailable);
                    //client.LocalDomain = "teamrockmandu.com";
                client.LocalDomain = "livecustomer.chat";

                //client.Authenticate("subin@teamrockmandu.com", "123!@#Rashi!!123!@#Rashi!!");
                client.Authenticate("noreply@livecustomer.chat", "123!@#Rashi!!");
                client.Send(mimeMessage);
                    Console.WriteLine("The mail has been sent successfully !!");
                    Console.ReadLine();
                    client.Disconnect(true);
            
                }

        }

        public Task SendSmsAsync(string number, string message)
        {
            // Plug in your SMS service here to send a text message.
            return Task.FromResult(0);
        }

    }
 
//using System;
//using System.Collections.Generic;
//using System.Diagnostics;
//using System.Net;
//using System.Net.Http;
//using System.Net.Http.Headers;
//using System.Text;
//using System.Threading.Tasks;

//public class MailService
//    {
//        // the domain name you have verified in your Mailgun account
//        const string DOMAIN = "livecustomer.chat";
//        // your API Key used to send mail through the Mailgun API
//        const string API_KEY = "key-619b0827230b9af343d4c10b1bf80031";
//        public async Task<bool> SendAsync(string from, string to, string subject, string message)
//        {

//            var client = new HttpClient();
//            client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Basic", Convert.ToBase64String(UTF8Encoding.UTF8.GetBytes("api" + ":" + API_KEY)));
//            var form = new Dictionary<string, string>();
//            form["from"] = from;
//            form["to"] = to;
//            form["subject"] = subject;
//            form["text"] = message;
//            var response = await client.PostAsync("https://api.mailgun.net/v3/" + DOMAIN + "/messages", new FormUrlEncodedContent(form));
//            if (response.StatusCode == HttpStatusCode.OK)
//            {
//                return true;
//            }
//            else
//            {
//                return false;
//            }
//            //RestClient client1 = new RestClient();
//            //client1.BaseUrl = new Uri("https://api.mailgun.net/v3");
//            //client1.Authenticator =
//            //    new HttpBasicAuthenticator("api",
//            //                                API_KEY);
//            //RestRequest request = new RestRequest();
//            //request.AddParameter("domain", DOMAIN, ParameterType.UrlSegment);
//            //request.Resource = "{domain}/messages";
//            //request.AddParameter("from", "Excited User <noreply@" + DOMAIN + ">");
//            //request.AddParameter("to", "masterstill@gmail.com");
//            ////request.AddParameter("to", "YOU@YOUR_DOMAIN_NAME");
//            //request.AddParameter("subject", "   Hello");
//            //request.AddParameter("text", "Testing some Mailgun awesomness!");
//            //request.Method = Method.POST;
//            //var kk = client1.ExecuteAsyncPost(request, null, HttpMethod.Post.ToString());
//           // RestClient client = new RestClient();
//           // client.BaseUrl = new Uri("https://api.mailgun.net/v3");
//           // client.Authenticator =
//           //     new HttpBasicAuthenticator("api",
//           //                                 API_KEY);
//           // RestRequest request = new RestRequest();
//           // request.AddParameter("domain", DOMAIN, ParameterType.UrlSegment);
//           // request.Resource = "{domain}/messages.mime";
//           // request.AddParameter("from", "Live Customer Chat <noreply@"   +   DOMAIN +  ">");
//           // request.AddParameter("to", "masterstill@gmail.com");
//           // request.AddParameter("subject", "Hello");
//           // request.AddParameter("text", "Testing some Mailgun awesomness!");
//           // // request.AddFile("message", Path.Combine("files", "message.mime"));
//           // request.Method = Method.POST;
//           //var kkk= client.ExecuteAsync(request,null);
            
//           // return true;
//        }
//    }
}
