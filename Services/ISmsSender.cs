﻿using System.Threading.Tasks;

namespace SignalRMasterServer.Services
{
    public interface ISmsSender
    {
        Task SendSmsAsync(string number, string message);
    }
}
