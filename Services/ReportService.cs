﻿using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using Newtonsoft.Json.Serialization;
using SignalRMasterServer.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Threading.Tasks;
namespace SignalRMasterServer.Services
{
    public static class ReportService
    {
        public static ReportViewModel Report(int WebsiteId)
        {
            //Check if admin of the website
            ReportViewModel r = new ReportViewModel();
            var blankReport = new ReportDb
            {
                Answered_Call_Count = 0,
                UnAnswered_Call_Count = 0,
                User_Count = 0
            };
            var currentWebsite = ElasticSearchGlobal.GetWebsitebyWebsiteId(WebsiteId);
            ReportDb TodayReport = ElasticSearchGlobal.GetDashBoardReportByWebsiteForDate(WebsiteId, DateTime.Now);
            ReportDb YesterdayReport = ElasticSearchGlobal.GetDashBoardReportByWebsiteForDate(WebsiteId, DateTime.Now.AddDays(-1));
            ReportDb TwoDaysAgoReport = ElasticSearchGlobal.GetDashBoardReportByWebsiteForDate(WebsiteId, DateTime.Now.AddDays(-2));

            if (TodayReport == null) TodayReport = blankReport;
            if (YesterdayReport == null) YesterdayReport = blankReport;
            if (TwoDaysAgoReport == null) TwoDaysAgoReport = blankReport;
            //r.widget1 = new Widgets
            //{
            //    currentRange = enumRangesDay.Today,
            //    ranges = new DayRanges
            //    {
            //        Two_Days_Ago = Global.FilterEnumString(enumRangesDay.Two_Days_Ago.ToString()),
            //        Yesterday = enumRangesDay.Yesterday.ToString(),
            //        Today = enumRangesDay.Today.ToString(),
            //    },
            //    data = new data
            //    {
            //        label = "Users Online",
            //        count = new countDays
            //        {
            //            Today = TodayReport.Unique_User_Count,
            //            Two_Days_Ago = TwoDaysAgoReport.Unique_User_Count,
            //            Yesterday = YesterdayReport.Unique_User_Count
            //        },
            //        extra = new extra
            //        {
            //            count = new countDays
            //            {
            //                Today = TodayReport.Unique_User_Count,
            //                Yesterday = TwoDaysAgoReport.Unique_User_Count,
            //                Two_Days_Ago = YesterdayReport.Unique_User_Count,
            //            },
            //            label = "Total New Users Online "
            //        }
            //    },
            //    detail = "Lets see",
            //};
            if (ElasticSearchGlobal.GetDashBoardReportByWebsiteForDate(WebsiteId, DateTime.Now) == null)
            {
                return r;
            }
            var This_Week_Reports = ElasticSearchGlobal.GetDashBoardReportByWebsiteForPreviousWeek(WebsiteId, enumRangesWeek.This_Week);
            var Last_Week_Reports = ElasticSearchGlobal.GetDashBoardReportByWebsiteForPreviousWeek(WebsiteId, enumRangesWeek.Last_Week);
            var Two_Weeks_Ago_Reports = ElasticSearchGlobal.GetDashBoardReportByWebsiteForPreviousWeek(WebsiteId, enumRangesWeek.Two_Weeks_Ago);
            List<XY> This_Week_Answered_Data = new List<XY>();
            List<XY> This_Week_Unanswered_Data = new List<XY>();
            List<XY> Last_Week_Answered_Data = new List<XY>();
            List<XY> Last_Week_Unanswered_Data = new List<XY>();
            List<XY> Two_Weeks_Ago_Answered_Data = new List<XY>();
            List<XY> Two_Weeks_Ago_Unanswered_Data = new List<XY>();
            foreach (var items in This_Week_Reports)
            {
                XY x = new XY();
                x.x = items.ReportDate.Date.Day;
                x.y = items.Answered_Call_Count;
                This_Week_Answered_Data.Add(x);
                x.y = items.Unique_User_Count;
                This_Week_Unanswered_Data.Add(x);
            }
            foreach (var items in Last_Week_Reports)
            {
                XY x = new XY();
                x.x = items.ReportDate.Date.Day;
                x.y = items.Answered_Call_Count;
                Last_Week_Answered_Data.Add(x);
                x.y = items.Unique_User_Count;
                Last_Week_Unanswered_Data.Add(x);
            }
            foreach (var items in Two_Weeks_Ago_Reports)
            {
                XY x = new XY();
                x.x = items.ReportDate.Date.Day;
                x.y = items.Answered_Call_Count;
                Two_Weeks_Ago_Answered_Data.Add(x);
                x.y = items.Unique_User_Count;
                Two_Weeks_Ago_Unanswered_Data.Add(x);
            }
            r.widget5 = new ChartWidget
            {
                title = "Total Users Vs Answered Call",
                ranges = new WeekRanges
                {
                    Two_Weeks_Ago = Global.FilterEnumString(enumRangesWeek.Two_Weeks_Ago.ToString()),
                    Last_Week = Global.FilterEnumString(enumRangesWeek.Last_Week.ToString()),
                    This_Week = Global.FilterEnumString(enumRangesWeek.This_Week.ToString()),
                },
                mainChart = new List<mainChart>
                     {
                         new mainChart
                        {
                         key = "Answered Calls",
                         values = new  GraphWeekClass
                             {
                                 This_Week= This_Week_Answered_Data,
                                 Last_Week = Last_Week_Answered_Data,
                                 Two_Weeks_ago =  Two_Weeks_Ago_Answered_Data,
                             }
                        },
                         new mainChart
                        {
                         key = "Unanswered Calls",
                         values = new  GraphWeekClass
                             {
                                 This_Week= This_Week_Unanswered_Data,
                                 Last_Week =Last_Week_Unanswered_Data,
                                 Two_Weeks_ago = Two_Weeks_Ago_Unanswered_Data
                             }
                        }
                     }
            };
            return r;
        }
    }
    public static class AnalyticsReport
    {

        public static AnalyticsWidget GetAnalyticsWidget(int websiteId,string[] CurrentUserIp = null,int NoOfUsers=0)
        {
            var This_Month_Data = ElasticSearchGlobal.GetDashBoardReportByWebsiteForThisMonth(websiteId, AnalyticsReport.enumRangesMonth.This_Month).OrderBy(x => x.ReportDate.Day).DistinctBy(x=>x.ReportDate.Day);
            //var Last_Month_Data = ElasticSearchGlobal.GetDashBoardReportByWebsiteForThisMonth(websiteId, AnalyticsReport.enumRangesMonth.Last_Month).OrderBy(x => x.ReportDate.Day).Distinct();
            //var Two_Months_Ago_Data = ElasticSearchGlobal.GetDashBoardReportByWebsiteForThisMonth(websiteId, AnalyticsReport.enumRangesMonth.Two_Months_Ago).OrderBy(x => x.ReportDate.Day).Distinct();
            List<XY> sessionXy = new List<XY>();
            List<XY> pageView = new List<XY>();
            List<XY> PagesSessions = new List<XY>();
            List<XY> AvgSessionDuration = new List<XY>();
            foreach (var items in This_Month_Data.OrderBy(x => x.ReportDate.Day))
            {
                sessionXy.Add(new XY
                {
                    x = items.ReportDate.Day,
                    y = items.Sessions
                });
                pageView.Add(new XY
                {
                    x = items.ReportDate.Day,
                    y = items.Pageviews
                });
                PagesSessions.Add(new XY
                {
                    x = items.ReportDate.Day,
                    y = items.Unique_User_Count
                });
                AvgSessionDuration.Add(new XY
                {
                    x = items.ReportDate.Day,
                    y = items.Answered_Call_Count
                });
            }
            var AnalyticsWidget = new AnalyticsWidget();
            AnalyticsWidget.widget1 = new Widget1
            {
                Title = "OverView",
                OnlineUsers = NoOfUsers,
                Bigchart = new BigChart
                {
                    Chart = new Chart[] {
                        new Chart
                        {
                            key = "Sessions",
                            Values= sessionXy
                        }
                    }
                },
                Sessions = new SessionspageviewspagesSessionsavgSessionDuration
                { 
                     Title =  "Sessions",
                     value = "23423",
                    Chart = new Chart[] {
                        new Chart
                        {
                            key = "Sessions",
                            Values= sessionXy
                        }
                    }
                },
                Pageviews = new SessionspageviewspagesSessionsavgSessionDuration
                {
                    Title = "Pageviews",
                    value = "67867",
                    Chart = new Chart[] {
                        new Chart
                        {
                            key = "Sessions",
                            Values= pageView
                        }
                    }
                },
                PagesSessions = new SessionspageviewspagesSessionsavgSessionDuration
                {
                    Title = "pagesSessions",
                    value = "7878",
                    Chart = new Chart[] {
                        new Chart
                        {
                            key = "Sessions",
                             Values= PagesSessions
                        }
                    }
                },
                AvgSessionDuration = new SessionspageviewspagesSessionsavgSessionDuration
                {
                    Title = "Pageviews",
                    value = "67867",
                    Chart = new Chart[] {
                        new Chart
                        {
                            key = "Sessions",
                            Values= sessionXy
                        }
                    }
                }
            };
            var Markers = ElasticSearchGlobal.GetCoordsByIpArray(CurrentUserIp);
            AnalyticsWidget.widget2 = new Widget2
            {
                Title = "Current Visitors",
                Map = new Map
                {
                    Center = new Coords
                    {
                        Latitude = Markers.SingleOrDefault().coords.Latitude,
                        Longitude = Markers.SingleOrDefault().coords.Longitude
                    },
                    Zoom = 1,

                    Options = new MapOptions
                    {
                        MinZoom = 1.5,
                        Scrollwheel = false,
                    },
                    Markers = Markers
                }
            };
            AnalyticsWidget.widget3 = new Widget3
            {
                Title = "Top 5 Pages",
                ranges = new countMonths
                {
                     This_Month = "This Month",
                     Last_Month = "Last Month",
                    Two_Months_Ago = "Two Months Ago",
                },
                CurrentRange = enumRangesMonth.This_Month,
                Pages = new Pages
                {
                    Last_Month = new List<PagesPathValue>
                    {
                        new PagesPathValue
                        {
                            Path = "/dashboard",
                            Value = 30
                        },
                        new PagesPathValue
                        {
                            Path = "/Index",
                            Value = 50
                        }
                    },
                    This_Month = new List<PagesPathValue>
                    {
                        new PagesPathValue
                        {
                            Path = "/ramro/hamro",
                            Value = 30
                        },
                        new PagesPathValue
                        {
                            Path = "/Index/YestaiCha",
                            Value = 50
                        }
                    },
                    Two_Months_Ago = new List<PagesPathValue>
                    {
                        new PagesPathValue
                        {
                            Path = "/ramro/hamro",
                            Value = 30
                        },
                        new PagesPathValue
                        {
                            Path = "/Index/YestaiCha",
                            Value = 50
                        }
                    },

                }

            };
            return AnalyticsWidget;
        }
        public class Report
        {
            public string title { get; set; }
            public int onlineUsers { get; set; }
            public XY values { get; set; }

        }
        public class Markers
        {
            [JsonProperty("id")]
            public int Id { get; set; }
            public Coords coords { get; set; }
            public bool show { get; set; }
            public string sessions { get; set; }
        }
        public class Coords
        {
            [JsonProperty("latitude")]
            public double Latitude { get; set; }
            [JsonProperty("longitude")]
            public double Longitude { get; set; }
        }
        public class SessionspageviewspagesSessionsavgSessionDuration
        {
            [JsonProperty("title")]
            public string Title { get; set; }
            public string value { get; set; }
            [JsonProperty("chart")]
            public Chart[] Chart { get; set; }
        }
        public class AnalyticsWidget
        {
            public Widget1 widget1 { get; set; }
            public Widget2 widget2 { get; set; }
            public Widget3 widget3 { get; set; }
        }
        public class Widget1
        {
            [JsonProperty("title")]
            public string Title { get; set; }
            [JsonProperty("onlineUsers")]
            public int OnlineUsers { get; set; }
            [JsonProperty("bigchart")]
            public BigChart Bigchart { get; set; }
            [JsonProperty("sessions")]
            public SessionspageviewspagesSessionsavgSessionDuration Sessions { get; set; }
            [JsonProperty("pageviews")]
            public SessionspageviewspagesSessionsavgSessionDuration Pageviews { get; set; }
            [JsonProperty("pagesSessions")]
            public SessionspageviewspagesSessionsavgSessionDuration PagesSessions { get; set; }
            [JsonProperty("avgSessionDuration")]
            public SessionspageviewspagesSessionsavgSessionDuration AvgSessionDuration { get; set; }

        }
        public class Widget2
        {
            [JsonProperty("title")]
            public string Title { get; set; }
            [JsonProperty("map")]
            public Map Map { get; set; }
        }
        public class Widget3
        {
            [JsonProperty("title")]
            public string Title { get; set; }
            public countMonths ranges { get; set; }
            [JsonProperty("currentRange")]
            public enumRangesMonth CurrentRange { get; set; }
            [JsonProperty("pages")]
            public Pages Pages { get; set; }
        }
        public class Pages
        {
            [JsonProperty("LM")]
            public List<PagesPathValue> Last_Month { get; set; }
            [JsonProperty("TM")]
            public List<PagesPathValue> This_Month { get; set; }
            [JsonProperty("2MA")]
            public List<PagesPathValue> Two_Months_Ago { get; set; }
        }
        public class PagesPathValue
        {
            [JsonProperty("path")]
            public string Path { get; set; }
            [JsonProperty("value")]
            public int Value { get; set; }
        }
        public class BigChart
        {
            [JsonProperty("chart")]
            public Chart[] Chart { get; set; }
        }
        public class Chart
        {
            public string key { get; set; }
            [JsonProperty("values")]
            public List<XY> Values { get; set; }
        }
        public class Map
        {
            [JsonProperty("center")]
            public Coords Center { get; set; }
            [JsonProperty("zoom")]
            public int Zoom { get; set; }
            [JsonProperty("options")]
            public MapOptions Options { get; set; }
            [JsonProperty("markers")]
            public List<Markers> Markers { get; set; }

        }
        public class MapOptions
        {
            [JsonProperty("minZoom")]
            public double MinZoom { get; set; }
            [JsonProperty("scrollwheel")]
            public bool Scrollwheel { get; set; }

        }
        [JsonConverter(typeof(StringEnumConverter))]
        public enum enumRangesMonth
        {
            [EnumMember(Value = "2MA")]
            Two_Months_Ago,
            [EnumMember(Value = "LM")]
            Last_Month,
            [EnumMember(Value = "TM")]
            This_Month
        }
        public class countMonths
        {
            [JsonProperty("2MA")]
            public string Two_Months_Ago { get; set; }
            [JsonProperty("LM")]
            public string Last_Month { get; set; }
            [JsonProperty("TM")]
            public string This_Month { get; set; }
        }

    }
}

