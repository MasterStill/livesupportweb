﻿using System.Threading.Tasks;

namespace SignalRMasterServer.Services
{
    public interface IEmailSender
    {
        Task SendEmailAsync(string email, string subject, string message, string FromAddress = "noreply@livecustomer.chat", string FromAdressTitle = "Live Customer Chat");
    }
}
