﻿using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using SignalRMasterServer.Data;
using SignalRMasterServer.Models;
using SignalRMasterServer.Services;
using Microsoft.Extensions.FileProviders;
using System.IO;
using Microsoft.Extensions.PlatformAbstractions;
using Swashbuckle.Swagger.Model;
using Mindscape.Raygun4Net;
//using ElasticsearchCRUD;
using Microsoft.Net.Http.Headers;

namespace SignalRMasterServer
{
    public class Startup
    {
        //private static string _applicationPath = string.Empty;
        private static string _contentRootPath = string.Empty;
        int sslPort = 0;
        public Startup(IHostingEnvironment env)
        {


            _contentRootPath = env.ContentRootPath;
            var builder = new ConfigurationBuilder()
                .SetBasePath(env.ContentRootPath)
                .AddJsonFile("appsettings.json", optional: true, reloadOnChange: true)
                .AddJsonFile($"appsettings.{env.EnvironmentName}.json", optional: true);

            if (env.IsDevelopment())
            {
                // For more details on using the user secret store see http://go.microsoft.com/fwlink/?LinkID=532709                
                builder.AddUserSecrets("aspnet-SignalRMasterServer-5ad7ac65-5d8d-4a4d-9ead-71b0d4adc3b7");
            }

            builder.AddEnvironmentVariables();
            Configuration = builder.Build();
        }

        public IConfigurationRoot Configuration { get; }




        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddCors(options =>
      {
          options.AddPolicy("CorsPolicy",
              builder => builder.AllowAnyOrigin()
              .AllowAnyMethod()
              .AllowAnyHeader()
              .AllowCredentials());
      });

            //  services.AddCors(options => options.AddPolicy("AllowAll", p => p.AllowAnyOrigin()
            //                                                         .AllowAnyMethod()
            //                                                          .AllowAnyHeader()));   

            services.Configure<IdentityOptions>(options =>
            {
                options.Password.RequireDigit = false;
                options.Password.RequiredLength = 5;
                options.Password.RequireLowercase = false;
                options.Password.RequireUppercase = false;
            });
            services.Configure<ForwardedHeadersOptions>(options =>
            {
                //options.ForwardedHeaders = ForwardedHeaders.XForwardedProto;
                //options.ForwardedHeaders = Microsoft.AspNetCore.HttpOverrides.ForwardedHeaders.All;
                options.ForwardedHeaders = Microsoft.AspNetCore.HttpOverrides.ForwardedHeaders.XForwardedFor;
                //options.ForwardedHeaders = Microsoft.AspNetCore.HttpOverrides.ForwardedHeaders.All;
            });
            services.AddSwaggerGen();
            services.ConfigureSwaggerGen(options =>
            {
                options.SingleApiVersion(new Info
                {
                    Version = "v1",
                    Title = "Live Support API",
                    Description = "Api for LIVE SUPPORT System Rockmandu Developer",
                    TermsOfService = "None"
                });
                //options.IncludeXmlComments(pathToDoc);
                options.DescribeAllEnumsAsStrings();
                //options.OperationFilter<SwaggerIConfig>();
                var basePath = PlatformServices.Default.Application.ApplicationBasePath;
                //options.IncludeXmlComments(basePath + "\\Repairmandu.api.xml");
                //options.OperationFilter<SwaggerIFileConfig>();
            });

            // Add framework services.
            services.AddDbContext<AppDbContext>(options =>
                options.UseSqlServer(Configuration.GetConnectionString("DefaultConnection")
                ));


            //services.AddIdentity<ApplicationUser, ApplicationRole>(config =>
            //{
            //    config.SignIn.RequireConfirmedEmail = true;
            //})

            services.AddIdentity<ApplicationUser, ApplicationRole>
            (config =>
            {
                config.SignIn.RequireConfirmedEmail = true;
            })
            .AddUserStore<ApplicationUserStore>()
            .AddUserManager<ApplicationUserManager>()
            .AddRoleStore<ApplicationRoleStore>()
            .AddRoleManager<ApplicationRoleManager>()
            .AddSignInManager<ApplicationSignInManager>()
            .AddDefaultTokenProviders();

            //services.AddIdentity<ApplicationUser, ApplicationRole>();
            //.AddEntityFrameworkStores<AppDbContext, Guid>()
            //.AddDefaultTokenProviders();            
            //.AddRoleStore<RoleStore<ApplicationRole, AppDbContext, int, ApplicationUserRole, ApplicationRoleClaim>>()
            //.AddUserStore<ApplicationUserStore>();

            services.AddSignalR(options =>
            {
                options.Hubs.EnableDetailedErrors = true;
                options.Hubs.EnableJavaScriptProxies = true;

            });
            services.AddMvc();

            //Hockey App Installation
            services.AddRaygun(Configuration);


            //Amazon SES Configuration
            //services.AddDefaultAWSOptions(Configuration.Aws());
            //services.AddAWSService<IAmazonS3>();
            //services.AddAWSService<IAmazonDynamoDB>();







            // Add application services.
            //services.AddScoped<IUserStore<ApplicationUser>, ApplicationUserStore>();
            services.AddTransient<IEmailSender, AuthMessageSender>();
            services.AddTransient<ISmsSender, AuthMessageSender>();
            //services.AddTransient<ElasticsearchContext>();
            //services.AddTransient<IHubIncomingInvokerContext, IHubIncomingInvokerContext>();
            services.Configure<SmtpOptions>(Configuration);
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env, ILoggerFactory loggerFactory)
        {
            // app.UseCors("AllowAll");            
            app.UseRaygun();
            app.UseCors("CorsPolicy");
            // app.UseCors(builder =>
            //         builder
            //         .AllowAnyHeader()
            //         .AllowAnyMethod()
            //         .AllowCredentials()
            //         .AllowAnyOrigin()
            //         );


            app.UseFileServer();

            var provider = new PhysicalFileProvider(
                Path.Combine(_contentRootPath, "node_modules")
            );
            var _fileServerOptions = new FileServerOptions();
            _fileServerOptions.RequestPath = "/node_modules";
            _fileServerOptions.StaticFileOptions.FileProvider = provider;
            _fileServerOptions.EnableDirectoryBrowsing = true;
            app.UseFileServer(_fileServerOptions);

            loggerFactory.AddConsole(Configuration.GetSection("Logging"));
            loggerFactory.AddDebug();

            //if (env.IsDevelopment())
            //{
                app.UseDeveloperExceptionPage();
                app.UseDatabaseErrorPage();
                app.UseBrowserLink();
            //}
            //else
            //{
            //    app.UseExceptionHandler("/Home/Error");
            //}

            //app.UseStaticFiles();


            //app.UseStaticFiles(new StaticFileOptions
            //{
            //    OnPrepareResponse = ctx =>
            //    {
            //        const int durationInSeconds = 60 * 60;
            //        ctx.Context.Response.Headers[HeaderNames.CacheControl] =
            //            "public,max-age=" + durationInSeconds;
            //    }
            //});
            app.UseStaticFiles();

            // Add external authentication middleware below. To configure them please see http://go.microsoft.com/fwlink/?LinkID=532715
            app.UseSwagger();
            app.UseSwaggerUi();
            app.UseHttpMethodOverride();


            app.UseIdentity();
            app.UseCookieAuthentication();
            app.UseWebSockets();
            app.UseSignalR();
            app.UseFacebookAuthentication(new FacebookOptions()
            {
                AppId = "1373568719321446",
                AppSecret = "f6ae5ff1c7d78d69b81f2632f675caf8"
            });


            //app.UseMicrosoftAccountAuthentication(new MicrosoftAccountOptions()
            //{
            //    ClientId = "694e43e0-0c4c-40c1-b26b-500b15c22f78",
            //    ClientSecret = "Ci6ifbBC9fsoFHQ8t2F9X35"
            //});            
            app.UseTwitterAuthentication(new TwitterOptions()
            {
                ConsumerKey= "6ZS2R0kfUrtrYUbAB0Pk0BkD2",
               ConsumerSecret = "wd1GYuETHGvvSO5JVq0pa3D3ud2Yprm1plk5mOmxD51x71Mynr"
            });            
            app.UseGoogleAuthentication(new GoogleOptions()
            {
                ClientId = "455315278744-k2c8for0mmqaf69b52i5g7cl5u8ucua4.apps.googleusercontent.com",
                ClientSecret = "4OAIYNgFwSSzFQG5oGw07izj",
                //OnAuthenticated = (context) =>
                //{
                //    //This following line is need to retrieve the profile image
                //    //context.Identity.AddClaim(new System.Security.Claims.Claim("urn:google:accesstoken", context.AccessToken, ClaimValueTypes.String, "Google"));

                //    return Task.FromResult(0);
                //}
            });
           // app.Use(async (context, next) =>
           //{
           //    if (!context.Request.Host.Host.Contains("localhost") && !context.Request.Path.Value.Contains("livesupport"))
           //    {
           //     //    if (!context.Request.Host.Host.Contains("ngrok"))
           //     //    {
           //            string protoHeader = context.Request.Headers["X-Forwarded-Proto"];
           //            if (protoHeader == null) protoHeader = "";
           //            if (context.Request.IsHttps || protoHeader.ToLower().Equals("https"))
           //            {
           //                await next();
           //            }
           //            else
           //            {
           //                {
           //                    var httpsUrl = $"https://{context.Request.Host.Host}{context.Request.Path}";
           //                    context.Response.Redirect(httpsUrl);
           //                }
           //            }
           //     //    }
           //     //    else
           //     //    { await next(); }
           //    }
           //    else
           //    {
           //        await next();
           //    }
           //});
            app.UseMvc(routes =>
            {
                routes.MapRoute(
                    name: "default",
                    template: "{controller=Home}/{action=Index}/{id?}");
            });
            // app.UseRewriter(new RewriteOptions().AddRedirectToHttpsPermanent());

        }
    }
}