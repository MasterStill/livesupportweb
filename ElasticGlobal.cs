﻿using System;
using System.Collections.Generic;
using System.IO;
using Microsoft.AspNetCore.Mvc;
using SignalRMasterServer.Controllers;
using SignalRMasterServer.Models;
using SignalRMasterServer.Services;
using static SignalRMasterServer.Controllers.ScriptController;
using Mindscape.Raygun4Net;
using System.Net.Http;
using Newtonsoft.Json;
using System.Text;
using Nest;
using Elasticsearch.Net;
using System.Linq;
//using ElasticsearchCRUD;
using SignalRMasterServer.Data;
using static SignalRMasterServer.Services.AnalyticsReport;

namespace SignalRMasterServer
{
    public static class ElasticSearchGlobal
    {
        private static ElasticClient _elasticClient;
        //private static ElasticsearchContext _elasticContext;
        private const string ConnectionString = "http://localhost:9200/";
        private const string IndexName = "livecustomerchat";
        //private static readonly IElasticsearchMappingResolver _elasticSearchMappingResolver = new ElasticsearchMappingResolver();
        static ElasticSearchGlobal()
        {
            var nodes = new Uri[]
                {
                new Uri(ConnectionString
                //new Uri("http://myserver2:9200"),
                //new Uri("http://myserver3:9200")
                )};

            var pool = new StaticConnectionPool(nodes);
            var settings = new ConnectionSettings(pool);

            settings.DisableDirectStreaming(true);

            _elasticClient = new ElasticClient(settings);
            //_elasticContext = new ElasticsearchContext(ConnectionString, _elasticSearchMappingResolver);

        }
        public static List<Markers> GetCoordsByIpArray(string[] Ips) //= DateTime.Now)
        {
            try
            {
                List<Markers> markers = new List<Markers>();                
                List <Coords> returnCoordinates = new List<Coords>();
                var searchResponse = _elasticClient.Search<IpInfoDb>(s => s
                    .Take(1000)
                    .Index("ipinfodb")
                    .Type("ipinformation")
                    .Query(q =>
                         q.Terms(
                             m => m
                             .Field(x => x.ipaddress)
                             .Terms<string>(Ips))
                             //&& q.MatchPhrase(x => x.Field(m => m.Delflag).Query("false"))
                        )                
                    );
                var reportDate = searchResponse.Documents;
                foreach(var items in reportDate)
                {
                    Markers m = new Markers();
                    m.show = false;
                    m.sessions = items.ipaddress;
                    m.coords = new Coords
                        {
                            Latitude = Convert.ToDouble(items.latitude),
                            Longitude = Convert.ToDouble(items.longitude)
                        };                    
                    markers.Add(m);
                }
                return markers;
            }
            catch (Exception ex)
            {
            }
            return null;
        }
        public static ReportDb GetDashBoardReportByWebsiteForDate(int websiteId,DateTime ReportForDateTime) //= DateTime.Now)
        {
            try
            {              
                var searchResponse = _elasticClient.Search<ReportDb>(s => s
                    .Take(1000)
                    .Index(IndexName)
                    .Type("reportdb")
                .Query(q =>
                    q.Match(m => m.Field(x => x.ReportDate.Date == ReportForDateTime.Date))
                    && q.MatchPhrase(m => m.Field(x => x.WebsiteId).Query(websiteId.ToString()))
                )
            );
                var reportDate = searchResponse.Documents;   
                return reportDate.FirstOrDefault();
            }
            catch (Exception ex)
            {
            }
            return null;
        }
        public static List<ReportDb> GetDashBoardReportByWebsiteForThisMonth(int websiteId, AnalyticsReport.enumRangesMonth enumRanges) //= DateTime.Now)
        {
            DateTime ReportForEndDateTime = DateTime.Now;
            DateTime ReportForStartDateTime = DateTime.Now;
            if (enumRanges == AnalyticsReport.enumRangesMonth.This_Month)
            {
                ReportForEndDateTime = DateTime.Now.AddDays(-30 * 1);
            }
            else if (enumRanges == AnalyticsReport.enumRangesMonth.Last_Month)
            {
                ReportForEndDateTime = DateTime.Now.AddDays(-30 * 2);
                ReportForStartDateTime = DateTime.Now.AddDays(-30 * 1);
            }
            else if (enumRanges == AnalyticsReport.enumRangesMonth.Two_Months_Ago)
            {
                ReportForEndDateTime = DateTime.Now.AddDays(-30 * 3);
                DateTime.Now.AddDays(-30 * 2);
            }
            try
            {
                var searchResponse = _elasticClient.Search<ReportDb>(s => s
                    .Take(1000)
                    .Index(IndexName)
                    .Type("reportdb")
                    .Query(q =>
                            q.DateRange(
                                    range => range
                                        .GreaterThan(ReportForStartDateTime)
                                        .LessThan(ReportForEndDateTime)
                                       )
                        &&
                            q.MatchPhrase(m => m.Field(x => x.WebsiteId).Query(websiteId.ToString()))
                )
            //q.Match(m => m.Field(x => x.ReportDate.Date > ReportForDateTime.Date))
            //&& q.MatchPhrase(m => m.Field(x => x.WebsiteId).Query(websiteId.ToString()))
            );
                var reportDate = searchResponse.Documents;
                return reportDate.ToList();//.FirstOrDefault();
            }
            catch (Exception ex)
            {
            }
            return null;
        }
        public static List<ReportDb> GetDashBoardReportByWebsiteForPreviousWeek(int websiteId,enumRangesWeek enumRangesWeek) //= DateTime.Now)
        {
            DateTime ReportForEndDateTime = DateTime.Now;
            DateTime ReportForStartDateTime = DateTime.Now;
            if (enumRangesWeek == enumRangesWeek.This_Week)
            {
                ReportForEndDateTime = DateTime.Now.AddDays(-7 * 1);
            }else if(enumRangesWeek == enumRangesWeek.Last_Week) {
                ReportForEndDateTime = DateTime.Now.AddDays(-7 * 2);
                ReportForStartDateTime = DateTime.Now.AddDays(-7 * 1);
            }
            else if(enumRangesWeek == enumRangesWeek.Two_Weeks_Ago)
            {
                ReportForEndDateTime = DateTime.Now.AddDays(-7*3);
                DateTime.Now.AddDays(-7 * 2);
            }
            try
            {
                var searchResponse = _elasticClient.Search<ReportDb>(s => s
                    .Index(IndexName)
                    .Type("reportdb")
                    .Query(q =>
                            q.DateRange(
                                    range => range
                                        .GreaterThan(ReportForStartDateTime)
                                        .LessThan(ReportForEndDateTime)
                                       )
                        && 
                            q.MatchPhrase(m => m.Field(x => x.WebsiteId).Query(websiteId.ToString()))
                )                
                //q.Match(m => m.Field(x => x.ReportDate.Date > ReportForDateTime.Date))
                //&& q.MatchPhrase(m => m.Field(x => x.WebsiteId).Query(websiteId.ToString()))
            );
                var reportDate = searchResponse.Documents;
                return reportDate.ToList();//.FirstOrDefault();
            }
            catch (Exception ex)
            {
            }
            return null;
        }
        public static string SaveReport(ReportDb u)
        {
            try
            {
                    var response = _elasticClient.Index(u, i => i                           
                            .Index(IndexName)
                          .Type("reportdb")
                          .Id(u.Id));
                    var testresult = response.Id;
                    return testresult;
            }
            catch (Exception ex)
            {
                Global.Live_Crash_Report(ex);
            }
            return null;
        }
        public static string AddSignalRUser(ElasticSignalRUser u)
        {
            try
            {
                if (GetSignalRUserByEmail(u.Email, u.WebsiteGuid) == null)
                {

                    var response = _elasticClient.Index(u, i => i
                          .Index(IndexName)
                          .Type("signalruser"));
                    //.Id(u.Id));
                    var testresult = response.Id;
                    return testresult;
                }
            }
            catch (Exception ex)
            {
                Global.Live_Crash_Report(ex);
            }
            return null;
        }
        public static string UpdateSignalRUser(SignalRUser e)
        {
            try
            {
                ElasticSignalRUser u = new ElasticSignalRUser(e);
                var response = _elasticClient.Update<SignalRUser, dynamic>(new DocumentPath<SignalRUser>(e.Id), k => k.Index(IndexName).Doc(u));
                Console.WriteLine(response);
            }
            catch (Exception ex)
            {
                Global.Live_Crash_Report(ex);
            }
            return null;
        }
        public static SignalRUser GetSignalRUser(string chatMessageId, string Groupname = null, string WebsiteGuid = null)
        {
            var kk = new SignalRUser();

            if (Groupname == null)
            {
                if (chatMessageId != null)
                {

                    var searchResponse = _elasticClient.Search<SignalRUser>(s => s
                    .Index(IndexName)
                    .Type("signalruser")
                    .Query(q =>
                        q.MatchPhrase(m => m.Field(x => x.ChatMessageId).Query(chatMessageId.ToString())
                       )
                    )
            );
                    var list = searchResponse.Hits.Select(h =>
                                       {
                                           //h.Source.Id = Convert.ToInt32(h.Id);
                                           h.Source.Id = h.Id;
                                           return h.Source;
                                       }).SingleOrDefault();
                    return list;
                }
            }
            else
            {
                //Need to make another function for below lines
                var searchResponse = _elasticClient.Search<SignalRUser>(s => s
                .Index(IndexName)
                .Type("signalruser")
                .Query(q =>
                    q.MatchPhrase(m => m.Field(x => x.GroupId).Query(Groupname))
                    &&
                    q.MatchPhrase(m => m.Field(x => x.WebsiteGuid).Query(WebsiteGuid)

                   )
                )
            );
                var list = searchResponse.Hits.Select(h =>
                                   {
                                       //h.Source.Id = Convert.ToInt32(h.Id);
                                       h.Source.Id = h.Id;
                                       return h.Source;
                                   }).SingleOrDefault();
                return list;
            }
            return null;
        }
        public static List<SignalRUser> GetSignalRUserByWebsite(string websiteName, int page)
        {
            var response = _elasticClient.Search<SignalRUser>(s => s
                    .Take(1000)
                   .Index(IndexName)
                   .Type("signalruser")
                   .Query(q =>
                     //q.Terms(
                         //m => m
                         //.Field(x => x.WebsiteGuid)
                         //.Terms<string>(GetWebsitebyName(websiteName).Guid.ToString())
                          q.MatchPhrase(x => x.Field(m => m.WebsiteGuid).Query(GetWebsitebyName(websiteName).Guid.ToString()))
                    ));
            var list = response.Hits.Select(h =>
                {                    
                    h.Source.Id = h.Id;
                    return h.Source;
                }).ToList();
                return list;
            }       
        public static SignalRUser GetSignalRUserByEmail(string email, string websiteId)
        {
            var kk = new SignalRUser();
            {
                var searchResponse = _elasticClient.Search<SignalRUser>(s => s
                .Index(IndexName)
                .Type("signalruser")
                .Query(q =>
                     q.MatchPhrase(m => m.Field(x => x.Email).Query(email))
                     && q.MatchPhrase(k => k.Field(y => y.Verified).Query(true.ToString()))
                     && q.MatchPhrase(k => k.Field(y => y.WebsiteGuid).Query(websiteId))
                )
            );
                var list = searchResponse.Hits.Select(h =>
                                   {
                                       //h.Source.Id = Convert.ToInt32(h.Id);
                                       h.Source.Id = h.Id;
                                       return h.Source;
                                   }).SingleOrDefault();
                return list;
            }
        }
        public static UserWebsiteDescription GetUserWebsiteDescriptionbyUserIdWebsiteId(int WebsiteId, int UserId)
        {
            var kk = new SignalRUser();
            var searchResponse = _elasticClient.Search<UserWebsiteDescription>(s => s
            .Index(IndexName)
            .Type(enumType.userwebsitedescription.ToString())
            .Query(q =>
                                     q.MatchPhrase(
                     m => m.Field(x => x.WebsiteId).Query(WebsiteId.ToString())) && q.MatchPhrase(
                         k => k.Field(u => u.UserId).Query(UserId.ToString()))
                )
            );
            // var list = searchResponse.Hits.Select(h =>
            //                    {                                       
            //                        h.Source.Id = h.Id;
            //                        return h.Source;
            //                    }).SingleOrDefault();
            var result = searchResponse.Documents.SingleOrDefault();
            return result;
        }
        public static List<UserWebsiteDescription> GetUserWebsiteDescriptionsbyUserId(int UserId)
        {
            var kk = new SignalRUser();
            var searchResponse = _elasticClient.Search<UserWebsiteDescription>(s => s
            .Index(IndexName)
            .Type("userwebsitedescription")
            .Query(q =>
                                     q.MatchPhrase(
                     m => m.Field(x => x.UserId).Query(UserId.ToString()))
                )
            );
            // var list = searchResponse.Hits.Select(h =>
            //                    {                                       
            //                        h.Source.Id = h.Id;
            //                        return h.Source;
            //                    }).SingleOrDefault();
            var result = searchResponse.Documents.ToList();
            return result;
        }
        public static string AddUserWebsiteDescription(UserWebsiteDescription u)
        {
            try
            {
                var response = _elasticClient.Index(u, i => i
                      .Index(IndexName)
                      .Type("userwebsitedescription"));
                return response.Id;
            }
            catch (Exception ex)
            {
                Global.Live_Crash_Report(ex);
            }
            return null;
        }
        public static string AddIpInfoDb(IpInfoDb u)
        {
            try
            {
                var response = _elasticClient.Index(u, i => i
                      .Index("ipinfodb")
                      .Type("ipinformation"));
            }
            catch (Exception ex)
            {
                Global.Live_Crash_Report(ex);
            }
            return null;
        }
        public static IpInfoDb GetIpInfoDb(string IpAddress)
        {
            try
            {
                var searchResponse = _elasticClient.Search<IpInfoDb>(s => s
                    .Index("ipinfodb")
                    .Type("ipinformation")
                    .Query(q =>
                        q.MatchPhrase(m => m.Field(x => x.ipaddress).Query(IpAddress))
                    ));
                var result = searchResponse.Documents;
                if (result.Any()) return result.FirstOrDefault();
            }
            catch (Exception ex)
            {
                Global.Live_Crash_Report(ex);
            }
            return null;
        }
        public static List<ChatEventMessage> GetChatEventMessage(string groupName,int page)
        {
            int count = 10;
            try
            {
                //var SortField = new SortField
                //{
                //    Order = SortOrder.Descending,
                //    Missing = "_last",
                //    UnmappedType = FieldType.Date,
                //    Mode = SortMode.Average
                //};
                var chatmessage = GetChatMessage(groupName);
                if (chatmessage != null)
                {
                    var searchResponse = _elasticClient.Search<ChatEventMessage>(s => s
                    .Sort(ss => ss
                        .Descending(p=>p.DateTime)
                        )
                    .Index(IndexName)
                    .Type("chateventmessage")
                    .Skip(page * count)
                    .Take(10)
                    .Query(q =>
                        q.MatchPhrase(m => m.Field(x => x.ChatMessageId).Query(chatmessage.Id.ToString())
                       )
                    )
                );
                    var list = searchResponse.Hits.Select(h =>
                                    {
                                        //h.Source.Id = Convert.ToInt32(h.Id);
                                        h.Source.Id = h.Id;
                                        return h.Source;
                                    }).ToList();
                    return list;
                }
                return null;
            }
            catch (Exception ex)
            {
                Global.Live_Crash_Report(ex);
            }
            return null;
        }
        public static string AddChatEventMessage(ElasticChatEventMessage e)
        {
            try
            {
                var response = _elasticClient.Index(e, i => i
                      .Index(IndexName)
                      .Type("chateventmessage"));
                return response.Id;
            }
            catch (Exception ex)
            {
                Global.Live_Crash_Report(ex);
            }
            return null;
        }
        public static void UpdateChatEventMessage(ChatEventMessage e)
        {
            try
            {
                var response = _elasticClient.Index(e, i => i
                      .Index(IndexName)
                      .Type("chateventmessage")
                      .Id(e.Id));
                var testresult = response.Id;
            }
            catch (Exception ex)
            {
                Global.Live_Crash_Report(ex);
            }
        }
        public static string AddChatMessage(ElasticChatMessage e)
        {
            try
            {
                var response = _elasticClient.Index(e, i => i
                      .Index(IndexName)
                      .Type("chatmessage")
                );
                var testresult = response.Id;
                return testresult;
            }
            catch (Exception ex)
            {
                Global.Live_Crash_Report(ex);
            }
            return null;
        }
        public static string UpdateChatMessage(ChatMessage e)
        {
            try
            {
                var response = _elasticClient.Index(e, i => i
                      .Index(IndexName)
                      .Type("chatmessage")
                );
                var testresult = response.Id;
            }
            catch (Exception ex)
            {
                Global.Live_Crash_Report(ex);
            }
            return null;
        }
        public static ChatMessage GetChatMessage(string groupId)
        {
            try
            {
                var searchResponse = _elasticClient.Search<ChatMessage>(s => s
                .Index(IndexName)
                .Type("chatmessage")
                .Query(q =>
                    q.MatchPhrase(m => m.Field(x => x.GroupId).Query(groupId)
                   )
                )
            );
                var chatmessage = searchResponse.Hits.Select(h =>
                                   {
                                       h.Source.Id = h.Id;
                                       return h.Source;
                                   }).ToList();
                if (chatmessage.Count > 0)
                    return chatmessage.SingleOrDefault();
                return null;
            }
            catch (Exception ex)
            {
                Global.Live_Crash_Report(ex);
            }
            return null;
        }

        public static void AddUpdateApplicationUser(ApplicationUser u)
        {
            try
            {
                var response = _elasticClient.Index(u, i => i
                      .Index(IndexName)
                      .Type("applicationuser")
                      .Id(u.Id));
                var testresult = response.Id;
            }
            catch (Exception ex)
            {
                Global.Live_Crash_Report(ex);
            }
        }

        public static ApplicationUser GetApplicationUserbyEmail(string email)
        {
            var searchResponse = _elasticClient.Search<ApplicationUser>(s => s
                .Index(IndexName)
                .Type("applicationuser")
                .Query(q =>
                    q.MatchPhrase(m => m.Field(x => x.Email).Query(email)
                   )
                )
            );
            var returnUser = searchResponse.Documents;
            if (returnUser.Count > 0)
                return returnUser.SingleOrDefault();
            return null;
        }
        public static ApplicationUser GetApplicationUserbyId(int id)
        {
            var searchResponse = _elasticClient.Search<ApplicationUser>(s => s
                .Index(IndexName)
                .Type("applicationuser")
                .Query(q =>
                    q.MatchPhrase(m => m.Field(x => x.Id).Query(id.ToString()
                   ))
                )
            );
            var returnUser = searchResponse.Documents;
            if (returnUser.Count > 0)
                return returnUser.SingleOrDefault();
            return null;
        }

        public static List<Advert> GetAdverts()
        {
            var searchResponse = _elasticClient.Search<Advert>(s => s
                .Index(IndexName)
                .Type("advert")
                .Query(q =>
                    q.Match(m => m.Field(x => x.Script).Query("*")
                   )
                )
            );
            var returnUser = searchResponse.Documents.ToList();
            return returnUser;
        }
        public static int GetApplicationUserID(string email)
        {
            return GetApplicationUserbyEmail(email).Id;
        }


        public static void AddUpdateApplicationUserRole(ApplicationUserRole role)
        {
            try
            {
                role.Website = null;
                role.User = null;
                var response = _elasticClient.Index(role, i => i
                      .Index(IndexName)
                      .Type("applicationuserrole"));
            }
            catch (Exception ex)
            {
                Global.Live_Crash_Report(ex);
            }
        }
        public static void AddUpdateChatMarco(ChatMarco marco)
        {
            try
            {
                var response = _elasticClient.Index(marco, i => i
                      .Index(IndexName)
                      .Type("chatmarco")
                      .Id(marco.Id));
            }
            catch (Exception ex)
            {
                Global.Live_Crash_Report(ex);
            }
        }
        public static int GetWebsiteIdByGuid(string WebsiteGuid)
        {
            try
            {
                var response = _elasticClient.Search<Website>(s => s
                   .Index(IndexName)
                   .Type("website")
                   .Query(q =>
                     q.MatchPhrase(
                         m => m.Field(x => x.Guid).Query(WebsiteGuid)
                         )
                        && q.MatchPhrase(x => x.Field(m => m.Delflag).Query("false"))
                    ));
                var result = response.Documents;
                if (result.Any())
                {
                    return result.SingleOrDefault().Id;
                }
            }
            catch (Exception ex)
            {
                Global.Live_Crash_Report(ex);
            }
            return 0;
        }
        public static string GetWebsiteNameById(int WebsiteId)
        {
            try
            {
                var response = _elasticClient.Search<Website>(s => s
                   .Index(IndexName)
                   .Type("website")
                   .Query(q =>
                     q.MatchPhrase(
                         m => m.Field(x => x.Id).Query(WebsiteId.ToString()))
                         && q.MatchPhrase(x => x.Field(m => m.Delflag).Query("false"))
                    ));
                var result = response.Documents;
                if (result.Any())
                {
                    return result.SingleOrDefault().Name;
                }
            }
            catch (Exception ex)
            {
                Global.Live_Crash_Report(ex);
            }
            return null;
        }
        public static WidgetOptions GetWidgetOptionsbyWebsiteId(int WebsiteId)
        {
            try
            {
                var response = _elasticClient.Search<WidgetOptions>(s => s
                   .Index(IndexName)
                   .Type("widgetoptions")
                   .Query(q =>
                     q.MatchPhrase(
                         m => m.Field(x => x.WebsiteId).Query(WebsiteId.ToString()))
                    ));
                var result = response.Documents;
                if (result.Any())
                {
                    return result.SingleOrDefault();
                }
            }
            catch (Exception ex)
            {
                Global.Live_Crash_Report(ex);
            }
            return null;
        }
        public static List<Website> GetWebsiteByUserName(string Username)
        {
            try
            {
                var userId = GetApplicationUserID(Username);
                var response = _elasticClient.Search<ApplicationUserRole>(s => s
                    .Take(1000)
                   .Index(IndexName)
                   .Type("applicationuserrole")
                   .Query(q =>
                     q.MatchPhrase(
                         m => m.Field(x => x.UserId).Query(userId.ToString()))
                    )
                    );
                var result = response.Documents;
                if (result.Any())
                {
                    return GetWebsitebyArrayofWebsiteID(result.Select(x => x.WebsiteId).ToArray().Select(x => x.ToString()).ToArray());
                }
            }
            catch (Exception ex)
            {
                Global.Live_Crash_Report(ex);
            }
            return null;
        }
        public static ApplicationUser AdminOnline(string websiteId)
        {
            try
            {
                //int websiteId = GetWebsiteIdByGuid(WebsiteGuid);
                var response = _elasticClient.Search<ApplicationUserRole>(s => s
                   .Index(IndexName)
                   .Type("applicationuserrole")
                   .Query(q =>
                     q.MatchPhrase(
                         m => m.Field(x => x.WebsiteId).Query(websiteId.ToString()))
                    ));
                var result = response.Documents.ToList();
                if (result.Any())
                {
                    var UserstoCheck = result.Select(x => x.UserId).ToArray().Select(x => x.ToString()).ToArray();
                    var response1 = _elasticClient.Search<ApplicationUser>(s => s
                   .Index(IndexName)
                   .Type("applicationuser")
                   .Query(q =>
                     q.Terms(
                         m => m
                         .Field(x => x.Id)
                         .Terms<string>(UserstoCheck))
                    ));
                    var result1 = response1.Documents.ToList().Where(y => y.Online == true).FirstOrDefault();
                    return result1;
                }
            }
            catch (Exception ex)
            {
                Global.Live_Crash_Report(ex);
            }
            return null;
        }
        public static ChatMarco GetChatMarcoByWebsiteTerm(string WebsiteName, string Term)
        {
            try
            {
                int websiteId = GetWebsitebyName(WebsiteName).Id;
                var response = _elasticClient.Search<ChatMarco>(s => s
                   .Index(IndexName)
                   .Type("chatmarco")
                   .Query(q =>
                     q.MatchPhrase(
                         m => m.Field(x => x.WebsiteId).Query(websiteId.ToString())) && q.MatchPhrase(
                             k => k.Field(u => u.Name).Query(Term))
                         )
                    );
                var result = response.Documents;
                if (result.Any())
                {
                    return result.SingleOrDefault();
                }
            }
            catch (Exception ex)
            {
                Global.Live_Crash_Report(ex);
            }
            return null;
        }
        public static List<Website> GetWebsiteByUserId(int UserId)
        {
            return GetWebsiteByUserName(GetApplicationUserbyId(UserId).Email);
        }
        private static List<Website> GetWebsitebyArrayofWebsiteID(string[] websiteId)
        {
            try
            {
                var response = _elasticClient.Search<Website>(s => s
                    .Take(1000)
                   .Index(IndexName)
                   .Type("website")
                   .Query(q =>
                     q.Terms(
                         m => m
                         .Field(x => x.Id)
                         .Terms<string>(websiteId))
                         && q.MatchPhrase(x => x.Field(m => m.Delflag).Query("false"))
                    )
                    );
                var result = response.Documents;
                if (result.Any())
                {
                    return result.ToList();
                }
            }
            catch (Exception ex)
            {
                Global.Live_Crash_Report(ex);
            }
            return null;
        }
        private static List<Website> GetWebsitebyArrayofUserID(string[] userId)
        {
            try
            {
                var response = _elasticClient.Search<Website>(s => s
                   .Index(IndexName)
                   .Type("website")
                   .Query(q =>
                     q.Terms(
                         m => m
                         .Field(x => x.Id)
                         .Terms<string>(userId))
                         && q.MatchPhrase(x => x.Field(m => m.Delflag).Query("false"))
                    )
                    .Take(50)
                    );
                var result = response.Documents;
                if (result.Any())
                {
                    return result.ToList();
                }
            }
            catch (Exception ex)
            {
                Global.Live_Crash_Report(ex);
            }
            return null;
        }
        public static void AddUpdateWebsite(Website w)
        {
            var response = _elasticClient.Index(w, i => i
                  .Index(IndexName)
                  .Type("website")
                  .Id(w.Id));
        }
        public static void AddUpdateWidgetOptions(WidgetOptions w)
        {
            var response = _elasticClient.Index(w, i => i
                  .Index(IndexName)
                  .Type("widgetoptions")
                  .Id(w.Id));
        }
        public static Website GetWebsitebyName(string Name)
        {
            try
            {
                var response = _elasticClient.Search<Website>(s => s
                  .Index(IndexName)
                  .Type("website")
                  .Query(q =>
                    q.MatchPhrase(
                        m => m.Field(x => x.Name).Query(Name))
                        && q.MatchPhrase(x => x.Field(m => m.Delflag).Query("false"))
                   ));
                var result = response.Documents;
                if (result.Any())
                    return result.SingleOrDefault();
            }
            catch (Exception ex)
            {
                Global.Live_Crash_Report(ex);
            }
            return null;
        }
        public static Website GetWebsitebyGuid(Guid websiteGuid)
        {
            try
            {
                var response = _elasticClient.Search<Website>(s => s
                  .Index(IndexName)
                  .Type("website")
                  .Query(q =>
                    q.MatchPhrase(
                        m => m.Field(x => x.Guid).Query(websiteGuid.ToString()))
                   && q.MatchPhrase(x => x.Field(m => m.Delflag).Query("false"))
                   ));
                var result = response.Documents;
                if (result.Any())
                    return result.SingleOrDefault();
            }
            catch (Exception ex)
            {
                Global.Live_Crash_Report(ex);
            }
            return null;
        }
        public static Website GetWebsitebyWebsiteId(int websiteId)
        {
            try
            {
                var response = _elasticClient.Search<Website>(s => s
                  .Index(IndexName)
                  .Type("website")
                  .Query(q =>
                    q.MatchPhrase(
                        m => m.Field(x => x.Id).Query(websiteId.ToString()))
                   && q.MatchPhrase(x => x.Field(m => m.Delflag).Query("false"))
                   ));
                var result = response.Documents;
                if (result.Any())
                    return result.SingleOrDefault();
            }
            catch (Exception ex)
            {
                Global.Live_Crash_Report(ex);
            }
            return null;
        }
    }
    public enum enumType
    {
        userwebsitedescription
    }
}