﻿using System;
using System.Collections.Generic;
using System.IO;
using Microsoft.AspNetCore.Mvc;
using SignalRMasterServer.Controllers;
using SignalRMasterServer.Models;
using SignalRMasterServer.Services;
using static SignalRMasterServer.Controllers.ScriptController;
using Mindscape.Raygun4Net;
using System.Net.Http;
using Newtonsoft.Json;
using System.Text;
using Nest;
using Elasticsearch.Net;
using System.Linq;
//using ElasticsearchCRUD;
using SignalRMasterServer.Data;

namespace SignalRMasterServer
{
    public static class Global
    {
        public static IEnumerable<TSource> DistinctBy<TSource, TKey>
 (this IEnumerable<TSource> source, Func<TSource, TKey> keySelector)
        {
            HashSet<TKey> seenKeys = new HashSet<TKey>();
            foreach (TSource element in source)
            {
                if (seenKeys.Add(keySelector(element)))
                {
                    yield return element;
                }
            }
        }
        public static int CacheDuration()
        {
            return 5;
        }
        public static void Live_Crash_Report(Exception ex)
        {
            new RockmanduErrorReporting("").ReportError(ex);
            new RaygunClient("8pzeRhLbAqKgNIQN3slglA==").SendInBackground(ex);
        }
        public static string FilterEnumString(string enumValue)
        {
            return enumValue.Replace("_", " ");
        }

        public static string filterStartingNumbers(string id)
        {
            if (Char.IsLetter(id[0])) return id;
            id = filterStartingNumbers(id.Substring(1));
            return id;
        }
        public static ScriptCss obfuscate(ScriptCss s)
        {

            string media_query_style = filterStartingNumbers(Guid.NewGuid().ToString().Replace("-", "_"));
            string background_image_styles = filterStartingNumbers(Guid.NewGuid().ToString().Replace("-", "_"));
            string dynamic_styles = filterStartingNumbers(Guid.NewGuid().ToString().Replace("-", "_"));
            string hangout = filterStartingNumbers(Guid.NewGuid().ToString().Replace("-", "_"));
            string head = filterStartingNumbers(Guid.NewGuid().ToString().Replace("-", "_"));
            string content_container = filterStartingNumbers(Guid.NewGuid().ToString().Replace("-", "_"));
            string content = filterStartingNumbers(Guid.NewGuid().ToString().Replace("-", "_"));
            string overlay = filterStartingNumbers(Guid.NewGuid().ToString().Replace("-", "_"));
            string card_menu = filterStartingNumbers(Guid.NewGuid().ToString().Replace("-", "_"));
            string header = filterStartingNumbers(Guid.NewGuid().ToString().Replace("-", "_"));
            string adminimage = filterStartingNumbers(Guid.NewGuid().ToString().Replace("-", "_"));
            string adminname = filterStartingNumbers(Guid.NewGuid().ToString().Replace("-", "_"));
            string supportstaffinfo = filterStartingNumbers(Guid.NewGuid().ToString().Replace("-", "_"));
            string offline_form = filterStartingNumbers(Guid.NewGuid().ToString().Replace("-", "_"));
            string offline_form_header = filterStartingNumbers(Guid.NewGuid().ToString().Replace("-", "_"));
            string form_header = filterStartingNumbers(Guid.NewGuid().ToString().Replace("-", "_"));
            string form_container = filterStartingNumbers(Guid.NewGuid().ToString().Replace("-", "_"));
            string input_container = filterStartingNumbers(Guid.NewGuid().ToString().Replace("-", "_"));
            string IdName = filterStartingNumbers(Guid.NewGuid().ToString().Replace("-", "_"));
            string IdEmail = filterStartingNumbers(Guid.NewGuid().ToString().Replace("-", "_"));
            string IdPhone_Number = filterStartingNumbers(Guid.NewGuid().ToString().Replace("-", "_"));
            string IdMessage = filterStartingNumbers(Guid.NewGuid().ToString().Replace("-", "_"));
            string offline_form_button = filterStartingNumbers(Guid.NewGuid().ToString().Replace("-", "_"));
            string message_sent = filterStartingNumbers(Guid.NewGuid().ToString().Replace("-", "_"));
            string company_info = filterStartingNumbers(Guid.NewGuid().ToString().Replace("-", "_"));
            string list_chat = filterStartingNumbers(Guid.NewGuid().ToString().Replace("-", "_"));
            string style_bg = filterStartingNumbers(Guid.NewGuid().ToString().Replace("-", "_"));



            s.HTML = s.HTML.Replace("media-query-style", media_query_style);
            s.Script = s.Script.Replace("media-query-style", media_query_style);

            s.HTML = s.HTML.Replace("background_image_styles", background_image_styles);
            s.Script = s.Script.Replace("background_image_styles", background_image_styles);

            s.HTML = s.HTML.Replace("dynamic-styles", dynamic_styles);
            s.Script = s.Script.Replace("dynamic-styles", dynamic_styles);


            s.HTML = s.HTML.Replace("hangout", hangout);
            s.Script = s.Script.Replace("#hangout", "#" + hangout);
            s.Css = s.Css.Replace("#hangout", "#" + hangout);

            s.HTML = s.HTML.Replace("'head'", "'" + head + "'");
            s.Css = s.Css.Replace("#head", "#" + head);
            s.Script = s.Script.Replace("#head", "#" + head);

            //s.Script = s.Script.Replace("'head'", "'"+head+"'");


            s.HTML = s.HTML.Replace("content-container", content_container);
            s.Script = s.Script.Replace("content-container", content_container);
            s.Css = s.Css.Replace("content-container", content_container);

            s.HTML = s.HTML.Replace("content", content);
            s.Script = s.Script.Replace("#content", "#" + content);
            s.Css = s.Css.Replace("#content", "#" + content);


            s.HTML = s.HTML.Replace("overlay", overlay);
            s.Css = s.Css.Replace(".overlay", "." + overlay);
            s.Script = s.Script.Replace(".overlay", "." + overlay);

            // s.HTML = s.HTML.Replace("card menu", card_menu);
            // s.Script = s.Script.Replace("card menu", card_menu);

            s.HTML = s.HTML.Replace("'header'", "'" + header + "'");
            s.Css = s.Css.Replace(".header", "." + header);

            s.HTML = s.HTML.Replace("adminimage", adminimage);
            s.Script = s.Script.Replace("adminimage", adminimage);

            s.HTML = s.HTML.Replace("adminname", adminname);
            s.Script = s.Script.Replace("adminname", adminname);

            s.HTML = s.HTML.Replace("supportstaffinfo", supportstaffinfo);
            s.Script = s.Script.Replace("supportstaffinfo", supportstaffinfo);

            s.HTML = s.HTML.Replace("offline-form-header", offline_form_header);
            //s.Script = s.Script.Replace("offline-form-header", offline_form_header);

            s.HTML = s.HTML.Replace("'offline-form'", "'" + offline_form + "'");
            s.Css = s.Css.Replace(".offline-form", "." + offline_form);
            s.Script = s.Script.Replace(".offline-form", "." + offline_form);

            s.HTML = s.HTML.Replace("'form-header'", "'" + form_header + "'");
            s.Css = s.Css.Replace(".form-header", "." + form_header);

            // s.HTML = s.HTML.Replace("form-container", form_container);
            // s.Script = s.Script.Replace("form-container", form_container);

            s.HTML = s.HTML.Replace("input-container", input_container);
            s.Css = s.Css.Replace(".input-container", "." + input_container);

            // s.HTML = s.HTML.Replace("id='Name'", "id='" + IdName + "'");
            // s.Script = s.Script.Replace("id='Name'", "id='" + IdName + "'");

            // s.HTML = s.HTML.Replace("id='Email'", "id='" + IdEmail + "'");
            // s.Script = s.Script.Replace("id='Email'", "id='" + IdEmail + "'");

            // s.HTML = s.HTML.Replace("id='Phone Number'", "id='" + IdPhone_Number + "'");
            // s.Script = s.Script.Replace("id='Phone Number'", "id='" + IdPhone_Number + "'");

            // s.HTML = s.HTML.Replace("id='Message'","id='"+ IdMessage +"'");
            // s.Script = s.Script.Replace("id='Message'","id='"+ IdMessage +"'");


            s.HTML = s.HTML.Replace("offline-form-button", offline_form_button);
            s.Script = s.Script.Replace("offline-form-button", offline_form_button);

            s.HTML = s.HTML.Replace("message-sent", message_sent);
            s.Script = s.Script.Replace("message-sent", message_sent);
            s.Css = s.Css.Replace("message-sent", message_sent);

            s.HTML = s.HTML.Replace("company-info", company_info);
            s.Css = s.Css.Replace("company-info", company_info);
            s.Script = s.Script.Replace("company-info", company_info);

            s.HTML = s.HTML.Replace("list-chat", list_chat);
            s.Css = s.Css.Replace(".list-chat", "." + list_chat);
            s.Script = s.Script.Replace(".list-chat", "." + list_chat);

            s.HTML = s.HTML.Replace("style-bg", style_bg);
            s.Script = s.Script.Replace("style-bg", style_bg);

            return s;
        }
        public static string SuperUser()
        {
            return "masterstill@gmail.com";
        }
        public static int PremiumUserDate()
        {
            return 30;
        }
        public static string BaseUrl()
        {
         //   return "http://0ff57244.ngrok.io";
            //## Todo : Replaced by customer subdomain
            if (System.IO.Directory.GetCurrentDirectory().ToLower().Contains("projects"))
            {
                //return "http://7909bbf3.ngrok.io";                            
                return "http://localhost:5000";
            }
            return "http://admin.livecustomer.chat";
        }
        public static string NoImage()
        {
            string protocol = "https";
            string domain = "admin.livecustomer.chat";
            string ImageLoacation = "/images/noimage.png";
            return protocol + "://" + domain + ImageLoacation;
        }
        public static string DevelopmentUserName()
        {
            // return "masterstill@gmail.com";
            //return "sishir_pokhrel2009@yahoo.com";
            return "masterstill@gmail.com";
            //return "subin@teamrockmandu.com";
        }
        public static IActionResult AuthorizationFailureRoute()
        {
            return new BadRequestObjectResult(AuthorizationFailureMessage());
        }
        public static string DefaultChatWidgetBackgroundColour()
        {
            return "Green";
        }
        public static int FreeWebsiteLimit()
        {
            return 10;
        }
        public static GenericResult AuthorizationFailureMessage()
        {
            return new GenericResult
            {
                Succeded = false,
                Message = "Not Authorized"
            };
        }
        public static bool DevelopmentMode()
        {
            return false;
        }
        public static string DefaultOfflineText()
        {
            return "Hey Hi we are not online now please leave a message.";
        }
        public static EmailFormat EmailTemplate(enumEmailTemplate template)
        {

            EmailFormat e = new EmailFormat();
            string messageBody = System.IO.File.ReadAllText("EmailTemplates/" + template.ToString() + ".html");
            switch (template)
            {
                case enumEmailTemplate.AssignedToRole:
                    break;
                case enumEmailTemplate.ConfirmationEmail:
                    e.Subject = "Confirm your account";
                    e.Message = messageBody;
                    break;
                case enumEmailTemplate.ForgotyourPassword:
                    e.Subject = "Reset your Password";
                    e.Message = messageBody;
                    break;
                case enumEmailTemplate.InvitationForRoleNewUser:
                    e.Subject = "Invitation by {fullname}";
                    e.Message = messageBody;
                    break;
                case enumEmailTemplate.WelcomeEmail:
                    e.Subject = "Welcome to Live Customer Chat";
                    e.Message = messageBody;
                    break;
                case enumEmailTemplate.RefferalInvitation:
                    break;
                default:
                    Console.WriteLine("No Tempelate Found");
                    break;
            }
            return e;
        }

        public static string WidgetCss(string WidgetName)
        {
            string chatWidgetCss = System.IO.File.ReadAllText("Widgets/" + WidgetName + "/" + "style.css");
            return "<style>" + chatWidgetCss + "</style>";
        }

        public static string WidgetJs(string WidgetName)
        {
            string chatWidgetCss = System.IO.File.ReadAllText("Widgets/" + WidgetName + "/" + "javascript.js");
            return chatWidgetCss;
        }
        public static string WidgetHtml(string WidgetName)
        {
            string chatWidgetCss = System.IO.File.ReadAllText("Widgets/" + WidgetName + "/" + "index.html");
            return chatWidgetCss;
        }
        public static string WidgetIframe()
        {
            string chatWidgetCss = System.IO.File.ReadAllText("Widgets/" + "iframe.html");
            return chatWidgetCss;
        }

        public static ScriptCss getWidgetScriptandMore(string widgetNAme = "widget1")
        {
            ScriptCss s = new ScriptCss();
            s.HTML = WidgetCss(widgetNAme) + WidgetHtml(widgetNAme);
            s.Script = WidgetJs(widgetNAme);
            s.Css = WidgetCss(widgetNAme);
            s.IFrame = WidgetIframe();
            return s;
        }

        public class EmailFormat
        {
            public string Subject { get; set; }
            public string Message { get; set; }
        }
        public enum enumEmailTemplate
        {
            WelcomeEmail, ConfirmationEmail, ForgotyourPassword, RefferalInvitation, InvitationForRoleNewUser, AssignedToRole
        }
        public class RockmanduErrorReporting
        {
            private string _applicationId;
            private static HttpClient client = new HttpClient();
            public RockmanduErrorReporting(string ApplicationId)
            {
                this._applicationId = ApplicationId;
            }
            public void ReportError(Exception ex)
            {
                try
                {
                    Uri uri = new Uri("http://localhost:5000/api/Error");
                    var data = JsonConvert.SerializeObject(ex);
                    client.PostAsync(uri, new StringContent(data, Encoding.UTF8, "application/json"));
                }
                catch
                {
                    Console.Write("Error Occured Report Error");
                }
            }
        }
    }
    
 
}