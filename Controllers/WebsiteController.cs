﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Authorization;
using SignalRMasterServer.Models;
using SignalRMasterServer.Data;
using Microsoft.EntityFrameworkCore;
using Microsoft.AspNetCore.Hosting;
using System.IO;
using SignalRMasterServer.Hubs;
using Microsoft.AspNetCore.SignalR.Infrastructure;
using Newtonsoft.Json;

namespace SignalRMasterServer.Controllers
{
    [Route("api/Website")]
    [Produces("application/json")]
    //[Authorize]
    public class WebsiteController : Controller
    {
        public WebsiteController(AppDbContext _context, IHostingEnvironment _hostingEnviroment, IConnectionManager _connectionManager)
        {
            this._connectionManager = _connectionManager;
            this._hostingEnviroment = _hostingEnviroment;
            this._context = _context;
        }
        [HttpGet("AllMarco/{WebsiteName}")]
        public IActionResult Get(string WebsiteName)
        {
            GenericResult g = new GenericResult();
            int websiteID = ElasticSearchGlobal.GetWebsitebyName(WebsiteName).Id; //_context.Website.Where(x => x.Name == WebsiteName).ToList().SingleOrDefault().Id;
            return new OkObjectResult(_context.ChatMarco.Include(x => x.Website).ToList().Where(x => x.WebsiteId == websiteID));
        }
       
        private readonly AppDbContext _context;
        private IHostingEnvironment _hostingEnviroment;
        private IConnectionManager _connectionManager;
        private int GetUserId()
        {
            string UserName = User.Identity.Name;
            if (UserName == null)
            {
                UserName = (Global.DevelopmentMode()) ? Global.DevelopmentUserName() : null;
            }
            return ElasticSearchGlobal.GetApplicationUserID(UserName);// GetApplicationUserID()
            //return UserID;
        }
        private bool IsInRole(string RoleName, int WebsiteId)
        {
            if (_context.UserRoles.Where(x => x.WebsiteId == WebsiteId).Where(x => x.Role.Name == RoleName).Where(x => x.User.UserName == GetUserName()).SingleOrDefault() == null)
            {
                return false;
            }
            return true;
        }
        //[HttpGet("setusersoffline")]
        //public void SetUserAsOffline()
        //{
        //    //TO DO from signalrAspect
        //    //var onlineUsers = _context.Users.Where(x => x.Online == true);
        //    //foreach(var items in onlineUsers)
        //    //{
        //    //    items.Online = false;
        //    //    _context.Users.Update(items);
        //    //}
        //    //_context.SaveChanges();
        //}

        [HttpGet]
        private IEnumerable<Website> GetAllUserWebsite()
        {
            var UserWebsites = ElasticSearchGlobal.GetWebsiteByUserId(GetUserId());//_context.UserRoles.Include(x => x.Website).Where(x => x.UserId == GetUserId()).Where(x => x.Website.Delflag == false).ToList().Select(x => x.Website).Distinct().ToList();
            return UserWebsites;
        }

        private int TotalNoOfFreeWebsite()
        {
            var UserWebsites = _context.Website.Where(x => x.CreatedBy.Email == GetUserName()).Where(x => x.Delflag == false).ToList();//.Where(x=>x.PaidWebsite == false).Count();
            if (UserWebsites == null) return 0;
            return UserWebsites.Where(x => x.PaidWebsite == false).Count();
        }


        [HttpGet("GetOnlyWebsite")]       
        public string GetOnlyUserWebsite()
        {  
            var UserWebsites = ElasticSearchGlobal.GetWebsiteByUserId(GetUserId());
            return JsonConvert.SerializeObject(UserWebsites.Select(x=> new { x.Id,x.Name }));
        }
        //private int TotalNoOfFreeWebsite()
        //{
        //    var UserWebsites = _context.Website.Where(x => x.CreatedBy.Email == GetUserName()).Where(x => x.Delflag == false).ToList();//.Where(x=>x.PaidWebsite == false).Count();
        //    if (UserWebsites == null) return 0;
        //    return UserWebsites.Where(x => x.PaidWebsite == false).Count();
        //}



        [HttpGet]
        public IActionResult GetWebsites()
        {
            List<WebsiteViewModel> wvm = new List<WebsiteViewModel>();
            var UserWebsites = GetAllUserWebsite();//_context.UserRoles.Include(x => x.Website).Where(x => x.UserId == GetUserId()).Where(x => x.Website.Delflag == false).ToList().Select(x => x.Website).Distinct();
            foreach (var items in UserWebsites)
            {
                WebsiteViewModel w = new WebsiteViewModel
                {
                    UserDescription = new UserDescription(),
                    Operators = new List<Operators>(),
                    Id = items.Id,
                    Alias = items.Alias,
                    Logo = items.Logo,
                    WebsiteType = items.WebstieType.ToString(),
                    Name = items.Name,
                    WidgetId = items.WidgetId,
                    Description = items.Description
                };
                var websiteOperators = _context.UserRoles.Where(x => x.WebsiteId == items.Id).Select(x => x.User).Distinct().ToList(); // Workaround for timebeing
                foreach (var item in websiteOperators)
                {                    
                    Operators operators = new Operators();
                    operators.FullName = (ElasticSearchGlobal.GetUserWebsiteDescriptionbyUserIdWebsiteId(w.Id, item.Id) != null) ? ElasticSearchGlobal.GetUserWebsiteDescriptionbyUserIdWebsiteId(w.Id, GetUserId()).FullName : item.FullName;
                    operators.Avatar= (ElasticSearchGlobal.GetUserWebsiteDescriptionbyUserIdWebsiteId(w.Id, item.Id) != null) ? ElasticSearchGlobal.GetUserWebsiteDescriptionbyUserIdWebsiteId(w.Id, GetUserId()).Avatar : item.Avatar;
                    w.Operators.Add(operators);                    
                }
                wvm.Add(w);
            }
            var userDEscription = ElasticSearchGlobal.GetUserWebsiteDescriptionsbyUserId(GetUserId());//_context.UserWebsiteDescription.Where(x => x.UserId == GetUserId()).Include(x => x.Website).ToList();
            foreach (var items in userDEscription)
            {
                wvm.Where(x => x.Name == ElasticSearchGlobal.GetWebsiteNameById(items.WebsiteId)).SingleOrDefault().UserDescription.Description = items.Description;
                wvm.Where(x => x.Name == ElasticSearchGlobal.GetWebsiteNameById(items.WebsiteId)).SingleOrDefault().UserDescription.Avatar = items.Avatar;
                wvm.Where(x => x.Name == ElasticSearchGlobal.GetWebsiteNameById(items.WebsiteId)).SingleOrDefault().UserDescription.FullName = items.FullName;
                wvm.Where(x => x.Name == ElasticSearchGlobal.GetWebsiteNameById(items.WebsiteId)).SingleOrDefault().UserDescription.FirstName = items.FirstName;
                wvm.Where(x => x.Name == ElasticSearchGlobal.GetWebsiteNameById(items.WebsiteId)).SingleOrDefault().UserDescription.LastName = items.LastName;
            }
            return new OkObjectResult(wvm);
        }        
        [HttpGet("Operators/{WebsiteId}")]
        public IActionResult GetOperators(int WebsiteId)
        {
            var userRoles = _context.UserRoles.Where(x => x.WebsiteId == WebsiteId)
                .Include(x => x.User)
                .Include((x => x.Role))
                .ToList();//.Select(x=> new {x.User.Fullname,x.User.Email,x.User.Id,x.User.Image,x.Role.Name}).ToList());
            List<WebisteOperator> Operators = new List<WebisteOperator>();
            foreach (var items in userRoles)
            {
                if (Operators.SingleOrDefault(x => x.UserId == items.UserId) == null)
                {
                    WebisteOperator o = new WebisteOperator();
                    o.Role = new List<string>();
                    o.UserId = items.UserId;
                    o.FullName = items.User.FullName;
                    o.Email = items.User.Email;
                    o.Avatar = (!string.IsNullOrEmpty(items.User.Image)) ? items.User.Image : Global.NoImage();
                    o.Role.Add(items.Role.Name);
                    o.Accepted = true;
                    Operators.Add(o);
                }
                else
                {
                    Operators.SingleOrDefault(x => x.UserId == items.UserId).Role.Add(items.Role.Name);
                }
            }
            var invitedUsers = _context.EmailInvitation.Where(x => x.WebsiteId == WebsiteId).Where(x => x.Accepted == false).Include(x => x.AddeddBy).ToList();
            //the above stated statement didnt worked ! so to get the Addedby full name the workadound done for now.
            foreach (var users in invitedUsers)
            {
                if (Operators.SingleOrDefault(x => x.Email == users.Email) == null)
                {
                    WebisteOperator o = new WebisteOperator();
                    o.Role = new List<string>();
                    o.Email = users.Email;
                    o.FullName = "Invited User";
                    o.Avatar = Global.NoImage();
                    o.Role.Add(users.Role.Name);
                    o.InvitedDate = users.InvitedDate;
                    o.InvitedBy = _context.Users.Find(users.AddedById).FullName;
                    o.Accepted = false;
                    Operators.Add(o);
                }
                else
                {
                    Operators.SingleOrDefault(x => x.Email == users.Email).Role.Add(users.Role.Name);
                }
            }
            return new OkObjectResult(Operators);
        }
        [HttpDelete("{WebsiteId}")]
        public IActionResult DeleteWebsite(int WebsiteId)
        {
            if (!IsInRole("Admin", WebsiteId))
            {
                return Global.AuthorizationFailureRoute();
            }
            var website = _context.Website.Find(WebsiteId);
            website.Delflag = true;
            _context.SaveChanges();
            ElasticSearchGlobal.AddUpdateWebsite(website);
            return new OkObjectResult(website);
        }
        private string GetUserName()
        {
            if (User.Identity.IsAuthenticated)
                return User.Identity.Name;
            return Global.DevelopmentUserName();
        }
        //private string GetUserId()
        //{
        private string SaveToBase64(string base64Image, string path, string filename)//string websitename)
        {
            if (string.IsNullOrEmpty(base64Image)) return null;
            base64Image = base64Image.Substring(22);
            Directory.CreateDirectory(Path.Combine(_hostingEnviroment.WebRootPath, path));
            var uploads = Path.Combine(_hostingEnviroment.WebRootPath, path, filename);
            var bytes = Convert.FromBase64String(base64Image);
            using (var imageFile = new FileStream(uploads, FileMode.Create))
            {
                imageFile.Write(bytes, 0, bytes.Length);
                imageFile.Flush();
            }
            return "https://admin.livecustomer.chat/" + path + "/" + filename;
        }
        //}
        public class UserDescriptionCreateViewModel
        {
            public int WebsiteId { get; set; }
            public string FirstName { get; set; }
            public string LastName { get; set; }
            public string Description { get; set; }
            public string Avatar { get; set; }
        }
        [HttpPost("UserDescription")]
        public IActionResult PostUserDescription([FromBody]UserDescriptionCreateViewModel u)
        {
            //todo check if user is in the role
            var userDescription = _context.UserWebsiteDescription.Where(x => x.WebsiteId == u.WebsiteId).Where(x => x.UserId == GetUserId()).SingleOrDefault();
            if (userDescription == null)
            {
                //New
                UserWebsiteDescription d = new UserWebsiteDescription
                {
                    FirstName = u.FirstName,
                    LastName = u.LastName,
                    Avatar = SaveToBase64(u.Avatar, "profileImage/website/" + u.WebsiteId, GetUserName() + ".jpg"),
                    Description = u.Description,
                    WebsiteId = u.WebsiteId,
                    UserId = GetUserId()
                };
                _context.UserWebsiteDescription.Add(d);
                _context.SaveChanges();
                ElasticSearchGlobal.AddUserWebsiteDescription(d);
                return new OkObjectResult(d);
            }
            else
            {// Update
                userDescription.Avatar = SaveToBase64(u.Avatar, "profileImage/website/" + u.WebsiteId, GetUserName() + ".jpg");
                userDescription.FirstName = u.FirstName;
                userDescription.LastName = u.LastName;
                userDescription.Description = u.Description;
                _context.Update(userDescription);
                _context.SaveChanges();
                ElasticSearchGlobal.AddUserWebsiteDescription(userDescription);
                return new OkObjectResult(u);
            }
        }
        [HttpPost]
        public IActionResult PostWebsitesite([FromBody]WebsiteCreateViewModel Website)
        {
            Website.Name = Website.Name.ToLower().Replace("https", "").Replace("http", "").Replace("://", "").Replace("www.", "").Replace("/","");
            if (WebsiteExists(Website.Name))
            {
                return new BadRequestObjectResult(new GenericResult
                {
                    Succeded = false,
                    Message = "Website Already Exists"
                });
            }
            if ((Global.FreeWebsiteLimit() < TotalNoOfFreeWebsite()))
            {
                return new BadRequestObjectResult(new GenericResult
                {
                    Succeded = false,
                    Message = "Website Free Quota Used ! Pay for any website and get three more in quotas ! or Use Referral to get more Websites Quota !"
                });
            }


            int createdbyId = GetUserId();
            Website w = new Website
            {
                Alias = Website.Alias,
                Name = Website.Name,
                WidgetId = 1,
                Description = Website.Description,
                WebstieType = WebsiteType.Free,
                Logo = Website.Logo,
                CreatedById = _context.Users.SingleOrDefault(x => x.UserName == GetUserName()).Id
            };
            try
            {
                _context.Website.Add(w);
                _context.SaveChanges();
                ElasticSearchGlobal.AddUpdateWebsite(w);
                ApplicationUserRole Role = new ApplicationUserRole
                {
                    RoleId = 1,
                    UserId = createdbyId,
                    WebsiteId = w.Id
                };
                _context.UserRoles.Add(Role);
                _context.SaveChanges();
                ElasticSearchGlobal.AddUpdateApplicationUserRole(Role);
                try
                {
                    _connectionManager.GetHubContext<CustomerSupportHub>().Clients.Group(_context.Users.Where(x => x.Email == GetUserName()).SingleOrDefault().SignalrGuid.ToString()).onUpdateMe(SignalrUpdate.WebsiteControllerPostWebsitesite.ToString());
                }
                catch (Exception ex)
                {
                    Global.Live_Crash_Report(ex);
                }
                return new OkObjectResult(w);

            }
            catch (Exception ex)
            {
                return new BadRequestObjectResult(new GenericResult
                {
                    Succeded = false,
                    Message = ex.Message

                });
            }
        }

        [HttpPost("{WebsiteId}")]
        public IActionResult PostLogo([FromBody]WebsiteEditViewModel Website, int WebsiteId)
        {
            if (!IsInRole("Admin", WebsiteId))
            {
                return Global.AuthorizationFailureRoute();
            }
            var websiteToModify = _context.Website.SingleOrDefault(x => x.Id == WebsiteId);
            //need to finalize the settings that are to be edited
            websiteToModify.Alias = Website.Alias;
            websiteToModify.Logo = SaveToBase64(Website.Logo, "webistelogo", websiteToModify.Name.Replace(":", "_") + ".jpg");
            websiteToModify.Description = Website.Description;
            _context.Website.Update(websiteToModify);
            _context.SaveChanges();
            ElasticSearchGlobal.AddUpdateWebsite(websiteToModify);
            return new OkObjectResult(Website);

        }

        private bool WebsiteExists(string WebsiteName)
        {
            //Need to workaround here
            var website = ElasticSearchGlobal.GetWebsitebyName(WebsiteName); //_context.Website.Where(x => x.Name.ToLower() == WebsiteName.ToLower()).FirstOrDefault();
            if (website == null) return false;
            return true;
        }

        public class WebsiteCreateViewModel
        {
            public string Name { get; set; }
            public string Alias { get; set; }
            public string Logo { get; set; }
            public string Description { get; set; }
            public WebsiteType WebstieType { get; set; }
        }
        public class WebsiteEditViewModel
        {
            public string Name { get; set; }
            public string Alias { get; set; }
            public string Logo { get; set; }
            public string Description { get; set; }
            //public WebsiteType WebstieType { get; set; }
        }
        public class WebsiteViewModel
        {
            public int Id { get; set; }
            public string Name { get; set; }
            public string Alias { get; set; }
            public string Logo { get; set; }
            public int WidgetId { get; set; }
            public string Description { get; set; }
            public List<Operators> Operators { get; set; }
            public UserDescription UserDescription { get; set; }
            public string WebsiteType { get; set; }
        }
        public class UserDescription
        {
            public string Avatar { get; set; }
            public string FirstName { get; set; }
            public string LastName { get; set; }
            public string FullName { get; set; }
            public string Description { get; set; }
        }
        public class Operators
        {
            public string FullName { get; set; }
            public string Avatar { get; set; }
        }
        public class WebisteOperator
        {
            public int UserId { get; set; }
            public string Avatar { get; set; }
            public string FullName { get; set; }
            public string Email { get; set; }
            public List<string> Role { get; set; }
            public DateTime? InvitedDate { get; set; }
            public bool Accepted { get; set; }
            public string InvitedBy { get; set; }
        }
    }

}
