﻿using System;
using System.Linq;
using Microsoft.AspNetCore.Mvc;
using SignalRMasterServer.Models;
using SignalRMasterServer.Data;
using System.Text.RegularExpressions;

namespace SignalRMasterServer.Controllers
{
    [Route("api/Script")]
    [Produces("application/json")]
    public class ScriptController : Controller
    {
        private readonly AppDbContext _context;
        public ScriptController(AppDbContext _context)
        {
            this._context = _context;
        }
        //[HttpGet]
        //public IActionResult GetScript()
        //{
        //    string content = "window.alert('Hi, Hamro Script Hunch Yetaa!')";
        //    var encryptedContent = Convert.ToBase64String(Encoding.ASCII.GetBytes(content));    
        //    return new OkObjectResult(new ScriptViewModel
        //    {
        //        Script = "<script src='" + $"data:text/javascript;base64,{encryptedContent}" + "'></script>"
        //    });
        //}
        [HttpGet("{WebsiteName}")]
        public IActionResult GetScriptForWebsite(string WebsiteName)
        {
            return new OkObjectResult(GlobalWidget.MainScript(WebsiteName));
        }

        private int GetUserId()
        {
            string UserName = User.Identity.Name;
            if (UserName == null)
            {
                UserName = (Global.DevelopmentMode()) ? Global.DevelopmentUserName() : null;
            }
            int UserID = _context.Users.SingleOrDefault(x => x.UserName == UserName).Id;
            return UserID;
        }
        public class ScriptViewModel
        {
            public string Script { get; set; }
        }
        [HttpGet("script/{websiteId}/script.js")]
        public string GetFinalScript(string websiteId)
        {
            // string content = "var _0xd121=['https://admin.livecustomer.chat','\x2F\x61\x70\x69\x2F\x73\x63\x72\x69\x70\x74\x2F\x67\x65\x74\x63\x6F\x6E\x74\x65\x6E\x74\x73\x2F','\x30\x30\x30\x30\x30\x30\x30\x30\x2D\x30\x30\x30\x30\x2D\x30\x30\x30\x30\x2D\x30\x30\x30\x30\x2D\x30\x30\x30\x30\x30\x30\x30\x30\x30\x30\x30\x38','\x61\x70\x70\x4E\x61\x6D\x65','\x4D\x69\x63\x72\x6F\x73\x6F\x66\x74\x20\x49\x6E\x74\x65\x72\x6E\x65\x74\x20\x45\x78\x70\x6C\x6F\x72\x65\x72','\x4D\x69\x63\x72\x6F\x73\x6F\x66\x74\x2E\x58\x4D\x4C\x48\x54\x54\x50','\x67\x65\x74','\x6F\x70\x65\x6E','\x6F\x6E\x72\x65\x61\x64\x79\x73\x74\x61\x74\x65\x63\x68\x61\x6E\x67\x65','\x73\x65\x6E\x64','\x72\x65\x61\x64\x79\x53\x74\x61\x74\x65','\x72\x65\x73\x70\x6F\x6E\x73\x65\x54\x65\x78\x74','\x70\x61\x72\x73\x65','\x69\x46\x72\x61\x6D\x65','\x61\x70\x70\x65\x6E\x64','\x62\x6F\x64\x79','\x6C\x69\x76\x65\x63\x75\x73\x74\x6F\x6D\x65\x72\x69\x66\x72\x61\x6D\x65','\x67\x65\x74\x45\x6C\x65\x6D\x65\x6E\x74\x42\x79\x49\x64','\x63\x6F\x6E\x74\x65\x6E\x74\x57\x69\x6E\x64\x6F\x77','\x63\x6F\x6E\x74\x65\x6E\x74\x44\x6F\x63\x75\x6D\x65\x6E\x74','\x64\x6F\x63\x75\x6D\x65\x6E\x74','\x68\x74\x6D\x6C','\x3C\x73\x63\x72\x69\x70\x74\x3E','\x73\x63\x72\x69\x70\x74','\x3C\x2F\x73\x63\x72\x69\x70\x74\x3E','\x77\x72\x69\x74\x65','\x63\x6C\x6F\x73\x65','\x72\x65\x61\x64\x79','\x75\x6E\x64\x65\x66\x69\x6E\x65\x64','\x63\x72\x65\x61\x74\x65\x45\x6C\x65\x6D\x65\x6E\x74','\x74\x79\x70\x65','\x74\x65\x78\x74\x2F\x6A\x61\x76\x61\x73\x63\x72\x69\x70\x74','\x73\x72\x63','\x68\x74\x74\x70\x73\x3A\x2F\x2F\x63\x64\x6E\x6A\x73\x2E\x63\x6C\x6F\x75\x64\x66\x6C\x61\x72\x65\x2E\x63\x6F\x6D\x2F\x61\x6A\x61\x78\x2F\x6C\x69\x62\x73\x2F\x6A\x71\x75\x65\x72\x79\x2F\x32\x2E\x31\x2E\x33\x2F\x6A\x71\x75\x65\x72\x79\x2E\x6D\x69\x6E\x2E\x6A\x73','\x6F\x6E\x6C\x6F\x61\x64','\x63\x6F\x6D\x70\x6C\x65\x74\x65','\x61\x70\x70\x65\x6E\x64\x43\x68\x69\x6C\x64','\x68\x65\x61\x64','\x67\x65\x74\x45\x6C\x65\x6D\x65\x6E\x74\x73\x42\x79\x54\x61\x67\x4E\x61\x6D\x65'];var http=createRequestObject();var response;var baseUrl=_0xd121[0];var ApiUrl=_0xd121[1];var websiteId=_0xd121[2];function createRequestObject(){var _0x5a71x7;var _0x5a71x8=navigator[_0xd121[3]];if(_0x5a71x8== _0xd121[4]){_0x5a71x7=  new ActiveXObject(_0xd121[5])}else {_0x5a71x7=  new XMLHttpRequest()};return _0x5a71x7}function sendReq(_0x5a71xa){http[_0xd121[7]](_0xd121[6],_0x5a71xa);http[_0xd121[8]]= handleResponse;http[_0xd121[9]](null)}function handleResponse(){if(http[_0xd121[10]]== 4){ManageCssAndScript(http[_0xd121[11]])}}function ManageCssAndScript(response){var _0x5a71xd=JSON[_0xd121[12]](response);$(_0xd121[15])[_0xd121[14]](_0x5a71xd[_0xd121[13]]);var _0x5a71xe=document[_0xd121[17]](_0xd121[16]),_0x5a71xf=_0x5a71xe[_0xd121[18]]|| _0x5a71xe,_0x5a71x10=_0x5a71xe[_0xd121[19]]|| _0x5a71xf[_0xd121[20]];$(_0x5a71x10)[_0xd121[27]](function(_0x5a71x11){_0x5a71x10[_0xd121[7]]();_0x5a71x10[_0xd121[25]](_0x5a71xd[_0xd121[21]]+ _0xd121[22]+ _0x5a71xd[_0xd121[23]]+ _0xd121[24]);_0x5a71x10[_0xd121[26]]()})}if( typeof jQuery== _0xd121[28]){var r=false;var t;var scriptJquery=document[_0xd121[29]](_0xd121[23]);scriptJquery[_0xd121[30]]= _0xd121[31];scriptJquery[_0xd121[32]]= _0xd121[33];scriptJquery[_0xd121[34]]= scriptJquery[_0xd121[8]]= function(){if(!r&& (!this[_0xd121[10]]|| this[_0xd121[10]]== _0xd121[35])){r= true;sendReq(baseUrl+ ApiUrl+ websiteId)}};document[_0xd121[38]](_0xd121[37])[0][_0xd121[36]](scriptJquery)}else {sendReq(baseUrl+ ApiUrl+ websiteId)}";
            string content = MyWidgetScript();
            //content = _context.Script.FirstOrDefault().WidgetScript.Replace("https://admin.livecustomer.chat",Global.BaseUrl());
            content = content.Replace("WEBSITEID", websiteId);

            return content;
        }

        [HttpGet("GetContents/{WebsiteId}/{Cookie=null}")]
        public ScriptCss Getaa(Guid WebsiteId, string cookie = null)
        {
            //if (cookie == "null") cookie = null;
            var useragent = Request.Headers["User-Agent"].ToString();
            bool isMobileDevice = MobileCheckUtil.fBrowserIsMobile(useragent);
            bool infoProvided = false;
            var website = ElasticSearchGlobal.GetWebsitebyGuid(WebsiteId);
            string websiteName = website.Name.Replace(":", "");
            // if (Request.Cookies["LiveSupportRockmanduCustomerInfo"] != null) // || Request.Cookies["LiveSupportRockmandu" + websiteName + "LoggedInUser"] != null)
            // {
            //     infoProvided = true;
            // }
            ScriptCss s = new ScriptCss();
            if (cookie != null)
                if (ElasticSearchGlobal.GetSignalRUser(null, cookie, website.Guid.ToString()) != null)
                    if (ElasticSearchGlobal.GetSignalRUser(null, cookie, website.Guid.ToString()).InfoProvided) infoProvided = true;
            s = GlobalWidget.ScriptCss(website.Id, cookie, infoProvided, _context, isMobileDevice);
            var ll = s;
            ll.HTML = ll.Css + ll.HTML;
            ll.Script = ll.Script;
            ll.Css = "";
            return ll;
        }

        [HttpGet("GetScriptId/{WebsiteName}")]
        public ScriptGuid GetScriptId(string WebsiteName)
        {
            WebsiteName = WebsiteName.Replace("https://", "").Replace("http://", "").Replace("www.", "");
            var website = ElasticSearchGlobal.GetWebsitebyName(WebsiteName);//_context.Website.Where(x => x.Name == WebsiteName).SingleOrDefault();
            if (website == null)
            {
                return null;
            }
            else
            {
                string websiteGuid = website.Guid.ToString();
                return new ScriptGuid
                {
                    Guid = websiteGuid
                };
            }
        }
        private string MyWidgetScript()
        {
            return @" 
            
            
   if (getParameterByName('auth') != null) {        
        setCookie('LiveSupportRockmandu' + getHostName() + 'LoggedInUser', getParameterByName('auth'), 30);        
        window.location = window.location.href.replace('?auth=' + getCookie('LiveSupportRockmandu' + getHostName() + 'LoggedInUser'), '');
    }         
   var http = createRequestObject();   
   var response;
   var baseUrl = '" + Global.BaseUrl() + @"';
   var ApiUrl ='/api/script/getcontents/';
   var websiteId = 'WEBSITEID';    
   function createRequestObject() {
       var obj;
       var browser = navigator.appName;
       if (browser == 'Microsoft Internet Explorer') {
           obj = new ActiveXObject(""Microsoft.XMLHTTP"");
       } else {
           obj = new XMLHttpRequest();
}
       return obj;
   }
   function sendReq(req)
{
    http.open('get', req);
    http.onreadystatechange = handleResponse;
    http.send(null);
}
//
function handleResponse()
{
    if (http.readyState == 4)
    {
        ManageCssAndScript(http.responseText);
    }
}
    var cookie;
    if (getCookie('LiveSupportRockmandu' + getHostName() + 'LoggedInUser') !== null){        
        cookie  = getCookie('LiveSupportRockmandu' + getHostName() + 'LoggedInUser');
    }else{
        if (getCookie('LiveSupportRockmandu' + getHostName()) !== null){
            cookie =  getCookie('LiveSupportRockmandu' + getHostName())            
        }
        else{
            cookie = null;            
        }
    }
function ManageCssAndScript(response)
{
    var json = JSON.parse(response);        
        $('body').append(json['iFrame']);
    var iframe = document.getElementById('livecustomeriframe'),
    iframeWin = iframe.contentWindow || iframe,
    iframeDoc = iframe.contentDocument || iframeWin.document;
       $(iframeDoc).ready(function(event) {
        iframeDoc.open();
        iframeDoc.write(json['html'] + '\<script>' + json['script'] + '<\/script>');
        iframeDoc.close();
    });
}
    if (typeof jQuery == 'undefined') {        
       var r = false; var t;
var scriptJquery = document.createElement('script');
scriptJquery.type = 'text/javascript';
       scriptJquery.src = 'https://cdnjs.cloudflare.com/ajax/libs/jquery/2.1.3/jquery.min.js';
       scriptJquery.onload = scriptJquery.onreadystatechange = function()
{
    if (!r && (!this.readyState || this.readyState == 'complete'))
    {
        r = true;
        sendReq(baseUrl + ApiUrl + websiteId + '/' + cookie);
    }
};
document.getElementsByTagName('head')[0].appendChild(scriptJquery);
   }
   else{
       sendReq(baseUrl + ApiUrl + websiteId + '/' + cookie);              
   }
 function getCookie(cname) {
        cname = cname.replace(':', '');
        var name = cname + '=';
        var ca = document.cookie.split(';');
        for (var i = 0; i < ca.length; i++) {
            var c = ca[i];
            while (c.charAt(0) === ' ') {
                c = c.substring(1);
            }
            if (c.indexOf(name) === 0) {
                return c.substring(name.length, c.length);
            }
        }
        return null;
    }
    function getHostName() {
        return window.location.host;
    }
   function setCookie(cname, cvalue, exdays) {
        cname = cname.replace(':', '');
        var d = new Date();
        d.setTime(d.getTime() + (exdays * 24 * 60 * 60 * 1000));
        var expires = 'expires=' + d.toUTCString();
        document.cookie = cname + '=' + cvalue + ';' + expires + ';path=/';
    }
   function getParameterByName(name) {
        url = window.location.href;
        name = name.replace(/[\[\]]/g, '\\$&');
        var regex = new RegExp('[?&]' + name + '(=([^&#]*)|&|#|$)'),
            results = regex.exec(url);
        if (!results) return null;
        if (!results[2]) return '';
        return decodeURIComponent(results[2].replace(/\+/g, ' '));
    }   
   
   
   
   
   
   
   
   ";

        }
    }
    public class ScriptGuid
    {
        public string Guid { get; set; }
    }
    public class ScriptCss
    {
        public string Script { get; set; }
        public string HTML { get; set; }
        public string Css { get; set; }
        public string IFrame { get; set; }
    }
    public static class MobileCheckUtil
    {
        static Regex MobileCheck = new Regex(@"android|(android|bb\d+|meego).+mobile|avantgo|bada\/|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od)|iris|kindle|lge |maemo|midp|mmp|mobile.+firefox|netfront|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\/|plucker|pocket|psp|series(4|6)0|symbian|treo|up\.(browser|link)|vodafone|wap|windows (ce|phone)|xda|xiino", RegexOptions.IgnoreCase | RegexOptions.Multiline | RegexOptions.Compiled);
        static Regex MobileVersionCheck = new Regex(@"1207|6310|6590|3gso|4thp|50[1-6]i|770s|802s|a wa|abac|ac(er|oo|s\-)|ai(ko|rn)|al(av|ca|co)|amoi|an(ex|ny|yw)|aptu|ar(ch|go)|as(te|us)|attw|au(di|\-m|r |s )|avan|be(ck|ll|nq)|bi(lb|rd)|bl(ac|az)|br(e|v)w|bumb|bw\-(n|u)|c55\/|capi|ccwa|cdm\-|cell|chtm|cldc|cmd\-|co(mp|nd)|craw|da(it|ll|ng)|dbte|dc\-s|devi|dica|dmob|do(c|p)o|ds(12|\-d)|el(49|ai)|em(l2|ul)|er(ic|k0)|esl8|ez([4-7]0|os|wa|ze)|fetc|fly(\-|_)|g1 u|g560|gene|gf\-5|g\-mo|go(\.w|od)|gr(ad|un)|haie|hcit|hd\-(m|p|t)|hei\-|hi(pt|ta)|hp( i|ip)|hs\-c|ht(c(\-| |_|a|g|p|s|t)|tp)|hu(aw|tc)|i\-(20|go|ma)|i230|iac( |\-|\/)|ibro|idea|ig01|ikom|im1k|inno|ipaq|iris|ja(t|v)a|jbro|jemu|jigs|kddi|keji|kgt( |\/)|klon|kpt |kwc\-|kyo(c|k)|le(no|xi)|lg( g|\/(k|l|u)|50|54|\-[a-w])|libw|lynx|m1\-w|m3ga|m50\/|ma(te|ui|xo)|mc(01|21|ca)|m\-cr|me(rc|ri)|mi(o8|oa|ts)|mmef|mo(01|02|bi|de|do|t(\-| |o|v)|zz)|mt(50|p1|v )|mwbp|mywa|n10[0-2]|n20[2-3]|n30(0|2)|n50(0|2|5)|n7(0(0|1)|10)|ne((c|m)\-|on|tf|wf|wg|wt)|nok(6|i)|nzph|o2im|op(ti|wv)|oran|owg1|p800|pan(a|d|t)|pdxg|pg(13|\-([1-8]|c))|phil|pire|pl(ay|uc)|pn\-2|po(ck|rt|se)|prox|psio|pt\-g|qa\-a|qc(07|12|21|32|60|\-[2-7]|i\-)|qtek|r380|r600|raks|rim9|ro(ve|zo)|s55\/|sa(ge|ma|mm|ms|ny|va)|sc(01|h\-|oo|p\-)|sdk\/|se(c(\-|0|1)|47|mc|nd|ri)|sgh\-|shar|sie(\-|m)|sk\-0|sl(45|id)|sm(al|ar|b3|it|t5)|so(ft|ny)|sp(01|h\-|v\-|v )|sy(01|mb)|t2(18|50)|t6(00|10|18)|ta(gt|lk)|tcl\-|tdg\-|tel(i|m)|tim\-|t\-mo|to(pl|sh)|ts(70|m\-|m3|m5)|tx\-9|up(\.b|g1|si)|utst|v400|v750|veri|vi(rg|te)|vk(40|5[0-3]|\-v)|vm40|voda|vulc|vx(52|53|60|61|70|80|81|83|85|98)|w3c(\-| )|webc|whit|wi(g |nc|nw)|wmlb|wonu|x700|yas\-|your|zeto|zte\-", RegexOptions.IgnoreCase | RegexOptions.Multiline | RegexOptions.Compiled);
        public static bool fBrowserIsMobile(string httpuseragent)
        {
            if (httpuseragent != null)
            {
                if (httpuseragent.Length < 4)
                    return false;

                if (MobileCheck.IsMatch(httpuseragent) || MobileVersionCheck.IsMatch(httpuseragent.Substring(0, 4)))
                    return true;
            }
            return false;
        }
    }
}
