﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Authorization;
using SignalRMasterServer.Models;
using SignalRMasterServer.Data;
using Microsoft.EntityFrameworkCore;
using Newtonsoft.Json;
namespace SignalRMasterServer.Controllers
{
    [Route("api/Widgets")]
    [Produces("application/json")]
    //[Authorize]
    public class WidgetController : Controller
    {
        private readonly AppDbContext _context;

        public WidgetController(AppDbContext _context)
        {
            this._context = _context;
        }
        [HttpGet]
        public IActionResult GetWidgets()
        {
            return new OkObjectResult(_context.Widgets.ToList());
        }
        public class WidgetSelectionViewModel
        {
            public int WebsiteId { get; set; }
            public int WidgetId { get; set; }
        }

        [HttpPost]
        public IActionResult SetWidget([FromBody] WidgetSelectionViewModel widget)
        {
            if (!IsInRole("Admin", widget.WebsiteId))
            {
                return Global.AuthorizationFailureRoute();
            }
            var wWebsite = _context.Website.Where((x => x.Id == widget.WebsiteId)).SingleOrDefault();
            wWebsite.WidgetId = widget.WidgetId;
            _context.Website.Update(wWebsite);
            _context.SaveChanges();
            ElasticSearchGlobal.AddUpdateWebsite(wWebsite);
            return Ok();
        }

        private bool IsInRole(string RoleName, int WebsiteId)
        {
            if (_context.UserRoles.Where(x => x.WebsiteId == WebsiteId).Where(x => x.Role.Name == RoleName).Where(x => x.User.UserName == GetUserName()).SingleOrDefault() == null)
            {
                return false;
            }
            return true;
        }
        private string GetUserName()
        {
            string UserName = User.Identity.Name;
            if (UserName == null)
            {
                UserName = (Global.DevelopmentMode()) ? Global.DevelopmentUserName() : null;
            }
            return UserName;
        }
        [HttpGet("{WebsiteId}/Options")]
        public IActionResult GetWidgetOptions(int websiteId)
        {
            if (!IsInRole("Admin", websiteId))
            {
                return Global.AuthorizationFailureRoute();
            }
            var widgetOptions = _context.WidgetOptions.SingleOrDefault(x => x.WebsiteId == websiteId);
            WidgetOptionsViewModel w = new WidgetOptionsViewModel();
            if (widgetOptions == null)
            {
                return new OkObjectResult(w);
            }
            w.EnableSSL = widgetOptions.EnableSSL;
            w.OfflineText = widgetOptions.OfflineText;
            w.OnlineText = widgetOptions.OnlineText;
            w.TextSize = widgetOptions.TextSize;
            w.TextStyle = widgetOptions.TextStyle;
            w.WidgetColor = widgetOptions.WidgetColor;
            w.AdminOfflineText =(widgetOptions.AdminOfflineText !=null)?widgetOptions.AdminOfflineText.Split(new string[] { "!!@@##$$%%" }, StringSplitOptions.None) : null;
            w.WidgetVisible = widgetOptions.WidgetVisible;
            w.WelcomeMessage = widgetOptions.WelcomeMessage;
            w.WidgetHeight = widgetOptions.WidgetHeight;
            w.WidgetWidth = widgetOptions.WidgetWidth;
            w.ForceWebsiteNameAndDescription = widgetOptions.ForceWebsiteNameAndDescription;
            widgetOptions.WebsiteId = websiteId;
            return new OkObjectResult(w);
        }

        [HttpPost("{WebsiteId}/Options")]
        public IActionResult SaveWidget([FromBody] WidgetOptionsCreateViewModel widgetOptions, int websiteId)
        {
            // WidgetOptions widgetOptinos = new WidgetOptions();            
            WidgetOptions w = new WidgetOptions();            
            if (!IsInRole("Admin", websiteId))
            {
                return Global.AuthorizationFailureRoute();
            }

            var websitetoChange = _context.WidgetOptions.SingleOrDefault(x => x.WebsiteId == websiteId);
            bool newWidget = false;
            if (websitetoChange != null)
            {
                websitetoChange.EnableSSL = widgetOptions.EnableSSL;
                websitetoChange.OfflineText = widgetOptions.OfflineText;
                websitetoChange.OnlineText = widgetOptions.OnlineText;
                websitetoChange.TextSize = widgetOptions.TextSize;
                websitetoChange.TextStyle = widgetOptions.TextStyle;
                websitetoChange.WidgetHeight = widgetOptions.WidgetHeight;
                websitetoChange.WidgetWidth = widgetOptions.WidgetWidth;
                websitetoChange.WidgetColor = widgetOptions.WidgetColor;
                string adminOfflinetexts = string.Empty;
                if (widgetOptions.AdminOfflineText != null)
                {
                    if (widgetOptions.AdminOfflineText.Count() > 0)
                    {
                        foreach(var items in widgetOptions.AdminOfflineText)
                        {

                            if (!string.IsNullOrEmpty(items))
                            {
                                adminOfflinetexts += items;
                                if (items != widgetOptions.AdminOfflineText.ToList().Last())
                                    adminOfflinetexts += "!!@@##$$%%";
                            }                            
                        }
                    }
                }

                websitetoChange.AdminOfflineText = adminOfflinetexts;
                websitetoChange.WidgetVisible = widgetOptions.WidgetVisible;
                websitetoChange.WelcomeMessage = widgetOptions.WelcomeMessage;
                websitetoChange.ForceWebsiteNameAndDescription = widgetOptions.ForceWebsiteNameAndDescription;
                _context.WidgetOptions.Update(websitetoChange);
                ElasticSearchGlobal.AddUpdateWidgetOptions(websitetoChange);
            }
            else
            {
                w.EnableSSL = widgetOptions.EnableSSL;
                w.OfflineText = widgetOptions.OfflineText;
                w.OnlineText = widgetOptions.OnlineText;
                w.TextSize = widgetOptions.TextSize;
                w.WidgetWidth = widgetOptions.WidgetWidth;
                w.WidgetHeight = widgetOptions.WidgetHeight;
                w.TextStyle = widgetOptions.TextStyle;
                w.WidgetColor = widgetOptions.WidgetColor;
                if (widgetOptions.AdminOfflineText != null)
                {
                    if (widgetOptions.AdminOfflineText.Count() > 0)
                    {
                        string adminOfflinetexts = string.Empty;
                        foreach(var items in adminOfflinetexts){
                            adminOfflinetexts += items + "!!@@##$$%%";
                        }
                    }
                }
                w.WidgetVisible = widgetOptions.WidgetVisible;
                w.WelcomeMessage = widgetOptions.WelcomeMessage;
                w.ForceWebsiteNameAndDescription = widgetOptions.ForceWebsiteNameAndDescription;
                newWidget = true;
                w.WebsiteId = websiteId;
                _context.WidgetOptions.Add(w);
            }
            try
            {
                _context.SaveChanges();
                ElasticSearchGlobal.AddUpdateWidgetOptions(w);
            }
            catch (Exception ex)
            {
                Global.Live_Crash_Report(ex);
            }

            if (newWidget) return new OkObjectResult(widgetOptions);
            return new OkObjectResult(websitetoChange);
        }
    }
}