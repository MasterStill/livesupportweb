﻿//using Microsoft.AspNetCore.Mvc;
//using SignalRMasterServer.Models;
//using SignalRMasterServer.Data;
//using Microsoft.AspNetCore.Hosting;
//using Microsoft.AspNetCore.SignalR.Infrastructure;
//using static SignalRMasterServer.Services.AnalyticsReport;

//namespace SignalRMasterServer.Controllers
//{
//    [Route("api/Analytics")]
//    [Produces("application/json")]
//    //[Authorize]
//    public class AnalyticsController : Controller
//    {
//        private readonly AppDbContext _context;
//        private IHostingEnvironment _hostingEnviroment;
//        private IConnectionManager _connectionManager;
//        public AnalyticsController(AppDbContext _context, IHostingEnvironment _hostingEnviroment, IConnectionManager _connectionManager)
//        {
//            this._connectionManager = _connectionManager;
//            this._hostingEnviroment = _hostingEnviroment;
//            this._context = _context;
//        }
//        [HttpGet("{WebsiteId}")]
//        public IActionResult Get(int websiteId)
//        {
//            GenericResult g = new GenericResult();
//            var AnalyticsWidget = GetAnalyticsWidget(websiteId);
//            return new OkObjectResult(AnalyticsWidget);
//        }
//    }
//}