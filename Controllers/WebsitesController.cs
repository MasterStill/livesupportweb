﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Authorization;
using static SignalRMasterServer.Services.PayPalService;
using System.Net.Http;
namespace SignalRMasterServer.Controllers
{
    public class WebsitesController : Controller
    {
        PayPalAccessToken accessToken;
        HttpClient http;

        [Authorize]
        public IActionResult Index()
        {
            return RedirectToAction(nameof(HomeController.Index), "Home");
        }
    }
}