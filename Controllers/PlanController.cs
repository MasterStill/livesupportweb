﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Authorization;
using SignalRMasterServer.Models;
using SignalRMasterServer.Data;
using Microsoft.EntityFrameworkCore;
namespace SignalRMasterServer.Controllers
{
    [Route("api/Plan")]
    [Produces("application/json")]
    //[Authorize]
    public class PLanController : Controller
    {
        private readonly AppDbContext _context;

        public PLanController(AppDbContext _context)
        {
            this._context = _context;
        }

        [HttpGet]
        public IActionResult GetPlans()
        {
            var result = _context.Plans.Include(x => x.PlanDescription).Include(x=>x.PlanPrice).ToList();
            List<PlanViewModel> P = new List<PlanViewModel>();
            foreach (var items in result)
            {
                PlanViewModel p = new PlanViewModel();
                //p.Price = items.Price;
                p.Plan = items.WebsiteType.ToString();
                p.Description = new List<PlanDescriptionViewModel>();
                p.Price = new List<PlanPriceViewModel>();
                foreach (var text in items.PlanDescription.OrderBy(x => x.Id))
                {
                    PlanDescriptionViewModel pdvm = new PlanDescriptionViewModel();
                    pdvm.Text = text.Text;
                    p.Description.Add(pdvm);
                }
                foreach(var plans in items.PlanPrice){
                    PlanPriceViewModel ppvm = new PlanPriceViewModel();
                    ppvm.Price = plans.Price;
                    ppvm.Term = plans.Term.ToString();                    
                    p.Price.Add(ppvm);
                }
                P.Add(p);
            }
            return new OkObjectResult(P);
        }
        public class PlanViewModel
        {
            public string Plan { get; set; }            
            public List<PlanDescriptionViewModel> Description { get; set; }
            public List<PlanPriceViewModel> Price { get; set; }
        }
        public class PlanDescriptionViewModel
        {
            public string Text { get; set; }
        }
        public class PlanPriceViewModel
        {
            public string Term { get; set; }
            public double Price { get; set; }
        }
    }
}