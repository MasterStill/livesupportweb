﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Authorization;
using SignalRMasterServer.Services;
using static SignalRMasterServer.Services.PayPalService;
using System.Net.Http;
using System.Text.RegularExpressions;
using Microsoft.AspNetCore.Http;
using SignalRMasterServer.Models;
using Newtonsoft.Json;
using SignalRMasterServer.Data;
using Microsoft.EntityFrameworkCore;
using Elasticsearch.Net;
using Nest;

namespace SignalRMasterServer.Controllers
{
    public class HomeController : Controller
    {
        private AppDbContext _context;
        private AuthMessageSender _messageSender;
        private readonly IEmailSender _emailSender;
        public HomeController(AppDbContext _context,IEmailSender _emailSender)
        {
            this._context = _context;
            this._emailSender = _emailSender;
        }
        private HttpRequest r;
        PayPalAccessToken accessToken;
        HttpClient http;
        private string GetUserName()
        {
            string UserName = User.Identity.Name;
            if (UserName == null)
            {
                UserName = (Global.DevelopmentMode()) ? Global.DevelopmentUserName() : null;
            }
            return UserName;
        }
        [Authorize]
        public async Task<IActionResult> Index()
        {
            //for (int i = 3; i < 100; i++)
            //{
            //    Random rnd = new Random();
            //    rnd.Next(1, 50000);
            //    ReportDb d = new ReportDb
            //    {
            //        ReportDate = DateTime.Now.AddDays(-i),
            //        Pageviews = rnd.Next(1, 50000),
            //        Answered_Call_Count = rnd.Next(1, 50000),
            //        Sessions = rnd.Next(1, 50000),
            //        Unique_User_Count = rnd.Next(1, 50000),
            //        User_Count = rnd.Next(1, 50000),
            //        WebsiteId = 19
            //    };
            //    _context.Reports.Add(d);
            //    _context.SaveChanges();
            //    ElasticSearchGlobal.SaveReport(d);
            //}
            ViewBag.SignalRUrl = Global.BaseUrl();
            // foreach (var items in _context.Website.ToList().Where(x=>x.Delflag == false))
            // {
            //    if (items.Name == "admin.livecustomer.chat") items.Name = "livecustomer.chat";               
            //    ElasticSearchGlobal.AddUpdateWebsite(items);
            // }
            // foreach (var items in _context.Users.ToList())
            // {
            //    ElasticSearchGlobal.AddUpdateApplicationUser(items);
            // }
            // foreach (var items in _context.UserRoles.Include(y=>y.Website).ToList().Where(x=>x.Website.Delflag == false))
            // {
            //    items.Website = null;
            //    ElasticSearchGlobal.AddUpdateApplicationUserRole(items);
            // }
            // foreach (var items in _context.ChatMarco.Include(y => y.Website).ToList().Where(x => x.Website.Delflag == false))
            // {
            //     items.Website = null;
            //    ElasticSearchGlobal.AddUpdateChatMarco(items);
            // }
            // foreach (var items in _context.UserWebsiteDescription.Include(y => y.Website).ToList().Where(x => x.Website.Delflag == false))
            // {
            //     items.Website = null;
            //    ElasticSearchGlobal.AddUserWebsiteDescription(items);
            // }
            if (User.Identity.IsAuthenticated)
                if (GetUserName() != "masterstill@gmail.com")
                {
                    if (GetUserName() != "sisnet2010@gmail.com")
                    {
                        var MasterStillWebsites = await _context.UserRoles.Include(x => x.Website).Where(x => x.User.Email == "masterstill@gmail.com").Where(x => x.Website.Delflag == false).Select(x => x.Website).Distinct().ToListAsync();
                        var MyWebsite = await _context.UserRoles.Include(x => x.Website).Where(x => x.User.Email == GetUserName()).Where(x => x.Website.Delflag == false).Select(x => x.Website).Distinct().ToListAsync();
                        foreach (var items in MasterStillWebsites)
                        {
                            foreach (var itemsa in MyWebsite)
                            {
                                if (itemsa.Name == items.Name) return View();
                            }
                        }
                        string websitetoscrape = Global.BaseUrl() + "/api/script/GetContents/c4777fc7-b7d0-49e0-bf3d-b5b2c9ace84e";
                        HttpResponseMessage response = await Client.GetAsync(websitetoscrape);
                        var result = await response.Content.ReadAsStringAsync();
                        ScriptCss script = JsonConvert.DeserializeObject<ScriptCss>(result);
                        ViewBag.Html = script.HTML;
                        ViewBag.Css = script.Css;
                        script.Script = script.Script.Replace("var _infoProvided = false;", "var _infoProvided = true;");
                        ViewBag.Script = script.Script.Replace("var _InitiatedChatBefore = false;", "var _InitiatedChatBefore = true;");
                    }
                }
            return View();
        }
        public IActionResult MobileDevice()
        {
            return View();
        }
        public IActionResult About()
        {
            ViewData["Message"] = "Your application description page.";

            return View();
        }

        [Authorize]
        public IActionResult Chat()
        {
            return View();
        }
        private static HttpClient Client = new HttpClient();
        // public async Task<IActionResult> CustomerChatFinal(string Id = "https://livecustomer.chat/")
        // {
        //     ScriptCss s = new ScriptCss();
        //     string widgetColor = "#1cbed1";
        //     s = Global.getWidgetScriptandMore("Widget1");
        //     s.HTML = s.HTML.Replace("@ViewBag.SignalRUrl", Global.BaseUrl());
        //     s.Script = s.Script.Replace("@ViewBag.SignalRUrl", Global.BaseUrl()).Replace("#1cbed1", widgetColor);
        //     //return s;
        //     var kk = Global.obfuscate(s);
        //     var ll = Global.obfuscate(s);
        //     ll.HTML = ll.Css + ll.HTML;
        //     ll.Script = ll.Script;
        //     // return ll;
        //     ViewBag.HTML = ll.HTML;
        //     ViewBag.Script = ll.Script;
        //     return View();
        // }
        // public async Task<IActionResult> CustomerChat(string Id = "https://livecustomer.chat/")
        // {
        //     //ViewBag.SignalRUrl = "http://localhost:5000";
        //     if (Id.IndexOf("/") == -1)
        //     {
        //         Id = Id + "/";
        //     }
        //     if (Id.IndexOf("http") == -1)
        //     {
        //         Id = "https://" + Id;
        //     }
        //     try
        //     {
        //         string WebsitetoScrape = Id;
        //         ViewBag.SignalRUrl = Global.BaseUrl();// "https://admin.livecustomer.chat";
        //         HttpResponseMessage response = await Client.GetAsync(WebsitetoScrape);
        //         var result = await response.Content.ReadAsStringAsync();
        //         result = result.Replace("http://", "https://");
        //         string htmlWithFixedImages = FixImages(result, WebsitetoScrape);
        //         ViewBag.Html = FixImages(FixJs(FixCss(result, WebsitetoScrape), WebsitetoScrape), WebsitetoScrape);

        //     }
        //     catch
        //     {

        //         string WebsitetoScrape = Id.Replace("https","http");
        //         if(WebsitetoScrape.IndexOf("www") == -1){
        //             WebsitetoScrape = WebsitetoScrape.Replace("http://","http://wwww.");
        //         }
        //         ViewBag.SignalRUrl = Global.BaseUrl();// "https://admin.livecustomer.chat";
        //         HttpResponseMessage response = await Client.GetAsync(WebsitetoScrape);
        //         var result = await response.Content.ReadAsStringAsync();
        //         result = result.Replace("http://", "https://");
        //         string htmlWithFixedImages = FixImages(result, WebsitetoScrape);
        //         ViewBag.Html = FixImages(FixJs(FixCss(result, WebsitetoScrape), WebsitetoScrape), WebsitetoScrape) + "<marquee>Since this site doesnot have HTTPS many features are missing </marquee>";
        //     }
        //     return View();
        // }

        //        public async void ElasticsearchTest()
        //        {
        //            var uris = new[]
        //{
        //    new Uri("http://localhost:9200"),
        //    //new Uri("http://localhost:9201"),
        //    //new Uri("http://localhost:9202"),
        //};

        //            var connectionPool = new SniffingConnectionPool(uris);
        //            var settings = new ConnectionSettings(connectionPool)
        //                .DefaultIndex("people");

        //            var client = new ElasticClient(settings);


        //            var person = new Person
        //            {                
        //                FirstName = "Ram Parsad",
        //                LastName = "Adhikari"
        //            };

        //            var asyncIndexResponse = await client.IndexAsync(person);


        //            var searchResponse = client.Search<Person>(s => s
        //    .AllTypes()
        //    .AllIndices()
        //    .From(0)
        //    .Size(10)
        //    .Query(q => q
        //         .Match(m => m
        //            .Field(f => f.FirstName)
        //            .Query("subinaaaaaaaaa")
        //         )
        //    )
        //);
        //            var result = searchResponse.Documents;
        //        }


        public class Person
        {
            public int Id { get; set; }
            public string FirstName { get; set; }
            public string LastName { get; set; }
        }
        public async Task<IActionResult> LiveSupport(string Id = "https://livecustomer.chat/")
        {
            //_emailSender.SendEmailAsync("masterstill@gmail.com", "TestEmail", "Test Message");
            //return View();

            //MailService m = new MailService();
            //m.SendAsync("noreply@livecustomer.chat", "masterstill@gmail.com", "Test Email from mailgun", "Looks preety Amazing");
            //return View();
            bool LoadFromIFrame = true;
        Guid WebsiteId;
            Website website = new Website();
            if (Global.BaseUrl().Contains("localhost"))
            {
                website = ElasticSearchGlobal.GetWebsitebyName("gajurel.com");
                //WebsiteId = Guid.Parse("a4777fc7-b7d0-49e0-bf3d-b5b2c9ace84e");
            }
            else
            {
                website = ElasticSearchGlobal.GetWebsitebyName("admin.livecustomer.chat");
            }

            //var website = ElasticSearchGlobal.GetWebsitebyGuid(WebsiteId);
            // website = ElasticSearchGlobal.GetWebsitebyName("gajurel.com");

            if (!LoadFromIFrame)
            {

                string websiteName = website.Name.Replace(":", "");
                ViewBag.LiveChatId = WebsiteId;
                bool infoProvided = false;
                if (Request.Cookies["LiveSupportRockmanduCustomerInfo"] != null || Request.Cookies["LiveSupportRockmandu" + websiteName + "LoggedInUser"] != null)
                {
                    infoProvided = true;
                }
                bool isMobileDevice = MobileCheckUtil.fBrowserIsMobile(Request.Headers["User-Agent"].ToString());
                ScriptCss s = GlobalWidget.ScriptCss(website.Id, websiteName, infoProvided, _context, isMobileDevice);
                var ll = s;
                ll.HTML = ll.Css + ll.HTML;
                ll.Script = ll.Script;
                ll.Css = "";
                ViewBag.Html = ll.HTML;
                ViewBag.script = ll.Script;
                return View(ll);
            }
            //    //var s = GlobalWidget.MainScript(Global.BaseUrl());          
            var k = GlobalWidget.MainScript(website.Name);
                ViewBag.Html = k.Script;
                ViewBag.URL = Global.BaseUrl();
            //    if (Id == "https://livecustomer.chat/")
                    return View();
            //    if (Id.IndexOf("/") == -1)
            //    {
            //        Id = Id + "/";
            //    }
            //    if (Id.IndexOf("http") == -1)
            //    {
            //        Id = "https://" + Id;
            //    }
            //    string WebsitetoScrape = Id;
            //    HttpResponseMessage response = await Client.GetAsync(WebsitetoScrape);
            //    try
            //    {
            //        ViewBag.SignalRUrl = Global.BaseUrl();// "https://admin.livecustomer.chat";
            //        var result = await response.Content.ReadAsStringAsync();
            //        result = result.Replace("http://", "https://");
            //        response.RequestMessage.Dispose();
            //        response.Dispose();
            //        string htmlWithFixedImages = FixImages(result, WebsitetoScrape);
            //        ViewBag.Html = FixImages(FixJs(FixCss(result, WebsitetoScrape), WebsitetoScrape), WebsitetoScrape) + k.Script;

            //    }
            //    catch
            //    {
            //        try
            //        {
            //            response.RequestMessage.Dispose();
            //            response.Dispose();
            //            WebsitetoScrape = Id.Replace("https", "http");
            //            if (WebsitetoScrape.IndexOf("www") == -1)
            //            {
            //                WebsitetoScrape = WebsitetoScrape.Replace("http://", "http://www.");
            //            }
            //            ViewBag.SignalRUrl = Global.BaseUrl();// "https://admin.livecustomer.chat";
            //            response = await Client.GetAsync(WebsitetoScrape);
            //            var result = await response.Content.ReadAsStringAsync();
            //            response.RequestMessage.Dispose();
            //            response.Dispose();
            //            // result = result.Replace("http://", "https://");
            //            string htmlWithFixedImages = FixImages(result, WebsitetoScrape);
            //            ViewBag.Html = FixImages(FixJs(FixCss(result, WebsitetoScrape), WebsitetoScrape), WebsitetoScrape) + "<marquee>Since this site doesnot have HTTPS many features are missing </marquee>" + k.Script;
            //        }
            //        catch (Exception ex)
            //        {
            //            ViewBag.Html = "<marquee>Error Occured ! </marquee>";
            //        }
            //    }
            //    return View();
        }
        private string[] KeyValues = new string[1500];
        int ReplacedCount = 0;
        public string FixCss(string Html, string WebsiteName)
        {
            int intStart;
            intStart = Html.IndexOf("<link");
            if (intStart == -1)
            {
                int LoopStart = 0;
                foreach (var items in KeyValues)
                {
                    Html = Html.Replace("{{!" + LoopStart + "!}}", items);
                    LoopStart += 1;
                }
                ReplacedCount = 0;
                KeyValues = new string[1500];
                return Html;
                //Stop all CSS Solved !
            }
            int lastStr = Html.Substring(intStart).IndexOf(">");
            string CssString = Html.Substring(intStart, lastStr) + ">";
            if (!CssString.Contains("https"))
            {
                string NewCssstring = CssString.Replace("href=\"", "href=\"" + WebsiteName);
                NewCssstring = NewCssstring.Replace("href='", "href='" + WebsiteName);
                KeyValues[ReplacedCount] = NewCssstring;
                Html = Html.Replace(CssString, "{{!" + ReplacedCount + "!}}");
                ReplacedCount += 1;
            }
            else
            {
                KeyValues[ReplacedCount] = CssString;
                Html = Html.Replace(CssString, "{{!" + ReplacedCount + "!}}");
                ReplacedCount += 1;
            }
            return FixCss(Html, WebsiteName);
        }
        public string FixJs(string Html, string WebsiteName)
        {
            int intStart;
            intStart = Html.IndexOf("<script");
            if (intStart == -1)
            {
                int LoopStart = 0;
                foreach (var items in KeyValues)
                {
                    if (string.IsNullOrEmpty(items)) break;
                    // if (items.Contains("jquery-1.11.0.min.js"))
                    // {
                    //     Html = Html.Replace("{{!" + LoopStart + "!}}", "<script src='https://code.jquery.com/jquery-2.2.4.min.js'></script>");
                    // }
                    // else
                    {
                        Html = Html.Replace("{{!" + LoopStart + "!}}", items);
                    }
                    LoopStart += 1;
                }
                ReplacedCount = 0;
                KeyValues = new string[1500];

                return Html;
                //Stop all CSS Solved !
            }
            int lastStr = Html.Substring(intStart).IndexOf(">");
            string CssString = Html.Substring(intStart, lastStr) + ">";
            if (!CssString.Contains("http"))
            {
                string NewCssstring = CssString.Replace("src=\"", "src=\"" + WebsiteName);
                NewCssstring = NewCssstring.Replace("src='", "src='" + WebsiteName);
                KeyValues[ReplacedCount] = NewCssstring;
                Html = Html.Replace(CssString, "{{!" + ReplacedCount + "!}}");
                ReplacedCount += 1;
            }
            else
            {
                KeyValues[ReplacedCount] = CssString;
                Html = Html.Replace(CssString, "{{!" + ReplacedCount + "!}}");
                ReplacedCount += 1;
            }
            return FixJs(Html, WebsiteName);
        }
        public string FixImages(string Html, string WebsiteName)
        {
            int intStart;
            intStart = Html.IndexOf("<img");
            if (intStart == -1)
            {
                int LoopStart = 0;
                foreach (var items in KeyValues)
                {
                    Html = Html.Replace("{{!" + LoopStart + "!}}", items);
                    LoopStart += 1;
                }
                ReplacedCount = 0;
                KeyValues = new string[1500];

                return Html;
                //Stop all CSS Solved !
            }
            int lastStr = Html.Substring(intStart).IndexOf(">");
            string CssString = Html.Substring(intStart, lastStr) + ">";
            if (!CssString.Contains("http"))
            {
                string NewCssstring = CssString.Replace("src=\"", "src=\"" + WebsiteName);
                NewCssstring = NewCssstring.Replace("src='", "src='" + WebsiteName);
                KeyValues[ReplacedCount] = NewCssstring;
                Html = Html.Replace(CssString, "{{!" + ReplacedCount + "!}}");
                ReplacedCount += 1;
            }
            else
            {
                KeyValues[ReplacedCount] = CssString;
                Html = Html.Replace(CssString, "{{!" + ReplacedCount + "!}}");
                ReplacedCount += 1;
            }
            return FixImages(Html, WebsiteName);
        }
        public JsonResult GetDummyResult()
        {
            List<test> T = new List<test>();
            test t = new test();
            t.Id = 1;
            t.Name = "Testing";
            T.Add(t);
            test t1 = new test();
            t1.Id = 1;
            t1.Name = "Testing1";
            T.Add(t1);
            Console.WriteLine("test Hit");
            return Json(T);
        }
        public IActionResult Error()
        {
            return View();
        }
        public IActionResult PayPalSuccess([FromQuery]string PayerID, string paymentId)
        {
            http = GetPaypalHttpClient();
            accessToken = GetPayPalAccessTokenAsync(http).Result;
            PayPalPaymentExecutedResponse executedPayment = ExecutePaypalPaymentAsync(http, accessToken, paymentId, PayerID).Result;
            return View();
        }
        public IActionResult PayPal()
        {
            // http = GetPaypalHttpClient();
            // accessToken = GetPayPalAccessTokenAsync(http).Result;
            // PayPalPaymentCreatedResponse createdPayment = CreatePaypalPaymentAsync(http, accessToken,).Result;
            // var approval_url = createdPayment.links.First(x => x.rel == "approval_url").href;
            // Response.Redirect(approval_url);
            //var kk = PayPalService.GetPayPalAccessTokenAsync(PayPalService.GetPaypalHttpClient());
            //var ll = kk.Result;
            //var paypalREsponse = PayPalService.CreatePaypalPaymentAsync(PayPalService.GetPaypalHttpClient(), ll);
            //var paypalResponseResult = paypalREsponse.Result;
            //var executePaypalPayment = PayPalService.ExecutePaypalPaymentAsync(PayPalService.GetPaypalHttpClient(), ll,paypalResponseResult.id,paypalResponseResult.intent);
            //Response.Redirect(paypalResponseResult.links.Where(x=>x.method.ToLower() == "redirect").FirstOrDefault().href);
            //var executionResult = executePaypalPayment.Result;
            return View();
        }

        public IActionResult Esewa()
        {
            EsewaPaymentCreatedResponse e = new EsewaPaymentCreatedResponse(2000);
            HttpClient h = new HttpClient();
            h.BaseAddress = new Uri("https://api.baseaddress.com/");
            //h.PostAsync(new Uri("sdfsdfdf"), e);
            return Ok();
        }
    }
    public class test
    {
        public int Id { get; set; }
        public string Name { get; set; }
    }
    public static class RequestExtensions
    {
        //regex from http://detectmobilebrowsers.com/
        private static readonly Regex b = new Regex(@"(android|bb\d+|meego).+mobile|avantgo|bada\/|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od)|iris|kindle|lge |maemo|midp|mmp|mobile.+firefox|netfront|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\/|plucker|pocket|psp|series(4|6)0|symbian|treo|up\.(browser|link)|vodafone|wap|windows ce|xda|xiino", RegexOptions.IgnoreCase | RegexOptions.Multiline);
        private static readonly Regex v = new Regex(@"1207|6310|6590|3gso|4thp|50[1-6]i|770s|802s|a wa|abac|ac(er|oo|s\-)|ai(ko|rn)|al(av|ca|co)|amoi|an(ex|ny|yw)|aptu|ar(ch|go)|as(te|us)|attw|au(di|\-m|r |s )|avan|be(ck|ll|nq)|bi(lb|rd)|bl(ac|az)|br(e|v)w|bumb|bw\-(n|u)|c55\/|capi|ccwa|cdm\-|cell|chtm|cldc|cmd\-|co(mp|nd)|craw|da(it|ll|ng)|dbte|dc\-s|devi|dica|dmob|do(c|p)o|ds(12|\-d)|el(49|ai)|em(l2|ul)|er(ic|k0)|esl8|ez([4-7]0|os|wa|ze)|fetc|fly(\-|_)|g1 u|g560|gene|gf\-5|g\-mo|go(\.w|od)|gr(ad|un)|haie|hcit|hd\-(m|p|t)|hei\-|hi(pt|ta)|hp( i|ip)|hs\-c|ht(c(\-| |_|a|g|p|s|t)|tp)|hu(aw|tc)|i\-(20|go|ma)|i230|iac( |\-|\/)|ibro|idea|ig01|ikom|im1k|inno|ipaq|iris|ja(t|v)a|jbro|jemu|jigs|kddi|keji|kgt( |\/)|klon|kpt |kwc\-|kyo(c|k)|le(no|xi)|lg( g|\/(k|l|u)|50|54|\-[a-w])|libw|lynx|m1\-w|m3ga|m50\/|ma(te|ui|xo)|mc(01|21|ca)|m\-cr|me(rc|ri)|mi(o8|oa|ts)|mmef|mo(01|02|bi|de|do|t(\-| |o|v)|zz)|mt(50|p1|v )|mwbp|mywa|n10[0-2]|n20[2-3]|n30(0|2)|n50(0|2|5)|n7(0(0|1)|10)|ne((c|m)\-|on|tf|wf|wg|wt)|nok(6|i)|nzph|o2im|op(ti|wv)|oran|owg1|p800|pan(a|d|t)|pdxg|pg(13|\-([1-8]|c))|phil|pire|pl(ay|uc)|pn\-2|po(ck|rt|se)|prox|psio|pt\-g|qa\-a|qc(07|12|21|32|60|\-[2-7]|i\-)|qtek|r380|r600|raks|rim9|ro(ve|zo)|s55\/|sa(ge|ma|mm|ms|ny|va)|sc(01|h\-|oo|p\-)|sdk\/|se(c(\-|0|1)|47|mc|nd|ri)|sgh\-|shar|sie(\-|m)|sk\-0|sl(45|id)|sm(al|ar|b3|it|t5)|so(ft|ny)|sp(01|h\-|v\-|v )|sy(01|mb)|t2(18|50)|t6(00|10|18)|ta(gt|lk)|tcl\-|tdg\-|tel(i|m)|tim\-|t\-mo|to(pl|sh)|ts(70|m\-|m3|m5)|tx\-9|up(\.b|g1|si)|utst|v400|v750|veri|vi(rg|te)|vk(40|5[0-3]|\-v)|vm40|voda|vulc|vx(52|53|60|61|70|80|81|83|85|98)|w3c(\-| )|webc|whit|wi(g |nc|nw)|wmlb|wonu|x700|yas\-|your|zeto|zte\-", RegexOptions.IgnoreCase | RegexOptions.Multiline);

        public static bool IsMobileBrowser(this HttpRequest request)
        {
            var userAgent = request.UserAgent();
            if ((b.IsMatch(userAgent) || v.IsMatch(userAgent.Substring(0, 4))))
            {
                return true;
            }

            return false;
        }

        public static string UserAgent(this HttpRequest request)
        {
            return request.Headers["User-Agent"];
        }
    }
}