﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Authorization;
using SignalRMasterServer.Models;
using SignalRMasterServer.Data;

namespace SignalRMasterServer.Controllers
{
    [Route("api/Marco")]
    [Produces("application/json")]

    public class MarcoController : Controller
    {
        private readonly AppDbContext _context;
        public MarcoController(AppDbContext _context)
        {
            this._context = _context;
        }
        //[HttpGet("MarcoGroup/Website/{WebsiteId}")]
        //public IActionResult GetMacroGroup()
        //{
        //    GenericResult g = new GenericResult();
        //    return new OkObjectResult(_context.MacroGroup.ToList());
        //}
        //[HttpPost("MarcoGroup")]
        //public IActionResult PostMacroGroup([FromBody]MacroGroupCreateViewModel Marco)
        //{
        //    return new OkObjectResult(new GenericResult());
        //}
        [HttpGet("Website/{WebsiteId}")]
        public IActionResult Get(int WebsiteId)
        {
            if (!IsInRole("Admin", WebsiteId))
            {
                return Global.AuthorizationFailureRoute();
            }
            GenericResult g = new GenericResult();
            return new OkObjectResult(_context.ChatMarco.ToList().Where(x => x.WebsiteId == WebsiteId));
        }
        [HttpPost]
        public IActionResult PostNewCharMarco([FromBody]MarcoCreateViewModel Marco)
        {
            if (!IsInRole("Admin", Marco.WebsiteId))
            {
                return Global.AuthorizationFailureRoute();
            }
            ChatMarco m = new ChatMarco
            {
                //DepartmentId = Marco.DepartmentId,
                KeyBoardShortcut = Marco.KeyBoardShortcut,
                Name = Marco.Name,
                TextorURL = Marco.TextorURL,
                MarcoType = Marco.MarcoType,
                WebsiteId = Marco.WebsiteId,
            };
            _context.ChatMarco.Add(m);
            _context.SaveChanges();
            return new OkObjectResult(m);
        }


        [HttpDelete("{Id}")]
        public IActionResult DeleteMarco(int Id)
        {
            var dbMarco = _context.ChatMarco.Find(Id);
            if (dbMarco != null)
            {
                if (!IsInRole("Admin", dbMarco.WebsiteId))
                {
                    return Global.AuthorizationFailureRoute();
                }
                _context.ChatMarco.Remove(dbMarco);
                _context.SaveChanges();
            }
            return new OkObjectResult("{}");
        }

        [HttpPost("{Id}")]
        public IActionResult EditMarco([FromBody]MarcoEditViewModel Marco, int Id)
        {
            var dbMarco = _context.ChatMarco.Find(Id);
            if (dbMarco != null)
            {
                if (!IsInRole("Admin", dbMarco.WebsiteId))
                {
                    return Global.AuthorizationFailureRoute();
                }
                dbMarco.KeyBoardShortcut = Marco.KeyBoardShortcut;
                dbMarco.Name = Marco.Name;
                dbMarco.TextorURL = Marco.TextorURL;
                dbMarco.MarcoType = Marco.MarcoType;
            }
            _context.ChatMarco.Update(dbMarco);
            _context.SaveChanges();
            return new OkObjectResult(dbMarco);
        }
        //[HttpGet("PushMarco/Website/{WebsiteId}")]
        //public IActionResult GetPushMarco()
        //{
        //    GenericResult g = new GenericResult();
        //    return new OkObjectResult(_context.PushMarco.ToList());
        //}

        //[HttpPost("PushMarco")]
        //public IActionResult PostPushMarco(PushMarcoCreateViewModel Marco)
        //{

        //    return new OkObjectResult(new GenericResult());
        //}
        private bool IsInRole(string RoleName, int WebsiteId)
        {
            if (_context.UserRoles.Where(x => x.WebsiteId == WebsiteId).Where(x => x.Role.Name == RoleName).Where(x => x.User.UserName == GetUserName()).SingleOrDefault() == null)
            {
                return false;
            }
            return true;
        }

        private string GetUserName()
        {
            string UserName = User.Identity.Name;
            if (UserName == null)
            {
                UserName = (Global.DevelopmentMode()) ? Global.DevelopmentUserName() : null;
            }
            return UserName;
        }
    }

    //public class DepartmentCreateViewModel
    //{
    //    //public int CultureId { get; set; }
    //    public int WebsiteId { get; set; }
    //    public string Name { get; set; }
    //}
    public class MarcoCreateViewModel
    {
        public string Name { get; set; }
        public MarcoType MarcoType { get; set; }
        public string TextorURL { get; set; }
        public string KeyBoardShortcut { get; set; }
        //public int DepartmentId { get; set; }        
        public int WebsiteId { get; set; }
    }

    public class MarcoEditViewModel
    {
        public string Name { get; set; }
        public MarcoType MarcoType { get; set; }
        public string TextorURL { get; set; }
        public string KeyBoardShortcut { get; set; }
        //public int DepartmentId { get; set; }                
    }
    //public class PushMarcoCreateViewModel
    //{
    //    //public int CultureId { get; set; }
    //    public int WebsiteId { get; set; }
    //    public string Name { get; set; }
    //    public string Url { get; set; }
    //}

    //public class ChatMarcoCreateViewModel
    //{
    //    //public int CultureId { get; set; }
    //    public int WebsiteId { get; set; }
    //    public int DepartmentId { get; set; }
    //    public string Title { get; set; }        
    //    public string Marco { get; set; }
    //}

}
