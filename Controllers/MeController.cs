﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Authorization;
using SignalRMasterServer.Models;
using SignalRMasterServer.Data;
using System.Text;
using System.IO;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.SignalR.Infrastructure;
using SignalRMasterServer.Hubs;
using Microsoft.Extensions.Caching.Memory;
using Microsoft.EntityFrameworkCore;

namespace SignalRMasterServer.Controllers
{
    [Route("api/Me")]
    [Produces("application/json")]
    public class MeController : Controller
    {
        private AppDbContext _context;
        private IHostingEnvironment _hostingEnviroment;
        private IMemoryCache _memoryCache;
        private IConnectionManager _connectionManager;
        public MeController(IMemoryCache _memoryCache,AppDbContext _context, IHostingEnvironment _hostingEnviroment, IConnectionManager _connectionManager)
        {
            this._memoryCache = _memoryCache;
            this._connectionManager = _connectionManager;
            this._hostingEnviroment = _hostingEnviroment;
            this._context = _context;
        }
        private string GetUserName()
        {
            string UserName = User.Identity.Name;
            if (UserName == null)
            {
                UserName = (Global.DevelopmentMode()) ? Global.DevelopmentUserName() : null;
            }
            return UserName;
        }
        [HttpGet("GenerateError")]
        public IActionResult GenerateError()
        {
            try
            {
                var kk = _context.Users.Where(x => x.Email == "sdfsdfSFsdf").SingleOrDefault().AdminGuid;
                return new OkObjectResult(new Exception());
            }
            catch (Exception ex)
            {
                return new OkObjectResult(ex);
            }

        }


        [HttpGet]
        public IActionResult Me()
        {

            // try
            // {
            //    var kk= _context.Users.Where(x => x.Email == "sdfsdfSFsdf").SingleOrDefault().AdminGuid;
            // }
            // catch(Exception ex)
            // {
            //     var kk = Json(ex);
            // }
            string currentHost = Global.BaseUrl();//HttpContext.Request.Host.Host;
            var user = ElasticSearchGlobal.GetApplicationUserbyEmail(GetUserName());// _context.Users.Where(x => x.UserName == GetUserName()).Select(x => new { x.FullName, x.Email, x.Image, x.FirstName, x.LastName, x.Description, x.RegisteredDate }).SingleOrDefault();

            MeViewModel m = new MeViewModel
            {
                LogoffUrl = currentHost + "/account/logoff/",
                ManageURL = currentHost + "/Manage/",
                FullName = user.FullName,
                Avatar = (!string.IsNullOrEmpty(user.Image)) ? user.Image : Global.NoImage(),
                FirstName = user.FirstName,
                LastName = user.LastName,
                Description = user.Description,
                Advert = (user.RegisteredDate.AddDays(Global.PremiumUserDate()) < DateTime.Now) ? LoadAdvert() : "",
//                WidgetScript = GlobalWidget.MainScript("admin.livecustomer.chat").Script             
            };
            if (GetUserName() != "masterstill@gmail.com")
            {
               if (GetUserName() != "sisnet2010@gmail.com")
               {
                   var MasterStillWebsites =  ElasticSearchGlobal.GetWebsiteByUserName("masterstill@gmail.com"); //_context.UserRoles.Include(x => x.Website).Where(x => x.User.Email == "masterstill@gmail.com").Where(x => x.Website.Delflag == false).Select(x => x.Website).Distinct().ToList();
                   var MyWebsite =  ElasticSearchGlobal.GetWebsiteByUserName(GetUserName()); //_context.UserRoles.Include(x => x.Website).Where(x => x.User.Email == GetUserName()).Where(x => x.Website.Delflag == false).Select(x => x.Website).Distinct().ToList();
                   foreach (var items in MasterStillWebsites)
                   {
                       foreach (var itemsa in MyWebsite)
                       {
                           if (itemsa.Name == items.Name) return View();
                       }
                        //string strictOnly1UsebaseUrl = Global.BaseUrl().Replace("http://","").Replace("https://","");
                        m.WidgetScript = GlobalWidget.MainScript("livecustomer.chat").Script;
                        //ipt(strictOnly1UsebaseUrl).Script;
                   }

               }
            }
            return new OkObjectResult(m);
        }
        private string LoadAdvert()
        {
            //##to do load  from cache

            return "";// return _context.Advert.FirstOrDefault().Script;
        }
        public class WhatViewModel
        {
            public string What { get; set; }
            public string Value { get; set; }
        }

        public class MeViewModel
        {
            public string FirstName { get; set; }
            public string LastName { get; set; }
            public string FullName { get; set; }
            public string Email { get; set; }
            public string Image { get; set; }
            public string Avatar { get; set; }
            public string LogoffUrl { get; set; }
            public string Advert { get; set; }
            public string ManageURL { get; set; }
            public string Description { get; set; }
            public string WidgetScript { get; set; }

        }
        [HttpPost]
        public IActionResult EditUser([FromBody]List<WhatViewModel> whatViewModel)
        {
            var userTomodify = _context.Users.Where(x => x.UserName == GetUserName()).SingleOrDefault();
            if (userTomodify != null)
            {
                foreach (var items in whatViewModel)
                {
                    if (items.What.ToLower() == "firstname")
                        userTomodify.FirstName = items.Value;
                    if (items.What.ToLower() == "lastname")
                        userTomodify.LastName = items.Value;
                    if (items.What.ToLower() == "description")
                        userTomodify.Description = items.Value;
                    if (items.What.ToLower() == "avatar")
                    {
                        userTomodify.Image = SaveToBase64(items.Value, userTomodify.Email);
                    }
                }
                _context.Users.Update(userTomodify);
                _context.SaveChanges();
                _memoryCache.Set(userTomodify.Email, userTomodify, new MemoryCacheEntryOptions()
                .SetAbsoluteExpiration(TimeSpan.FromMinutes(Global.CacheDuration())));
                //_context.Dispose();
                try
                {
                    _connectionManager.GetHubContext<CustomerSupportHub>().Clients.Group(userTomodify.SignalrGuid.ToString()).onUpdateMe(SignalrUpdate.MeControllerEditUser.ToString());
                }
                catch (Exception ex)
                {
                    Global.Live_Crash_Report(ex);
                }
                //need to convert to jpg file and save;                    
                    
                return new OkObjectResult(userTomodify);
                //  _connectionManager.GetHubContext<CustomerSupportHub>().Clients.
                //_connectionManager.GetHubContext<CustomerSupportHub>().Clients.Group("").Refresh();
            }
            return new BadRequestObjectResult(new GenericResult
            {
                Message = "User Not Found",
                Succeded = false
            });
        }
        private string SaveToBase64(string base64Image, string userEmail)
        {
            base64Image = base64Image.Substring(22);
            string path = "profileImage" + "/" + userEmail + ".jpg";
            var uploads = Path.Combine(_hostingEnviroment.WebRootPath, path);
            var bytes = Convert.FromBase64String(base64Image);
            using (var imageFile = new FileStream(uploads, FileMode.Create))
            {
                imageFile.Write(bytes, 0, bytes.Length);
                imageFile.Flush();
            }
            return "https://admin.livecustomer.chat/" + path;
        }
    }
}