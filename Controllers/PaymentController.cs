﻿using System;
using System.Linq;
using Microsoft.AspNetCore.Mvc;
using SignalRMasterServer.Models;
using SignalRMasterServer.Data;
using static SignalRMasterServer.Services.PayPalService;
using System.Net.Http;
using Microsoft.EntityFrameworkCore;

namespace SignalRMasterServer.Controllers
{
    [Route("api/Payment")]
    [Produces("application/json")]
    public class PaymentController : Controller
    {
        PayPalAccessToken accessToken;

        HttpClient http;
        private readonly AppDbContext _context;
        public PaymentController(AppDbContext _context)
        {
            this._context = _context;
        }

        [HttpGet("{WebsiteName}")]
        public IActionResult GetPaymentHistory(string WebsiteName)
        {
            return new OkObjectResult(null);
        }

        [HttpGet("{method}/{term}/{plan}/{websiteid}")]
        public IActionResult ProcessPayment(string plan, string term, string method,int websiteid)
        {            
            var selectedPlan = _context.Plans.Where(x=>x.WebsiteType.ToString()==plan).Include(x=>x.PlanPrice).SingleOrDefault();
            var selectedWebsite = _context.Website.Where(x=>x.Id == websiteid).SingleOrDefault();
            var price = selectedPlan.PlanPrice.Where(x=>x.Term.ToString() == term).SingleOrDefault().Price;

            http = GetPaypalHttpClient();
            accessToken = GetPayPalAccessTokenAsync(http).Result;
            PayPalPaymentCreatedResponse createdPayment = CreatePaypalPaymentAsync(http, accessToken,price).Result;            
            var approval_url = createdPayment.links.First(x => x.rel == "approval_url").href;
            Response.Redirect(approval_url);
            return  View();
        }
        public class PaymentCreateViewModel
        {
            public string Plan { get; set; }
            public string Term { get; set; }
        }
        private int GetUserId()
        {
            string UserName = User.Identity.Name;
            if (UserName == null)
            {
                UserName = (Global.DevelopmentMode()) ? Global.DevelopmentUserName() : null;
            }
            int UserID = _context.Users.SingleOrDefault(x => x.UserName == UserName).Id;
            return UserID;
        }


    }
}