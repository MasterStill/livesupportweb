﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Authorization;
using SignalRMasterServer.Models;
using SignalRMasterServer.Data;
using Microsoft.EntityFrameworkCore;
using Microsoft.AspNetCore.Hosting;
using System.IO;

namespace SignalRMasterServer.Controllers
{
    [Route("api/Referral")]
    [Produces("application/json")]
    //[Authorize]
    public class ReferralController : Controller
    {
        private readonly AppDbContext _context;
        public ReferralController(AppDbContext _context)
        {

            this._context = _context;
        }
        private int GetUserId()
        {
            string UserName = User.Identity.Name;
            if (UserName == null)
            {
                UserName = (Global.DevelopmentMode()) ? Global.DevelopmentUserName() : null;
            }
            int UserID = _context.Users.SingleOrDefault(x => x.UserName == UserName).Id;
            return UserID;
        }
        private bool IsInRole(string RoleName, int WebsiteId)
        {
            if (_context.UserRoles.Where(x => x.WebsiteId == WebsiteId).Where(x => x.Role.Name == RoleName).Where(x => x.User.UserName == GetUserName()).SingleOrDefault() == null)
            {
                return false;
            }
            return true;
        }
        [HttpGet]
        public IActionResult GetAffiliates()
        {
            var result = _context.Referral.Include(y=>y.ReferralBy).Where(x=>x.ReferralBy.Id == GetUserId()).ToList();
            return new OkObjectResult(result);
        }



        private string GetUserName()
        {
            if (User.Identity.IsAuthenticated)
                return User.Identity.Name;
            return Global.DevelopmentUserName();
        }    
        [HttpPost]
        public IActionResult AddReferral([FromBody]ReferralCreateViewModel referral)
        {           
            Referral r = new Referral
            {
                ReferralById = GetUserId() ,// for timebeing ## To Do 
                FullName = referral.FullName,
                Email = referral.Email,
                Website = referral.Website,
                Status = enumReferral.Pending,
                ReferralDate = DateTime.Now
            };
            _context.Referral.Add(r);
            _context.SaveChanges();
            return new OkObjectResult(r);
        }
        private bool WebsiteExists(string WebsiteName)
        {
            //Need to workaround here
            var website = _context.Website.Where(x => x.Name.ToLower() == WebsiteName.ToLower()).FirstOrDefault();
            if (website == null) return false;
            return true;
        }
        public class WebsiteCreateViewModel
        {
            public string Name { get; set; }
            public string Alias { get; set; }
            public string Logo { get; set; }
            public WebsiteType WebstieType { get; set; }
        }
        public class WebsiteEditViewModel
        {
            public string Name { get; set; }
            public string Alias { get; set; }
            public string Logo { get; set; }
            //public WebsiteType WebstieType { get; set; }
        }
    }
}