﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography.X509Certificates;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Authorization;
using SignalRMasterServer.Models;
using SignalRMasterServer.Data;
using Microsoft.EntityFrameworkCore;
using static SignalRMasterServer.Global;
using SignalRMasterServer.Services;
using SignalRMasterServer.Hubs;
using Microsoft.AspNetCore.SignalR.Infrastructure;

namespace SignalRMasterServer.Controllers
{
    [Route("api/Roles")]
    [Produces("application/json")]
    //[Authorize]
    public class RolesController : Controller
    {
        private readonly AppDbContext _context;
        private readonly IEmailSender _emailSender;
        private IConnectionManager _connectionManager;
        public RolesController(AppDbContext _context, IEmailSender emailSender,IConnectionManager _connecitonManager)
        {
            this._connectionManager = _connecitonManager;
            _emailSender = emailSender;
            this._context = _context;
        }
        [HttpGet]
        public IActionResult AllRoles()
        {
            var AllRoles = _context.Roles.ToList().Select(x => x.Name);
            return new OkObjectResult(AllRoles);
        }

        [HttpPost]
        public IActionResult setUserToRole([FromBody] RoleViewModel r)
        {
            if (!IsInRole("Admin", r.WebsiteId))
            {
                return Global.AuthorizationFailureRoute();
            }
            ApplicationUserRole role = new ApplicationUserRole();
            role.WebsiteId = r.WebsiteId;
            role.RoleId = _context.Roles.SingleOrDefault(x => x.Name == r.RoleName).Id;
            role.UserId = GetUserId(r.UserEmail, r.WebsiteId, role.RoleId);
            if (role.UserId == 0)
            { // User Not signed up so send invitation email
                var invitedbyUser = _context.Users.SingleOrDefault(x => x.Email == GetUserName());
                var kk = Global.EmailTemplate(enumEmailTemplate.InvitationForRoleNewUser);
                kk.Subject = kk.Subject.Replace("{fullname}", (invitedbyUser.FullName != null) ? invitedbyUser.FullName : invitedbyUser.Email);
                kk.Message = kk.Message.Replace("{fullname}", invitedbyUser.FullName);
                kk.Message = kk.Message.Replace("{emailaddress}", invitedbyUser.Email);
                kk.Message = kk.Message.Replace("{rolename}", r.RoleName);
                kk.Message = kk.Message.Replace("{websitename}", _context.Website.SingleOrDefault(x => x.Id == r.WebsiteId).Name);
                kk.Message = kk.Message.Replace("{invitedemail}", r.UserEmail);
                _emailSender.SendEmailAsync(r.UserEmail, kk.Subject,
                       kk.Message);
            }
            try
            {
                _context.UserRoles.Add(role);
                _context.SaveChanges();

                try
                {
                    var RoleGivenToUser = _context.Users.Where(x => x.Id == role.UserId).SingleOrDefault();
                    _connectionManager.GetHubContext<CustomerSupportHub>().Clients.Group(RoleGivenToUser.SignalrGuid.ToString()).onUpdateMe(SignalrUpdate.RolesControllersetUserToRole.ToString());
                }
                catch (Exception ex)
                {
                        
                }
            }
            catch
            {

            }
            return Ok();
        }
        private int GetUserId(string Email, int WebsiteId, int RoleId)
        {
            var user = _context.Users.SingleOrDefault((x => x.UserName == Email));
            if (user == null)
            {
                return UserNotInDb(Email, WebsiteId, RoleId);
            }
            else
            {
                return user.Id;
            }

        }
        private int UserNotInDb(string Email, int WebsiteId, int RoleId)
        {
            var user = new EmailInvitation
            {
                Email = Email,
                WebsiteId = WebsiteId,
                InvitedDate = DateTime.Now,
                RoleId = RoleId,
                AddedById = GetUserId()
            };
            try
            {
                _context.Add(user);
                _context.SaveChanges();
                //Invite the user for the Role
            }
            catch { }
            return 0;// Need to workaround this;
        }
        [HttpPost("RemoveUserFromRole")]
        public IActionResult RemoveUserFromRole([FromBody] RoleViewModel r)
        {
            int websiteId = r.WebsiteId;
            if (!IsInRole("Admin", websiteId))
            {
                return Global.AuthorizationFailureRoute();
            }
            try
            {
                ApplicationUserRole role = _context.UserRoles.Where(x=>x.Role.Name == r.RoleName).Where(x=>x.User.Email == r.UserEmail).Where(x=>x.WebsiteId == r.WebsiteId).SingleOrDefault(); //new ApplicationUserRole();                
                var totalNoofAdminUser = _context.UserRoles.Where(x => x.Role.Name == "Admin").Where(x => x.WebsiteId == r.WebsiteId).Count();
                if (totalNoofAdminUser == 1 && role !=null)
                {
                    return new BadRequestObjectResult(new GenericResult
                    {
                        Succeded = false,
                        Message = "There need to be atleast one Admin"
                    });
                }
                _context.UserRoles.Remove(role);
                _context.SaveChanges();
            }
            catch
            {
                //User Chaina so remove from invitation list            
                EmailInvitation e = _context.EmailInvitation.Where(x => x.Email == r.UserEmail).Where(x => x.Role.Name == r.RoleName).Where(x=>x.Accepted == false).SingleOrDefault();
                _context.EmailInvitation.Remove(e);
                _context.SaveChanges();
                return Ok();
            }
            return Ok();
        }
        private string GetUserName()
        {
            string UserName = User.Identity.Name;
            if (UserName == null)
            {
                UserName = (Global.DevelopmentMode()) ? Global.DevelopmentUserName() : null;
            }
            return UserName;
        }
        private int GetUserId()
        {
            string UserName = User.Identity.Name;
            if (UserName == null)
            {
                UserName = (Global.DevelopmentMode()) ? Global.DevelopmentUserName() : null;
            }
            int UserID = _context.Users.SingleOrDefault(x => x.UserName == UserName).Id;
            return UserID;
        }
        private bool IsInRole(string RoleName, int WebsiteId)
        {
            if (_context.UserRoles.Where(x => x.WebsiteId == WebsiteId).Where(x => x.Role.Name == RoleName).Where(x => x.User.UserName == GetUserName()).SingleOrDefault() == null)
            {
                return false;
            }
            return true;
        }
        public class RoleViewModel
        {
            public int WebsiteId;
            public string UserEmail;
            public string RoleName;
        }
    }
}
