﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Authorization;
using SignalRMasterServer.Services;
using static SignalRMasterServer.Services.PayPalService;
using System.Net.Http;
using System.Text.RegularExpressions;
using Microsoft.AspNetCore.Http;
using SignalRMasterServer.Models;
using System.Text;
using System.Net.Http.Headers;
using SignalRMasterServer.Data;
namespace SignalRMasterServer.Controllers
{
    [Route("api/OneSignale")]
    [Produces("application/json")]
    public class OneSignalController : Controller
    {
        private AppDbContext _context;
        static HttpClient client = new HttpClient();
        public string OneSignalAuthToken = "Basic ODZlZjMwNWYtNWY5OS00NzY3LWIxNmItOTgzMWM2YzFhMzNk";

        public OneSignalController(AppDbContext _context)
        {
            this._context = _context;
        }
        [HttpPost]
        public async Task<IActionResult> Index([FromBody]PushMessage Message)
        {
            SendPushNotification(Message);
            return View();
        }
        public class OneSignal
        {
            public string app_id { get; set; }
            public string contents { get; set; }
            public string[] included_segments;
        }
        public class PushMessage
        {
            public string Message { get; set; }
            public string Title { get; set; }

            public MessageType MessageType { get; set; }
            public List<string> DeviceIds { get; set; }

        }
        private void SendPushNotification(PushMessage p)
        {
            try
            {
                client.BaseAddress = new Uri(OneSignalUrl("notifications"));
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                client.DefaultRequestHeaders.TryAddWithoutValidation("Authorization", OneSignalAuthToken);
            }
            catch
            {
            }
            string deviceIds = string.Empty;
            foreach (var items in p.DeviceIds)
            {
                deviceIds += "'" + items + "'";
            }
            if (p.DeviceIds.Count > 0) deviceIds = "[" + deviceIds.Replace("''", "','") + "]";

            string OnlyFew;
            if (p.MessageType == MessageType.All)
            {
                OnlyFew = "'included_segments':['All']";
            }
            else
            {
                OnlyFew = "'include_player_ids':" + deviceIds;
            }

            string json = "{'app_id':'"+ OneSignalAppId() +"','contents': {'en': '" + p.Message + "'}, " + OnlyFew + ",'headings':{'en':'" + p.Title + "'}}";
            string milekoData = json.Replace("'", "\"");
            Console.WriteLine(milekoData);
            HttpRequestMessage request = new HttpRequestMessage(HttpMethod.Post, OneSignalUrl("notifications"));
            request.Content = new StringContent(milekoData,
                                    Encoding.UTF8,
                                    "application/json");
            var asdasdas = client.SendAsync(request)
             .ContinueWith(responseTask =>
             {
                 var lamokha = responseTask.Result;
             });

        }
        public enum MessageType
        {
            All, Few
        }
        private string OneSignalUrl(string Segment){
            return "https://onesignal.com/api/v1/" + Segment;
            
        }
        private string OneSignalAppId(){
            return "2e573a27-672a-478a-8c60-b254c888d9e3";
        }
    }
}