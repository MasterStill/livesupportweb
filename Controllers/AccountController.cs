﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.Extensions.Logging;
using SignalRMasterServer.Data;
using SignalRMasterServer.Models;
using SignalRMasterServer.Models.AccountViewModels;
using SignalRMasterServer.Services;
using static SignalRMasterServer.Global;

namespace SignalRMasterServer.Controllers
{
    [Authorize]
    public class AccountController : Controller
    {
        private readonly UserManager<ApplicationUser> _userManager;
        private readonly SignInManager<ApplicationUser> _signInManager;
        private readonly IEmailSender _emailSender;
        private readonly ISmsSender _smsSender;
        private readonly ILogger _logger;
        private AppDbContext _context;

        public AccountController(
            UserManager<ApplicationUser> userManager,
            SignInManager<ApplicationUser> signInManager,
            IEmailSender emailSender,
            ISmsSender smsSender,
            ILoggerFactory loggerFactory,
            AppDbContext _context
            )
        {
            this._context = _context;
            _userManager = userManager;
            _signInManager = signInManager;
            _emailSender = emailSender;
            _smsSender = smsSender;
            _logger = loggerFactory.CreateLogger<AccountController>();
        }
        //public AccountController()
        //{
        //    _userManager = new UserManager<ApplicationUser>();
        //}
        //
        // GET: /Account/Login
        [HttpGet]
        [AllowAnonymous]
        public IActionResult Login(string returnUrl = null)
        {
            string username = string.Empty;
            string password = string.Empty;
            if (Global.BaseUrl().ToLower().Contains("localhost") || Global.BaseUrl().ToLower().Contains("ngrok"))
            {
                username = "masterstill@gmail.com";//Global.DevelopmentUserName();
                password = "123!@#Subinn!!";
            }
            ViewBag.Username = username;
            ViewBag.Password = password;
            ViewBag.ReturnUrl = returnUrl;
            return View();
        }

        //
        // POST: /Account/Login
        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Login(LoginViewModel model, string returnUrl = null)
        {


            //  ViewData["Error"] = sb.ToString();
            ViewData["ReturnUrl"] = returnUrl;
            if (ModelState.IsValid)
            {
                var user = await _userManager.FindByEmailAsync(model.Email);
                if (user != null)
                {
                    if (!await _userManager.IsEmailConfirmedAsync(user))
                    {
                        ViewBag.Error = "You must have a confirmed email to log in.";
                        //ModelState.AddModelError(string.Empty,
                        //            "You must have a confirmed email to log in.");
                        return View(model);
                    }
                }
                var result = await _signInManager.PasswordSignInAsync(model.Email, model.Password, model.RememberMe, lockoutOnFailure: true);
                if (result.Succeeded)
                {
                    LogLogin(user.Id);
                    ViewBag.Error = "User logged in.";
                    //_logger.LogInformation(1, "User logged in.");
                    return RedirectToLocal(returnUrl);
                }
                if (result.RequiresTwoFactor)
                {
                    return RedirectToAction(nameof(SendCode), new { ReturnUrl = returnUrl, RememberMe = model.RememberMe });
                }
                if (result.IsLockedOut)
                {
                    _logger.LogWarning(2, "User account locked out.");
                    return View("Lockout");
                }
                else
                {
                    ViewBag.Error = "Invalid login attempt.";

                    //ModelState.AddModelError(string.Empty, "Invalid login attempt.");
                    return View(model);
                }
            }
            return View(model);
        }


        private async void LogLogin(int userId)
        {
            //_context.Logs.Add(new Log
            //{
            //    Event = enumEvent.Login,
            //    UserId = userId
            //});
            //try
            //{
            //    await _context.SaveChangesAsync();
            //}
            //catch(Exception ex)
            //{
            //    Global.Live_Crash_Report(ex);
            //}
        }
        private async void LogLogOff(int userId)
        {
            //_context.Logs.Add(new Log
            //{
            //    Event = enumEvent.LogOut,
            //    UserId = userId
            //});
            //try
            //{
            //await _context.SaveChangesAsync();
            //}
            //catch (Exception ex)
            //{
            //    Global.Live_Crash_Report(ex);
            //}

        }

        [HttpGet]
        [AllowAnonymous]
        public bool LoginCheck(string username, string password)
        {
            return true;
        }


        //
        //GET: /Account/Register
        [HttpGet]
        [AllowAnonymous]
        public IActionResult Register(string returnUrl = null)
        {
            ViewData["ReturnUrl"] = returnUrl;
            return View();
        }

        //POST: /Account/Register
        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Register(RegisterViewModel model, string returnUrl = null)
        {
            ViewData["ReturnUrl"] = returnUrl;
            if (ModelState.IsValid)
            {
                var user = new ApplicationUser { UserName = model.Email, Email = model.Email };
                var result = await _userManager.CreateAsync(user, model.Password);
                if (result.Succeeded)
                {
                    //Check if user is invited for a role for a website
                    var isinvited = _context.EmailInvitation.Where(x => x.Email == model.Email).Where(x => x.Accepted == false);
                    if (isinvited.Count() > 0)
                    {
                        try
                        {
                            foreach (var items in isinvited)
                            {
                                ApplicationUserRole role = new ApplicationUserRole();
                                role.WebsiteId = items.WebsiteId;
                                role.RoleId = items.RoleId;
                                role.UserId = user.Id;
                                _context.UserRoles.Add(role);
                                items.AcceptedDate = DateTime.Now;
                                items.Accepted = true;
                                _context.EmailInvitation.Update(items);
                            }
                            _context.SaveChanges();
                        }
                        catch { }
                    }
                    var code = await _userManager.GenerateEmailConfirmationTokenAsync(user);
                    var callbackUrl = Url.Action("ConfirmEmail", "Account", new { userId = user.Id, code = code }, protocol: HttpContext.Request.Scheme);
                    var template = Global.EmailTemplate(enumEmailTemplate.ConfirmationEmail);
                    await _emailSender.SendEmailAsync(model.Email, template.Subject,
                       template.Message.Replace("{callbackUrl}", callbackUrl));
                    //await _signInManager.SignInAsync(user, isPersistent: false);
                    _logger.LogInformation(3, "User created a new account with password.");
                    return View("ValidateEmail");
                    //return RedirectToLocal(returnUrl);
                }
                AddErrors(result);
            }
            // If we got this far, something failed, redisplay form
            return View(model);
        }
        //
        // POST: /Account/LogOff
        //[HttpPost]
        //[ValidateAntiForgeryToken]
        [HttpGet]
        public async Task<IActionResult> LogOff()
        {
            // var LogOutUser = _context.Users.Where(x => x.Email == User.Identity.Name).SingleOrDefault();
            // LogOutUser.Online = false;
            // LogLogOff(LogOutUser.Id);
            // _context.Users.Update(LogOutUser);ts
            // System.Threading.Thread.Sleep(1000);
            // _context.SaveChanges();
            await _signInManager.SignOutAsync();
            _logger.LogInformation(4, "User logged out.");
            return RedirectToAction(nameof(HomeController.Index), "Home");
        }
        [HttpGet]
        public IActionResult ValidateEmail()
        {
            return View();
        }

        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public IActionResult ExternalLogin(string provider, string returnUrl = null)
        {            
            var redirectUrl = Url.Action("ExternalLoginCallback", "Account", new { ReturnUrl = returnUrl });
            var properties = _signInManager.ConfigureExternalAuthenticationProperties(provider, redirectUrl);
            return Challenge(properties, provider);
        }

        [HttpGet]
        [AllowAnonymous]
        public IActionResult ExternalLoginClient(string provider, string returnUrl = null,string WebsiteId = null)
        {
            // provider = "Google";
            // WebsiteId = ElasticSearchGlobal.GetWebsitebyName("gajurel.com").Guid.ToString();
            //var signalrUser = Request.Cookies["LiveSupportRockmandu" + Request.Host.Value.Replace(":", "")];            
            string redirectUrl = Global.BaseUrl() + "/Account/ExternalLoginCallbackClient?returnUrl=" + returnUrl + "&websiteId=" + WebsiteId;//+ "/Url.Action("https://admin.livecustomer.chat/Accounts/ExternalLoginCallbackClient", "Account", new { ReturnUrl = returnUrl , WebsiteId = WebsiteId });            
            var properties = _signInManager.ConfigureExternalAuthenticationProperties(provider, redirectUrl);
            return Challenge(properties, provider);
        }
        [HttpGet]
        [AllowAnonymous]
        public async Task<IActionResult> ExternalLoginCallbackClient(string returnUrl = null, string WebsiteId = null,string remoteError = null)
        {
            
            // WebsiteId = ElasticSearchGlobal.GetWebsitebyName("gajurel.com").Guid.ToString();
            // returnUrl = "http://gajurel.com/";
            string WebsiteName = ElasticSearchGlobal.GetWebsitebyGuid(Guid.Parse(WebsiteId)).Name.Replace(":","");            
            var signalrUser = Request.Cookies["LiveSupportRockmandu" + WebsiteName.Replace(":", "")];
            string ProfilePicture = string.Empty;
            if (remoteError != null)
            {
                ModelState.AddModelError(string.Empty, $"Error from external provider: {remoteError}");
                return View(nameof(Login));
            }
            var info = await _signInManager.GetExternalLoginInfoAsync();
            // if (info == null)
            // {
            //     return RedirectToAction(nameof(Login));
            // }
            //Get Profile Pictur of user            
            var userId = info.ProviderKey;
            HttpClient c = new HttpClient();
            string ProfileURL = string.Empty;
            if (info.LoginProvider == "Facebook")
            {
                ProfileURL = "https://graph.facebook.com/v2.8/" + userId + "/Picture";
                ProfileURL = "https://graph.facebook.com/" + userId + "/picture";
            }
            if (info.LoginProvider == "Google")
            {
                ProfileURL = "https://www.googleapis.com/plus/v1/people/" + userId + "?fields=image&key=AIzaSyDeEfBA9Mfrr5EA995o9lvV07cvzm8PmOA";
            }
            if (info.LoginProvider == "Microsoft")
            {
                ProfileURL = "https://apis.live.net/v5.0/" + userId + "/picture?type=large";
            }
            using (var client = new HttpClient())
            {
                bool isClient = false;
                try
                {
                    bool.TryParse(HttpContext.Request.Query["PageTitle"].ToString(), out isClient);
                }
                catch { }
                var ProfilePictureresult = client.GetAsync(ProfileURL).Result;
                if (info.LoginProvider == "Facebook")
                {
                    //string responseJson = await ProfilePictureresult.Content.ReadAsStringAsync();
                    ProfilePicture = ProfilePictureresult.RequestMessage.RequestUri.ToString();

                }
                if (info.LoginProvider == "Google")
                {
                    try
                    {

                        string responseJson = await ProfilePictureresult.Content.ReadAsStringAsync();
                        ProfilePicture = responseJson.Substring(responseJson.IndexOf("url"), responseJson.IndexOf(",") - responseJson.IndexOf("url")).Replace("url\":", "").Replace("\"", "");
                    }
                    catch (Exception e)
                    {
                        Console.WriteLine(e);
                    }
                }
            }
            SignalRUser u = new SignalRUser();
            //Check if already There
            u = ElasticSearchGlobal.GetSignalRUserByEmail(info.Principal.FindFirstValue(ClaimTypes.Email),WebsiteId);
            if (u != null)
            {                               
                setCookie(WebsiteName,u.GroupId);
                //Response.Cookies.Append("LiveSupportRockmandu" + WebsiteName, u.GroupId,options1);
                // Need to check the website from chat Message
            }
            if (u == null)
                //check if lately registered
                if(signalrUser !=null)u = ElasticSearchGlobal.GetSignalRUser(null, signalrUser,WebsiteId);
            if (u == null)
            {
                ElasticSignalRUser newUser = new ElasticSignalRUser();
                newUser.Avatar = ProfilePicture;
                newUser.FullName = info.Principal.FindFirstValue(ClaimTypes.GivenName) + " " + info.Principal.FindFirstValue(ClaimTypes.Surname);
                newUser.Email = info.Principal.FindFirstValue(ClaimTypes.Email);
                newUser.Verified = true;
                newUser.InfoProvided = true;
                newUser.WebsiteGuid = WebsiteId.ToString();
                newUser.GroupId = Guid.NewGuid().ToString() + Guid.NewGuid().ToString();
                ElasticSearchGlobal.AddSignalRUser(newUser);                
                returnUrl += "?auth=" +  newUser.GroupId;
                //string WebsiteName = ElasticSearchGlobal.GetWebsitebyGuid(Guid.Parse(WebsiteId)).Name.Replace(":","");                
                //Response.Cookies.Append("LiveSupportRockmandu" + WebsiteName,  newUser.GroupId,options1);
                setCookie(WebsiteName,newUser.GroupId);
                //Need to do if the user has not sent any message before
                // ElasticSignalRUser k = new ElasticSignalRUser{
                //     k.Avatar = ProfilePicture,
                //     k.Email = info.Principal.FindFirstValue(ClaimTypes.Email),

                // }
            }
            else
            {
                u.Avatar = ProfilePicture;
                u.FullName = info.Principal.FindFirstValue(ClaimTypes.GivenName) + " " + info.Principal.FindFirstValue(ClaimTypes.Surname);
                u.Email = info.Principal.FindFirstValue(ClaimTypes.Email);
                u.Verified = true;
                u.InfoProvided = true;
                ElasticSearchGlobal.UpdateSignalRUser(u);
                returnUrl += "?auth=" +  u.GroupId;
            }
            CookieOptions options = new CookieOptions();
            options.Expires = DateTime.Now.AddDays(1);
            Response.Cookies.Append("LiveSupportRockmanduCustomerInfo", "true",options);
            // Sign in the user with this external login provider if the user already has a login.
            // var result = await _signInManager.ExternalLoginSignInAsync(info.LoginProvider, info.ProviderKey, isPersistent: false);
            // if (result.Succeeded)
            // {
            //     _logger.LogInformation(5, "User logged in with {Name} provider.", info.LoginProvider);
            // }
            return Redirect(returnUrl);
            // if (result.RequiresTwoFactor)
            // {
            //     return RedirectToAction(nameof(SendCode), new { ReturnUrl = returnUrl });
            // }
            // if (result.IsLockedOut)
            // {
            //     return View("Lockout");
            // }
            // else
            // {
            //     // If the user does not have an account, then ask the user to create an account.
            //     ViewData["ReturnUrl"] = returnUrl;
            //     ViewData["LoginProvider"] = info.LoginProvider;

            //     var email = info.Principal.FindFirstValue(ClaimTypes.Email);
            //     string firstName = info.Principal.FindFirstValue(ClaimTypes.GivenName);
            //     ViewData["FirstName"] = firstName;
            //     string lastName = info.Principal.FindFirstValue(ClaimTypes.Surname);

            //     var picture = ProfilePicture;
            //     //info.Principal. UserInformationEndpoint
            //     string image = (string.IsNullOrEmpty(picture)) ? Global.NoImage() : picture; // Need to work around here
            //     ViewData["Image"] = image;

            //     return View("ExternalLoginConfirmation", new ExternalLoginConfirmationViewModel { Email = email, FirstName = firstName, LastName = lastName, Image = image });
            // }
        }

private void setCookie(string WebsiteName,string value){
    CookieOptions options1 = new CookieOptions();
    options1.Expires = DateTime.Now.AddDays(Global.CacheDuration());
    //Response.Cookies.Append("LiveSupportRockmandu" + WebsiteName + "LoggedInUser",  value,options1);        
}
        //
        // GET: /Account/ExternalLoginCallback
        [HttpGet]
        [AllowAnonymous]
        public async Task<IActionResult> ExternalLoginCallback(string returnUrl = null, string remoteError = null)
        {
            string ProfilePicture = string.Empty;
            if (remoteError != null)
            {
                ModelState.AddModelError(string.Empty, $"Error from external provider: {remoteError}");
                return View(nameof(Login));
            }
            var info = await _signInManager.GetExternalLoginInfoAsync();
            if (info == null)
            {
                return RedirectToAction(nameof(Login));
            }
            //Get Profile Pictur eof user            
            var userId = info.ProviderKey;
            HttpClient c = new HttpClient();
            string ProfileURL = string.Empty;
            if (info.LoginProvider == "Facebook")
            {
                ProfileURL = "https://graph.facebook.com/v2.8/" + userId + "/Picture";
                ProfileURL = "https://graph.facebook.com/" + userId + "/picture";
            }
            if (info.LoginProvider == "Google")
            {
                ProfileURL = "https://www.googleapis.com/plus/v1/people/" + userId + "?fields=image&key=AIzaSyDeEfBA9Mfrr5EA995o9lvV07cvzm8PmOA";
                //                {
                //                    image:
                //                    {
                //                        url: "https://lh6.googleusercontent.com/-8CUNqFBupAs/AAAAAAAAAAI/AAAAAAAAADg/yhqCEPDTlpA/photo.jpg?sz=50",
                //isDefault: false
                //                    }
                //}
                //Above is the response  of google Api for Images
            }
            if (info.LoginProvider == "Microsoft")
            {
                ProfileURL = "https://apis.live.net/v5.0/" + userId + "/picture?type=large";
            }
            using (var client = new HttpClient())
            {
                bool isClient = false;
                try
                {
                    bool.TryParse(HttpContext.Request.Query["PageTitle"].ToString(), out isClient);
                }
                catch { }
                var ProfilePictureresult = client.GetAsync(ProfileURL).Result;
                if (info.LoginProvider == "Facebook")
                {
                    //string responseJson = await ProfilePictureresult.Content.ReadAsStringAsync();
                    ProfilePicture = ProfilePictureresult.RequestMessage.RequestUri.ToString();

                }
                if (info.LoginProvider == "Google")
                {
                    try
                    {

                        string responseJson = await ProfilePictureresult.Content.ReadAsStringAsync();
                        ProfilePicture = responseJson.Substring(responseJson.IndexOf("url"), responseJson.IndexOf(",") - responseJson.IndexOf("url")).Replace("url\":", "").Replace("\"", "");
                    }
                    catch (Exception e)
                    {
                        Console.WriteLine(e);
                    }
                }
            }
            // Sign in the user with this external login provider if the user already has a login.
            var result = await _signInManager.ExternalLoginSignInAsync(info.LoginProvider, info.ProviderKey, isPersistent: false);
            if (result.Succeeded)
            {
                _logger.LogInformation(5, "User logged in with {Name} provider.", info.LoginProvider);
                return RedirectToLocal(returnUrl);
            }
            if (result.RequiresTwoFactor)
            {
                return RedirectToAction(nameof(SendCode), new { ReturnUrl = returnUrl });
            }
            if (result.IsLockedOut)
            {
                return View("Lockout");
            }
            else
            {
                // If the user does not have an account, then ask the user to create an account.
                ViewData["ReturnUrl"] = returnUrl;
                ViewData["LoginProvider"] = info.LoginProvider;

                var email = info.Principal.FindFirstValue(ClaimTypes.Email);
                string firstName = info.Principal.FindFirstValue(ClaimTypes.GivenName);
                ViewData["FirstName"] = firstName;
                string lastName = info.Principal.FindFirstValue(ClaimTypes.Surname);

                var picture = ProfilePicture;
                //info.Principal. UserInformationEndpoint
                string image = (string.IsNullOrEmpty(picture)) ? Global.NoImage() : picture; // Need to work around here
                ViewData["Image"] = image;

                return View("ExternalLoginConfirmation", new ExternalLoginConfirmationViewModel { Email = email, FirstName = firstName, LastName = lastName, Image = image });
            }
        }

        //
        // POST: /Account/ExternalLoginConfirmation
        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> ExternalLoginConfirmation(ExternalLoginConfirmationViewModel model, string returnUrl = null)
        {
            if (ModelState.IsValid)
            {
                // Get the information about the user from the external login provider
                var info = await _signInManager.GetExternalLoginInfoAsync();
                if (info == null)
                {
                    return View("ExternalLoginFailure");
                }
                var user = new ApplicationUser { UserName = model.Email, Email = model.Email, FirstName = model.FirstName, LastName = model.LastName, Image = model.Image };
                var result = await _userManager.CreateAsync(user);
                if (result.Succeeded)
                {
                    result = await _userManager.AddLoginAsync(user, info);
                    if (result.Succeeded)
                    {
                        await _signInManager.SignInAsync(user, isPersistent: false);
                        _logger.LogInformation(6, "User created an account using {Name} provider.", info.LoginProvider);
                        return RedirectToLocal(returnUrl);
                    }
                }
                AddErrors(result);
            }

            ViewData["ReturnUrl"] = returnUrl;
            return View(model);
        }

        // GET: /Account/ConfirmEmail
        [HttpGet]
        [AllowAnonymous]
        public async Task<IActionResult> ConfirmEmail(string userId, string code)
        {
            if (userId == null || code == null)
            {
                return View("Error");
            }
            var user = await _userManager.FindByIdAsync(userId);
            if (user == null)
            {
                return View("Error");
            }
            var result = await _userManager.ConfirmEmailAsync(user, code);
            if (result.Succeeded)
            {
                // Need to check if the user is invited to send his task email for now ok !                
                var template = Global.EmailTemplate(enumEmailTemplate.WelcomeEmail);
                await _emailSender.SendEmailAsync(user.Email, template.Subject, template.Message.Replace("{firstname}", "user.Firstname").Replace(" {firstname},", ""));
            }
            return View(result.Succeeded ? "ConfirmEmail" : "Error");
        }

        //
        // GET: /Account/ForgotPassword
        [HttpGet]
        [AllowAnonymous]
        public IActionResult ForgotPassword()
        {
            return View();
        }

        //
        // POST: /Account/ForgotPassword
        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> ForgotPassword(ForgotPasswordViewModel model)
        {
            if (ModelState.IsValid)
            {
                var user = await _userManager.FindByNameAsync(model.Email);
                if (user == null || !(await _userManager.IsEmailConfirmedAsync(user)))
                {
                    return View("ForgotPasswordConfirmation");
                }
                var code = await _userManager.GeneratePasswordResetTokenAsync(user);
                var callbackUrl = Url.Action("ResetPassword", "Account", new { userId = user.Id, code = code }, protocol: HttpContext.Request.Scheme);
                EmailFormat e = Global.EmailTemplate(enumEmailTemplate.ForgotyourPassword);
                await _emailSender.SendEmailAsync(model.Email, e.Subject, e.Message.Replace("{callbackUrl}", callbackUrl).Replace("{firstname}", user.FirstName).Replace("Hi<strong> <strong>,", "Hi,"));
                return View("ForgotPasswordConfirmation");
            }
            return View(model);
        }
        [HttpGet]
        [AllowAnonymous]
        public IActionResult ForgotPasswordConfirmation()
        {
            return View();
        }

        //
        // GET: /Account/ResetPassword
        [HttpGet]
        [AllowAnonymous]
        public IActionResult ResetPassword(string code = null)
        {
            return code == null ? View("Error") : View();
        }

        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> ResetPassword(ResetPasswordViewModel model)
        {
            if (!ModelState.IsValid)
            {
                return View(model);
            }
            var user = await _userManager.FindByNameAsync(model.Email);
            if (user == null)
            {
                // Don't reveal that the user does not exist
                return RedirectToAction(nameof(AccountController.ResetPasswordConfirmation), "Account");
            }
            var result = await _userManager.ResetPasswordAsync(user, model.Code, model.Password);
            if (result.Succeeded)
            {
                return RedirectToAction(nameof(AccountController.ResetPasswordConfirmation), "Account");
            }
            AddErrors(result);
            return View();
        }

        //
        // GET: /Account/ResetPasswordConfirmation
        [HttpGet]
        [AllowAnonymous]
        public IActionResult ResetPasswordConfirmation()
        {
            return View();
        }

        //
        // GET: /Account/SendCode
        [HttpGet]
        [AllowAnonymous]
        public async Task<ActionResult> SendCode(string returnUrl = null, bool rememberMe = false)
        {
            var user = await _signInManager.GetTwoFactorAuthenticationUserAsync();
            if (user == null)
            {
                return View("Error");
            }
            var userFactors = await _userManager.GetValidTwoFactorProvidersAsync(user);
            var factorOptions = userFactors.Select(purpose => new SelectListItem { Text = purpose, Value = purpose }).ToList();
            return View(new SendCodeViewModel { Providers = factorOptions, ReturnUrl = returnUrl, RememberMe = rememberMe });
        }

        //
        // POST: /Account/SendCode
        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> SendCode(SendCodeViewModel model)
        {
            if (!ModelState.IsValid)
            {
                return View();
            }

            var user = await _signInManager.GetTwoFactorAuthenticationUserAsync();
            if (user == null)
            {
                return View("Error");
            }

            // Generate the token and send it
            var code = await _userManager.GenerateTwoFactorTokenAsync(user, model.SelectedProvider);
            if (string.IsNullOrWhiteSpace(code))
            {
                return View("Error");
            }

            var message = "Your security code is: " + code;
            if (model.SelectedProvider == "Email")
            {
                await _emailSender.SendEmailAsync(await _userManager.GetEmailAsync(user), "Security Code", message);
            }
            else if (model.SelectedProvider == "Phone")
            {
                await _smsSender.SendSmsAsync(await _userManager.GetPhoneNumberAsync(user), message);
            }

            return RedirectToAction(nameof(VerifyCode), new { Provider = model.SelectedProvider, ReturnUrl = model.ReturnUrl, RememberMe = model.RememberMe });
        }

        //
        // GET: /Account/VerifyCode
        [HttpGet]
        [AllowAnonymous]
        public async Task<IActionResult> VerifyCode(string provider, bool rememberMe, string returnUrl = null)
        {
            // Require that the user has already logged in via username/password or external login
            var user = await _signInManager.GetTwoFactorAuthenticationUserAsync();
            if (user == null)
            {
                return View("Error");
            }
            return View(new VerifyCodeViewModel { Provider = provider, ReturnUrl = returnUrl, RememberMe = rememberMe });
        }

        //
        // POST: /Account/VerifyCode
        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> VerifyCode(VerifyCodeViewModel model)
        {
            if (!ModelState.IsValid)
            {
                return View(model);
            }

            // The following code protects for brute force attacks against the two factor codes.
            // If a user enters incorrect codes for a specified amount of time then the user account
            // will be locked out for a specified amount of time.
            var result = await _signInManager.TwoFactorSignInAsync(model.Provider, model.Code, model.RememberMe, model.RememberBrowser);
            if (result.Succeeded)
            {
                return RedirectToLocal(model.ReturnUrl);
            }
            if (result.IsLockedOut)
            {
                _logger.LogWarning(7, "User account locked out.");
                return View("Lockout");
            }
            else
            {
                ModelState.AddModelError(string.Empty, "Invalid code.");
                return View(model);
            }
        }

        #region Helpers

        private void AddErrors(IdentityResult result)
        {
            foreach (var error in result.Errors)
            {
                ModelState.AddModelError(string.Empty, error.Description);
            }
        }

        private Task<ApplicationUser> GetCurrentUserAsync()
        {
            return _userManager.GetUserAsync(HttpContext.User);
        }

        private IActionResult RedirectToLocal(string returnUrl)
        {
            if (Url.IsLocalUrl(returnUrl))
            {
                return Redirect(returnUrl);
            }
            else
            {
                return RedirectToAction(nameof(HomeController.Index), "Home");
            }
        }

        #endregion
    }
}
