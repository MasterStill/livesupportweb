﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Hosting;
using Nest;
using SignalRMasterServer.Models;

namespace SignalRMasterServer
{
    public class Program
    {
        // public static Uri EsNode;
        // public static ConnectionSettings EsConfig;
        // public static ElasticClient EsClient;
        public static void Main(string[] args)
        {
            // EsNode = new Uri("http://localhost:9200/");
            // EsConfig = new ConnectionSettings(EsNode);
            // EsClient = new ElasticClient(EsConfig);
            // var settings = new IndexSettings { NumberOfReplicas = 1, NumberOfShards = 2 };
            // var indexConfig = new IndexState
            // {
            //     Settings = settings
            // };

            // if (!EsClient.IndexExists("website").Exists)
            // {
            //     EsClient.CreateIndex("website", c => c
            //     .InitializeUsing(indexConfig)
            //     .Mappings(m => m.Map<Website>(mp => mp.AutoMap())));
            // }


            // if (!EsClient.IndexExists("signaruser").Exists)
            // {
            //     EsClient.CreateIndex("signalruser", c => c
            //     .InitializeUsing(indexConfig)
            //     .Mappings(m => m.Map<SignalRUser>(mp => mp.AutoMap())));
            // }
            // if (!EsClient.IndexExists("chatmarco").Exists)
            // {
            //     EsClient.CreateIndex("chatmarco", c => c
            //     .InitializeUsing(indexConfig)
            //     .Mappings(m => m.Map<ChatMarco>(mp => mp.AutoMap())));
            // }
            // if (!EsClient.IndexExists("department").Exists)
            // {
            //     EsClient.CreateIndex("department", c => c
            //     .InitializeUsing(indexConfig)
            //     .Mappings(m => m.Map<Department>(mp => mp.AutoMap())));
            // }
            // if (!EsClient.IndexExists("advert").Exists)
            // {
            //     EsClient.CreateIndex("advert", c => c
            //     .InitializeUsing(indexConfig)
            //     .Mappings(m => m.Map<Advert>(mp => mp.AutoMap())));
            // }
            // if (!EsClient.IndexExists("emailinvitation").Exists)
            // {
            //     EsClient.CreateIndex("emailinvitation", c => c
            //     .InitializeUsing(indexConfig)
            //     .Mappings(m => m.Map<EmailInvitation>(mp => mp.AutoMap())));
            // }
            // if (!EsClient.IndexExists("culture").Exists)
            // {
            //     EsClient.CreateIndex("culture", c => c
            //     .InitializeUsing(indexConfig)
            //     .Mappings(m => m.Map<Culture>(mp => mp.AutoMap())));
            // }
            // if (!EsClient.IndexExists("widget").Exists)
            // {
            //     EsClient.CreateIndex("widget", c => c
            //     .InitializeUsing(indexConfig)
            //     .Mappings(m => m.Map<Widget>(mp => mp.AutoMap())));
            // }
            // if (!EsClient.IndexExists("widgetoptions").Exists)
            // {
            //     EsClient.CreateIndex("widgetoptions", c => c
            //     .InitializeUsing(indexConfig)
            //     .Mappings(m => m.Map<WidgetOptions>(mp => mp.AutoMap())));
            // }
            // if (!EsClient.IndexExists("chatmessage").Exists)
            // {
            //     EsClient.CreateIndex("chatmessage", c => c
            //     .InitializeUsing(indexConfig)
            //     .Mappings(m => m.Map<ChatMessage>(mp => mp.AutoMap())));
            // }
            // if (!EsClient.IndexExists("script").Exists)
            // {
            //     EsClient.CreateIndex("script", c => c
            //     .InitializeUsing(indexConfig)
            //     .Mappings(m => m.Map<Script>(mp => mp.AutoMap())));
            // }
            // if (!EsClient.IndexExists("chateventmessage").Exists)
            // {
            //     EsClient.CreateIndex("chateventmessage", c => c
            //     .InitializeUsing(indexConfig)
            //     .Mappings(m => m.Map<ChatEventMessage>(mp => mp.AutoMap())));
            // }
            // if (!EsClient.IndexExists("referral").Exists)
            // {
            //     EsClient.CreateIndex("referral", c => c
            //     .InitializeUsing(indexConfig)
            //     .Mappings(m => m.Map<Referral>(mp => mp.AutoMap())));
            // }
            // if (!EsClient.IndexExists("userwebsitedescription").Exists)
            // {
            //     EsClient.CreateIndex("userwebsitedescription", c => c
            //     .InitializeUsing(indexConfig)
            //     .Mappings(m => m.Map<UserWebsiteDescription>(mp => mp.AutoMap())));
            // }
            // //if (!EsClient.IndexExists("ipnfodb").Exists)
            // //{
            // //    EsClient.CreateIndex("ipinfodb", c => c
            // //    .InitializeUsing(indexConfig)
            // //    .Mappings(m => m.Map<IpInfoDb>(mp => mp.AutoMap())));
            // //}
            // if (!EsClient.IndexExists("plans").Exists)
            // {
            //     EsClient.CreateIndex("plans", c => c
            //     .InitializeUsing(indexConfig)
            //     .Mappings(m => m.Map<Plans>(mp => mp.AutoMap())));
            // }
            // if (!EsClient.IndexExists("log").Exists)
            // {
            //     EsClient.CreateIndex("log", c => c
            //     .InitializeUsing(indexConfig)
            //     .Mappings(m => m.Map<Log>(mp => mp.AutoMap())));
            // }
            // if (!EsClient.IndexExists("plandescription").Exists)
            // {
            //     EsClient.CreateIndex("plandescription", c => c
            //     .InitializeUsing(indexConfig)
            //     .Mappings(m => m.Map<PlanDescription>(mp => mp.AutoMap())));
            // }

            //if (!EsClient.IndexExists("reportdbs").Exists)
            //{
            //    EsClient.CreateIndex("reportdbs", c => c
            //    .InitializeUsing(indexConfig)
            //    .Mappings(m => m.Map<ReportDb>(mp => mp.AutoMap())));
            //}

            var host = new WebHostBuilder()
                 //.UseKestrel((options) =>
                 //   {
                 //       options.ThreadCount = 500;
                 //   })  
                 .UseKestrel()
                .UseUrls("http://localhost:5000")
                .UseContentRoot(Directory.GetCurrentDirectory())
                .UseIISIntegration()
                .UseStartup<Startup>()
                .Build();
            host.Run();
        }
        // .UseKestrel((options) =>
        //         {
        //             options.ThreadCount = 1000;s
        //         })
    }
}
