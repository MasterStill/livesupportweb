﻿using System;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore.Migrations;

namespace SignalRMasterServer.Migrations
{
    public partial class Pla111naaaa : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_PlanDescription_Plans_PlanWebsiteType_PlanTerm",
                table: "PlanDescription");

            migrationBuilder.DropPrimaryKey(
                name: "PK_Plans",
                table: "Plans");

            migrationBuilder.DropIndex(
                name: "IX_PlanDescription_PlanWebsiteType_PlanTerm",
                table: "PlanDescription");

            migrationBuilder.DropColumn(
                name: "Term",
                table: "Plans");

            migrationBuilder.DropColumn(
                name: "PlanTerm",
                table: "PlanDescription");

            migrationBuilder.RenameColumn(
                name: "Price",
                table: "Plans",
                newName: "YearlyPrice");

            migrationBuilder.AddColumn<double>(
                name: "MonthlyPrice",
                table: "Plans",
                nullable: false,
                defaultValue: 0.0);

            migrationBuilder.AddColumn<double>(
                name: "QuaterlyPrice",
                table: "Plans",
                nullable: false,
                defaultValue: 0.0);

            migrationBuilder.AddPrimaryKey(
                name: "PK_Plans",
                table: "Plans",
                column: "WebsiteType");

            migrationBuilder.CreateTable(
                name: "PlanPrice",
                columns: table => new
                {
                    PlanId = table.Column<int>(nullable: false),
                    Term = table.Column<int>(nullable: false),
                    PlanWebsiteType = table.Column<int>(nullable: true),
                    Price = table.Column<double>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_PlanPrice", x => new { x.PlanId, x.Term });
                    table.ForeignKey(
                        name: "FK_PlanPrice_Plans_PlanWebsiteType",
                        column: x => x.PlanWebsiteType,
                        principalTable: "Plans",
                        principalColumn: "WebsiteType",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateIndex(
                name: "IX_PlanDescription_PlanWebsiteType",
                table: "PlanDescription",
                column: "PlanWebsiteType");

            migrationBuilder.CreateIndex(
                name: "IX_PlanPrice_PlanWebsiteType",
                table: "PlanPrice",
                column: "PlanWebsiteType");

            migrationBuilder.AddForeignKey(
                name: "FK_PlanDescription_Plans_PlanWebsiteType",
                table: "PlanDescription",
                column: "PlanWebsiteType",
                principalTable: "Plans",
                principalColumn: "WebsiteType",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_PlanDescription_Plans_PlanWebsiteType",
                table: "PlanDescription");

            migrationBuilder.DropTable(
                name: "PlanPrice");

            migrationBuilder.DropPrimaryKey(
                name: "PK_Plans",
                table: "Plans");

            migrationBuilder.DropIndex(
                name: "IX_PlanDescription_PlanWebsiteType",
                table: "PlanDescription");

            migrationBuilder.DropColumn(
                name: "MonthlyPrice",
                table: "Plans");

            migrationBuilder.DropColumn(
                name: "QuaterlyPrice",
                table: "Plans");

            migrationBuilder.RenameColumn(
                name: "YearlyPrice",
                table: "Plans",
                newName: "Price");

            migrationBuilder.AddColumn<int>(
                name: "Term",
                table: "Plans",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<int>(
                name: "PlanTerm",
                table: "PlanDescription",
                nullable: true);

            migrationBuilder.AddPrimaryKey(
                name: "PK_Plans",
                table: "Plans",
                columns: new[] { "WebsiteType", "Term" });

            migrationBuilder.CreateIndex(
                name: "IX_PlanDescription_PlanWebsiteType_PlanTerm",
                table: "PlanDescription",
                columns: new[] { "PlanWebsiteType", "PlanTerm" });

            migrationBuilder.AddForeignKey(
                name: "FK_PlanDescription_Plans_PlanWebsiteType_PlanTerm",
                table: "PlanDescription",
                columns: new[] { "PlanWebsiteType", "PlanTerm" },
                principalTable: "Plans",
                principalColumns: new[] { "WebsiteType", "Term" },
                onDelete: ReferentialAction.Restrict);
        }
    }
}
