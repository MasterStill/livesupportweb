﻿using System;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore.Migrations;

namespace SignalRMasterServer.Migrations
{
    public partial class Plan : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_PlanDescription_Plans_PlanWebsiteType",
                table: "PlanDescription");

            migrationBuilder.DropPrimaryKey(
                name: "PK_Plans",
                table: "Plans");

            migrationBuilder.DropIndex(
                name: "IX_PlanDescription_PlanWebsiteType",
                table: "PlanDescription");

            migrationBuilder.AddColumn<int>(
                name: "Term",
                table: "Plans",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<int>(
                name: "PlanTerm",
                table: "PlanDescription",
                nullable: true);

            migrationBuilder.AddPrimaryKey(
                name: "PK_Plans",
                table: "Plans",
                columns: new[] { "WebsiteType", "Term" });

            migrationBuilder.CreateIndex(
                name: "IX_PlanDescription_PlanWebsiteType_PlanTerm",
                table: "PlanDescription",
                columns: new[] { "PlanWebsiteType", "PlanTerm" });

            migrationBuilder.AddForeignKey(
                name: "FK_PlanDescription_Plans_PlanWebsiteType_PlanTerm",
                table: "PlanDescription",
                columns: new[] { "PlanWebsiteType", "PlanTerm" },
                principalTable: "Plans",
                principalColumns: new[] { "WebsiteType", "Term" },
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_PlanDescription_Plans_PlanWebsiteType_PlanTerm",
                table: "PlanDescription");

            migrationBuilder.DropPrimaryKey(
                name: "PK_Plans",
                table: "Plans");

            migrationBuilder.DropIndex(
                name: "IX_PlanDescription_PlanWebsiteType_PlanTerm",
                table: "PlanDescription");

            migrationBuilder.DropColumn(
                name: "Term",
                table: "Plans");

            migrationBuilder.DropColumn(
                name: "PlanTerm",
                table: "PlanDescription");

            migrationBuilder.AddPrimaryKey(
                name: "PK_Plans",
                table: "Plans",
                column: "WebsiteType");

            migrationBuilder.CreateIndex(
                name: "IX_PlanDescription_PlanWebsiteType",
                table: "PlanDescription",
                column: "PlanWebsiteType");

            migrationBuilder.AddForeignKey(
                name: "FK_PlanDescription_Plans_PlanWebsiteType",
                table: "PlanDescription",
                column: "PlanWebsiteType",
                principalTable: "Plans",
                principalColumn: "WebsiteType",
                onDelete: ReferentialAction.Restrict);
        }
    }
}
