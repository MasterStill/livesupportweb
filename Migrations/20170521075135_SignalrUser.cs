﻿using System;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore.Migrations;

namespace SignalRMasterServer.Migrations
{
    public partial class SignalrUser : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Reports_Website_WebsiteId",
                table: "Reports");

            migrationBuilder.DropIndex(
                name: "IX_Reports_WebsiteId",
                table: "Reports");

            migrationBuilder.AddColumn<string>(
                name: "GroupId",
                table: "SignalRUser",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "GroupId",
                table: "SignalRUser");

            migrationBuilder.CreateIndex(
                name: "IX_Reports_WebsiteId",
                table: "Reports",
                column: "WebsiteId");

            migrationBuilder.AddForeignKey(
                name: "FK_Reports_Website_WebsiteId",
                table: "Reports",
                column: "WebsiteId",
                principalTable: "Website",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }
    }
}
