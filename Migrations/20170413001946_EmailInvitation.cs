﻿using System;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore.Migrations;

namespace SignalRMasterServer.Migrations
{
    public partial class EmailInvitation : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "AddedById",
                table: "EmailInvitation",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<int>(
                name: "AddeddById",
                table: "EmailInvitation",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_EmailInvitation_AddeddById",
                table: "EmailInvitation",
                column: "AddeddById");

            migrationBuilder.AddForeignKey(
                name: "FK_EmailInvitation_AspNetUsers_AddeddById",
                table: "EmailInvitation",
                column: "AddeddById",
                principalTable: "AspNetUsers",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_EmailInvitation_AspNetUsers_AddeddById",
                table: "EmailInvitation");

            migrationBuilder.DropIndex(
                name: "IX_EmailInvitation_AddeddById",
                table: "EmailInvitation");

            migrationBuilder.DropColumn(
                name: "AddedById",
                table: "EmailInvitation");

            migrationBuilder.DropColumn(
                name: "AddeddById",
                table: "EmailInvitation");
        }
    }
}
