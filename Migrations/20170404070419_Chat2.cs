﻿using System;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore.Migrations;

namespace SignalRMasterServer.Migrations
{
    public partial class Chat2 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateIndex(
                name: "IX_ChatEventMessage_ChatMessageId",
                table: "ChatEventMessage",
                column: "ChatMessageId");

            migrationBuilder.AddForeignKey(
                name: "FK_ChatEventMessage_ChatMessage_ChatMessageId",
                table: "ChatEventMessage",
                column: "ChatMessageId",
                principalTable: "ChatMessage",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_ChatEventMessage_ChatMessage_ChatMessageId",
                table: "ChatEventMessage");

            migrationBuilder.DropIndex(
                name: "IX_ChatEventMessage_ChatMessageId",
                table: "ChatEventMessage");
        }
    }
}
