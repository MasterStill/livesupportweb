﻿using System;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore.Migrations;
using Microsoft.EntityFrameworkCore.Metadata;

namespace SignalRMasterServer.Migrations
{
    public partial class ChatMessageandChatEventMessageremovedfromdB : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_IpInfoDb_ChatMessage_ChatMessageId",
                table: "IpInfoDb");

            migrationBuilder.DropForeignKey(
                name: "FK_SignalRUser_ChatMessage_ChatMessageId",
                table: "SignalRUser");

            migrationBuilder.DropTable(
                name: "ChatEventMessage");

            migrationBuilder.DropTable(
                name: "ChatMessage");

            migrationBuilder.DropIndex(
                name: "IX_SignalRUser_ChatMessageId",
                table: "SignalRUser");

            migrationBuilder.DropIndex(
                name: "IX_IpInfoDb_ChatMessageId",
                table: "IpInfoDb");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "ChatMessage",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    GroupId = table.Column<string>(nullable: true),
                    WebsiteId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ChatMessage", x => x.Id);
                    table.ForeignKey(
                        name: "FK_ChatMessage_Website_WebsiteId",
                        column: x => x.WebsiteId,
                        principalTable: "Website",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "ChatEventMessage",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    AdminId = table.Column<int>(nullable: true),
                    ChatMessageId = table.Column<int>(nullable: false),
                    DateTime = table.Column<DateTime>(nullable: false),
                    Message = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ChatEventMessage", x => x.Id);
                    table.ForeignKey(
                        name: "FK_ChatEventMessage_AspNetUsers_AdminId",
                        column: x => x.AdminId,
                        principalTable: "AspNetUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_ChatEventMessage_ChatMessage_ChatMessageId",
                        column: x => x.ChatMessageId,
                        principalTable: "ChatMessage",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateIndex(
                name: "IX_SignalRUser_ChatMessageId",
                table: "SignalRUser",
                column: "ChatMessageId");

            migrationBuilder.CreateIndex(
                name: "IX_IpInfoDb_ChatMessageId",
                table: "IpInfoDb",
                column: "ChatMessageId");

            migrationBuilder.CreateIndex(
                name: "IX_ChatEventMessage_AdminId",
                table: "ChatEventMessage",
                column: "AdminId");

            migrationBuilder.CreateIndex(
                name: "IX_ChatEventMessage_ChatMessageId",
                table: "ChatEventMessage",
                column: "ChatMessageId");

            migrationBuilder.CreateIndex(
                name: "IX_ChatMessage_WebsiteId",
                table: "ChatMessage",
                column: "WebsiteId");

            migrationBuilder.AddForeignKey(
                name: "FK_IpInfoDb_ChatMessage_ChatMessageId",
                table: "IpInfoDb",
                column: "ChatMessageId",
                principalTable: "ChatMessage",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_SignalRUser_ChatMessage_ChatMessageId",
                table: "SignalRUser",
                column: "ChatMessageId",
                principalTable: "ChatMessage",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }
    }
}
