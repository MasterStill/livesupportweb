﻿using System;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore.Migrations;

namespace SignalRMasterServer.Migrations
{
    public partial class Chat5 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_ChatMessage_Website_WebsiteId",
                table: "ChatMessage");

            migrationBuilder.DropIndex(
                name: "IX_ChatMessage_WebsiteId",
                table: "ChatMessage");

            migrationBuilder.DropColumn(
                name: "WebsiteId",
                table: "ChatMessage");

            migrationBuilder.AddColumn<int>(
                name: "WebsiteId",
                table: "ChatEventMessage",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.CreateIndex(
                name: "IX_ChatEventMessage_WebsiteId",
                table: "ChatEventMessage",
                column: "WebsiteId");

            migrationBuilder.AddForeignKey(
                name: "FK_ChatEventMessage_Website_WebsiteId",
                table: "ChatEventMessage",
                column: "WebsiteId",
                principalTable: "Website",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_ChatEventMessage_Website_WebsiteId",
                table: "ChatEventMessage");

            migrationBuilder.DropIndex(
                name: "IX_ChatEventMessage_WebsiteId",
                table: "ChatEventMessage");

            migrationBuilder.DropColumn(
                name: "WebsiteId",
                table: "ChatEventMessage");

            migrationBuilder.AddColumn<int>(
                name: "WebsiteId",
                table: "ChatMessage",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.CreateIndex(
                name: "IX_ChatMessage_WebsiteId",
                table: "ChatMessage",
                column: "WebsiteId");

            migrationBuilder.AddForeignKey(
                name: "FK_ChatMessage_Website_WebsiteId",
                table: "ChatMessage",
                column: "WebsiteId",
                principalTable: "Website",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }
    }
}
