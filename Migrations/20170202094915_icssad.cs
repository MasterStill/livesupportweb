﻿using System;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore.Migrations;

namespace SignalRMasterServer.Migrations
{
    public partial class icssad : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AlterColumn<int>(
                name: "DepartmentId",
                table: "ChatMarco",
                nullable: true,
                oldClrType: typeof(int));

            migrationBuilder.CreateIndex(
                name: "IX_ChatMarco_WebsiteId",
                table: "ChatMarco",
                column: "WebsiteId");

            migrationBuilder.AddForeignKey(
                name: "FK_ChatMarco_Website_WebsiteId",
                table: "ChatMarco",
                column: "WebsiteId",
                principalTable: "Website",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_ChatMarco_Website_WebsiteId",
                table: "ChatMarco");

            migrationBuilder.DropIndex(
                name: "IX_ChatMarco_WebsiteId",
                table: "ChatMarco");

            migrationBuilder.AlterColumn<int>(
                name: "DepartmentId",
                table: "ChatMarco",
                nullable: false,
                oldClrType: typeof(int),
                oldNullable: true);
        }
    }
}
