﻿using System;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore.Migrations;

namespace SignalRMasterServer.Migrations
{
    public partial class Chat3 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_ChatMessage_Website_WebsiteId1",
                table: "ChatMessage");

            migrationBuilder.DropForeignKey(
                name: "FK_ChatMessage_Website_WebsiteIdId",
                table: "ChatMessage");

            migrationBuilder.DropIndex(
                name: "IX_ChatMessage_WebsiteId1",
                table: "ChatMessage");

            migrationBuilder.DropIndex(
                name: "IX_ChatMessage_WebsiteIdId",
                table: "ChatMessage");

            migrationBuilder.DropColumn(
                name: "WebsiteId1",
                table: "ChatMessage");

            migrationBuilder.DropColumn(
                name: "WebsiteIdId",
                table: "ChatMessage");

            migrationBuilder.AddColumn<int>(
                name: "WebsiteId",
                table: "ChatMessage",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.CreateIndex(
                name: "IX_ChatMessage_WebsiteId",
                table: "ChatMessage",
                column: "WebsiteId");

            migrationBuilder.AddForeignKey(
                name: "FK_ChatMessage_Website_WebsiteId",
                table: "ChatMessage",
                column: "WebsiteId",
                principalTable: "Website",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_ChatMessage_Website_WebsiteId",
                table: "ChatMessage");

            migrationBuilder.DropIndex(
                name: "IX_ChatMessage_WebsiteId",
                table: "ChatMessage");

            migrationBuilder.DropColumn(
                name: "WebsiteId",
                table: "ChatMessage");

            migrationBuilder.AddColumn<int>(
                name: "WebsiteId1",
                table: "ChatMessage",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "WebsiteIdId",
                table: "ChatMessage",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_ChatMessage_WebsiteId1",
                table: "ChatMessage",
                column: "WebsiteId1");

            migrationBuilder.CreateIndex(
                name: "IX_ChatMessage_WebsiteIdId",
                table: "ChatMessage",
                column: "WebsiteIdId");

            migrationBuilder.AddForeignKey(
                name: "FK_ChatMessage_Website_WebsiteId1",
                table: "ChatMessage",
                column: "WebsiteId1",
                principalTable: "Website",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_ChatMessage_Website_WebsiteIdId",
                table: "ChatMessage",
                column: "WebsiteIdId",
                principalTable: "Website",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }
    }
}
