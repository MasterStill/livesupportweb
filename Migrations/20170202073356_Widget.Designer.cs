﻿using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;
using SignalRMasterServer.Data;
using SignalRMasterServer.Models;

namespace SignalRMasterServer.Migrations
{
    [DbContext(typeof(AppDbContext))]
    [Migration("20170202073356_Widget")]
    partial class Widget
    {
        protected override void BuildTargetModel(ModelBuilder modelBuilder)
        {
            modelBuilder
                .HasAnnotation("ProductVersion", "1.1.0-rtm-22752")
                .HasAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

            modelBuilder.Entity("SignalRMasterServer.Models.ApplicationRole", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("ConcurrencyStamp")
                        .IsConcurrencyToken();

                    b.Property<string>("Name")
                        .HasMaxLength(256);

                    b.Property<string>("NormalizedName")
                        .HasMaxLength(256);

                    b.HasKey("Id");

                    b.HasIndex("NormalizedName")
                        .IsUnique()
                        .HasName("RoleNameIndex");

                    b.ToTable("AspNetRoles");
                });

            modelBuilder.Entity("SignalRMasterServer.Models.ApplicationRoleClaim", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("ClaimType");

                    b.Property<string>("ClaimValue");

                    b.Property<int>("RoleId");

                    b.HasKey("Id");

                    b.HasIndex("RoleId");

                    b.ToTable("AspNetRoleClaims");
                });

            modelBuilder.Entity("SignalRMasterServer.Models.ApplicationUser", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<int>("AccessFailedCount");

                    b.Property<string>("ConcurrencyStamp")
                        .IsConcurrencyToken();

                    b.Property<string>("Email")
                        .HasMaxLength(256);

                    b.Property<bool>("EmailConfirmed");

                    b.Property<string>("Firstname");

                    b.Property<string>("Image");

                    b.Property<string>("LastName");

                    b.Property<bool>("LockoutEnabled");

                    b.Property<DateTimeOffset?>("LockoutEnd");

                    b.Property<string>("NormalizedEmail")
                        .HasMaxLength(256);

                    b.Property<string>("NormalizedUserName")
                        .HasMaxLength(256);

                    b.Property<string>("PasswordHash");

                    b.Property<string>("PhoneNumber");

                    b.Property<bool>("PhoneNumberConfirmed");

                    b.Property<string>("SecurityStamp");

                    b.Property<Guid>("SignalrGuid");

                    b.Property<bool>("TwoFactorEnabled");

                    b.Property<string>("UserName")
                        .HasMaxLength(256);

                    b.HasKey("Id");

                    b.HasIndex("NormalizedEmail")
                        .HasName("EmailIndex");

                    b.HasIndex("NormalizedUserName")
                        .IsUnique()
                        .HasName("UserNameIndex");

                    b.ToTable("AspNetUsers");
                });

            modelBuilder.Entity("SignalRMasterServer.Models.ApplicationUserClaim", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("ClaimType");

                    b.Property<string>("ClaimValue");

                    b.Property<int>("UserId");

                    b.HasKey("Id");

                    b.HasIndex("UserId");

                    b.ToTable("AspNetUserClaims");
                });

            modelBuilder.Entity("SignalRMasterServer.Models.ApplicationUserLogin", b =>
                {
                    b.Property<string>("LoginProvider");

                    b.Property<string>("ProviderKey");

                    b.Property<string>("ProviderDisplayName");

                    b.Property<int>("UserId");

                    b.HasKey("LoginProvider", "ProviderKey");

                    b.HasIndex("UserId");

                    b.ToTable("AspNetUserLogins");
                });

            modelBuilder.Entity("SignalRMasterServer.Models.ApplicationUserRole", b =>
                {
                    b.Property<int>("RoleId");

                    b.Property<int>("UserId");

                    b.Property<int>("WebsiteId");

                    b.HasKey("RoleId", "UserId", "WebsiteId");

                    b.HasIndex("WebsiteId");

                    b.ToTable("ApplicationUserRole");
                });

            modelBuilder.Entity("SignalRMasterServer.Models.ApplicationUserToken", b =>
                {
                    b.Property<int>("UserId");

                    b.Property<string>("LoginProvider");

                    b.Property<string>("Name");

                    b.Property<string>("Value");

                    b.HasKey("UserId", "LoginProvider", "Name");

                    b.ToTable("AspNetUserTokens");
                });

            modelBuilder.Entity("SignalRMasterServer.Models.ChatMarco", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<int?>("CultureId");

                    b.Property<int>("DepartmentId");

                    b.Property<string>("KeyBoardShortcut");

                    b.Property<int>("MarcoType");

                    b.Property<string>("Name");

                    b.Property<string>("TextorURL");

                    b.Property<int>("WebsiteId");

                    b.HasKey("Id");

                    b.HasIndex("DepartmentId");

                    b.ToTable("ChatMarco");
                });

            modelBuilder.Entity("SignalRMasterServer.Models.Culture", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("Name");

                    b.HasKey("Id");

                    b.ToTable("Culture");
                });

            modelBuilder.Entity("SignalRMasterServer.Models.Department", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<int?>("CultureId");

                    b.Property<string>("Name");

                    b.Property<int>("WebsiteId");

                    b.HasKey("Id");

                    b.HasIndex("CultureId");

                    b.HasIndex("WebsiteId");

                    b.ToTable("Department");
                });

            modelBuilder.Entity("SignalRMasterServer.Models.SignalRUser", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("ConnectionId");

                    b.Property<string>("Email");

                    b.Property<string>("PhoneNumber");

                    b.Property<int>("SiteId");

                    b.HasKey("Id");

                    b.HasIndex("SiteId");

                    b.ToTable("SignalRUser");
                });

            modelBuilder.Entity("SignalRMasterServer.Models.Website", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("Alias");

                    b.Property<string>("CopyrightNotice");

                    b.Property<int>("CreatedById");

                    b.Property<bool>("Delflag");

                    b.Property<string>("Logo");

                    b.Property<string>("Name");

                    b.Property<bool>("PaidWebsite");

                    b.Property<int>("WebstieType");

                    b.Property<int>("WidgetId");

                    b.HasKey("Id");

                    b.HasIndex("CreatedById");

                    b.HasIndex("WidgetId");

                    b.ToTable("Website");
                });

            modelBuilder.Entity("SignalRMasterServer.Models.Widget", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("Name");

                    b.HasKey("Id");

                    b.ToTable("Widgets");
                });

            modelBuilder.Entity("SignalRMasterServer.Models.ApplicationRoleClaim", b =>
                {
                    b.HasOne("SignalRMasterServer.Models.ApplicationRole")
                        .WithMany("Claims")
                        .HasForeignKey("RoleId");
                });

            modelBuilder.Entity("SignalRMasterServer.Models.ApplicationUserClaim", b =>
                {
                    b.HasOne("SignalRMasterServer.Models.ApplicationUser")
                        .WithMany("Claims")
                        .HasForeignKey("UserId");
                });

            modelBuilder.Entity("SignalRMasterServer.Models.ApplicationUserLogin", b =>
                {
                    b.HasOne("SignalRMasterServer.Models.ApplicationUser")
                        .WithMany("Logins")
                        .HasForeignKey("UserId");
                });

            modelBuilder.Entity("SignalRMasterServer.Models.ApplicationUserRole", b =>
                {
                    b.HasOne("SignalRMasterServer.Models.Website", "Website")
                        .WithMany()
                        .HasForeignKey("WebsiteId")
                        .OnDelete(DeleteBehavior.Cascade);
                });

            modelBuilder.Entity("SignalRMasterServer.Models.ChatMarco", b =>
                {
                    b.HasOne("SignalRMasterServer.Models.Department", "Department")
                        .WithMany("Marco")
                        .HasForeignKey("DepartmentId");
                });

            modelBuilder.Entity("SignalRMasterServer.Models.Department", b =>
                {
                    b.HasOne("SignalRMasterServer.Models.Culture", "Culture")
                        .WithMany()
                        .HasForeignKey("CultureId");

                    b.HasOne("SignalRMasterServer.Models.Website", "Website")
                        .WithMany("Department")
                        .HasForeignKey("WebsiteId");
                });

            modelBuilder.Entity("SignalRMasterServer.Models.SignalRUser", b =>
                {
                    b.HasOne("SignalRMasterServer.Models.Website", "Site")
                        .WithMany()
                        .HasForeignKey("SiteId");
                });

            modelBuilder.Entity("SignalRMasterServer.Models.Website", b =>
                {
                    b.HasOne("SignalRMasterServer.Models.ApplicationUser", "CreatedBy")
                        .WithMany()
                        .HasForeignKey("CreatedById");

                    b.HasOne("SignalRMasterServer.Models.Widget", "Widget")
                        .WithMany()
                        .HasForeignKey("WidgetId");
                });
        }
    }
}
