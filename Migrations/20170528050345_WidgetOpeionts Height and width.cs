﻿using System;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore.Migrations;
using Microsoft.EntityFrameworkCore.Metadata;

namespace SignalRMasterServer.Migrations
{
    public partial class WidgetOpeiontsHeightandwidth : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "IpInfoDb");

            migrationBuilder.AddColumn<string>(
                name: "WidgetHeight",
                table: "WidgetOptions",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "WidgetWidth",
                table: "WidgetOptions",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "WidgetHeight",
                table: "WidgetOptions");

            migrationBuilder.DropColumn(
                name: "WidgetWidth",
                table: "WidgetOptions");

            migrationBuilder.CreateTable(
                name: "IpInfoDb",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    ChatMessageId = table.Column<int>(nullable: false),
                    Cityname = table.Column<string>(nullable: true),
                    Countrycode = table.Column<string>(nullable: true),
                    Countryname = table.Column<string>(nullable: true),
                    Ipaddress = table.Column<string>(nullable: true),
                    Latitude = table.Column<string>(nullable: true),
                    Longitude = table.Column<string>(nullable: true),
                    Regionname = table.Column<string>(nullable: true),
                    Timezone = table.Column<string>(nullable: true),
                    Zipcode = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_IpInfoDb", x => x.Id);
                });
        }
    }
}
