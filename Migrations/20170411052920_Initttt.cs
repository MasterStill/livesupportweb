﻿using System;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore.Migrations;

namespace SignalRMasterServer.Migrations
{
    public partial class Initttt : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "ChatMessageId",
                table: "SignalRUser",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<string>(
                name: "FullName",
                table: "SignalRUser",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_SignalRUser_ChatMessageId",
                table: "SignalRUser",
                column: "ChatMessageId");

            migrationBuilder.AddForeignKey(
                name: "FK_SignalRUser_ChatMessage_ChatMessageId",
                table: "SignalRUser",
                column: "ChatMessageId",
                principalTable: "ChatMessage",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_SignalRUser_ChatMessage_ChatMessageId",
                table: "SignalRUser");

            migrationBuilder.DropIndex(
                name: "IX_SignalRUser_ChatMessageId",
                table: "SignalRUser");

            migrationBuilder.DropColumn(
                name: "ChatMessageId",
                table: "SignalRUser");

            migrationBuilder.DropColumn(
                name: "FullName",
                table: "SignalRUser");
        }
    }
}
