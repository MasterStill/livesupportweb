﻿using System;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore.Migrations;

namespace SignalRMasterServer.Migrations
{
    public partial class aaaa : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_SignalRUser_Website_SiteId",
                table: "SignalRUser");

            migrationBuilder.DropIndex(
                name: "IX_SignalRUser_SiteId",
                table: "SignalRUser");

            migrationBuilder.DropColumn(
                name: "SiteId",
                table: "SignalRUser");

            migrationBuilder.RenameColumn(
                name: "ConnectionId",
                table: "SignalRUser",
                newName: "Avatar");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.RenameColumn(
                name: "Avatar",
                table: "SignalRUser",
                newName: "ConnectionId");

            migrationBuilder.AddColumn<int>(
                name: "SiteId",
                table: "SignalRUser",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.CreateIndex(
                name: "IX_SignalRUser_SiteId",
                table: "SignalRUser",
                column: "SiteId");

            migrationBuilder.AddForeignKey(
                name: "FK_SignalRUser_Website_SiteId",
                table: "SignalRUser",
                column: "SiteId",
                principalTable: "Website",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }
    }
}
