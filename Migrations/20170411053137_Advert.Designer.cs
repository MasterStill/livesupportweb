﻿using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;
using SignalRMasterServer.Data;
using SignalRMasterServer.Models;

namespace SignalRMasterServer.Migrations
{
    [DbContext(typeof(AppDbContext))]
    [Migration("20170411053137_Advert")]
    partial class Advert
    {
        protected override void BuildTargetModel(ModelBuilder modelBuilder)
        {
            modelBuilder
                .HasAnnotation("ProductVersion", "1.1.0-rtm-22752")
                .HasAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

            modelBuilder.Entity("SignalRMasterServer.Models.Advert", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("Script");

                    b.HasKey("Id");

                    b.ToTable("Advert");
                });

            modelBuilder.Entity("SignalRMasterServer.Models.ApplicationRole", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("ConcurrencyStamp")
                        .IsConcurrencyToken();

                    b.Property<string>("Name")
                        .HasMaxLength(256);

                    b.Property<string>("NormalizedName")
                        .HasMaxLength(256);

                    b.HasKey("Id");

                    b.HasIndex("NormalizedName")
                        .IsUnique()
                        .HasName("RoleNameIndex");

                    b.ToTable("AspNetRoles");
                });

            modelBuilder.Entity("SignalRMasterServer.Models.ApplicationRoleClaim", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("ClaimType");

                    b.Property<string>("ClaimValue");

                    b.Property<int>("RoleId");

                    b.HasKey("Id");

                    b.HasIndex("RoleId");

                    b.ToTable("AspNetRoleClaims");
                });

            modelBuilder.Entity("SignalRMasterServer.Models.ApplicationUser", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<int>("AccessFailedCount");

                    b.Property<string>("AdminGuid");

                    b.Property<string>("ConcurrencyStamp")
                        .IsConcurrencyToken();

                    b.Property<string>("Email")
                        .HasMaxLength(256);

                    b.Property<bool>("EmailConfirmed");

                    b.Property<string>("Firstname");

                    b.Property<string>("Image");

                    b.Property<string>("LastName");

                    b.Property<bool>("LockoutEnabled");

                    b.Property<DateTimeOffset?>("LockoutEnd");

                    b.Property<string>("NormalizedEmail")
                        .HasMaxLength(256);

                    b.Property<string>("NormalizedUserName")
                        .HasMaxLength(256);

                    b.Property<string>("PasswordHash");

                    b.Property<string>("PhoneNumber");

                    b.Property<bool>("PhoneNumberConfirmed");

                    b.Property<string>("SecurityStamp");

                    b.Property<Guid>("SignalrGuid");

                    b.Property<bool>("TwoFactorEnabled");

                    b.Property<string>("UserName")
                        .HasMaxLength(256);

                    b.HasKey("Id");

                    b.HasIndex("NormalizedEmail")
                        .HasName("EmailIndex");

                    b.HasIndex("NormalizedUserName")
                        .IsUnique()
                        .HasName("UserNameIndex");

                    b.ToTable("AspNetUsers");
                });

            modelBuilder.Entity("SignalRMasterServer.Models.ApplicationUserClaim", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("ClaimType");

                    b.Property<string>("ClaimValue");

                    b.Property<int>("UserId");

                    b.HasKey("Id");

                    b.HasIndex("UserId");

                    b.ToTable("AspNetUserClaims");
                });

            modelBuilder.Entity("SignalRMasterServer.Models.ApplicationUserLogin", b =>
                {
                    b.Property<string>("LoginProvider");

                    b.Property<string>("ProviderKey");

                    b.Property<string>("ProviderDisplayName");

                    b.Property<int>("UserId");

                    b.HasKey("LoginProvider", "ProviderKey");

                    b.HasIndex("UserId");

                    b.ToTable("AspNetUserLogins");
                });

            modelBuilder.Entity("SignalRMasterServer.Models.ApplicationUserRole", b =>
                {
                    b.Property<int>("RoleId");

                    b.Property<int>("UserId");

                    b.Property<int>("WebsiteId");

                    b.HasKey("RoleId", "UserId", "WebsiteId");

                    b.HasIndex("UserId");

                    b.HasIndex("WebsiteId");

                    b.ToTable("ApplicationUserRole");
                });

            modelBuilder.Entity("SignalRMasterServer.Models.ApplicationUserToken", b =>
                {
                    b.Property<int>("UserId");

                    b.Property<string>("LoginProvider");

                    b.Property<string>("Name");

                    b.Property<string>("Value");

                    b.HasKey("UserId", "LoginProvider", "Name");

                    b.ToTable("AspNetUserTokens");
                });

            modelBuilder.Entity("SignalRMasterServer.Models.ChatEventMessage", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<int?>("AdminId");

                    b.Property<int>("ChatMessageId");

                    b.Property<DateTime>("DateTime");

                    b.Property<string>("Message");

                    b.HasKey("Id");

                    b.HasIndex("AdminId");

                    b.HasIndex("ChatMessageId");

                    b.ToTable("ChatEventMessage");
                });

            modelBuilder.Entity("SignalRMasterServer.Models.ChatMarco", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<int?>("CultureId");

                    b.Property<int?>("DepartmentId");

                    b.Property<string>("KeyBoardShortcut");

                    b.Property<int>("MarcoType");

                    b.Property<string>("Name");

                    b.Property<string>("TextorURL");

                    b.Property<int>("WebsiteId");

                    b.HasKey("Id");

                    b.HasIndex("DepartmentId");

                    b.HasIndex("WebsiteId");

                    b.ToTable("ChatMarco");
                });

            modelBuilder.Entity("SignalRMasterServer.Models.ChatMessage", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("FullName");

                    b.Property<string>("GroupId");

                    b.Property<int>("WebsiteId");

                    b.HasKey("Id");

                    b.HasIndex("WebsiteId");

                    b.ToTable("ChatMessage");
                });

            modelBuilder.Entity("SignalRMasterServer.Models.Culture", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("Name");

                    b.HasKey("Id");

                    b.ToTable("Culture");
                });

            modelBuilder.Entity("SignalRMasterServer.Models.Department", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<int?>("CultureId");

                    b.Property<string>("Name");

                    b.Property<int>("WebsiteId");

                    b.HasKey("Id");

                    b.HasIndex("CultureId");

                    b.HasIndex("WebsiteId");

                    b.ToTable("Department");
                });

            modelBuilder.Entity("SignalRMasterServer.Models.SignalRUser", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<int>("ChatMessageId");

                    b.Property<string>("ConnectionId");

                    b.Property<string>("Email");

                    b.Property<string>("FullName");

                    b.Property<string>("PhoneNumber");

                    b.Property<int>("SiteId");

                    b.HasKey("Id");

                    b.HasIndex("ChatMessageId");

                    b.HasIndex("SiteId");

                    b.ToTable("SignalRUser");
                });

            modelBuilder.Entity("SignalRMasterServer.Models.Website", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("Alias");

                    b.Property<string>("CopyrightNotice");

                    b.Property<int>("CreatedById");

                    b.Property<bool>("Delflag");

                    b.Property<string>("Logo");

                    b.Property<string>("Name");

                    b.Property<bool>("PaidWebsite");

                    b.Property<int>("WebstieType");

                    b.Property<int>("WidgetId");

                    b.HasKey("Id");

                    b.HasIndex("CreatedById");

                    b.HasIndex("WidgetId");

                    b.ToTable("Website");
                });

            modelBuilder.Entity("SignalRMasterServer.Models.Widget", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("Image");

                    b.Property<string>("Link");

                    b.Property<string>("Name");

                    b.HasKey("Id");

                    b.ToTable("Widgets");
                });

            modelBuilder.Entity("SignalRMasterServer.Models.WidgetOptions", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<bool>("EnableSSL");

                    b.Property<string>("OfflineText");

                    b.Property<string>("OnlineText");

                    b.Property<string>("TextSize");

                    b.Property<string>("TextStyle");

                    b.Property<int>("WebsiteId");

                    b.Property<string>("WidgetColor");

                    b.Property<string>("WidgetText");

                    b.Property<bool>("WidgetVisible");

                    b.HasKey("Id");

                    b.HasIndex("WebsiteId");

                    b.ToTable("WidgetOptions");
                });

            modelBuilder.Entity("SignalRMasterServer.Models.ApplicationRoleClaim", b =>
                {
                    b.HasOne("SignalRMasterServer.Models.ApplicationRole")
                        .WithMany("Claims")
                        .HasForeignKey("RoleId");
                });

            modelBuilder.Entity("SignalRMasterServer.Models.ApplicationUserClaim", b =>
                {
                    b.HasOne("SignalRMasterServer.Models.ApplicationUser")
                        .WithMany("Claims")
                        .HasForeignKey("UserId");
                });

            modelBuilder.Entity("SignalRMasterServer.Models.ApplicationUserLogin", b =>
                {
                    b.HasOne("SignalRMasterServer.Models.ApplicationUser")
                        .WithMany("Logins")
                        .HasForeignKey("UserId");
                });

            modelBuilder.Entity("SignalRMasterServer.Models.ApplicationUserRole", b =>
                {
                    b.HasOne("SignalRMasterServer.Models.ApplicationRole", "Role")
                        .WithMany("Users")
                        .HasForeignKey("RoleId")
                        .OnDelete(DeleteBehavior.Cascade);

                    b.HasOne("SignalRMasterServer.Models.ApplicationUser", "User")
                        .WithMany("Roles")
                        .HasForeignKey("UserId")
                        .OnDelete(DeleteBehavior.Cascade);

                    b.HasOne("SignalRMasterServer.Models.Website", "Website")
                        .WithMany("Roles")
                        .HasForeignKey("WebsiteId")
                        .OnDelete(DeleteBehavior.Cascade);
                });

            modelBuilder.Entity("SignalRMasterServer.Models.ChatEventMessage", b =>
                {
                    b.HasOne("SignalRMasterServer.Models.ApplicationUser", "Admin")
                        .WithMany()
                        .HasForeignKey("AdminId");

                    b.HasOne("SignalRMasterServer.Models.ChatMessage", "ChatMessage")
                        .WithMany()
                        .HasForeignKey("ChatMessageId");
                });

            modelBuilder.Entity("SignalRMasterServer.Models.ChatMarco", b =>
                {
                    b.HasOne("SignalRMasterServer.Models.Department")
                        .WithMany("Marco")
                        .HasForeignKey("DepartmentId");

                    b.HasOne("SignalRMasterServer.Models.Website", "Website")
                        .WithMany()
                        .HasForeignKey("WebsiteId");
                });

            modelBuilder.Entity("SignalRMasterServer.Models.ChatMessage", b =>
                {
                    b.HasOne("SignalRMasterServer.Models.Website", "Website")
                        .WithMany()
                        .HasForeignKey("WebsiteId");
                });

            modelBuilder.Entity("SignalRMasterServer.Models.Department", b =>
                {
                    b.HasOne("SignalRMasterServer.Models.Culture", "Culture")
                        .WithMany()
                        .HasForeignKey("CultureId");

                    b.HasOne("SignalRMasterServer.Models.Website", "Website")
                        .WithMany("Department")
                        .HasForeignKey("WebsiteId");
                });

            modelBuilder.Entity("SignalRMasterServer.Models.SignalRUser", b =>
                {
                    b.HasOne("SignalRMasterServer.Models.ChatMessage", "ChatMessage")
                        .WithMany()
                        .HasForeignKey("ChatMessageId");

                    b.HasOne("SignalRMasterServer.Models.Website", "Site")
                        .WithMany()
                        .HasForeignKey("SiteId");
                });

            modelBuilder.Entity("SignalRMasterServer.Models.Website", b =>
                {
                    b.HasOne("SignalRMasterServer.Models.ApplicationUser", "CreatedBy")
                        .WithMany()
                        .HasForeignKey("CreatedById");

                    b.HasOne("SignalRMasterServer.Models.Widget", "Widget")
                        .WithMany()
                        .HasForeignKey("WidgetId");
                });

            modelBuilder.Entity("SignalRMasterServer.Models.WidgetOptions", b =>
                {
                    b.HasOne("SignalRMasterServer.Models.Website", "Website")
                        .WithMany()
                        .HasForeignKey("WebsiteId");
                });
        }
    }
}
