﻿using System;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore.Migrations;

namespace SignalRMasterServer.Migrations
{
    public partial class Plans1 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "PlanWebsiteType",
                table: "PlanDescription",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_PlanDescription_PlanWebsiteType",
                table: "PlanDescription",
                column: "PlanWebsiteType");

            migrationBuilder.AddForeignKey(
                name: "FK_PlanDescription_Plans_PlanWebsiteType",
                table: "PlanDescription",
                column: "PlanWebsiteType",
                principalTable: "Plans",
                principalColumn: "WebsiteType",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_PlanDescription_Plans_PlanWebsiteType",
                table: "PlanDescription");

            migrationBuilder.DropIndex(
                name: "IX_PlanDescription_PlanWebsiteType",
                table: "PlanDescription");

            migrationBuilder.DropColumn(
                name: "PlanWebsiteType",
                table: "PlanDescription");
        }
    }
}
