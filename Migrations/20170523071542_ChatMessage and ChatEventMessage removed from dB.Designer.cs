﻿using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;
using SignalRMasterServer.Data;
using SignalRMasterServer.Models;

namespace SignalRMasterServer.Migrations
{
    [DbContext(typeof(AppDbContext))]
    [Migration("20170523071542_ChatMessage and ChatEventMessage removed from dB")]
    partial class ChatMessageandChatEventMessageremovedfromdB
    {
        protected override void BuildTargetModel(ModelBuilder modelBuilder)
        {
            modelBuilder
                .HasAnnotation("ProductVersion", "1.1.1")
                .HasAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

            modelBuilder.Entity("SignalRMasterServer.Models.Advert", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("Script");

                    b.HasKey("Id");

                    b.ToTable("Advert");
                });

            modelBuilder.Entity("SignalRMasterServer.Models.ApplicationRole", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("ConcurrencyStamp")
                        .IsConcurrencyToken();

                    b.Property<string>("Name")
                        .HasMaxLength(256);

                    b.Property<string>("NormalizedName")
                        .HasMaxLength(256);

                    b.HasKey("Id");

                    b.HasIndex("NormalizedName")
                        .IsUnique()
                        .HasName("RoleNameIndex");

                    b.ToTable("AspNetRoles");
                });

            modelBuilder.Entity("SignalRMasterServer.Models.ApplicationRoleClaim", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("ClaimType");

                    b.Property<string>("ClaimValue");

                    b.Property<int>("RoleId");

                    b.HasKey("Id");

                    b.HasIndex("RoleId");

                    b.ToTable("AspNetRoleClaims");
                });

            modelBuilder.Entity("SignalRMasterServer.Models.ApplicationUser", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<int>("AccessFailedCount");

                    b.Property<string>("AdminGuid");

                    b.Property<string>("ConcurrencyStamp")
                        .IsConcurrencyToken();

                    b.Property<string>("Description");

                    b.Property<string>("Email")
                        .HasMaxLength(256);

                    b.Property<bool>("EmailConfirmed");

                    b.Property<string>("FirstName");

                    b.Property<string>("IP");

                    b.Property<string>("Image");

                    b.Property<string>("LastName");

                    b.Property<bool>("LockoutEnabled");

                    b.Property<DateTimeOffset?>("LockoutEnd");

                    b.Property<string>("NormalizedEmail")
                        .HasMaxLength(256);

                    b.Property<string>("NormalizedUserName")
                        .HasMaxLength(256);

                    b.Property<bool>("Online");

                    b.Property<string>("PasswordHash");

                    b.Property<string>("PhoneNumber");

                    b.Property<bool>("PhoneNumberConfirmed");

                    b.Property<DateTime>("RegisteredDate");

                    b.Property<string>("SecurityStamp");

                    b.Property<Guid>("SignalrGuid");

                    b.Property<bool>("TwoFactorEnabled");

                    b.Property<string>("UserName")
                        .HasMaxLength(256);

                    b.HasKey("Id");

                    b.HasIndex("NormalizedEmail")
                        .HasName("EmailIndex");

                    b.HasIndex("NormalizedUserName")
                        .IsUnique()
                        .HasName("UserNameIndex");

                    b.ToTable("AspNetUsers");
                });

            modelBuilder.Entity("SignalRMasterServer.Models.ApplicationUserClaim", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("ClaimType");

                    b.Property<string>("ClaimValue");

                    b.Property<int>("UserId");

                    b.HasKey("Id");

                    b.HasIndex("UserId");

                    b.ToTable("AspNetUserClaims");
                });

            modelBuilder.Entity("SignalRMasterServer.Models.ApplicationUserLogin", b =>
                {
                    b.Property<string>("LoginProvider");

                    b.Property<string>("ProviderKey");

                    b.Property<string>("ProviderDisplayName");

                    b.Property<int>("UserId");

                    b.HasKey("LoginProvider", "ProviderKey");

                    b.HasIndex("UserId");

                    b.ToTable("AspNetUserLogins");
                });

            modelBuilder.Entity("SignalRMasterServer.Models.ApplicationUserRole", b =>
                {
                    b.Property<int>("RoleId");

                    b.Property<int>("UserId");

                    b.Property<int>("WebsiteId");

                    b.HasKey("RoleId", "UserId", "WebsiteId");

                    b.HasIndex("UserId");

                    b.HasIndex("WebsiteId");

                    b.ToTable("ApplicationUserRole");
                });

            modelBuilder.Entity("SignalRMasterServer.Models.ApplicationUserToken", b =>
                {
                    b.Property<int>("UserId");

                    b.Property<string>("LoginProvider");

                    b.Property<string>("Name");

                    b.Property<string>("Value");

                    b.HasKey("UserId", "LoginProvider", "Name");

                    b.ToTable("AspNetUserTokens");
                });

            modelBuilder.Entity("SignalRMasterServer.Models.ChatMarco", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<int?>("CultureId");

                    b.Property<int?>("DepartmentId");

                    b.Property<string>("KeyBoardShortcut");

                    b.Property<int>("MarcoType");

                    b.Property<string>("Name");

                    b.Property<string>("TextorURL");

                    b.Property<int>("WebsiteId");

                    b.HasKey("Id");

                    b.HasIndex("DepartmentId");

                    b.HasIndex("WebsiteId");

                    b.ToTable("ChatMarco");
                });

            modelBuilder.Entity("SignalRMasterServer.Models.Culture", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("Name");

                    b.HasKey("Id");

                    b.ToTable("Culture");
                });

            modelBuilder.Entity("SignalRMasterServer.Models.Department", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<int?>("CultureId");

                    b.Property<string>("Name");

                    b.Property<int>("WebsiteId");

                    b.HasKey("Id");

                    b.HasIndex("CultureId");

                    b.HasIndex("WebsiteId");

                    b.ToTable("Department");
                });

            modelBuilder.Entity("SignalRMasterServer.Models.EmailInvitation", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<bool>("Accepted");

                    b.Property<DateTime?>("AcceptedDate");

                    b.Property<int>("AddedById");

                    b.Property<int?>("AddeddById");

                    b.Property<string>("Email");

                    b.Property<DateTime>("InvitedDate");

                    b.Property<int>("RoleId");

                    b.Property<int>("WebsiteId");

                    b.HasKey("Id");

                    b.HasIndex("AddeddById");

                    b.HasIndex("RoleId");

                    b.HasIndex("WebsiteId");

                    b.ToTable("EmailInvitation");
                });

            modelBuilder.Entity("SignalRMasterServer.Models.IpInfoDb", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<int>("ChatMessageId");

                    b.Property<string>("Cityname");

                    b.Property<string>("Countrycode");

                    b.Property<string>("Countryname");

                    b.Property<string>("Ipaddress");

                    b.Property<string>("Latitude");

                    b.Property<string>("Longitude");

                    b.Property<string>("Regionname");

                    b.Property<string>("Timezone");

                    b.Property<string>("Zipcode");

                    b.HasKey("Id");

                    b.ToTable("IpInfoDb");
                });

            modelBuilder.Entity("SignalRMasterServer.Models.Log", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<DateTime>("Date");

                    b.Property<int>("Event");

                    b.Property<string>("Remarks");

                    b.Property<int>("UserId");

                    b.HasKey("Id");

                    b.HasIndex("UserId");

                    b.ToTable("Logs");
                });

            modelBuilder.Entity("SignalRMasterServer.Models.PlanDescription", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<int>("Order");

                    b.Property<int?>("PlanWebsiteType");

                    b.Property<string>("Text");

                    b.HasKey("Id");

                    b.HasIndex("PlanWebsiteType");

                    b.ToTable("PlanDescription");
                });

            modelBuilder.Entity("SignalRMasterServer.Models.PlanPrice", b =>
                {
                    b.Property<int>("PlanId");

                    b.Property<int>("Term");

                    b.Property<int?>("PlanWebsiteType");

                    b.Property<double>("Price");

                    b.HasKey("PlanId", "Term");

                    b.HasIndex("PlanWebsiteType");

                    b.ToTable("PlanPrice");
                });

            modelBuilder.Entity("SignalRMasterServer.Models.Plans", b =>
                {
                    b.Property<int>("WebsiteType");

                    b.Property<double>("MonthlyPrice");

                    b.Property<double>("QuaterlyPrice");

                    b.Property<double>("YearlyPrice");

                    b.HasKey("WebsiteType");

                    b.ToTable("Plans");
                });

            modelBuilder.Entity("SignalRMasterServer.Models.Referral", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<DateTime?>("AcceptedDate");

                    b.Property<bool>("Delflag");

                    b.Property<string>("Email");

                    b.Property<string>("FullName");

                    b.Property<int>("ReferralById");

                    b.Property<DateTime>("ReferralDate");

                    b.Property<int>("Status");

                    b.Property<string>("Website");

                    b.HasKey("Id");

                    b.HasIndex("ReferralById");

                    b.ToTable("Referral");
                });

            modelBuilder.Entity("SignalRMasterServer.Models.ReportDb", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<int>("Answered_Call_Count");

                    b.Property<DateTime>("ReportDate");

                    b.Property<int>("UnAnswered_Call_Count");

                    b.Property<int>("Unique_User_Count");

                    b.Property<int>("User_Count");

                    b.Property<int>("WebsiteId");

                    b.HasKey("Id");

                    b.ToTable("Reports");
                });

            modelBuilder.Entity("SignalRMasterServer.Models.Script", b =>
                {
                    b.Property<int>("ScriptId")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("WidgetScript");

                    b.HasKey("ScriptId");

                    b.ToTable("Script");
                });

            modelBuilder.Entity("SignalRMasterServer.Models.SignalRUser", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("Avatar");

                    b.Property<bool>("Blocked");

                    b.Property<int>("ChatMessageId");

                    b.Property<string>("Email");

                    b.Property<string>("FullName");

                    b.Property<string>("GroupId");

                    b.Property<string>("PhoneNumber");

                    b.HasKey("Id");

                    b.ToTable("SignalRUser");
                });

            modelBuilder.Entity("SignalRMasterServer.Models.UserWebsiteDescription", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("Avatar");

                    b.Property<string>("Description");

                    b.Property<string>("FirstName");

                    b.Property<string>("LastName");

                    b.Property<int>("UserId");

                    b.Property<int>("WebsiteId");

                    b.HasKey("Id");

                    b.HasIndex("UserId");

                    b.HasIndex("WebsiteId");

                    b.ToTable("UserWebsiteDescription");
                });

            modelBuilder.Entity("SignalRMasterServer.Models.Website", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("Alias");

                    b.Property<string>("CopyrightNotice");

                    b.Property<int>("CreatedById");

                    b.Property<DateTime>("CreatedDate");

                    b.Property<bool>("Delflag");

                    b.Property<string>("Description");

                    b.Property<Guid>("Guid");

                    b.Property<string>("Logo");

                    b.Property<string>("Name");

                    b.Property<bool>("PaidWebsite");

                    b.Property<int>("WebstieType");

                    b.Property<int>("WidgetId");

                    b.HasKey("Id");

                    b.HasIndex("CreatedById");

                    b.HasIndex("WidgetId");

                    b.ToTable("Website");
                });

            modelBuilder.Entity("SignalRMasterServer.Models.Widget", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("Image");

                    b.Property<string>("Link");

                    b.Property<string>("Name");

                    b.HasKey("Id");

                    b.ToTable("Widgets");
                });

            modelBuilder.Entity("SignalRMasterServer.Models.WidgetOptions", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("AdminOfflineText");

                    b.Property<bool>("EnableSSL");

                    b.Property<bool>("ForceWebsiteNameAndDescription");

                    b.Property<string>("OfflineText");

                    b.Property<string>("OnlineText");

                    b.Property<string>("TextSize");

                    b.Property<string>("TextStyle");

                    b.Property<int>("WebsiteId");

                    b.Property<string>("WelcomeMessage");

                    b.Property<string>("WidgetColor");

                    b.Property<string>("WidgetText");

                    b.Property<bool>("WidgetVisible");

                    b.HasKey("Id");

                    b.HasIndex("WebsiteId");

                    b.ToTable("WidgetOptions");
                });

            modelBuilder.Entity("SignalRMasterServer.Models.ApplicationRoleClaim", b =>
                {
                    b.HasOne("SignalRMasterServer.Models.ApplicationRole")
                        .WithMany("Claims")
                        .HasForeignKey("RoleId");
                });

            modelBuilder.Entity("SignalRMasterServer.Models.ApplicationUserClaim", b =>
                {
                    b.HasOne("SignalRMasterServer.Models.ApplicationUser")
                        .WithMany("Claims")
                        .HasForeignKey("UserId");
                });

            modelBuilder.Entity("SignalRMasterServer.Models.ApplicationUserLogin", b =>
                {
                    b.HasOne("SignalRMasterServer.Models.ApplicationUser")
                        .WithMany("Logins")
                        .HasForeignKey("UserId");
                });

            modelBuilder.Entity("SignalRMasterServer.Models.ApplicationUserRole", b =>
                {
                    b.HasOne("SignalRMasterServer.Models.ApplicationRole", "Role")
                        .WithMany("Users")
                        .HasForeignKey("RoleId")
                        .OnDelete(DeleteBehavior.Cascade);

                    b.HasOne("SignalRMasterServer.Models.ApplicationUser", "User")
                        .WithMany("Roles")
                        .HasForeignKey("UserId")
                        .OnDelete(DeleteBehavior.Cascade);

                    b.HasOne("SignalRMasterServer.Models.Website", "Website")
                        .WithMany("Roles")
                        .HasForeignKey("WebsiteId")
                        .OnDelete(DeleteBehavior.Cascade);
                });

            modelBuilder.Entity("SignalRMasterServer.Models.ChatMarco", b =>
                {
                    b.HasOne("SignalRMasterServer.Models.Department")
                        .WithMany("Marco")
                        .HasForeignKey("DepartmentId");

                    b.HasOne("SignalRMasterServer.Models.Website", "Website")
                        .WithMany()
                        .HasForeignKey("WebsiteId");
                });

            modelBuilder.Entity("SignalRMasterServer.Models.Department", b =>
                {
                    b.HasOne("SignalRMasterServer.Models.Culture", "Culture")
                        .WithMany()
                        .HasForeignKey("CultureId");

                    b.HasOne("SignalRMasterServer.Models.Website", "Website")
                        .WithMany("Department")
                        .HasForeignKey("WebsiteId");
                });

            modelBuilder.Entity("SignalRMasterServer.Models.EmailInvitation", b =>
                {
                    b.HasOne("SignalRMasterServer.Models.ApplicationUser", "AddeddBy")
                        .WithMany()
                        .HasForeignKey("AddeddById");

                    b.HasOne("SignalRMasterServer.Models.ApplicationRole", "Role")
                        .WithMany()
                        .HasForeignKey("RoleId");

                    b.HasOne("SignalRMasterServer.Models.Website", "Website")
                        .WithMany()
                        .HasForeignKey("WebsiteId");
                });

            modelBuilder.Entity("SignalRMasterServer.Models.Log", b =>
                {
                    b.HasOne("SignalRMasterServer.Models.ApplicationUser", "User")
                        .WithMany()
                        .HasForeignKey("UserId");
                });

            modelBuilder.Entity("SignalRMasterServer.Models.PlanDescription", b =>
                {
                    b.HasOne("SignalRMasterServer.Models.Plans", "Plan")
                        .WithMany("PlanDescription")
                        .HasForeignKey("PlanWebsiteType");
                });

            modelBuilder.Entity("SignalRMasterServer.Models.PlanPrice", b =>
                {
                    b.HasOne("SignalRMasterServer.Models.Plans", "Plan")
                        .WithMany("PlanPrice")
                        .HasForeignKey("PlanWebsiteType");
                });

            modelBuilder.Entity("SignalRMasterServer.Models.Referral", b =>
                {
                    b.HasOne("SignalRMasterServer.Models.ApplicationUser", "ReferralBy")
                        .WithMany()
                        .HasForeignKey("ReferralById");
                });

            modelBuilder.Entity("SignalRMasterServer.Models.UserWebsiteDescription", b =>
                {
                    b.HasOne("SignalRMasterServer.Models.ApplicationUser", "User")
                        .WithMany()
                        .HasForeignKey("UserId");

                    b.HasOne("SignalRMasterServer.Models.Website", "Website")
                        .WithMany()
                        .HasForeignKey("WebsiteId");
                });

            modelBuilder.Entity("SignalRMasterServer.Models.Website", b =>
                {
                    b.HasOne("SignalRMasterServer.Models.ApplicationUser", "CreatedBy")
                        .WithMany()
                        .HasForeignKey("CreatedById");

                    b.HasOne("SignalRMasterServer.Models.Widget", "Widget")
                        .WithMany()
                        .HasForeignKey("WidgetId");
                });

            modelBuilder.Entity("SignalRMasterServer.Models.WidgetOptions", b =>
                {
                    b.HasOne("SignalRMasterServer.Models.Website", "Website")
                        .WithMany()
                        .HasForeignKey("WebsiteId");
                });
        }
    }
}
