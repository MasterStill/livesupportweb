﻿using System;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore.Migrations;
using Microsoft.EntityFrameworkCore.Metadata;

namespace SignalRMasterServer.Migrations
{
    public partial class userRolesaaaaa : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropPrimaryKey(
                name: "PK_ApplicationUserRole",
                table: "ApplicationUserRole");

            migrationBuilder.DropIndex(
                name: "IX_ApplicationUserRole_RoleId_UserId_WebsiteId",
                table: "ApplicationUserRole");

            migrationBuilder.DropColumn(
                name: "Id",
                table: "ApplicationUserRole");

            migrationBuilder.AddPrimaryKey(
                name: "PK_ApplicationUserRole",
                table: "ApplicationUserRole",
                columns: new[] { "RoleId", "UserId", "WebsiteId" });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropPrimaryKey(
                name: "PK_ApplicationUserRole",
                table: "ApplicationUserRole");

            migrationBuilder.AddColumn<int>(
                name: "Id",
                table: "ApplicationUserRole",
                nullable: false,
                defaultValue: 0)
                .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

            migrationBuilder.AddPrimaryKey(
                name: "PK_ApplicationUserRole",
                table: "ApplicationUserRole",
                column: "Id");

            migrationBuilder.CreateIndex(
                name: "IX_ApplicationUserRole_RoleId_UserId_WebsiteId",
                table: "ApplicationUserRole",
                columns: new[] { "RoleId", "UserId", "WebsiteId" });
        }
    }
}
