﻿using System;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore.Migrations;

namespace SignalRMasterServer.Migrations
{
    public partial class userRoles : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropUniqueConstraint(
                name: "AK_AspNetUserRoles_UserId",
                table: "AspNetUserRoles");

            migrationBuilder.DropUniqueConstraint(
                name: "AK_AspNetUserRoles_RoleId_UserId",
                table: "AspNetUserRoles");

            migrationBuilder.DropColumn(
                name: "Description",
                table: "AspNetRoles");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "Description",
                table: "AspNetRoles",
                nullable: true);

            migrationBuilder.AddUniqueConstraint(
                name: "AK_AspNetUserRoles_UserId",
                table: "AspNetUserRoles",
                column: "UserId");

            migrationBuilder.AddUniqueConstraint(
                name: "AK_AspNetUserRoles_RoleId_UserId",
                table: "AspNetUserRoles",
                columns: new[] { "RoleId", "UserId" });
        }
    }
}
