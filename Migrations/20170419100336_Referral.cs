﻿using System;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore.Migrations;

namespace SignalRMasterServer.Migrations
{
    public partial class Referral : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<DateTime>(
                name: "AcceptedDate",
                table: "Referral",
                nullable: true);

            migrationBuilder.AddColumn<bool>(
                name: "Delflag",
                table: "Referral",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<int>(
                name: "ReferralById",
                table: "Referral",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<DateTime>(
                name: "ReferralDate",
                table: "Referral",
                nullable: false,
                defaultValue: new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified));

            migrationBuilder.CreateIndex(
                name: "IX_Referral_ReferralById",
                table: "Referral",
                column: "ReferralById");

            migrationBuilder.AddForeignKey(
                name: "FK_Referral_AspNetUsers_ReferralById",
                table: "Referral",
                column: "ReferralById",
                principalTable: "AspNetUsers",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Referral_AspNetUsers_ReferralById",
                table: "Referral");

            migrationBuilder.DropIndex(
                name: "IX_Referral_ReferralById",
                table: "Referral");

            migrationBuilder.DropColumn(
                name: "AcceptedDate",
                table: "Referral");

            migrationBuilder.DropColumn(
                name: "Delflag",
                table: "Referral");

            migrationBuilder.DropColumn(
                name: "ReferralById",
                table: "Referral");

            migrationBuilder.DropColumn(
                name: "ReferralDate",
                table: "Referral");
        }
    }
}
