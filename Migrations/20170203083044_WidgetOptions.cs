﻿using System;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore.Migrations;
using Microsoft.EntityFrameworkCore.Metadata;

namespace SignalRMasterServer.Migrations
{
    public partial class WidgetOptions : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "WidgetOptions",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    EnableSSL = table.Column<string>(nullable: true),
                    OfflineText = table.Column<string>(nullable: true),
                    OnlineText = table.Column<string>(nullable: true),
                    TextSize = table.Column<string>(nullable: true),
                    TextStyle = table.Column<string>(nullable: true),
                    WebsiteId = table.Column<int>(nullable: false),
                    WidgetColor = table.Column<string>(nullable: true),
                    WidgetText = table.Column<string>(nullable: true),
                    WidgetVisible = table.Column<bool>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_WidgetOptions", x => x.Id);
                    table.ForeignKey(
                        name: "FK_WidgetOptions_Website_WebsiteId",
                        column: x => x.WebsiteId,
                        principalTable: "Website",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateIndex(
                name: "IX_WidgetOptions_WebsiteId",
                table: "WidgetOptions",
                column: "WebsiteId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "WidgetOptions");
        }
    }
}
