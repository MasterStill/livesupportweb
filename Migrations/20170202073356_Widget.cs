﻿using System;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore.Migrations;
using Microsoft.EntityFrameworkCore.Metadata;

namespace SignalRMasterServer.Migrations
{
    public partial class Widget : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "WidgetId",
                table: "Website",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.CreateTable(
                name: "Widgets",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Name = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Widgets", x => x.Id);
                });

            migrationBuilder.CreateIndex(
                name: "IX_Website_WidgetId",
                table: "Website",
                column: "WidgetId");

            migrationBuilder.AddForeignKey(
                name: "FK_Website_Widgets_WidgetId",
                table: "Website",
                column: "WidgetId",
                principalTable: "Widgets",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Website_Widgets_WidgetId",
                table: "Website");

            migrationBuilder.DropTable(
                name: "Widgets");

            migrationBuilder.DropIndex(
                name: "IX_Website_WidgetId",
                table: "Website");

            migrationBuilder.DropColumn(
                name: "WidgetId",
                table: "Website");
        }
    }
}
