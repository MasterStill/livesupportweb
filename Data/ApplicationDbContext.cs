﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using SignalRMasterServer.Models;
using Microsoft.EntityFrameworkCore.Metadata;

namespace SignalRMasterServer.Data
{
    public abstract class ApplicationDbContext<TUser, TRole, TKey, TUserClaim, TUserRole, TUserLogin, TRoleClaim, TUserToken> : IdentityDbContext<TUser, TRole, TKey, TUserClaim, TUserRole, TUserLogin, TRoleClaim, TUserToken>
    where TUser : IdentityUser<TKey, TUserClaim, TUserRole, TUserLogin>
    where TRole : IdentityRole<TKey, TUserRole, TRoleClaim>
    where TKey : IEquatable<TKey>
    where TUserClaim : IdentityUserClaim<TKey>
    where TUserRole : IdentityUserRole<TKey>
    where TUserLogin : IdentityUserLogin<TKey>
    where TRoleClaim : IdentityRoleClaim<TKey>
    where TUserToken : IdentityUserToken<TKey>
    {
        public ApplicationDbContext(DbContextOptions options) : base(options) { }
        protected ApplicationDbContext() { }

        public new DbSet<TRoleClaim> RoleClaims { get; set; }
        public new DbSet<TRole> Roles { get; set; }
        public new DbSet<TUserClaim> UserClaims { get; set; }
        public new DbSet<TUserLogin> UserLogins { get; set; }
        public new DbSet<TUserRole> UserRoles { get; set; }
        public new DbSet<TUser> Users { get; set; }
        public new DbSet<TUserToken> UserTokens { get; set; }


        protected override void OnModelCreating(ModelBuilder builder)
        {
            foreach (var relationship in builder.Model.GetEntityTypes().SelectMany(e => e.GetForeignKeys()))
            {
                relationship.DeleteBehavior = DeleteBehavior.Restrict;
            }
            base.OnModelCreating(builder);
            builder.Ignore<ApplicationUserRole>();
            builder.Entity<ApplicationUserRole>().HasKey(x => new { x.RoleId, x.UserId, x.WebsiteId });
            // builder.Entity<ChatMessage>().HasKey(x => new { x.Id });

            //builder.Ignore<ApplicationUserRole>();
            //builder.Entity<ApplicationUser>().HasMany(u => u.Roles).WithOne().HasForeignKey(ur => ur.UserId).IsRequired();
            //builder.Entity<ApplicationRole>().HasMany(r => r.Users).WithOne().HasForeignKey(ur => ur.RoleId).IsRequired();
            //builder.Entity<ApplicationUserRole>(entity =>
            //{
            //    entity.HasKey(x => new { x.Id});
            //    entity.HasIndex(x => new { x.RoleId, x.UserId, x.WebsiteId})  ;
            //});

        }
    }
    public class AppDbContext : ApplicationDbContext<ApplicationUser, ApplicationRole, int, ApplicationUserClaim, ApplicationUserRole, ApplicationUserLogin, ApplicationRoleClaim, ApplicationUserToken>
    {
    //    public AppDbContext(ShardMap shardMap, T shardingKey, string connectionStr)
    //    : base(CreateDDRConnection(shardMap, shardingKey, connectionStr), 
    //    true /* contextOwnsConnection */)
    //{
    //    }
        public AppDbContext(DbContextOptions<AppDbContext> options)
            : base(options)
        {
        }
        //public DbSet<SignalRUserConversation> SignalRUserConversation { get; set; }
        //public DbSet<SignalRUser> SignalRUser { get; set; }
        //public DbSet<MacroGroup> MacroGroup { get; set; }
        public DbSet<ChatMarco> ChatMarco { get; set; }
        public DbSet<Department> Department { get; set; }
        public DbSet<Advert> Advert { get; set; }
        public DbSet<EmailInvitation> EmailInvitation { get; set; }
        public DbSet<Culture> Culture { get; set; }
        //public DbSet<PushMarco> PushMarco { get; set; }
        public DbSet<Website> Website { get; set; }
        public DbSet<Widget> Widgets { get; set; }
        public DbSet<WidgetOptions> WidgetOptions { get; set; }
        // public DbSet<ChatMessage> ChatMessage { get; set; }
        public DbSet<Script> Script{ get; set; }
        // public DbSet<ChatEventMessage> ChatEventMessage { get; set; }
        public DbSet<Referral> Referral { get; set; }
        public DbSet<UserWebsiteDescription> UserWebsiteDescription { get; set; }
        public DbSet<Website> Website1 { get; set; }
       //public DbSet<IpInfoDb> IpInfoDb { get; set; }
        public DbSet<Plans> Plans{ get; set; }
        public DbSet<Log> Logs { get; set; }
        public DbSet<ReportDb> Reports { get; set; }
        public DbSet<PlanDescription> PlanDescription { get; set; }
        //public new DbSet<ApplicationUserClaim> UserClaims { get; set; }
        protected override void OnModelCreating(ModelBuilder builder)
        {
            foreach (var relationship in builder.Model.GetEntityTypes().SelectMany(e => e.GetForeignKeys()))
            {
                relationship.DeleteBehavior = DeleteBehavior.Restrict;
            }
            base.OnModelCreating(builder);
            builder.Ignore<ApplicationUserRole>();
            builder.Entity<ApplicationUserRole>().HasKey(x => new { x.RoleId, x.UserId, x.WebsiteId });
            builder.Entity<Plans>().HasKey(x => new {x.WebsiteType});
            builder.Entity<PlanPrice>().HasKey(x => new { x.PlanId, x.Term });
            // builder.Entity<ChatMessage>().HasKey(x => new { x.Id});
            //builder.Entity<ApplicationUserRole>().HasKey(x => new { x.Id});
            //builder.Ignore<ApplicationUserRole>();
            //builder.Entity<ApplicationUser>().HasMany(u => u.Roles).WithOne().HasForeignKey(ur => ur.UserId).IsRequired();
            //builder.Entity<ApplicationRole>().HasMany(r => r.Users).WithOne().HasForeignKey(ur => ur.RoleId).IsRequired();
            //builder.Entity<ApplicationUserRole>(entity =>
            //{
            //    entity.HasKey(x => new { x.Id });
            //    entity.HasIndex(x => new { x.RoleId, x.UserId, x.WebsiteId }).IsUnique(true);
            //});
            // Customize the ASP.NET Identity model and override the defaults if needed.
            // For example, you can rename the ASP.NET Identity table names and more.
            // Add your customizations after calling base.OnModelCreating(builder);
        }
    }
}