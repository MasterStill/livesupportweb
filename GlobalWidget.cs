﻿using System;
using System.Collections.Generic;
using System.IO;
using Microsoft.AspNetCore.Mvc;
using SignalRMasterServer.Controllers;
using SignalRMasterServer.Models;
using SignalRMasterServer.Services;
using static SignalRMasterServer.Controllers.ScriptController;
using Mindscape.Raygun4Net;
using System.Net.Http;
using Newtonsoft.Json;
using System.Text;
using SignalRMasterServer.Data;
using System.Linq;
namespace SignalRMasterServer
{
    public static class GlobalWidget
    {
        public static ScriptViewModel MainScript(string WebsiteName)
        {
            string websiteGuid;
            //string content = "<script> var _0x4aa8=['appName','Microsoft Internet Explorer','Microsoft.XMLHTTP','get','open','onreadystatechange','send','readyState','responseText','parse','innerHTML','body','html','script','createElement','type','text/javascript','createTextNode','appendChild','https://admin.livecustomer.chat/api/script/getcontents/1'];var http=createRequestObject();var response;function createRequestObject(){var _0xf234x4;var _0xf234x5=navigator[_0x4aa8[0]];if(_0xf234x5== _0x4aa8[1]){_0xf234x4=  new ActiveXObject(_0x4aa8[2])}else {_0xf234x4=  new XMLHttpRequest()};return _0xf234x4}function sendReq(_0xf234x7){http[_0x4aa8[4]](_0x4aa8[3],_0xf234x7);http[_0x4aa8[5]]= handleResponse;http[_0x4aa8[6]](null)}function handleResponse(){if(http[_0x4aa8[7]]== 4){ManageCssAndScript(http[_0x4aa8[8]])}}function ManageCssAndScript(response){var _0xf234xa=JSON[_0x4aa8[9]](response);document[_0x4aa8[11]][_0x4aa8[10]]+= _0xf234xa[_0x4aa8[12]];var _0xf234xb=document[_0x4aa8[14]](_0x4aa8[13]);_0xf234xb[_0x4aa8[15]]= _0x4aa8[16];_0xf234xb[_0x4aa8[18]](document[_0x4aa8[17]](_0xf234xa[_0x4aa8[13]]));document[_0x4aa8[11]][_0x4aa8[18]](_0xf234xb)}sendReq(_0x4aa8[19])</script>";
            //string content = "<script>var _0x712f=['https://admin.livecustomer.chat','/api/script/getcontents/','00000000-0000-0000-0000-000000000008','appName','Microsoft Internet Explorer','Microsoft.XMLHTTP','get','open','onreadystatechange','send','readyState','responseText','parse','innerHTML','body','iFrame','livecustomeriframe','getElementById','contentWindow','contentDocument','document','html','<script>','script','<\\/script>','write','close','ready','undefined','createElement','type','text/javascript','src','https://cdnjs.cloudflare.com/ajax/libs/jquery/2.1.3/jquery.min.js','onload','complete','appendChild','head','getElementsByTagName'];var http=createRequestObject();var response;var baseUrl=_0x712f[0];var ApiUrl=_0x712f[1];var websiteId=_0x712f[2];function createRequestObject(){var _0x33cax7;var _0x33cax8=navigator[_0x712f[3]];if(_0x33cax8== _0x712f[4]){_0x33cax7=  new ActiveXObject(_0x712f[5])}else {_0x33cax7=  new XMLHttpRequest()};return _0x33cax7}function sendReq(_0x33caxa){http[_0x712f[7]](_0x712f[6],_0x33caxa);http[_0x712f[8]]= handleResponse;http[_0x712f[9]](null)}function handleResponse(){if(http[_0x712f[10]]== 4){ManageCssAndScript(http[_0x712f[11]])}}function ManageCssAndScript(response){var _0x33caxd=JSON[_0x712f[12]](response);document[_0x712f[14]][_0x712f[13]]+= _0x33caxd[_0x712f[15]];var _0x33caxe=document[_0x712f[17]](_0x712f[16]),_0x33caxf=_0x33caxe[_0x712f[18]]|| _0x33caxe,_0x33cax10=_0x33caxe[_0x712f[19]]|| _0x33caxf[_0x712f[20]];$(_0x33cax10)[_0x712f[27]](function(_0x33cax11){_0x33cax10[_0x712f[7]]();_0x33cax10[_0x712f[25]](_0x33caxd[_0x712f[21]]+ _0x712f[22]+ _0x33caxd[_0x712f[23]]+ _0x712f[24]);_0x33cax10[_0x712f[26]]()})}if( typeof jQuery== _0x712f[28]){var r=false;var t;var scriptJquery=document[_0x712f[29]](_0x712f[23]);scriptJquery[_0x712f[30]]= _0x712f[31];scriptJquery[_0x712f[32]]= _0x712f[33];scriptJquery[_0x712f[34]]= scriptJquery[_0x712f[8]]= function(){if(!r&& (!this[_0x712f[10]]|| this[_0x712f[10]]== _0x712f[35])){r= true;sendReq(baseUrl+ ApiUrl+ websiteId)}};document[_0x712f[38]](_0x712f[37])[0][_0x712f[36]](scriptJquery)}else {sendReq(baseUrl+ ApiUrl+ websiteId)} </script>";
            string content = @"<script> 
   (function(i, s, o, g, r, a, m){
                i['LiveCustomerChat'] = r; i[r] = i[r] || function(){
                    (i[r].q = i[r].q ||[]).push(arguments)},i[r].l = 1 * new Date(); a = s.createElement(o),
  m = s.getElementsByTagName(o)[0]; a.async = 1; a.src = g; m.parentNode.insertBefore(a, m)
        })(window, document, 'script', '" + Global.BaseUrl() + @"/api/Script/script/!!script!!/script.js', 'ga');
</script> ";
            //content = _context.Script.FirstOrDefault().WidgetScript;
            try
            {
                websiteGuid = ElasticSearchGlobal.GetWebsitebyName(WebsiteName).Guid.ToString();//_context.Website.Where(x => x.Name == WebsiteName).SingleOrDefault().Guid.ToString();
            }
            catch
            {
                return null;
            }
            string Script = content.Replace("!!script!!", websiteGuid);
            return new ScriptViewModel
            {
                Script = Script
            };
        }
        
        public static ScriptCss ScriptCss(int WebsiteId, string signalrrUser, bool infoProvided, AppDbContext _context, bool MobileDevice)
        {
            string widgetColor = "#1cbed1";
            var widgetOptions = ElasticSearchGlobal.GetWidgetOptionsbyWebsiteId(WebsiteId);//_context.WidgetOptions.Where(x => x.Website.Guid == WebsiteId).SingleOrDefault();
            var website = ElasticSearchGlobal.GetWebsitebyWebsiteId(WebsiteId);
            try
            {
                widgetColor = (widgetOptions != null) ? widgetOptions.WidgetColor.TrimEnd().TrimStart() : widgetColor;
            }
            catch
            {

            }


            
            //var widgetColor = _context.WidgetOptions.Where(x=>x.websiteid == WebsiteName).SingleOrDefault().WidgetColor;
            //var encryptedContent = Convert.ToBase64String(Encoding.ASCII.GetBytes(Widget1Script()));
            ScriptCss s = new ScriptCss();
            UserWebsiteDescription UwD = null;//new UserWebsiteDescription();
            s = Global.getWidgetScriptandMore("Widget1");
            s.HTML = s.HTML.Replace("@ViewBag.SignalRUrl", Global.BaseUrl());
            s.IFrame = s.IFrame.Replace("WebsiteName", Global.BaseUrl());
            s.Script = s.Script.Replace("@ViewBag.SignalRUrl", Global.BaseUrl());            
            if (widgetOptions != null)
                if (widgetOptions.AdminOfflineText != null)
                {
                    s.Script = s.Script.Replace("offlineMessage: []", "offlineMessage: ['" + widgetOptions.AdminOfflineText.Replace("!!@@##$$%%", "','") + "']");
                }
            s.HTML = s.HTML.Replace("#0F9D58", widgetColor);
            s.Css = s.Css.Replace("#0F9D58", widgetColor);
            var OnlineAdmin = ElasticSearchGlobal.AdminOnline(WebsiteId.ToString());//_context.UserRoles.Where(x => x.Website.Guid == WebsiteId).Select(x => x.User).Distinct().Where(x => x.Online == true).FirstOrDefault();
            var Website = ElasticSearchGlobal.GetWebsitebyWebsiteId(WebsiteId);//_context.Website.Where(x => x.Guid == WebsiteId).SingleOrDefault();
            string AdminName = string.Empty;
            string AdmnDescription = string.Empty;
            string AdminNameTitle = string.Empty;
            string AdminAvatar = string.Empty;
            if (OnlineAdmin != null)
            {                
                UwD = ElasticSearchGlobal.GetUserWebsiteDescriptionbyUserIdWebsiteId(Website.Id, OnlineAdmin.Id);//_context.UserWebsiteDescription.Where(x => x.Website.Guid == WebsiteId).Where(x => x.User.Id == OnlineAdmin.Id).SingleOrDefault();
                if (OnlineAdmin.FullName != null) AdminName = OnlineAdmin.FullName;
                if (OnlineAdmin.Description != null) AdmnDescription = OnlineAdmin.Description;
                AdminAvatar = (UwD != null) ? UwD.Avatar : OnlineAdmin.Avatar;
                if (UwD != null)
                {
                    if (UwD.FullName != null) AdminName = UwD.FullName;
                    if (UwD.Description != null) AdmnDescription = UwD.Description;
                    if (UwD.Avatar != null) AdminAvatar = UwD.Avatar;
                }
                AdminNameTitle = AdminName + "- Online";
            }
            else
            {
                s.Script = s.Script.Replace("isOffline: false", "isOffline: true");
                AdminNameTitle =  "Live Support - Offline";
                AdminName = (!string.IsNullOrEmpty(Website.Alias) ? Website.Alias : Website.Name);
                AdmnDescription = Website.Description;
                AdminAvatar = (Website.Logo != null) ? Website.Logo : Global.NoImage();
                try
                {
                    string offlineText = _context.WidgetOptions.Where(x => x.WebsiteId == Website.Id).SingleOrDefault().OfflineText;
                    s.HTML = s.HTML.Replace("<h3>Hey Hi we are not online now please leave a message.</h3>", "<h3>" + (!string.IsNullOrEmpty(offlineText) ? offlineText : Global.DefaultOfflineText()) + "</h3>");
                }
                catch { }
            }
            s.Script = s.Script.Replace("adminName: ''", "adminName: '" + AdminName + "'");
            s.Script = s.Script.Replace("toolBarTitle: ''", "toolBarTitle: '" + AdminNameTitle + "'");
            s.Script = s.Script.Replace("adminImage: ''", "adminImage: '" + AdminAvatar + "'");
            s.Script = s.Script.Replace("adminDescription: ''", "adminDescription: '" + AdmnDescription + "'");

            if (Global.BaseUrl().Contains("localhost") || Global.BaseUrl().Contains("ngrok"))
            {
                s.HTML = s.HTML.Replace("vueurl", "vue.js");
                s.HTML = s.HTML.Replace("document.domain", "//document.domain");
                s.IFrame = s.IFrame.Replace("document.domain", "//document.domain");
                s.Script = s.Script.Replace("document.domain", "//document.domain");
                //s.Script = s.Script.Replace("SetWidgetHeight('55px');", "//SetWidgetHeight('55px');\n");
                //s.Script = s.Script.Replace("SetWidgetWidth('401px');", "//SetWidgetWidth('401px')\n");                
                //Console.WriteLine(u);
            }
            else
            {
                // string RequestFromDomain = ($"{Request.Host}");
                s.HTML = s.HTML.Replace("vueurl", "vue.min.js");
                s.HTML = s.HTML.Replace("{{domainname}}", Website.Name);
                s.IFrame = s.IFrame.Replace("{{domainname}}", Website.Name);
                s.Script = s.Script.Replace("{{domainname}}", Website.Name);
            }
            s.Script = s.Script.Replace("document.title", "top.document.title");
            if (Website.PaidWebsite == true || (Website.CreatedDate.AddDays(30) > DateTime.Now))
            {
                s.HTML = s.HTML.Replace("<div id='company-info' class='company-info' v-if=\"route=='company-info'\"><strong>Widgets For Website In Minutes<br><br><a class=\"c\"href='https://www.livecustomer.chat'>Live Customer Chat</a></strong></div>", "");
                s.HTML = s.HTML.Replace("<h4 @click=\"route='company-info'\">Powered By : Live Customer Chat</h4>", "");
                s.Css = s.Css.Replace("ulChatHeight", "437px");//"437px");
                s.Css = s.Css.Replace("contentContainerHeight", "89%");//"437px");
            }
            else
            {
                s.Css = s.Css.Replace("contentContainerHeight", "80%");//"437px");
                s.Css = s.Css.Replace("ulChatHeight", "416px");
            }
            if (infoProvided)
            {
                var kk = ElasticSearchGlobal.GetSignalRUser(null, signalrrUser,Website.Guid.ToString());
                if (kk != null)
                    s.Script = s.Script.Replace("isBlocked: false", "isBlocked: " + kk.Blocked.ToString().ToLower());
                s.Script = s.Script.Replace("infoProvided: false", "infoProvided: true");
            }
            string GroupId = signalrrUser;//Request.Cookies["LiveSupportRockmandu" + Website.Name.Replace(":", "")];
            var iniatedChatBefore = ElasticSearchGlobal.GetChatMessage(GroupId);//_context.ChatMessage.Where(x => x.GroupId == GroupId).SingleOrDefault();
            if (iniatedChatBefore != null)
            {
                s.Script = s.Script.Replace("InitiatedChatBefore: false", "InitiatedChatBefore: true");
            }
            string widgetMinimizedHeight = "55px";
            string widgetHeight="";
            string widgetWidth = String.Empty; ;
            if(website.WidgetId == 1)
            {
                //s.Script = s.Script.Replace("widgetType:1", "widgetType: '" + '1' + "'");
                widgetHeight = MobileDevice ? "100%" : (widgetOptions != null) ? widgetOptions.WidgetHeight + "%" : "535px";
                widgetWidth = MobileDevice ? "100%" : (widgetOptions != null) ? widgetOptions.WidgetWidth + "%" : "405px";
            }
            else if(website.WidgetId == 2)
            {
                s.Script = s.Script.Replace("widgetType:1", "widgetType: " + '2');
                widgetHeight = MobileDevice ? "100%" : (widgetOptions != null) ? widgetOptions.WidgetHeight + "%" : "134px";
                widgetWidth = MobileDevice ? "100%" : (widgetOptions != null) ? widgetOptions.WidgetWidth + "%" : "109px";
            }
            string widgetMinimizedWidth = widgetWidth;
            string widgetMaxmizedWidth = widgetMinimizedWidth;

            s.Script = s.Script.Replace("widgetMaxmizedHeight: ''", "widgetMaxmizedHeight: '" + widgetHeight + "'");
            s.Script = s.Script.Replace("widgetMinimizedHeight: ''", "widgetMinimizedHeight: '" + widgetMinimizedHeight + "'");
            s.Script = s.Script.Replace("widgetMaxmizedWidth: ''", "widgetMaxmizedWidth: '" + widgetMaxmizedWidth + "'");
            s.Script = s.Script.Replace("widgetMinimizedWidth: ''", "widgetMinimizedWidth: '" + widgetMinimizedWidth + "'");
            s.Script = s.Script.Replace("widgetWidth: ''", "widgetWidth: '" + widgetMinimizedWidth + "'");
            s.Script = s.Script.Replace("widgetHeight: ''", "widgetHeight: '" + widgetMinimizedHeight + "'");
            s.Css = s.Css.Replace("HANGOUTHEIGHT", widgetMinimizedHeight);
            s.Script = s.Script.Replace("&WebsiteId=", "&WebsiteId=" + Website.Guid);
            s.Script = s.Script.Replace("'http://localhost:5000/'","'" + Global.BaseUrl() + "/'");
            s.Script = s.Script.Replace("'http://localhost:5000/'","'" + Global.BaseUrl() + "/'");
            // s.Css = s.Css.Replace("widgetWidth","100");
            return s;
        }
        // public bool MobileDevice(){
        // }
    }
}