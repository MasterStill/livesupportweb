﻿using Nest;

namespace SignalRMasterServer.Models
{
    public class Widget
    {
        public Widget()
        {
            
        }
        public int Id { get; set; }
        public string Name { get; set; }
        public string Link { get; set; }
        public string Image { get; set; }
    }

    public class WidgetOptions
    {
        public int Id { get; set; }
        public bool WidgetVisible { get; set; }
        public bool EnableSSL { get; set; }
        public string WelcomeMessage { get; set; }
        public bool ForceWebsiteNameAndDescription { get; set; }
        public string WidgetColor { get; set; }
        public string TextSize { get; set; }
        public string TextStyle { get; set; }
        public string OnlineText { get; set; }
        public string OfflineText { get; set; }
        public string AdminOfflineText { get; set; }
        public string WidgetText { get; set; }
        public int WidgetHeight { get; set; }
        public int WidgetWidth { get; set; }
        public int WebsiteId { get; set; }
        [Text(Ignore = true)]
        public virtual  Website Website { get; set;  }
    }
    public class WidgetOptionsCreateViewModel{
        public int Id { get; set; }
        public int WidgetHeight { get; set; }
        public int WidgetWidth { get; set; }
        public bool WidgetVisible { get; set; }
        public bool EnableSSL { get; set; }
        public string WelcomeMessage { get; set; }
        public bool ForceWebsiteNameAndDescription { get; set; }
        public string WidgetColor { get; set; }
        public string TextSize { get; set; }
        public string TextStyle { get; set; }
        public string OnlineText { get; set; }
        public string OfflineText { get; set; }
        public string[] AdminOfflineText { get; set; }
        public string WidgetText { get; set; }
        public int WebsiteId { get; set; }
        public virtual  Website Website { get; set;  }
    }
    public class WidgetOptionsViewModel{
         public int Id { get; set; }
        public bool WidgetVisible { get; set; }
        public bool EnableSSL { get; set; }
        public string WelcomeMessage { get; set; }
        public bool ForceWebsiteNameAndDescription { get; set; }
        public string WidgetColor { get; set; }
        public string TextSize { get; set; }
        public string TextStyle { get; set; }
        public int WidgetHeight { get; set; }
        public int WidgetWidth { get; set; }
        public string OnlineText { get; set; }
        public string OfflineText { get; set; }
        public string[] AdminOfflineText { get; set; }
        public string WidgetText { get; set; }
        public int WebsiteId { get; set; }
        public virtual  Website Website { get; set;  }
    }
}