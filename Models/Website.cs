﻿using Nest;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace SignalRMasterServer.Models
{
    [ElasticsearchType(Name = "website")]
    public class Website
    {
        public Website()
        {
            this.Department = new List<Department>();
            this.Roles = new List<ApplicationUserRole>();
            this.Guid = Guid.NewGuid();
            this.CreatedDate = DateTime.Now;
        }
        public int Id { get; set; }
        public int CreatedById { get; set; }
        public bool Delflag { get; set; }
        public DateTime CreatedDate { get; set; }
        [Text(Ignore = true)]
        public virtual ApplicationUser CreatedBy { get; set; }
        public string Alias { get; set; }
        public string Name { get; set; }
        public string Logo { get; set; }
        public bool PaidWebsite { get; set; }
        public WebsiteType WebstieType { get; set; }

        [Text(Ignore = true)]
        public virtual List<Department> Department { get; set; }
        public string CopyrightNotice { get; set; }
        public string Description { get; set; }
        //public bool AdminOnline {get;set;}
        public int WidgetId { get; set; }

        [Text(Ignore = true)]
        public virtual Widget Widget { get; set; }
        [Text(Ignore = true)]
        public virtual List<ApplicationUserRole> Roles { get; set; }
        public Guid Guid { get; set; }
        // public WebsiteUserRole WebsiteUserRole {get;set;}        
    }
    // public class WebsiteUserRole
    // {        
    //     public int UserId { get; set; }
    //     public int RoleId { get; set; }
    // }
    public class Twilo
    {
        // Other settings removed for brevity
        public string TwilioUrl { get; set; }
        public string TwilioId { get; set; }
        public string TwilioToken { get; set; }
        public string TwilioPhoneNumber { get; set; }
    }
    public class ChatMessage : ElasticChatMessage
    {
        [Key]
        public string Id { get; set; }
    }
    public class ChatEventMessage : ElasticChatEventMessage
    {
        [Key]
        public string Id { get; set; }
    }

    public class ElasticChatMessage
    {
        //public int Id { get; set; }
        //public string FullName { get; set; }
        public string GroupId { get; set; }
        public int WebsiteId { get; set; }
        [Text(Ignore = true)]
        public virtual Website Website { get; set; }
    }
    public class ElasticChatEventMessage
    {
        public DateTime DateTime { get; set; }
        public string ChatMessageId { get; set; }
        public string Message { get; set; }
        public int? AdminId { get; set; }
        //[Text(Ignore = true)]
        //public virtual ApplicationUser Admin { get; set; }
        //[Text(Ignore = true)]
        //public virtual ChatMessage ChatMessage { get; set; }
    }
    public enum WebsiteType
    {
        Free, Standard, Premium, Enterprise
    }
    public class IpInfoDb
    {
        public string ipaddress { get; set; }
        public string countrycode { get; set; }
        public string countryname { get; set; }
        public string cityname { get; set; }
        public string regionname { get; set; }
        public string zipcode { get; set; }
        public string timezone { get; set; }
        public string latitude { get; set; }
        public string longitude { get; set; }    
    }
    public class Plans
    {
        public Plans()
        {
            this.PlanDescription = new List<Models.PlanDescription>();
            this.PlanPrice = new List<Models.PlanPrice>();
        }
        public WebsiteType WebsiteType { get; set; }
        public double MonthlyPrice { get; set; }
        public double QuaterlyPrice { get; set; }
        public double YearlyPrice { get; set; }
        [Text(Ignore = true)]
        public virtual List<PlanDescription> PlanDescription { get; set; }
        [Text(Ignore = true)]
        public virtual List<PlanPrice> PlanPrice { get; set; }

    }
    public enum PlanTerm
    {
        Monthly, Quaterly, Yearly
    }
    public class PlanPrice
    {
        public int PlanId { get; set; }
        public PlanTerm Term { get; set; }
        public double Price { get; set; }
        public virtual Plans Plan { get; set; }
    }
    public class PlanDescription
    {
        public int Id { get; set; }
        public string Text { get; set; }
        public int Order { get; set; }
        public Plans Plan { get; set; }
    }


    public class Script
    {
        public int ScriptId { get; set; }
        public string WidgetScript { get; set; }
    }
    public enum SignalrUpdate
    {
        MeControllerEditUser, WebsiteControllerPostWebsitesite, RolesControllersetUserToRole
    }
    public enum CoreReport
    {
        UserList
    }
}