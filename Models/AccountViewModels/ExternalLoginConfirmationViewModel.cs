﻿
using System.ComponentModel.DataAnnotations;

namespace SignalRMasterServer.Models.AccountViewModels
{
    public class ExternalLoginConfirmationViewModel
    {
        [Required]
        [EmailAddress]
        public string Email { get; set; }

        public string FirstName { get; set;}

        public string LastName { get; set; }

        public string Image { get; set; }
    }
}
