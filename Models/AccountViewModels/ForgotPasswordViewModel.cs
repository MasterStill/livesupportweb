﻿using System.ComponentModel.DataAnnotations;

namespace SignalRMasterServer.Models.AccountViewModels
{
    public class ForgotPasswordViewModel
    {
        [Required]
        [EmailAddress]
        public string Email { get; set; }
    }
}
