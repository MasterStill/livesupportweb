﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Security.Claims;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.DependencyInjection.Extensions;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using SignalRMasterServer.Data;
using Nest;
namespace SignalRMasterServer.Models
{
    // Add profile data for application users by adding properties to the ApplicationUser class
    //public class ApplicationUser : IdentityUser<int, ApplicationUserClaim, ApplicationUserRole, ApplicationUserLogin>
    public class ApplicationUser : IdentityUser<int, ApplicationUserClaim, ApplicationUserRole, ApplicationUserLogin>//, ApplicationUserClaim, ApplicationUserRole, ApplicationUserLogin>
    {
        public ApplicationUser() {
            this.RegisteredDate = DateTime.Now;
            this.SignalrGuid = Guid.NewGuid();
        }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Description { get; set; }
        public bool Online { get; set; }
        public string AdminGuid { get; set; }
        public Guid SignalrGuid { get; set; }
        public string Image { get; set; }
        public DateTime RegisteredDate { get; set; }
        public string IP { get; set; }
        public string FullName
        {
            get
            {
                return FirstName + " " + LastName;
            }
        }
        public string Avatar
        {
            get
            {
                return Image;
            }
        }
        //public List<string> Sites { get; set; }               
    }
    public enum UserType
    {
        User, SupportStaff, AccountStaff, CommunityAdmin
    }
    public class Log
    {
        public Log()
        {
            this.Date = DateTime.Now;
        }
        public int Id { get; set; }
        public enumEvent Event { get; set; }
        public int UserId { get; set; }
        public ApplicationUser  User { get; set; }
        public string Remarks { get; set; }
        public DateTime Date { get; set; }
    }
    public enum enumEvent{
        Login,LogOut    
    }
    public class EmailInvitation{
        public int Id {get;set;}
        public string Email  {get;set;}
        public int WebsiteId {get;set;}
        [Text(Ignore = true)]
        public virtual Website Website{get;set;}
        public Boolean Accepted  {get;set;}
        public DateTime? AcceptedDate  {get;set;}
        public DateTime InvitedDate  {get;set;}
        public  int RoleId {get;set;}
        [Text(Ignore = true)]
        public ApplicationRole Role {get;set;}
        public int AddedById {get;set;}
        [Text(Ignore = true)]
        public ApplicationUser AddeddBy{get;set;}
    }
    public class UserWebsiteDescription{
        public int Id {get;set;}
        public int UserId {get;set;}
        public int  WebsiteId {get;set;}
        public string FirstName {get;set;}
        public string LastName {get;set;}
        public string FullName {
            get{return FirstName + " "  + LastName;}
        }
        public string Description {get;set;}
        public string Avatar {get;set;}
        [Text(Ignore = true)]
        public virtual Website Website {get;set;}
        [Text(Ignore = true)]
        public virtual ApplicationUser User {get;set;}


    }
    public class Subscribers{        
        public int Id {get;set;}
        public int  UserId {get;set;}
        public ApplicationUser User {get;set;}        
    }
 public class GenericResult
    {
            public bool Succeded { get; set; }
            public string Message { get; set; }
    }
 public class LoadMoreClientViewModel
        {
            public string groupName { get; set; }
            public string fullName { get; set; }
            public string lastMessage { get; set; }
            public string avatar { get; set; }
            public bool online {get;set;}
            public string webSite { get; set; }
        }
        public enum enumReferral{
            Pending,Accepted,Invalid
        }
        public class Referral{
            public int Id {get;set;}
            public string FullName {get;set;}            
            public string Email {get;set;}            
            public enumReferral Status {get;set;}
            public DateTime ReferralDate {get;set;}
            public DateTime? AcceptedDate {get;set;}
            public Boolean Delflag {get;set;}
            public int ReferralById {get;set;}
            public string Website {get;set;}
            public ApplicationUser ReferralBy {get;set;}
        }
        public class ReferralCreateViewModel{
            public string FullName {get;set;}
            public string Email {get;set;}
            public string Website {get;set;}

        }

        // public class DemoUserName{
        //     public int Id {get;set;}
        //     public int Username {get;set;}
        // }
    public class ApplicationUserClaim : IdentityUserClaim<int> { }
    public class ApplicationUserLogin : IdentityUserLogin<int> { }
    public class ApplicationUserToken : IdentityUserToken<int> { }

    public class ApplicationRoleManager : RoleManager<ApplicationRole>
    {
        public ApplicationRoleManager(IRoleStore<ApplicationRole> store, IEnumerable<IRoleValidator<ApplicationRole>> roleValidators, ILookupNormalizer keyNormalizer, IdentityErrorDescriber errors, ILogger<RoleManager<ApplicationRole>> logger, IHttpContextAccessor contextAccessor) : base(store, roleValidators, keyNormalizer, errors, logger, contextAccessor)
        {
        }
    }

    public class ApplicationRoleStore :
        RoleStore<ApplicationRole, AppDbContext, int, ApplicationUserRole, ApplicationRoleClaim>
    {
        public ApplicationRoleStore(AppDbContext context, IdentityErrorDescriber describer = null) : base(context, describer)
        {
        }

        protected override ApplicationRoleClaim CreateRoleClaim(ApplicationRole role, Claim claim)
        {
            throw new NotImplementedException();
        }
    }
    public class ApplicationUserRole : IdentityUserRole<int>
    {
        //public int Id { get; set; }
        public int WebsiteId { get; set; }
        public virtual Website Website { get; set; }
        public virtual ApplicationUser User { get; set; }
        public virtual ApplicationRole Role { get; set; }
    }
    public class ApplicationRole : IdentityRole<int, ApplicationUserRole, ApplicationRoleClaim>
    {
    }
    public class ApplicationRoleClaim : IdentityRoleClaim<int> { }
    public class ApplicationSignInManager : SignInManager<ApplicationUser>
    {
        public ApplicationSignInManager(UserManager<ApplicationUser> userManager, IHttpContextAccessor contextAccessor, IUserClaimsPrincipalFactory<ApplicationUser> claimsFactory, IOptions<IdentityOptions> optionsAccessor, ILogger<SignInManager<ApplicationUser>> logger) : base(userManager, contextAccessor, claimsFactory, optionsAccessor, logger)
        {
        }
    }
    public class ApplicationUserManager : UserManager<ApplicationUser>
    {
        public ApplicationUserManager(IUserStore<ApplicationUser> store, IOptions<IdentityOptions> optionsAccessor, IPasswordHasher<ApplicationUser> passwordHasher, IEnumerable<IUserValidator<ApplicationUser>> userValidators, IEnumerable<IPasswordValidator<ApplicationUser>> passwordValidators, ILookupNormalizer keyNormalizer, IdentityErrorDescriber errors, IServiceProvider services, ILogger<UserManager<ApplicationUser>> logger) : base(store, optionsAccessor, passwordHasher, userValidators, passwordValidators, keyNormalizer, errors, services, logger)
        {
        }
    }
    public class ApplicationUserStore : UserStore<ApplicationUser, ApplicationRole, AppDbContext, int, ApplicationUserClaim, ApplicationUserRole, ApplicationUserLogin, ApplicationUserToken, ApplicationRoleClaim>
    {
        public ApplicationUserStore(AppDbContext context, IdentityErrorDescriber describer = null) : base(context, describer)
        {
        }
        private bool _disposed;
        private DbSet<ApplicationUserLogin> _logins;
        private DbSet<ApplicationUserRole> _roles;
        public virtual int WebsiteId { get; set; }
        private DbSet<ApplicationUserLogin> Logins
        {
            get { return _logins ?? (_logins = Context.Set<ApplicationUserLogin>()); }
        }
        private DbSet<ApplicationUserRole> Roles
        {
            get { return _roles ?? (_roles = Context.Set<ApplicationUserRole>()); }
        }
        private void ThrowIfInvalid()
        {
            if (_disposed)
                throw new ObjectDisposedException(GetType().Name);
            if (EqualityComparer<int>.Default.Equals(WebsiteId, default(int)))
                throw new InvalidOperationException("The TenantId has not been set.");
        }       

        protected override ApplicationUserClaim CreateUserClaim(ApplicationUser user, Claim claim)
        {
            ApplicationUserClaim c= new ApplicationUserClaim();
            c.ClaimType = claim.Type;
            c.ClaimValue = claim.Value;
            c.UserId = user.Id;
            return c;
        }

        protected override ApplicationUserLogin CreateUserLogin(ApplicationUser user, UserLoginInfo login)
        {
            ApplicationUserLogin a = new ApplicationUserLogin();
            a.UserId = user.Id;
            a.LoginProvider = login.LoginProvider;
            a.ProviderDisplayName = login.ProviderDisplayName;
            a.ProviderKey = login.ProviderKey;
            return a;
        }

        protected override ApplicationUserToken CreateUserToken(ApplicationUser user, string loginProvider, string name, string value)
        {
            ApplicationUserToken t = new ApplicationUserToken();
            t.UserId = user.Id;
            t.LoginProvider = loginProvider;
            t.Value = value;
            t.Name = name;
            return t;
        }
        protected override ApplicationUserRole CreateUserRole(ApplicationUser user, ApplicationRole role)
        {
            ApplicationUserRole r= new ApplicationUserRole();
            r.WebsiteId = WebsiteId;
            r.RoleId = role.Id;
            Context.UserRoles.Add(r);
            Context.SaveChanges();
            return r;
        }
    }
}