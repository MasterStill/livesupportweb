﻿using System.ComponentModel.DataAnnotations;
namespace SignalRMasterServer.Models
{
    public class EsewaPaymentCreatedResponse
    {
        public EsewaPaymentCreatedResponse(double amount)
        {
            amt = amount;
            txAmt = 0.13 * amt;
            tAmt = amt + txAmt + psc + pdc;
        }
        [Required]
        public double amt { get; set; } // Amount(NRs) of the product/item which user have selected from merchant site for purchase/order.
        [Required]
        public double txAmt { get; set; }
        public double psc => 0;// Product service charge, or service charge.
        public double pdc => 0;// Product delivery charge, amount to be charge in case product/item have to be home delivered or so.                
        //Eg : tAmt = amt + txAmt + psc + pdc esewa will provide merchant with a code/id that will represent the merchant during transaction.
        // For TEST, merchant can use "testmerchant" as merchant code.
        //For LIVE, please contact esewa management to receive the code.
        public double tAmt { get; set; }
        [Required]
        public string pid { get; set; } // Product ID, a unique id/code that represents the item.
        [Required]
        public string su => "https://admin.livecustomer.chat/Esewa/PaymentConfirmation";
        [Required]
        public string fu => "https://admin.livecustomer.chat/Esewa/PaymentFailure";
    }
}