﻿using Nest;
using System.Collections.Generic;
namespace SignalRMasterServer.Models
{
    //public class MacroGroup
    //{        
    //    public int Id { get; set; }            
    //    public string Name { get; set; }
    //    public int? CultureId { get; set; }
    //    public int  WebsiteId { get; set; }
    //    public Website Website { get; set; }
    //    public Culture Culture { get; set; }
    //}
    public class ChatMarco
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public MarcoType MarcoType { get; set; }
        public string TextorURL { get; set; }
        public string KeyBoardShortcut { get; set; }
       // public int DepartmentId { get; set; }
       // public virtual Department Department { get; set; }
        public int? CultureId { get; set; }
        public int WebsiteId { get; set; }
        [Text(Ignore = true)]
        public virtual  Website Website { get; set; }
    }
    public enum MarcoType
    {
        Text,Push
    }
    public class Department
    {
        public Department()
        {
            this.Marco = new List<ChatMarco>();
        }
        public int Id { get; set; }
        public string Name { get; set; }
        public int WebsiteId { get; set; }
        public int? CultureId { get; set; }
        public Culture Culture { get; set; }
        [Text(Ignore = true)]
        public Website Website{ get; set; }
        [Text(Ignore = true)]
        public virtual List<ChatMarco> Marco { get; set; }
    }
    public class Culture
    {
        public int  Id { get; set; }
        public string Name { get; set; }
    }

    //public class PushMarco
    //{
    //    public int Id { get; set; }
    //    public int WebsiteId { get; set; }
    //    public virtual Website Website { get; set; }
    //    public string Name { get; set; }
    //    public string Url { get; set; }
    //    //public int MarcoGroup { get; set; }
    //    //public MacroGroup MacroGroup { get; set; }
    //}
}
