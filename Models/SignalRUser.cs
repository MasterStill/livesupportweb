﻿
using Nest;
using System;
using System.Collections.Generic;

namespace SignalRMasterServer.Models
{
    public class SignalRUser : ElasticSignalRUser
    {
        public string Id { get; set; }
    }
    public class ElasticSignalRUser
    {
        public ElasticSignalRUser() { }
        public ElasticSignalRUser(SignalRUser s)
        {
            this.PhoneNumber = s.PhoneNumber;
            this.Avatar = s.Avatar;
            this.Blocked = s.Blocked;
            this.ChatMessageId = s.ChatMessageId;
            this.Email = s.Email;
            this.FullName = s.FullName;
            this.GroupId = s.GroupId;
            this.Verified = s.Verified;
            this.PhoneNumber = s.PhoneNumber;
            this.InfoProvided = s.InfoProvided;
            this.WebsiteGuid = s.WebsiteGuid;
            this.IpAddress = s.IpAddress;
        }
        //public int Id { get; set; }
        // public string ConnectionId { get; set; }
        public string PhoneNumber { get; set; }
        public string Avatar { get; set; }
        public string Email { get; set; }
        public string WebsiteGuid { get; set; }
        public bool Verified { get; set; }
        public bool Blocked { get; set; }
        public string FullName { get; set; }
        public string GroupId { get; set; }
        public bool InfoProvided { get; set; }
        public string ChatMessageId { get; set; }
        public string IpAddress { get; set; }
        public List<SitePage> SitePages {get;set;}
    }
    public class SitePage
    {
        public string Url { get; set; }
        public string PageTitle { get; set; }
        public DateTime DateTime { get; set; }
    }
    public class Advert
    {
        public int Id { get; set; }
        public string Script { get; set; }
    }
    //public class SignalRUserConversation
    //{
    //    public int Id { get; set; }
    //    public string Message { get; set; }
    //    public string CustomerId { get; set; }
    //    public DateTime DateTime { get; set; }
    //    public virtual SignalRUser Customer { get; set; }
    //    public int? UserId { get; set; }
    //    public virtual ApplicationUser User { get; set; }
    //}
}