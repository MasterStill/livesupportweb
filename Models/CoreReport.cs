﻿using Nest;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
namespace SignalRMasterServer.Models
{
    [ElasticsearchType(Name = "reportdbs")]
    public class ReportDb
    {
        public ReportDb(){
            this.ReportDate = DateTime.Now;
        }
        public int Id { get; set; }
        public int WebsiteId { get; set; }
        public int Pageviews { get; set; }
        public int Sessions { get; set; }
        public double PageViewSessions{ get {
                return (Pageviews == 0) ? 0 : (Pageviews / Sessions);                
            }
        }
        //public double AvgSessionDuration
        //{
        //    get
        //    {
        //        return (Count /(Pageviews / Sessions));
        //    }
        //}
        //public Website Website { get; set; }
        public int User_Count { get; set; }
        public int Unique_User_Count { get; set; }
        public int Answered_Call_Count { get; set; }
        public int UnAnswered_Call_Count { get; set; }        
        public DateTime ReportDate{get;set;}
    }
}