﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Runtime.Serialization;
using Newtonsoft.Json.Converters;

namespace SignalRMasterServer.Models
{
    public class ReportViewModel
    {
        public Widgets widget1 { get; set; }
        public Widgets widget2 { get; set; }
        public Widgets widget3 { get; set; }
        public Widgets widget4 { get; set; }
        public ChartWidget widget5 { get; set; }
    }
    public class Widgets
    {        
        public DayRanges ranges { get; set; }
        public enumRangesDay currentRange { get; set; }
        public data data { get; set; }
        public string detail { get; set; }
        //public extra extra { get; set; }
    }
    public class ChartWidget
    {
        public string title { get; set; }
        public WeekRanges ranges { get; set; }
        public List<mainChart> mainChart { get; set; }
        //public 
    }    
    public class supporting
    {

    }
    public class DayRanges
    {
        [JsonProperty("2DA")]
        public string Two_Days_Ago { get; set; } // Tomorrow
        [JsonProperty("Y")]
        public string Yesterday { get; set; } // Yesterday
        [JsonProperty("T")]
        public string Today { get; set; } //Today
    }

    public class WeekRanges
    {
        [JsonProperty("2WA")]
        public string Two_Weeks_Ago { get; set; } // Tomorrow
        [JsonProperty("L")]
        public string Last_Week { get; set; } // Yesterday
        [JsonProperty("T")]
        public string This_Week { get; set; } //Today
    }


    public class data
    {
        public string label { get; set; }
        public countDays count { get; set; }
        public extra extra { get; set; }
    }
    public class extra
    {
        public string label { get; set; }
        public countDays count { get; set; }
    }
    public class countDays
    {
        [JsonProperty("2DA")]
        public int Two_Days_Ago { get; set; }
        [JsonProperty("Y")]
        public int Yesterday { get; set; }
        [JsonProperty("T")]
        public int Today { get; set; }
    }
    public class countWeeks
    {
        [JsonProperty("2WA")]
        public int Two_Week_Ago { get; set; }
        [JsonProperty("L")]
        public int Last_Week { get; set; }
        [JsonProperty("T")]
        public int This_Week { get; set; }
    }
    public class countMonths
    {
        [JsonProperty("2MA")]
        public int Two_Months_Ago { get; set; }
        [JsonProperty("L")]
        public int Last_Month { get; set; }
        [JsonProperty("T")]
        public int This_Month { get; set; }
    }
    [JsonConverter(typeof(StringEnumConverter))]
    public enum enumRangesDay
    {

        [EnumMember(Value = "2DA")]
        Two_Days_Ago,
        [EnumMember(Value = "Y")]
        Yesterday,
        [EnumMember(Value = "T")]
        Today
    }
    public enum enumRangesWeek
    {
        [EnumMember(Value = "2WA")]
        Two_Weeks_Ago,
        [EnumMember(Value = "L")]
        Last_Week,
        [EnumMember(Value = "T")]
        This_Week
    }
    public enum enumRangesMonth
    {
        [EnumMember(Value = "2MA")]
        Two_Months_Ago,
        [EnumMember(Value = "L")]
        Last_Month,
        [EnumMember(Value = "T")]
        This_Month
    }
    public class mainChart
    {
        public string key { get; set; }
        public GraphWeekClass values { get; set; }

    }
        
    public class GraphWeekClass
    {
        [JsonPropertyAttribute("2WA")]
        public List<XY> Two_Weeks_ago { get; set; }
        [JsonPropertyAttribute("L")]
        public List<XY> Last_Week { get; set; }
        [JsonPropertyAttribute("T")]
        public List<XY> This_Week { get; set; }
    }

    //public class supporting
    //{
    //    public string 
    //}

    //public class created
    //{
    //    public string label { get; set; }
    //    public GraphWeekClass count { get; set; }
    //}

    public class XY
    {
        public int x { get; set; }
        public int y { get; set; }
    }
    [JsonConverter(typeof(StringEnumConverter))]
    public enum enumDayName
    {
    
        [EnumMember(Value = "Su")]    
        Sunday,
        [EnumMember(Value = "Mo")]
        Monday,
        [EnumMember(Value = "Tu")]
        Tuesday,
        [EnumMember(Value = "Tu")]
        Wednesday,
        [EnumMember(Value = "Th")]
        Thursday,
        [EnumMember(Value = "Fr")]
        Friday
    }
}

