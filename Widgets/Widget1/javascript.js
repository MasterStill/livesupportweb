var chatApp = null;
(function() {
    var broadcaster = {};
    var private_routes = {
        offline_form: 'offline-form',
        company_info: 'company-info'
    };
    chatApp = new Vue({
        el: '#LiveCustomerChat',
        data: {
            scrollPreviousMessage: 0,
            baseUrl: 'http://localhost:5000/',
            connected: false,
            currentUrl: '',
            collapsed: true,
            minimized: false,
            hasHistory: false,
            adminTyping: false,
            toolBarTitle: '',
            widgetType:1,
            toolBarOfflineTitle: 'Live Support - Offline',
            toolBarOnlineTitle: '',
            offlieForm: {},
            typingAvatar: '',
            user_message: '',
            route: '',
            sideMenuOpened: false,
            isBlocked: false,
            isOffline: false,
            isLoading: false,
            adminName: '',
            adminImage: '',
            adminDescription: '',
            infoProvided: false,
            forceInfoProvided: false,
            noPreviousMessage: false,
            forceCompanyInfo: false,
            InitiatedChatBefore: false,
            widgetHeight: '',
            widgetMaxmizedHeight: '',
            widgetMinimizedHeight: '',
            widgetWidth: '',
            widgetMaxmizedWidth: '',
            widgetMinimizedWidth: '',
            widgetMinimzedWidth: '',
            askForMorePages: true,
            chatMessage: [],
            offlineMessage: [],
            informationform: {}
        },
        mounted: function () {            
            this.currentUrl = this.baseUrl + 'Account/ExternalLoginClient?returnUrl=' + window.location.href + '&WebsiteId=';
        },
        destroyed: function() {},
        methods: {
            scrollToButtom: function() {
                Vue.nextTick(function() {
                    var chatBox = document.querySelector('#list-chat ul')
                    if (chatBox !== null) {
                        chatBox.scrollTop = chatBox.scrollHeight;
                        console.log(chatBox.scrollHeight);
                    }
                })
            },
            scrollTop: function() {
                Vue.nextTick(function() {
                    var chatBox = document.querySelector('#list-chat ul')
                    chatBox.scrollTop = 10;
                })
            },
            foo: function(ev) {
                var scrollEl = ev.target;
                if (scrollEl.scrollTop === 0) {
                    if (this.askForMorePages === true) {
                        if (utils.getCookie('LiveSupportRockmandu' + utils.getHostName() + "LoggedInUser") === null) {
                            broadcaster.sendPreviousMessage(utils.getCookie('LiveSupportRockmandu' + utils.getHostName()), this.scrollPreviousMessage);
                        }
                        else {
                            broadcaster.sendPreviousMessage(utils.getCookie('LiveSupportRockmandu' + utils.getHostName() + "LoggedInUser"), this.scrollPreviousMessage);
                        }
                    }
                }
            },
            socialLogin(provider) {
                PushMarco(this.baseUrl + "Account/ExternalLoginClient?returnUrl=" + window.location.href.replace('#', '') + "&WebsiteId=&provider=" + provider);
            },
            'handleOffileFormSubmit': function() {
                broadcaster.offlineCustomerInfo(this.offlieForm)
                this.changeRoute('messageSent');
            },
            'handleInformationSubmit': function() {
                console.log(this.informationform);
                broadcaster.customerInfoChanged(this.informationform)
                this.infoProvided = true;
            },
            showBackButton: function() {
                return (this.sideMenuOpened || this.route === 'customerInfoForm' || this.route === 'company-info')
            },
            changeRoute: function(route) {
                this.route = route;
            },
            minimizeorMazmized: function() {
                if (this.collapsed) {
                    SetWidgetHeight(this.widgetMinimizedHeight);
                    SetWidgetWidth(this.widgetMinimizedWidth);
                } else {
                    SetWidgetHeight(this.widgetMaxmizedHeight);
                    SetWidgetWidth(this.widgetMaxmizedWidth);
                }
            },
            showInfoForm: function() {
                this.changeRoute('customerInfoForm');
            },
            sendMessage: function() {
                if (this.connected) {
                    broadcaster.sendmessage(this.user_message, null);
                    this.user_message = '';
                }
            },
            client_usertyping: function() {
                if (this.connected)
                    broadcaster.userTyping(null);
            }
        }
    });
    loadjQuery(function () {
        chatApp.minimizeorMazmized();
        loadSinglar(function() {
            loadScript(baseUrl + 'signalr/hubs', connectToSinglar);
        })
    })

    function SetWidgetHeight(height) {                
        //alert(chatApp.widgetHeight);
        chatApp.widgetHeight = height;
        //alert(height);
        //alert(chatApp.widgetHeight);
        //if (parent.IframeHeight === undefined) {
        //    if (top.IframeHeight !== undefined)
        //        alert("Ifram Height" + height);
        //        top.IframeHeight(height);
        //} else {
            if (chatApp.collapsed) {
                setTimeout(function () { parent.IframeHeight(height); }, 700);
            }
            else {
                parent.IframeHeight(height); 
            }
        //}
    }

    function PushMarco(URL) {
        top.PushMarco(URL);
    }

    function SetWidgetWidth(width) {
        // chatApp.widgetWidth = width;
        // this.chatApp.widgetWidth = width;
        // this.widgetWidth = width;
        if (chatApp !== undefined && chatApp !== null) {
            chatApp.widgetWidth = width;
        }
        if (parent.IframeWidth === undefined) {
            if (top.IframeWidth !== undefined)
                top.IframeWidth(width);
        } else {
            parent.IframeWidth(width);
        }
    }

    function inititlizeWidget($broadCaster) {
        if (chatApp.chatMessage.length === 0)
            {
            if (utils.getCookie('LiveSupportRockmandu' + utils.getHostName() + "LoggedInUser") === null) {
            broadcaster.sendPreviousMessage(utils.getCookie('LiveSupportRockmandu' + utils.getHostName()),0);
        }
        else {
                broadcaster.sendPreviousMessage(utils.getCookie('LiveSupportRockmandu' + utils.getHostName() + "LoggedInUser"), 0);
            }
        }
        chatApp.connected = true;
        chatApp.isLoading = true;
        console.log($broadCaster)
    }

    function loadScript(url, callBack) {
        var scriptJquery = document.createElement('script');

        scriptJquery.type = 'text/javascript';

        scriptJquery.src = url;

        scriptJquery.onload = scriptJquery.onreadystatechange = function() {

            console.log('Loading...!', url);

            if ((!this.readyState || this.readyState === 'complete')) {
                callBack();
            }
        };

        document.getElementsByTagName('head')[0].appendChild(scriptJquery);
    }

    function loadjQuery(callBack) {
        loadScript('https://cdnjs.cloudflare.com/ajax/libs/jquery/2.1.3/jquery.min.js', callBack)
    }

    function loadSinglar(callBack) {
        loadScript('https://cdnjs.cloudflare.com/ajax/libs/signalr.js/2.2.1/jquery.signalR.min.js', callBack)
    }

    function connectToSinglar() {
        var cookieName
        if (utils.getCookie('LiveSupportRockmandu' + utils.getHostName() + 'LoggedInUser') !== '') {
            cookieName = utils.getCookie('LiveSupportRockmandu' + utils.getHostName() + 'LoggedInUser');
        } else {
            cookieName = utils.getCookie('LiveSupportRockmandu' + utils.getHostName());
        }
        $.connection.hub.url = baseUrl + 'signalr';
        $.connection.hub.qs = {
            'PageTitle': document.title,
            'PageURL': document.URL,
            'Host': utils.getHostName(),
            'Cookie': cookieName
        };
        var hub = $.connection.customerSupportHub;
        registerEventListener(hub);
        $.connection.hub.start(function() {

            broadcaster = createBroadcaster(hub.server);
            inititlizeWidget(broadcaster);           
        })
        $.connection.hub.disconnected(function() {
            setTimeout(function() {
                $.connection.hub.start();
            }, 1000);
        });

    }
    
    function registerEventListener(hub) {
        hub.client.onNewMessage = function(responseData) {
            chatApp.chatMessage.push(responseData);
            chatApp.adminTyping = false;
            chatApp.typingAvatar = '';
            console.log(responseData, 'onNewMessage')
            if (chatApp.isOffline) {
                var kk = { Message: chatApp.offlineMessage[Math.floor(Math.random() * chatApp.offlineMessage.length)], User: { IsAdmin: true, avatar: chatApp.adminImage } };
                console.log(kk);
                chatApp.chatMessage.push(kk);
            }
            chatApp.scrollToButtom();
        }
        hub.client.OfflineCustomerInfo = function(responseData) {
            console.log(responseData, 'OfflineCustomerInfo')
        }
        hub.client.onBlocked = function(responseData) {
            chatApp.toolBarOnlineTitle = chatApp.toolBarTitle;
            chatApp.isBlocked = true;
            chatApp.toolBarTitle = chatApp.toolBarOfflineTitle;
        }
        hub.client.onUnBlocked = function(responseData) {
            chatApp.isBlocked = false;
            chatApp.toolBarTitle = chatApp.toolBarOnlineTitle;
        }
        hub.client.onSupportRequestAccepted = function(responseData) {
            console.log(responseData, 'onSupportRequestAccepted')
        }
        hub.client.onCustomerInfoChanged = function(responseData) {
            utils.setCookie('LiveSupportRockmanduCustomerInfo', responseData, 30)
            chatApp.infoProvided = true;
            chatApp.forceInfoProvided = false;
            chatApp.route = '';
        }
        hub.client.onOfflineCustomerInfo = function(responseData) {
            utils.setCookie('LiveSupportRockmanduCustomerInfo', responseData, 30)
            this.infoProvided = true;
        }
        hub.client.onAdminStatus = function(responseData) {
            if (responseData.online === true) {
                chatApp.isOffline = false;
                chatApp.adminName = responseData.fullName;
                chatApp.adminImage = responseData.avatar;
                chatApp.adminDescription = responseData.description;
                chatApp.toolBarTitle = chatApp.adminName + " - Online";
            }
            console.log(responseData, 'onAdminStatus')
        }
        hub.client.onUserTyping = function(responseData) {
            chatApp.adminTyping = true;
            chatApp.typingAvatar = responseData.avatar
            console.log(responseData, 'onUserTyping')
        }
        hub.client.onUserStoppedTyping = function(responseData) {
            chatApp.adminTyping = false;
            chatApp.typingAvatar = '';
        }
        hub.client.onNewPushMarco = function(responseData) {
            PushMarco(responseData);
            console.log(responseData, 'onNewPushMarco')
        }
        hub.client.onSupportGroupIssued = function(responseData) {
            utils.setCookie('LiveSupportRockmandu' + utils.getHostName(), responseData, 30);
        }
        hub.client.onAdminSpecialMessage = function(responseData) {
            console.log(responseData, 'onAdminSpecialMessage')
        }
        hub.client.onGetClientInformation = function(responseData) {
            chatApp.route = 'customerInfoForm';
        }
        hub.client.onAuthorization = function(responseData) {
            this.InitiatedChatBefore = true;
            if (responseData === null) {
                chatApp.noPreviousMessage = true;
                return;
            }
            chatApp.chatMessage = chatApp.chatMessage.length === 0 ? responseData.reverse() : responseData.reverse().concat(chatApp.chatMessage);
            if (chatApp.scrollPreviousMessage === 0) {
                chatApp.scrollToButtom();
            } else {
                chatApp.scrollTop();
            }
            chatApp.scrollPreviousMessage += 1;
        }
    }

    function createBroadcaster(server) {
        return {
            server: server,
            sendmessage: server.message,
            userTyping: server.userTyping,
            userStoppedTyping: server.userStoppedTyping,
            offlineCustomerInfo: server.offlineCustomerInfo,
            customerInfoChanged: server.customerInfoChanged,
            sendPreviousMessage: server.sendPreviousMessage,
        }
    }
})();

var utils = {
    getHostName: function() {
        return window.location.host;
    },
    getCookie: function(cname) {
        cname = cname.replace(':', '');
        var name = cname + '=';
        var ca = document.cookie.split(';');
        for (var i = 0; i < ca.length; i++) {
            var c = ca[i];
            while (c.charAt(0) === ' ') {
                c = c.substring(1);
            }
            if (c.indexOf(name) === 0) {
                return c.substring(name.length, c.length);
            }
        }
        return "";
    },
    setCookie: function(cname, cvalue, exdays) {
        cname = cname.replace(':', '');
        var d = new Date();
        d.setTime(d.getTime() + (exdays * 24 * 60 * 60 * 1000));
        var expires = "expires=" + d.toUTCString();
        document.cookie = cname + '=' + cvalue + ';' + expires + ';path=/';
    }
}
function ConsoleInit() {
    var log = console.log;
    console.log = function () {
        if (utils.getCookie('LiveCustomerChatSupport') === 'true')
            log.apply(this, Array.prototype.slice.call(arguments));
    };
}
function Weed() {
    utils.setCookie('LiveCustomerChatSupport', 'true', 1);
}
this.ConsoleInit();