$(document).ready(function(){
	$('.chat_head').click(function(){
		$('.chat_body').slideToggle('slow');
	});
	
    $(document).on('click', '.msg_head', function () {
        $(this).parent().find('.msg_wrap').slideToggle('slow');
    });

    $(document).on('click', '.close', function (e) {
        e.stopPropagation();
        var userId = $(this).closest('.msg_box').attr('data-user-id');
        closeChatBox(userId);
        $(this).closest('.msg_box').remove();
    });
	
    $('.user').click(function () {

        var id = $(this).attr('id');

        //load from ajax the user replies,
        // for now i am using my own data

        var user;

        initData().map(function (u) {
            if (u.id == id) {
                user = u;
            }
        })

        //we have the data for replies and now cook the msg box

        var messageBox = createChatBox(user);

        if ($('[data-user-id="' + id + '"]').length === 1) {
            $('[data-user-id="' + id + '"]').remove();
            closeChatBox(id);
        }

        $(document).find('body').append(messageBox);

    });
	
    var keyPressCount = 0;
	$(document).on('keypress','textarea',
        function (e) {
            if (keyPressCount++ % 5 == 0) {
                var chat = $.connection.customerSupportHub;                
                chat.server.userTyping();
              }
        if (e.keyCode == 13) {
            e.preventDefault();
            var msg = $(this).val();
			$(this).val('');
			if(msg!='')
                var chat = $.connection.customerSupportHub;
                chat.server.message(msg,null);
                keyPressCount= 0;
        }
    });
	
});